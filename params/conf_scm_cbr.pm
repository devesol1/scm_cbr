#!/usr/bin/perl
#
###############################################################################
#      Fichier : conf_scm_cbr.pm
#        Objet : Ce fichier contient les paramétres de configuration de BRTYROCHER 
# Société      : e-SOLUTIONS 
#
# Historique
#
###############################################################################
use strict;

package conf_scm_cbr;

require Exporter;
BEGIN {
	@conf_scm_cbr::ISA       = qw(Exporter);
	@conf_scm_cbr::EXPORT    = qw(
		$FCS_format_date_out
		$FCS_format_heure_out
		$FCS_timeout_password
		$fcs_cloud_scm_web
		$fcs_serveur_mail_prod
		$fcs_racine_web
		$qf_racine_web
		@FCS_base
		%FCS_base_list
		%FCS_HashLoginAdmin
		$FCS_timeout_session

	);

	@conf_scm_cbr::EXPORT_OK = qw();
	$conf_scm_cbr::VERSION   = 1.00;
}

use strict;

# Hash contenant les logins des administrateurs du systéme
%conf_cbryrocher::FCS_HashLoginAdmin = (
   'mhuet'          => 1,
   'ethaunier'      => 1,
   'xpicart'      => 1,
   'jboucly'      => 1,
   'cdelamarre'     => 1
);


# Liste des bases de données é mettre é jour dans lors d'un changement de mot de passe
@conf_scm_cbr::FCS_base = ("yrgroup","cot_yr","yrocher");
%conf_scm_cbr::FCS_base_list = (yrgroup =>'Road Tracing',cot_yr=> 'Quot@Freight',yrocher=>'Forwarding Control System');
%conf_scm_cbr::FCS_server_list = (yrgroup => 'localhost', cot_yr=> 'localhost', yrocher=> 'localhost');

# Timeout sessions web
$conf_scm_cbr::FCS_timeout_session = '20m';
# Timeout password
$conf_scm_cbr::FCS_timeout_password = 360; # nombre de jours de validité du mot de passe


# Format d'affichage des dates (PostgreSQL)
$conf_scm_cbr::FCS_format_date_out  = 'DD/MM/YYYY';

# Format d'affichage de l'heure (PostgreSQL)
$conf_scm_cbr::FCS_format_heure_out = 'HH24:MI:SS';


