#!/usr/bin/perl

#####################################################################################################
# Bibliotheques de fonctions pour BRTYROCHER
# Nom          : lib_scm_cbr.pm
# Societe      : e-SOLUTIONS
# Modification :
#
#
#####################################################################################################
use strict;

package lib_scm_cbr;

# Appel des bibliotheques :
use HTML::Template;
use MD5;
use DBI;
use Class::Date qw(:errors date localdate gmdate now);

use DateTime;               ### xp 04/2013
use Date::Calc qw(:all);    ### 04/2013

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv( __FILE__, 'PATH_CBR_ROOT_DIR' ) . 'params';
use conf_scm_cbr;

require Exporter;

BEGIN {
    @lib_scm_cbr::ISA    = qw(Exporter);
    @lib_scm_cbr::EXPORT = qw(
      Get_First_Year
      Get_Last_Year
      Get_Year_List
      Get_Month_List
      Get_Num_Month
      Get_Type_Container_List
      Get_Forwarder_List
      Get_Country_List
      Get_Port_List
      FCS_AffichePageAuthentification
      FCS_GereConnexion
      FCS_VerifEtatFCS
      FCS_VerifLogin
      FCS_CreeTicketSession
      FCS_AfficheErreurEntete
      FCS_AfficheErreur
      FCS_AjouteEvenementSession
      FCS_Entete
      FCS_Ticket2Login
      FCS_VerifTicket
      FCS_AfficheMessage
      FCS_ClotureSession
      FCS_AffichePageMiseJourPswd
      FCS_AffichePageRecuperationPswd
      FCS_randomPassword
      FCS_DHTML_Menu
      is_user_in_group
      is_action_allowed

    );

    @lib_scm_cbr::EXPORT_OK = qw();
    $lib_scm_cbr::VERSION   = 1.00;
}

# Variables internes
my $debug           = 0;
my $FCS_Login       = '';
my %FCS_MenuActions = ();
my $FCS_timeout_password =
  360;    #xp 03/2103 defini la frequence de modification des mots de passe

sub Get_First_Year {
    my ($dbhandle) = @_;
    my $sqlr = " SELECT min(annee)
		FROM periods
	";

    my $rs = $dbhandle->prepare($sqlr);
    $rs->execute();
    if ( $dbhandle->errstr ne undef ) {
        $rs->finish;
        $dbhandle->disconnect;
        exit;
    }
    my $annee = $rs->fetchrow;
    $rs->finish;

    return $annee;
}

sub Get_Last_Year {
    my ($dbhandle) = @_;
    my $sqlr = " SELECT max(annee)
		FROM periods
	";

    my $rs = $dbhandle->prepare($sqlr);
    $rs->execute();
    if ( $dbhandle->errstr ne undef ) {
        $rs->finish;
        $dbhandle->disconnect;
        exit;
    }
    my $annee = $rs->fetchrow;
    $rs->finish;

    return $annee;
}

sub Get_Year_List {
    my ( $dbhandle, $annee ) = @_;
    my @loop;
    my $sqlr = " SELECT DISTINCT ON (periods.annee) 
		periods.annee
		FROM periods
		ORDER BY periods.annee 
	";

    my $rs = $dbhandle->prepare($sqlr);
    $rs->execute();
    if ( $dbhandle->errstr ne undef ) {
        $rs->finish;
        $dbhandle->disconnect;
        exit;
    }

    my %hash_query = ();
    $hash_query{'year'} = 'All';
    push @loop, \%hash_query;
    while ( my @query = $rs->fetchrow ) {
        my %hash_query = ();
        $hash_query{'year'} = $query[0];
        if ( $hash_query{'year'} eq $annee ) { $hash_query{'selected'} = '1'; }
        push @loop, \%hash_query;
    }
    $rs->finish;

    return @loop;
}

sub Get_Num_Month {
    my ($mois) = @_;
    my $numero = '';

    if    ( $mois eq 'January' )   { $numero = '01'; }
    elsif ( $mois eq 'February' )  { $numero = '02'; }
    elsif ( $mois eq 'March' )     { $numero = '03'; }
    elsif ( $mois eq 'April' )     { $numero = '04'; }
    elsif ( $mois eq 'May' )       { $numero = '05'; }
    elsif ( $mois eq 'June' )      { $numero = '06'; }
    elsif ( $mois eq 'July' )      { $numero = '07'; }
    elsif ( $mois eq 'August' )    { $numero = '08'; }
    elsif ( $mois eq 'September' ) { $numero = '09'; }
    elsif ( $mois eq 'October' )   { $numero = '10'; }
    elsif ( $mois eq 'November' )  { $numero = '11'; }
    elsif ( $mois eq 'December' )  { $numero = '12'; }

    return $numero;
}

sub Get_Month_List {
    my ($mois) = @_;
    my @loop;

    my %hash_query = ();
    $hash_query{'month'} = 'January';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'February';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'March';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'April';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'May';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'June';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'July';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'August';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'September';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'October';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'November';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    my %hash_query = ();
    $hash_query{'month'} = 'December';
    if ( $hash_query{'month'} eq $mois ) { $hash_query{'selected'} = '1'; }
    push @loop, \%hash_query;

    return @loop;
}

sub Get_Type_Container_List {
    my ($dbhandle) = @_;
    my @loop;
    my $sqlr = " SELECT list_ct_types.name 
		FROM list_ct_types
	";

    my $rs = $dbhandle->prepare($sqlr);
    $rs->execute();
    if ( $dbhandle->errstr ne undef ) {
        $rs->finish;
        $dbhandle->disconnect;
        exit;
    }

    while ( my @query = $rs->fetchrow ) {
        push @loop, $query[0];
    }
    $rs->finish;

    return @loop;
}

sub Get_Forwarder_List {
    my ( $dbhandle, $Forwarder ) = @_;
    my @loop;
    my $sqlr = " SELECT 
		forwarders.name
		FROM forwarders
		ORDER BY forwarders.name 
	";

    my $rs = $dbhandle->prepare($sqlr);
    $rs->execute();
    if ( $dbhandle->errstr ne undef ) {
        $rs->finish;
        $dbhandle->disconnect;
        exit;
    }

    my %hash_query = ();
    $hash_query{'forwarder'} = 'All';
    push @loop, \%hash_query;
    while ( my @query = $rs->fetchrow ) {
        my %hash_query = ();
        $hash_query{'forwarder'} = $query[0];
        if ( $hash_query{'forwarder'} eq $Forwarder ) {
            $hash_query{'selected'} = '1';
        }
        push @loop, \%hash_query;
    }
    $rs->finish;

    return @loop;
}

sub Get_Country_List {
    my ( $dbhandle, $Country, $port ) = @_;
    my @loop;
    my $sqlr = '';

    if ( $port eq 'pol' ) {
        $sqlr = " SELECT DISTINCT ON (ports.country)
	               ports.country
	               FROM ports
		       WHERE is_pol = '1'
	               ORDER BY ports.country
	  ";
    }
    elsif ( $port eq 'pod' ) {
        $sqlr = " SELECT DISTINCT ON (ports.country)
	               ports.country
	               FROM ports
		       WHERE is_pod = '1'
	               ORDER BY ports.country
	    ";
    }
    else {
        $sqlr = " SELECT DISTINCT ON (ports.country)
		ports.country
		FROM ports
		ORDER BY ports.country 
	    ";
    }

    my $rs = $dbhandle->prepare($sqlr);
    $rs->execute();
    if ( $dbhandle->errstr ne undef ) {
        $rs->finish;
        $dbhandle->disconnect;
        exit;
    }

    my %hash_query = ();
    $hash_query{'country'} = 'All';
    push @loop, \%hash_query;
    while ( my @query = $rs->fetchrow ) {
        my %hash_query = ();
        $hash_query{'country'} = $query[0];
        if ( $hash_query{'country'} eq $Country ) {
            $hash_query{'selected'} = '1';
        }
        push @loop, \%hash_query;
    }
    $rs->finish;

    return @loop;
}

sub Get_Port_List {
    my ( $dbhandle, $Country, $port, $portname ) = @_;

    my @loop;
    my $sqlr = '';

    if ( $port eq 'pol' ) {
        $sqlr = " SELECT ports.name
	               FROM ports
		       WHERE is_pol = '1' ";
    }
    elsif ( $port eq 'pod' ) {
        $sqlr = " SELECT ports.name)
	               FROM ports
		       WHERE is_pod = '1' ";
    }
    if ( $Country ne "%" ) {
        $sqlr = $sqlr . " AND ports.country  = '$Country' ";
    }
    $sqlr = $sqlr . " ORDER BY ports.name ";

    my $rs = $dbhandle->prepare($sqlr);
    $rs->execute();
    if ( $dbhandle->errstr ne undef ) {
        $rs->finish;
        $dbhandle->disconnect;
        exit;
    }

    my %hash_query = ();
    $hash_query{'port'} = 'All';
    push @loop, \%hash_query;
    while ( my @query = $rs->fetchrow ) {
        my %hash_query = ();
        $hash_query{'port'} = $query[0];
        if ( $hash_query{'port'} eq $portname ) {
            $hash_query{'selected'} = '1';
        }
        push @loop, \%hash_query;
    }
    $rs->finish;

    return @loop;

}

sub FCS_AffichePageAuthentification() {
    my $mode    = shift @_;
    my $message = shift @_;
print $mode;
    # Initialisation des variables internes :
    my $template = HTML::Template->new(
        filename => esolGetEnv( __FILE__, 'PATH_CBR_WEB_DIR' )
          . '/templates/ecran_authentification.tmpl',
        die_on_bad_params => 0,
        global_vars       => 1
    );

    if ( $ENV{'GATEWAY_INTERFACE'} =~ /^CGI-Perl/ ) {

        # Si on est sous mod_perl
        # On obtient le contexte de la requete HTTP
        my $hdl_http = Apache->request;
        $hdl_http->send_http_header('text/html');
    }
    else {
        # Si on est en mode CGI classique
    }

    if ( $mode == 0 ) {
        $template->param( ACCES_OK => 1 );
        $template->param( IMAGE    => "images/bandeau_groupe.jpg" );
        $template->param( MESSAGE  => '' );
    }

    # mode == 9 si BRT down, $message contient la raison de l'arret
    elsif ( $mode == 9 ) {
        $template->param( ACCES_OK => 0 );
        $template->param( IMAGE    => "images/fcs_bandeau_rouge_ss_txt.gif" );
        $template->param( MESSAGE  => $message );
    }

    # Le ticket est faux ou trop vieux on demande une nouvelle connexion
    else {
        $template->param( ACCES_OK => 1 );
        $template->param( IMAGE    => "images/fcs_bandeau_rouge_ss_txt.gif" );
        $template->param(
            MESSAGE => "Acces refuse. Veuillez vous identifier svp." );
    }

    print $template->output;
    exit;
}

sub FCS_GereConnexion {
    print STDERR "[FCS_GereConnexion] $0\n" if ($debug);

    # On recupere les parametres :
    my $hdl_base   = shift @_;
    my $ticket     = shift @_;
    my $username   = shift @_;
    my $password   = shift @_;
    my $adresse_ip = shift @_;
    my $navigateur = shift @_;

    # Initialisation des variables internes :
    my $acces                 = 0;
    my $res_verif_ticket      = '';
    my $res_new_ticket        = '';
    my @tab_commande          = ();
    my $nom_commande          = '';
    my $res_verif_fcs_etat    = '';
    my $res_verif_fcs_message = '';

    if ($hdl_base) {
        ( $res_verif_fcs_etat, $res_verif_fcs_message ) =
          &FCS_VerifEtatFCS($hdl_base);
    }
    else {
        $res_verif_fcs_etat = 0;
        print STDERR "[ATTENTION] Base de donnees inaccessible ($0).\n";
    }

    if ( $res_verif_fcs_etat != 1 ) {
        if ($hdl_base) {
            $hdl_base->disconnect;
        }
        &FCS_AffichePageAuthentification( 9, $res_verif_fcs_message );
    }

    # On recupere le nom du script
    # On enleve le path et l'extension, la chaene obtenue est l'"action"
    @tab_commande = split( /\//, $0 );
    $nom_commande = $tab_commande[ scalar @tab_commande - 1 ];
    $nom_commande =~ s/(.*)\.(.*)/$1/;

    print STDERR "[FCS_GereConnexion] appelee par $nom_commande\n" if ($debug);

    if ( $ticket eq "" and ( $username ne "" and $password ne "" ) ) {

        # On verifie le login
        ( $acces, $res_new_ticket ) =
          &FCS_VerifLogin( $hdl_base, $username, $password, $adresse_ip,
            $navigateur );

        if ( $acces == 0 ) {
            $hdl_base->disconnect;
            &FCS_AffichePageAuthentification(1);
        }
        $ticket = $res_new_ticket;
    }
    elsif ( $ticket ne "" ) {

        # On verifie le ticket :
        if (
            &FCS_VerifTicket(
                $hdl_base, $ticket, $username,
                $password, $adresse_ip, $navigateur
            ) != 1
          )
        {
            $hdl_base->disconnect;
            &FCS_AffichePageAuthentification(2);
        }
        $acces = 1;
    }
    else {
        # On demande l'authentification :
        $hdl_base->disconnect;
        &FCS_AffichePageAuthentification(0);
    }

    %FCS_MenuActions = ();
    my $requete = q/SELECT ac.id_action FROM actions ac
    	WHERE ac.etat = 1
    	AND (
    	ac.id_action in (SELECT ac_l.id_action FROM action_logins ac_l JOIN utilisateurs u ON ac_l.login = u.login AND u.etat = 1 AND u.login = ?)
    	OR ac.id_action in (SELECT ac_g.id_action FROM action_groupes ac_g JOIN groupes g ON ac_g.groupe = g.groupe AND g.etat = 1 AND g.groupe in (SELECT groupe FROM groupe_logins WHERE login = ?))
	)
    /;
    my $curseur_actions = $hdl_base->prepare($requete) or die $DBI::errstr;
    $curseur_actions->execute( $FCS_Login, $FCS_Login );
    my $indice_tableauActions = 1
      ; # Indice dans le tableau des menus dans le code javascript voir fichier entete

    while ( my @tab_row_actions = $curseur_actions->fetchrow_array ) {
        for ( my $i = 0 ; $i < scalar @tab_row_actions ; $i++ ) {
            $tab_row_actions[$i] =~ s/^\s+//;
            $tab_row_actions[$i] =~ s/\s+$//;
        }
        unless ( exists $FCS_MenuActions{ $tab_row_actions[0] } ) {
            $FCS_MenuActions{ $tab_row_actions[0] } = $indice_tableauActions;
            $indice_tableauActions++;
        }
    }
    $curseur_actions->finish or die $DBI::errstr;

    # On verifie les droits d'acces :
    unless ( exists $FCS_MenuActions{$nom_commande} ) {
        $hdl_base->disconnect;

        # L'action n'est pas autorisee
        if ( $ENV{'GATEWAY_INTERFACE'} =~ /^CGI-Perl/ ) {

            # Si on est sous mod_perl
            # On obtient le contexte de la requete HTT
            my $hdl_http = Apache->request;
            $hdl_http->send_http_header('text/html');
            print "Content-type: text/html\r\n\r\n";
        }
        else {
            # Si on est en mode CGI classique
            print "Content-type: text/html\r\n\r\n";
        }

        &FCS_AfficheErreurEntete(
            "[$nom_commande] Access not allowed",
            "authentification.psp",
            "POST",
            $ticket,
            "You are not allowed to access fonction '$nom_commande'"
        );
        exit;
    }

    &FCS_AjouteEvenementSession( $hdl_base, $ticket, $nom_commande, '' );
    return $acces, $ticket;
}

sub FCS_VerifEtatFCS {

    # Variable locales :
    my $dbh     = shift @_;
    my $sth     = '';
    my $etat    = '';
    my $message = '';
    my @row_tab = ();

    $sth = $dbh->prepare("SELECT * FROM etat_systeme") or die $DBI::errstr;
    $sth->execute;

    while ( @row_tab = $sth->fetchrow_array ) {
        $etat    = $row_tab[0];
        $message = $row_tab[1];
    }
    $sth->finish or die $DBI::errstr;

    return $etat, $message;
}

sub FCS_VerifLogin {

    # On recupere les parametres :
    my $dbh        = shift @_;
    my $username   = shift @_;
    my $password   = shift @_;
    my $adresse_ip = shift @_;
    my $navigateur = shift @_;

    # Initialisation des variables internes :
    my $acces = 0;
    my $pswd_valid =
      1;    # defini la validite temporelle du mot de passe Xp 03/2013
    my $sth              = '';
    my $res_select_login = '';
    my $new_ticket       = '';

    $sth = $dbh->prepare(
"SELECT * FROM utilisateurs where etat = 1 and login = ? and password = ?"
    ) or die $DBI::errstr;
    $sth->execute( $username, $password );
    $res_select_login = $sth->rows;
    $sth->finish or die $DBI::errstr;

    if ( $res_select_login != 1 ) {
        $dbh->disconnect;
        &FCS_AffichePageAuthentification(3);
    }
    else {
        $new_ticket =
          &FCS_CreeTicketSession( $dbh, $username, $password, $adresse_ip,
            $navigateur );
    }

    ############################################  Xp 03/2013

    $sth =
      $dbh->prepare("SELECT * FROM utilisateurs where etat = 1 and login = ?")
      or die $DBI::errstr;
    $sth->execute($username);

    while ( my $data = $sth->fetchrow_hashref ) {
        my $date;
        $date = $data->{'date'};
        my $date_courante = DateTime->now();

        if ( !defined($date)
          ) ## une date de creation non definie ne deconnecte pas l'utilisateur-phase de test
        {
            $date = DateTime->now();
        }

# Decomposition du format DateTime,permet de repondre e une modification du format date dans la base de donnee
        my ( $annee1, $mois1, $jour1 ) = split( '-', $date );
        my ( $annee2, $mois2, $jour2 ) = split( '-', $date_courante );
        my ($jour3) = split( 'T', $jour2 );
        my $NombreJoursValidPaswd = Delta_Days( ( $annee1, $mois1, $jour1 ),
            ( $annee2, $mois2, $jour2 ) );

        if ( $NombreJoursValidPaswd > $FCS_timeout_password ) {
            $pswd_valid = 0;
        }
    }

    $sth =
      $dbh->prepare("SELECT * FROM utilisateurs where etat = 1 and login = ?")
      or die $DBI::errstr;
    $sth->execute($username);

    if ( $res_select_login != 1 || $pswd_valid == 0 ) {
        $sth->finish;
        $dbh->disconnect;
        if ( $res_select_login == 1 && $pswd_valid == 0 ) {
            &FCS_AffichePageMiseJourPswd( 1, $username );
        }
        else {
            &FCS_AffichePageAuthentification(4);
        }
    }
    else {
        $new_ticket =
          &FCS_CreeTicketSession( $dbh, $username, $password, $adresse_ip,
            $navigateur );
    }

    ######################################

    return 1, $new_ticket;
}

sub FCS_CreeTicketSession {

    # On recupere les parametres :
    my $base_dbh   = shift @_;
    my $username   = shift @_;
    my $password   = shift @_;
    my $adresse_ip = shift @_;
    my $navigateur = shift @_;

    $adresse_ip = '';

    # Initialisation des variables internes :
    my $base_sth     = '';
    my $date_loc     = now;
    my $chaine_date  = '';
    my $chaine_heure = '';
    my $md5          = new MD5;
    my $md5Cle       = new MD5;
    my $ticket       = '';
    my $cleVerif     = '';

    $chaine_date =
        $date_loc->year
      . sprintf( "%02d", $date_loc->month )
      . sprintf( "%02d", $date_loc->day );
    $chaine_heure =
        sprintf( "%02d", $date_loc->hour ) . ":"
      . sprintf( "%02d", $date_loc->min ) . ":"
      . sprintf( "%02d", $date_loc->sec );

    $md5->add( $username . $chaine_date . $chaine_heure . $password . $$ );
    $ticket = unpack( "H*", $md5->digest() );

    $md5Cle->add( $ticket . $adresse_ip . $navigateur );
    $cleVerif = unpack( "H*", $md5Cle->digest() );

    # On supprime les anciens tickets :
    $base_dbh->do(
        "DELETE FROM sessions WHERE login = '$username' and ticket != '$ticket'"
    );

    $base_sth = $base_dbh->prepare(
"INSERT INTO sessions VALUES('$ticket','$chaine_date','$chaine_heure','$username','$adresse_ip','$navigateur','$cleVerif')"
    ) or die $DBI::errstr;
    $base_sth->execute;
    $base_sth->finish or die $DBI::errstr;

    $FCS_Login = $username;

    return $ticket;
}

sub FCS_AfficheErreurEntete {

    # On recupere les parametres :
    my $titre_page = shift @_;
    my $script     = shift @_;    # le nom du script e executer
    my $methode    = shift @_;    # la methode e utiliser
    my $ticket     = shift @_;    # le ticket
    my $texte      = shift @_;    # le texte du message

    &FCS_Entete( $ticket, $titre_page );

#la fonction peut recevoir un 6eme parametre qui est une ref sur un hash de parametres e passer au script:
    if ( scalar @_ > 0 ) {

        #on teste qu'il s'agit bien d'une ref sur un hash:
        my $ref_parametres = shift @_;

        if ( ref($ref_parametres) eq "HASH" ) {
            FCS_AfficheErreur( $script, $methode, $ticket, $texte,
                $ref_parametres );
        }
        else {
            FCS_AfficheErreur( $script, $methode, $ticket, $texte );
        }
    }
    else {
        FCS_AfficheErreur( $script, $methode, $ticket, $texte );
    }
}

sub FCS_AfficheErreur {

    # On recupere les parametres :
    my $script  = shift @_;    # le nom du script e executer
    my $methode = shift @_;    # la methode e utiliser
    my $ticket  = shift @_;    # le ticket
    my $texte   = shift @_;    # le teste du message

    my $template_mess = HTML::Template->new(
        filename => esolGetEnv( __FILE__, 'PATH_CBR_WEB_DIR' )
          . '/templates/erreur.tmpl',
        global_vars       => 1,
        die_on_bad_params => 1
    );

    my @tab_parametres = ();

#la fonction peut recevoir un 5eme parametre qui est une ref sur un hash de parametres e passer au script:
    if ( scalar @_ > 0 ) {

        #on teste qu'il s'agit bien d'une ref sur un hash:
        my $ref_parametres = shift @_;

        if ( ref($ref_parametres) eq "HASH" ) {

           # on transforme ce hash en tab de hash ayant pour cles, CLE et VALEUR
            foreach my $cle ( keys %$ref_parametres ) {
                my %hash_temp = ();
                $hash_temp{CLE}    = $cle;
                $hash_temp{VALEUR} = $$ref_parametres{$cle};

                push @tab_parametres, \%hash_temp;
            }

            # on rajoute le ticket e ce hash:
            push @tab_parametres, { CLE => "TICKET", VALEUR => $ticket };
        }
        else {
            # on initialise un hash avec pour cle: TICKET
            push @tab_parametres, { CLE => "TICKET", VALEUR => $ticket };
        }
    }
    else {
        # on initialise un hash avec pour cle: TICKET
        push @tab_parametres, { CLE => "TICKET", VALEUR => $ticket };
    }

    $template_mess->param( SCRIPT                 => $script );
    $template_mess->param( METHODE                => $methode );
    $template_mess->param( LOOP_PARAMETRES_SCRIPT => \@tab_parametres );
    $template_mess->param( MESSAGE_ERREUR         => $texte );
    print $template_mess->output;
}

sub FCS_AjouteEvenementSession {
    my $base_dbh    = shift @_;
    my $ticket      = shift @_;
    my $action      = shift @_;
    my $commentaire = shift @_;

    if ( $commentaire eq '' ) {
        $commentaire = undef;
    }

    my $base_sth = $base_dbh->prepare(
"INSERT INTO session_evenements (ticket, login, action, commentaire) VALUES (?, ?, ?, ?)"
    );
    $base_sth->execute( $ticket, &FCS_Ticket2Login( $base_dbh, $ticket ),
        $action, $commentaire );

    if ( $base_dbh->errstr ne undef ) {
        print STDERR
"[FCS_AjouteEvenementSession] Erreur ajoute evenement pour la session $ticket action : $action\n";
    }
    $base_sth->finish;
}

sub FCS_Entete {
    my $ticket     = shift @_;
    my $titre_page = shift @_;

    my $affiche_admin = 0;

    if ( $FCS_HashLoginAdmin{$FCS_Login} == 1 ) {
        $affiche_admin = 1;
    }

    my @jour_semaine = (
        "Dimanche", "Lundi",    "Mardi", "Mercredi",
        "Jeudi",    "Vendredi", "Samedi"
    );
    my @lemois = (
        "Janvier",   "Fevrier", "Mars",     "Avril",
        "Mai",       "Juin",    "Juillet",  "Aout",
        "Septembre", "Octobre", "Novembre", "Decembre"
    );

    my $lejour =
        $jour_semaine[ (localtime)[6] ] . ' '
      . (localtime)[3] . ' '
      . $lemois[ (localtime)[4] ] . ' '
      . ( (localtime)[5] + 1900 );

    # Recuperation du nom du client/division
    my $dbh;
    (
        $dbh = DBI->connect(
            esolGetEnv( __FILE__, 'BDD_CBR_DSN' ),
            esolGetEnv( __FILE__, 'BDD_USER' ),
            esolGetEnv( __FILE__, 'BDD_PASSWORD' ),
            { AutoCommit => 1 }
        )
      )
      or &FCS_AfficheErreur( "authentification.psp", "POST", $ticket,
        "Connexion base impossible." );
    my $login       = &FCS_Ticket2Login( $dbh, $ticket );
    my $is_old_menu = 0;

    #	$is_old_menu = 0 if($FCS_Login eq 'cdelamarre' || $FCS_Login eq '');

    my $entete_template = HTML::Template->new(
        filename => esolGetEnv( __FILE__, 'PATH_CBR_WEB_DIR' )
          . '/templates/entete_menus.tmpl',
        global_vars       => 1,
        die_on_bad_params => 0
    );

    foreach my $action ( keys %FCS_MenuActions ) {
        $entete_template->param( $action => 1 );
        $entete_template->param(
            'indice_' . $action => $FCS_MenuActions{$action} );
    }

    $entete_template->param( JOUR                 => $lejour );
    $entete_template->param( TICKET               => $ticket );
    $entete_template->param( TITRE_PAGE           => $titre_page );
    $entete_template->param( is_print_select_menu => 1 ) if ($is_old_menu);
    $entete_template->param( AFFICHE_ADMIN        => $affiche_admin );

    print $entete_template->output;

    &FCS_DHTML_Menu($ticket) if ( !$is_old_menu );

   # On initialise les menus car le hash est globale et donc partager entre tous
   # les programmes qui utilisent fcs_procetfonc...
    %FCS_MenuActions = ();
}

sub FCS_Ticket2Login {
    my $base_dbh = shift @_;
    my $ticket   = shift @_;

    my $login = '';
    my $base_sth =
      $base_dbh->prepare("SELECT login FROM sessions where ticket = '$ticket'")
      or print STDERR "[FCS_Ticket2Login] : ", $DBI::errstr, "\n";

    $base_sth->execute;
    my @row_tab = $base_sth->fetchrow_array;
    $login = $row_tab[0];
    $base_sth->finish or die $DBI::errstr;
    $login =~ s/^\s+//;
    $login =~ s/\s+$//;

    return $login;
}

sub FCS_VerifTicket {

    # On recupere les parametres :
    my $dbh        = shift @_;
    my $ticket     = shift @_;
    my $username   = shift @_;
    my $password   = shift @_;
    my $adresse_ip = shift @_;
    my $navigateur = shift @_;
    my $retour     = 0;

    # On invalide la verification de l'adresse IP
    $adresse_ip = '';

    # Initialisation des variables internes :
    my $sth          = '';
    my $verif_ticket = '';
    my $md5Cle       = new MD5;
    my $maCle        = '';
    my $cleRef       = '';
    my $login_tmp    = '';
    my $indic_limite = '';
    my $date_now     = now;
    my $chaine_date =
        $date_now->year
      . sprintf( "%02d", $date_now->month )
      . sprintf( "%02d", $date_now->day );
    my $chaine_heure =
        sprintf( "%02d", $date_now->hour ) . ":"
      . sprintf( "%02d", $date_now->min ) . ":"
      . sprintf( "%02d", $date_now->sec );

    $sth = $dbh->prepare(
        "SELECT cleverif, date, heure, login FROM sessions where ticket = ?")
      or die $DBI::errstr;
    $sth->execute($ticket) or die $DBI::errstr;

    $verif_ticket = $sth->rows;
    my @row_tab = $sth->fetchrow_array;
    $cleRef    = $row_tab[0];
    $login_tmp = $row_tab[3];

    $login_tmp =~ s/^\s+//;
    $login_tmp =~ s/\s+$//;

    $sth->finish;

# On verifie que l'utilisateur est toujours actif etat = 1 dans la table utilisateurs
    $sth = $dbh->prepare("SELECT etat FROM utilisateurs where login = ?")
      or die $DBI::errstr;
    $sth->execute($login_tmp) or die $DBI::errstr;

    if ( $sth->rows != 1 ) {
        $verif_ticket = 0;
    }
    else {
        my @tab_login = $sth->fetchrow_array;

        if ( $tab_login[0] != 1 ) {
            $verif_ticket = 0;
        }
    }

    $sth->finish;

    if ( $verif_ticket == 1 ) {
        $md5Cle->add( $ticket . $adresse_ip . $navigateur );
        $maCle = unpack( "H*", $md5Cle->digest() );

        if ( $cleRef eq $maCle ) {
            $FCS_Login = $login_tmp;

         # On verifie maintenant que le ticket n'a pas depasse sa duree de vie :
            $indic_limite =
                date substr( $row_tab[1], 0, 4 ) . "-"
              . substr( $row_tab[1], 4, 2 ) . "-"
              . substr( $row_tab[1], 6, 2 ) . " "
              . $row_tab[2];
            $indic_limite = $indic_limite + esolGetEnv(__FILE__, 'TIMEOUT_SESSION');
            if ( $date_now > $indic_limite ) {
                print STDERR "[FCS_VerifTicket] ", $row_tab[1],
                  " ticket usage, nouvelle demande d'authentification\n"
                  if ($debug);
                $retour = 0;
            }
            else {
                $retour = 1;
            }

            # Pour les administrateurs on contourne la duree de vie du ticket
            if ( exists $FCS_HashLoginAdmin{$FCS_Login} ) {
                print STDERR "[FCS_VerifTicket] Admin pas de limite de temps\n"
                  if ($debug);
                $retour = 1;
            }
            else {
                print STDERR
                  "[FCS_VerifTicket] \$FCS_Login = '$FCS_Login', pas admin\n"
                  if ($debug);
            }

            if ( $retour == 1 ) {

                # Version qui serialise les parametres du CGI
                $sth = $dbh->prepare(
                    "UPDATE sessions SET date = ?, heure = ?  WHERE ticket = ?")
                  or die $DBI::errstr;
                $sth->execute( $chaine_date, $chaine_heure, $ticket );
                $sth->finish;
            }
            else {
                $sth = $dbh->prepare(
"UPDATE sessions SET ticket = ?, date = ?, heure = ? WHERE ticket = ?"
                ) or die $DBI::errstr;
                $sth->execute(
                    '-- fin session --', $chaine_date,
                    $chaine_heure,       $ticket
                );
                $sth->finish;
            }
        }
    }
    return $retour;
}

sub FCS_AfficheMessage {

    # On recupere les parametres :
    my $script  = shift @_;    # le nom du script e executer
    my $methode = shift @_;    # la methode e utiliser
    my $ticket  = shift @_;    # le ticket
    my $texte   = shift @_;    # le teste du message

    my $template_mess = HTML::Template->new(
        filename => esolGetEnv( __FILE__, 'PATH_CBR_WEB_DIR' )
          . '/templates/message.tmpl',
        global_vars       => 1,
        die_on_bad_params => 1
    );
    $template_mess->param( SCRIPT  => $script );
    $template_mess->param( METHODE => $methode );
    $template_mess->param( TICKET  => $ticket );
    $template_mess->param( MESSAGE => $texte );
    print $template_mess->output;
}

sub FCS_ClotureSession {
    my $base_dbh = shift @_;
    my $ticket   = shift @_;

    my $retour         = 1;
    my $update_session = $base_dbh->prepare(
        "UPDATE sessions set cleverif = '-- session ended --' where ticket = ?")
      or print STDERR "[FCS_Ticket2Login] : ", $DBI::errstr, "\n";

    $update_session->execute($ticket);

    if ( $base_dbh->errstr ne undef ) {
        print STDERR "[FCS_ClotureSession] Erreur cloture session\n";
        $retour = 0;
    }

    $update_session->finish;

    return $retour;
}

#
# Cette fonction permet d'afficher l'ecran de demande d'identification :
# Xp 03/2013

sub FCS_AffichePageMiseJourPswd() {

    # On recupere les parametres :
    my $mode     = shift @_;
    my $username = shift @_;
    my $message  = shift @_;
    my $welcome_msg =
"Pour s&eacute;curiser votre connexion, les mots de passes cr&eacute;&eacute;s ont une dur&eacute;e de validit&eacute; de $FCS_timeout_password jours.<br><br>Votre mot de passe est aujourd'hui expir&eacute;.
	Veuillez cr&eacute;er un nouveau mot de passe.<br>";

    # Initialisation des variables internes :
    my $template = HTML::Template->new(
        filename => esolGetEnv( __FILE__, 'PATH_CBR_WEB_DIR' )
          . '/templates/ecran_reauthentification.tmpl',
        die_on_bad_params => 0,
        global_vars       => 1
    );

    if ( $ENV{'GATEWAY_INTERFACE'} =~ /^CGI-Perl/ ) {

    }
    else {

    }

    if ( $mode == 0 ) {
        $template->param( ACCES_OK => 1 );
        $template->param( username => $username );
        $template->param( IMAGE    => "images/clear.gif" );
        $template->param( MESSAGE  => '' );

    }

    # mode == 9 si FCS down, $message contient la raison de l'arret
    elsif ( $mode == 9 ) {
        $template->param( ACCES_OK => 0 );
        $template->param( IMAGE    => "images/fcs_bandeau_rouge_ss_txt.gif" );
        $template->param( MESSAGE  => $message );
    }

    # Le mot de passe est trop vieux on demande de creer un nouveau mot de passe
    else {
        $template->param( ACCES_OK => 1 );
        $template->param( username => $username );
        $template->param( IMAGE    => "images/newpassword.png" );
        $template->param( MESSAGE  => $welcome_msg );
    }

    print $template->output;
    exit;
}

#
# Cette fonction permet d'afficher l'ecran de recuperation de mot de passe :
# Xp 03/2013

sub FCS_AffichePageRecuperationPswd() {

    # On recupere les parametres :
    my $mode    = shift @_;
    my $message = shift @_;

    # Initialisation des variables internes :
    my $template = HTML::Template->new(
        filename => esolGetEnv( __FILE__, 'PATH_CBR_WEB_DIR' )
          . '/templates/ecran_recuperation_pswd.tmpl',
        die_on_bad_params => 0,
        global_vars       => 1
    );

    if ( $ENV{'GATEWAY_INTERFACE'} =~ /^CGI-Perl/ ) {

        # Si on est sous mod_perl
        # On obtient le contexte de la requete HTTP
        #        my $hdl_http = Apache->request;
        #            print "Content-type: text/html\r\n\r\n";
        #        $hdl_http->send_http_header('text/html');
    }
    else {

        # Si on est en mode CGI classique
        #        print "Content-type: text/html\r\n\r\n";
    }

    if ( $mode == 0 ) {
        $template->param( ACCES_OK => 1 );
        $template->param( IMAGE    => "images/clear.gif" );
        $template->param( MESSAGE  => '' );
    }

    # mode == 9 si FCS down, $message contient la raison de l'arret
    elsif ( $mode == 9 ) {
        $template->param( ACCES_OK => 0 );
        $template->param( IMAGE    => "images/fcs_bandeau_rouge_ss_txt.gif" );
        $template->param( MESSAGE  => $message );
    }

    # Le mot de passe est trop vieux on demande de creer un nouveau mot de passe
    else {
        $template->param( ACCES_OK => 1 );
        $template->param( IMAGE    => "images/fcs_bandeau_rouge_ss_txt.gif" );
        $template->param( MESSAGE =>
"Veuillez saisir votre nom d'utilisateur et votre courriel pour obtenir un nouveau mot de passe"
        );
    }

    print $template->output;

}

#
# Cette fonction permet de generer un mot de passe aleatoire
# Xp 03/2013

sub FCS_randomPassword {
    my $password;
    my $_rand;

    my $password_length = $_[0];
    if ( !$password_length ) {
        $password_length = 10;
    }

    my @chars = split(
        " ",
"a b c d e f g h i j k l m n o p q r s t u v w x y z 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
    );

    srand;

    for ( my $i = 0 ; $i <= $password_length ; $i++ ) {
        $_rand = int( rand 41 );
        $password .= $chars[$_rand];
    }
    return $password;
}

sub FCS_DHTML_Menu {
    my ($ticket) = shift @_;
    my $template = HTML::Template->new(
        filename => esolGetEnv( __FILE__, 'PATH_CBR_WEB_DIR' )
          . '/templates/dhtml_menu.tmpl',
        global_vars       => 1,
        die_on_bad_params => 0
    );

    my $affiche_admin = 0;

    #	print $FCS_Login."]";
    #	print $FCS_HashLoginAdmin{$FCS_Login}."]";
    if ( $FCS_HashLoginAdmin{$FCS_Login} == 1 ) {
        $affiche_admin = 1;
    }

    my $dbh;
    (
        $dbh = DBI->connect(
            esolGetEnv( __FILE__, 'BDD_CBR_DSN' ),
            esolGetEnv( __FILE__, 'BDD_USER' ),
            esolGetEnv( __FILE__, 'BDD_PASSWORD' ),
            { AutoCommit => 1 }
        )
      )
      or &FCS_AfficheErreur( "authentification.psp", "POST", $ticket,
        "Connexion base impossible." );
    my $login = &FCS_Ticket2Login( $dbh, $ticket );

    $template->param( ticket => $ticket );
    foreach my $action ( keys %FCS_MenuActions ) {
        $template->param( AFFICHE_ADMIN       => $affiche_admin );
        $template->param( $action             => 1 );
        $template->param( 'indice_' . $action => $FCS_MenuActions{$action} );
    }

    print $template->output;

}

sub is_user_in_group {
    my ( $dbh, $ticket, $group ) = @_;
    my $value_to_return = 0;
    my $sqlr            = "
	SELECT *
	FROM sessions as s
	LEFT JOIN groupe_logins as gl
	ON s.login = gl.login
	WHERE ticket = '$ticket' 
	AND groupe = '$group'
	";

    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        &FCS_AfficheErreur( "authentification.psp", "POST", $ticket,
            $dbh->errstr . "<br><pre>" . $sqlr );
        $rs->finish;
        $dbh->disconnect;
        exit;
    }
    if ( $rs->rows > 0 ) {
        $value_to_return = 1;
    }
    return $value_to_return;

}

sub is_action_allowed {
    my ( $dbh, $ticket, $action, $dbug ) = @_;
    my $bool_to_return = 0;
    my @loop;
    my $sqlr;

    $sqlr .= " 
		SELECT * FROM
		(
		SELECT
		al.id_action as action,
		s.ticket as ticket
		FROM action_logins as al
		LEFT JOIN sessions as s
		ON s.login = al.login
		UNION
		SELECT
		ag.id_action as action,
		s.ticket as ticket
		FROM groupe_logins as gl
		LEFT JOIN action_groupes as ag
		ON ag.groupe = gl.groupe
		LEFT JOIN sessions as s
		ON s.login = gl.login
		) as rs
		WHERE ticket = '$ticket'
		AND action = '$action'
	";

    print "<pre>" . $sqlr . "</pre>" if ($dbug);
    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        &FCS_AfficheErreur( "authentification.psp", "POST", $ticket,
            "Request Error:" . $dbh->errstr . "<br>" . $sqlr );
        $rs->finish;
        $dbh->disconnect;
        exit;
    }
    if ( $rs->rows > 0 ) {
        $bool_to_return = 1;
    }

    return $bool_to_return;
}

return 1;
