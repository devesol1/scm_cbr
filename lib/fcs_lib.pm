#!/usr/bin/perl
#
###############################################################################
#      Fichier : fcs_lib.pm
#        Objet : Ce fichier contient les param�tres de configuration de FCS 
#		 TOUTES LES VARIABLES, FONCTIONS et PROCEDURES CONTENUES
#		 DANS CETTE LIBRAIRIE SONT COMMUNES � TOUS LES CLIENTS FCS
# Soci�t�      : e-SOLUTIONS 
# Cr�ation     : 04/04/2006 
#
# Historique   :
#
###############################################################################
use strict;

package fcs_lib;

use Class::Date qw(:errors date localdate gmdate now);

require Exporter;
BEGIN {
	@fcs_lib::ISA       = qw(Exporter);
	@fcs_lib::EXPORT    = qw(
                                $FCS_htmldoc
                                %FCS_HashLoginAdmin
                                $FCS_format_date_out
                                $FCS_format_heure_out
                                @FCS_tab_comm
                                @FCS_tab_civilite
                                @FCS_tab_oui_non
                                %fcs_hach_route_mode
                                %fcs_hach_ftl_ltl
				%hash_code_linerterm2libelle 
				%hash_libelle_linerterm2code
				FCS_htmlTOpdf 
				FCS_htmlTOps
				FCS_GenereListe
				FCS_GenereListeChiffres
				FCS_GenereListeCivilite
				FCS_GenereListeComm
				FCS_GenereListeOuiNon
				FCS_ConvertiFrenchDateToISO
				FCS_GetLinerTermList
				FCS_GetLinerTerm
				trimwhitespace
				remove_quotes
				set_float_to_db
				set_float_to_int
				ConvertiDateISOToFrenchDate
				FCS_GenereListeHeures
				FCS_GenereListeMinutes
				order_form_add_event
				is_user_in_group
				sub date_fr_to_en

                                );

	@fcs_lib::EXPORT_OK = qw();
	$fcs_lib::VERSION   = 1.00;
}

#----------------------------------------------------------------------------
# COMANDES
#----------------------------------------------------------------------------
# Commandes pour la transformation des fichiers HTML en PS
$fcs_lib::FCS_htmldoc = '/usr/bin/htmldoc';

#----------------------------------------------------------------------------
# CONSTANTES
#----------------------------------------------------------------------------
# Tableau contenant les modes d'envoi de documents
@fcs_lib::FCS_tab_comm     = ('E-Mail', 'Fax');

# Tableau contenant les civilit�s
@fcs_lib::FCS_tab_civilite = ('Miss', 'Mrs', 'Mr');

# Tableau Oui/Non 
@fcs_lib::FCS_tab_oui_non  = ('No', 'Yes');

# Hash contenant les logins des administrateurs du syst�me
%fcs_lib::FCS_HashLoginAdmin = (
    'ethaunier'  => 1,
    'cdelamarre' => 1,
    'jboucly' => 1,
    'xpicart' => 1,
);

# Format d'affichage des dates (PostgreSQL)
$fcs_lib::FCS_format_date_out  = 'DD/MM/YYYY';

# Format d'affichage de l'heure (PostgreSQL)
$fcs_lib::FCS_format_heure_out = 'HH24:MI:SS';

# Hash des modes de transport route
%fcs_lib::fcs_hach_route_mode = (
    '1' => 'Consolidation',
    '2' => 'Complete',
    '3' => 'Multipoint',
    '4' => 'Other'
);

# Hash des ftl_ltl
%fcs_lib::fcs_hach_ftl_ltl = (
    '1' => 'Full truck Load',
    '2' => 'Less than truck Load'
);

# Hash code linerterm => libell� linerterm
%fcs_lib::hash_code_linerterm2libelle = (
	'1' => 'CY/CY',
	'2' => 'CFS/CY',
	'3' => 'CFS/CFS',
	'4' => 'CY/CFS',
	'5' => 'LCL',
	'6' => 'Other'
);

# Hash libell� linerterm => code linerterm
%fcs_lib::hash_libelle_linerterm2code = (
	'CY/CY'   => '1',
	'CFS/CY'  => '2',
	'CFS/CFS' => '3',
	'CY/CFS'  => '4',
	'LCL'     => '5',
	'Other'   => '6' 
);

#----------------------------------------------------------------------------
# PROCEDURES
#----------------------------------------------------------------------------

# Fonction convertissant un ou plusieurs documents HTML en un unique documents PDF
sub FCS_htmlTOpdf {

    # On passe le ou les nom(s) du ou des fichiers � convertir en PDF
    my $liste_fichiers_html = '';
    my $tmp_date            = now;
    my $yyyymmddhhmm        = $tmp_date->year . sprintf( "%02d", $tmp_date->month ) . sprintf( "%02d", $tmp_date->day ) . sprintf( "%02d", $tmp_date->hour ) . sprintf( "%02d", $tmp_date->min );
    my $pdf_file            = "/tmp/" . $yyyymmddhhmm . $$ . ".pdf";

    unless ( scalar @_ > 0 ) {

        # Il n'y a aucun param�tre...
        return 0;
    }

    foreach my $html_file (@_) {
        unless ( -e $html_file ) {

            # Le fichier n'existe pas
            return 0;
        }

        unless ( -r $html_file ) {

            # Le fichier n'est pas lisible
            return 0;
        }
        $liste_fichiers_html .= " " . $html_file;
    }

    # Run HTMLDOC to provide the PDF file to the user...
    system "$fcs_lib::FCS_htmldoc -t pdf --quiet --no-truetype --webpage $liste_fichiers_html -f $pdf_file";

    if ( -e $pdf_file ) {

        # Si tout est ok on retourne 1 et le nom du fichier
        return 1, $pdf_file;
    }
    else {
        return 9;
    }
}

# Fonction convertissant un ou plusieurs documents HTML en un unique documents PostScript level 2.
sub FCS_htmlTOps {

    # On passe le ou les nom(s) du ou des fichiers � convertir en PostScript
    my $liste_fichiers_html = '';
    my $tmp_date            = now;
    my $yyyymmddhhmm        = $tmp_date->year . sprintf( "%02d", $tmp_date->month ) . sprintf( "%02d", $tmp_date->day ) . sprintf( "%02d", $tmp_date->hour ) . sprintf( "%02d", $tmp_date->min );
    my $ps_file             = "/tmp/" . $yyyymmddhhmm . $$ . ".ps";

    unless ( scalar @_ > 0 ) {

        # Il n'y a aucun param�tre...
        return 0;
    }

    foreach my $html_file (@_) {
        unless ( -e $html_file ) {

            # Le fichier n'existe pas
            return 0;
        }

        unless ( -r $html_file ) {

            # Le fichier n'est pas lisible
            return 0;
        }
        $liste_fichiers_html .= " " . $html_file;
    }

    # Run HTMLDOC to provide the ps file to the user...
    system "$fcs_lib::FCS_htmldoc -t ps2 --quiet --webpage $liste_fichiers_html -f $ps_file";

    if ( -e $ps_file ) {

        # Si tout est ok on retourne 1 et le nom du fichier
        return 1, $ps_file;
    }
    else {
        return 9;
    }
}

# Fonction retournant une r�f�rence sur un tableau de tableaux de hash.
# Elle prend 4 param�tres :
# - valeur de d�part (chiffre)
# - valeur de fin (chiffre)
# - valeur rep�re (chiffre)
# - un indicateur de commencer par un blanc (0 ou 1)
# Chaque tableau de hash contient 2 cl�s : valeur et selected
# 	'valeur' repr�sente un chiffre 
# 	'selected' est �gal � 1 si 'valeur' est �quivalente au 3�me param�tre pass� � cette fonction
sub FCS_GenereListeChiffres {
    my $borne_start = shift @_;
    my $borne_stop  = shift @_;
    my $selected    = shift @_;
    my $indic_blanc = shift @_;
    
    my @tab_tmp = ();
    my $len     = length $borne_stop;
    
    if($selected =~ /\d+/) {
        $selected = sprintf("%0" . $len . "d", $selected);
    }
    
    if($indic_blanc == 1) {
        my %hash_tmp = ();
        $hash_tmp{'valeur'}   = '';
        $hash_tmp{'selected'} = 0;
        push @tab_tmp, \%hash_tmp;
    }
    
    for(my $i = $borne_start ; $i < $borne_stop + 1 ; $i++) {
        my %hash_tmp = ();
        
        $hash_tmp{'valeur'} = sprintf("%0" . $len . "d", $i);
        
        if($hash_tmp{'valeur'} eq $selected) {
            $hash_tmp{'selected'} = 1;
        }
        else {
            $hash_tmp{'selected'} = 0;
        }
        push @tab_tmp, \%hash_tmp;
    }
    return \@tab_tmp;
}

sub FCS_GenereListeHeures {
    my $selected    = shift @_;
    my $indic_blanc = shift @_;

    my $borne_start = 0;
    my $borne_stop  = 23;
    
    my @tab_tmp = ();
    my $len     = length $borne_stop;
    
    if($selected =~ /\d+/) {
        $selected = sprintf("%0" . $len . "d", $selected);
    }
    
    if($indic_blanc == 1) {
        my %hash_tmp = ();
        $hash_tmp{'valeur'}   = '';
        $hash_tmp{'selected'} = 0;
        push @tab_tmp, \%hash_tmp;
    }
    
    for(my $i = $borne_start ; $i < $borne_stop + 1 ; $i++) {
        my %hash_tmp = ();
        
        $hash_tmp{'valeur'} = sprintf("%0" . $len . "d", $i);
        
        if($hash_tmp{'valeur'} eq $selected) {
            $hash_tmp{'selected'} = 1;
        }
        else {
            $hash_tmp{'selected'} = 0;
        }
        push @tab_tmp, \%hash_tmp;
    }
    return \@tab_tmp;
}

sub FCS_GenereListeMinutes {
    my $selected    = shift @_;
    my $indic_blanc = shift @_;

    my $borne_start = 0;
    my $borne_stop  = 59;
    
    my @tab_tmp = ();
    my $len     = length $borne_stop;
    
    if($selected =~ /\d+/) {
        $selected = sprintf("%0" . $len . "d", $selected);
    }
    
    if($indic_blanc == 1) {
        my %hash_tmp = ();
        $hash_tmp{'valeur'}   = '';
        $hash_tmp{'selected'} = 0;
        push @tab_tmp, \%hash_tmp;
    }
    
    for(my $i = $borne_start ; $i < $borne_stop + 1 ; $i+=5) {
        my %hash_tmp = ();
        
        $hash_tmp{'valeur'} = sprintf("%0" . $len . "d", $i);
        
        if($hash_tmp{'valeur'} eq $selected) {
            $hash_tmp{'selected'} = 1;
        }
        else {
            $hash_tmp{'selected'} = 0;
        }
        push @tab_tmp, \%hash_tmp;
    }
    return \@tab_tmp;
}

# Fonction retournant une r�f�rence sur un tableau de tableaux de hash pour HTML-Template. 
# Ce hash contient la liste de civilit�s.
# Elle prend 2 param�tres :
# - code de la civilit� s�lectionn�e
# - un indicateur pour commencer par un blanc (0 ou 1)
# Chaque tableau de hash contient 3 cl�s : valeur, libell� et selected
# 'value' repr�sente un chiffre 
# 'label' repr�sente un libell� 
# 'selected' est �gal � 1 si 'valeur' est �quivalente au 1er param�tre pass� � cette fonction
sub FCS_GenereListeCivilite {
    my $selected    = shift @_;
    my $indic_blanc = shift @_;
    
    return &fcs_lib::FCS_GenereListe(\@fcs_lib::FCS_tab_civilite, $selected, $indic_blanc);
}

# Fonction retournant une r�f�rence sur un tableau de tableaux de hash pour HTML-Template. 
# Ce hash contient la liste des moyens de communication.
# Elle prend 2 param�tres :
# - code de communication s�lectionn�
# - un indicateur pour commencer par un blanc (0 ou 1)
# Chaque tableau de hash contient 3 cl�s : valeur, libell� et selected
# 'value' repr�sente un chiffre 
# 'label' repr�sente un libell� 
# 'selected' est �gal � 1 si 'valeur' est �quivalente au 1er param�tre pass� � cette fonction
sub FCS_GenereListeComm {
    my $selected    = shift @_;
    my $indic_blanc = shift @_;

    return &fcs_lib::FCS_GenereListe(\@fcs_lib::FCS_tab_comm, $selected, $indic_blanc);
}

# Fonction retournent une r�f�rence sur un tableau de tableaux de hash pour HTML-Template.
# Elle prend 2 param�tres :
# - le code de la valeur s�lectionn�e
# - un indicateur pour commencer par un blanc (0 ou 1)
# Chaque tableau de hash contient 3 cl�s : valeur, libell� et selected
# 'value' repr�sente un chiffre 
# 'label' repr�sente un libell� 
# 'selected' est �gal � 1 si 'valeur' est �quivalente au 1er param�tre pass� � cette fonction
sub FCS_GenereListeOuiNon {
    my $selected    = shift @_;
    my $indic_blanc = shift @_;

    return &fcs_lib::FCS_GenereListe(\@fcs_lib::FCS_tab_oui_non, $selected, $indic_blanc);
}

# Fonction retournant une r�f�rence sur un tableau de tableaux de hash pour HTML-Template.
# Elle prend 3 param�tres :
# - Un tableau contenant la liste des libell�s servant � g�n�rer le select
# - valeur devant �tre s�lectionn�
# - un indicateur pour commencer par un blanc (0 ou 1)
# Chaque tableau de hash contient 3 cl�s : valeur, libell� et selected
# 'value' repr�sente un chiffre 
# 'label' repr�sente un libell� 
# 'selected' est �gal � 1 si 'valeur' est �quivalente au 1er param�tre pass� � cette fonction
sub FCS_GenereListe {
    my $ref_tab_label = shift @_;
    my $selected      = shift @_;
    my $indic_blanc   = shift @_;
    
    my @tab_tmp = ();

    if($indic_blanc == 1) {
        my %hash_tmp = ();
        $hash_tmp{'value'}    = 0;
        $hash_tmp{'label'}    = '';
        
        if($hash_tmp{'value'} eq $selected) {
            $hash_tmp{'selected'} = 1;
        }
        else {
            $hash_tmp{'selected'} = 0;
        }
        push @tab_tmp, \%hash_tmp;
    }
    
    for(my $i = 0 ; $i < scalar @$ref_tab_label ; $i++) {
        my %hash_tmp = ();
        
        $hash_tmp{'label'} = $$ref_tab_label[$i];
        $hash_tmp{'value'} = $i + 1;
        
        if($hash_tmp{'value'} eq $selected) {
            $hash_tmp{'selected'} = 1;
        }
        else {
            $hash_tmp{'selected'} = 0;
        }
        push @tab_tmp, \%hash_tmp;
    }
    return \@tab_tmp;
}

# Fonction de conversion d'une date au fromat fran�ais JJ/MM/AAAA
# au format ISO "condens�" YYYYMMDD. Si la date est vide la fonction 
# retourne undef de fa�on � �tre compatible avec la gestion de la valeur
# NULL dans DBI
sub FCS_ConvertiFrenchDateToISO {
    my $date_tmp = shift @_;
    
    my $date_iso = '';
    
    if($date_tmp eq '') {
        $date_iso = undef;
    }
    else {
        $date_iso  = substr $date_tmp, 6, 4;
        $date_iso .= substr $date_tmp, 3, 2;
        $date_iso .= substr $date_tmp, 0, 2;
    }
    
    return $date_iso;
}

sub ConvertiDateISOToFrenchDate {
    my $date_tmp = shift @_;
    
    my $french_date = '';
    
    if($date_tmp eq '') {
        $french_date = undef;
    }
    else {
        $french_date = substr $date_tmp, 0, 2;
        $french_date .= "/";
        $french_date .= substr $date_tmp, 3, 2;
        $french_date .= "/";
        $french_date  .= substr $date_tmp, 6, 4;
    }
    
    return $french_date;
}


sub FCS_GetLinerTermList {
  my ($idSelected ) = @_;
  my @loop;
   
  my %hash_query = ();
  foreach my $key (keys %fcs_lib::hash_code_linerterm2libelle ){
     my %hash_query = ();
	   
     $hash_query{'ID'}	= $key; 
     $hash_query{'DESIGNATION'}= $fcs_lib::hash_code_linerterm2libelle{$key}; 
     $hash_query{'ISSELECTED'}= "";
     if ( $hash_query{'DESIGNATION'} eq $idSelected ){
     	$hash_query{'ISSELECTED'}= " selected "; 
     }
     push @loop, \%hash_query;
  }
  
  return @loop;
}

sub FCS_GetLinerTerm {
  my ($idSelected ) = @_;
  return $fcs_lib::hash_code_linerterm2libelle{$idSelected}; 
}

# Remove whitespace from the start and end of the string
sub trimwhitespace {
	my ($string ) = @_;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

sub remove_quotes {
	my ($string ) = @_;
	$string =~ s/\'/ /g;
	return $string;
}

sub set_float_to_db {
	my ($string ) = @_;
	$string =~ s/\,/\./g;

	my @tab_float = split ( /\./, $string );
	$string = $tab_float[0].".".substr($tab_float[1], 0, 2);
	
	return $string;
}

sub set_float_to_int {
	my ($string ) = @_;
	$string =~ s/\,/\./g;

	my @tab_float = split ( /\./, $string );
	$string = $tab_float[0];
	
	return $string;
}

sub order_form_add_event{
	my ($dbh, $num_po_root, $ticket, $type_event, $event) = @_;
	my $sqlr = "
		SELECT
        DISTINCT ON ( p.num_po ) 
        num_po as num_po
		FROM (
			SELECT num_po
			FROM edit_po_header
			UNION
			SELECT num_po
			FROM spool_po_header
			UNION
			SELECT num_po
			FROM po_header
		) as p
        WHERE regexp_replace(TRIM(p.num_po), '([^_]*)(.*)', E'\\\\1', 'g') = regexp_replace('$num_po_root','([^_]*)(.*)', E'\\\\1', 'g' )
	";
    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
	if ( $dbh->errstr ne undef ) {
		 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}

	
	while (my $data = $rs->fetchrow_hashref) {
		my $num_po = $data->{'num_po'};
		
		my $sqlr = "
		INSERT INTO evenements_po 
		( id_po, date_evenement, heure_evenement, login, type, evenement )
		VALUES (
			'$num_po',
			TO_CHAR(NOW(), 'YYYYMMDD'), 
			TO_CHAR(NOW(), 'HH24MI'), 
			(SELECT DISTINCT login FROM sessions WHERE ticket = '$ticket'), 
			'$type_event', 
			'$event'
			);
		";
		
		    my $rs = $dbh->prepare($sqlr);
			$rs->execute();
			

			if ( $dbh->errstr ne undef ) {
				
				$rs->finish;
				$dbh->disconnect;
				exit;
			}
	}
	
}

sub is_user_in_group {
	my ($dbh,$ticket, $group) = @_;
	my $value_to_return = 0;	
	my $sqlr = "
	SELECT *
	FROM sessions as s
	LEFT JOIN groupe_logins as gl
	ON s.login = gl.login
	WHERE ticket = '$ticket' 
	AND groupe = '$group'
	";
    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
	if ( $dbh->errstr ne undef ) {
		
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
	if ($rs->rows>0) {
		$value_to_return = 1;
	}
	return $value_to_return;	
	
}

sub date_fr_to_en {
	my ($date_fr) = @_;
	return substr($date_fr,6,4).substr($date_fr,3,2).substr($date_fr,0,2);
}



#
1;
