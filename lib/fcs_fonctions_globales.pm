#!/usr/bin/perl
#
# Module contenant des fonctions trop generales pour etre classees dans un module particulier de FCS
#
# Societe      : e-SOLUTIONS C.T.
# Creation	   : 08/01/2003
# Modification : 29/01/2003 A.F.
#
# Historique
#
# 29/01/2003 A.F. : Modification pour nouveau module fcs_conf
#
###############################################################################
use strict;

package fcs_fonctions_globales;
use DBI;

# ENVIRONNEMENT GENERIQUE 
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use fcs_lib;

require Exporter;

BEGIN {
    @fcs_fonctions_globales::ISA       = qw(Exporter);
    @fcs_fonctions_globales::EXPORT    = qw(	FCS_cree_tab_incoterm 
						FCS_cree_tab_transport 
						FCS_cree_tab_linerterm 
						FCS_cree_tab_mode_transport_route 
						FCS_cree_tab_pays 
						FCS_cree_tab_division
						FCS_cree_tab_port 
						FCS_cree_tab_entrepot 
						FCS_cherche_infos_edit_po_header
						FCS_cherche_infos_edit_po_transport
						FCS_cherche_infos_edit_po_detail
						FCS_cree_tab_ftl_ltl);
    @fcs_fonctions_globales::EXPORT_OK = qw();
    $fcs_fonctions_globales::VERSION   = 1.00;
}

#===================================================================================================================================
#<SUB NAME = 'FCS_cherche_infos_edit_po_detail'>
#<COMMENT>
# fonction qui retourne un hach de tous les champs de la table edit_po_detail pour un num_po donne, une ligne de transport et une ligne detail donnee :
# parametres d'entree:
# - handle sur la base
# - num_po
# - num_ligne_transport
# - nim_ligne_detail
#fonction retournant:
#	une ref sur un hach qui a pour cle les champs de la table edit_po_detail
#</COMMENT>
sub FCS_cherche_infos_edit_po_detail {

    # on recupere les parametres:
    my $hdl_base      = shift @_;
    my $num_po        = shift @_;
    my $num_transport = shift @_;
    my $num_detail    = shift @_;

    # on padde les numeros
    $num_po    =~ s/^\s+//;
    $num_po    =~ s/\s+$//;
    $num_transport =~ s/^\s+//;
    $num_transport =~ s/\s+$//;
    $num_detail =~ s/^\s+//;
    $num_detail =~ s/\s+$//;
    my $requeteSQL = qq{SELECT num_po, num_ligne_transport, num_ligne_detail,
			set_article_acheteur, set_article_fournisseur, libelle_set_article, ean13_article,
			taille, couleur, inner_units, master_units, inner_master, inner_description,
			ean13_inner, master_description, ean13_master, units_inner, unit_cost,
			item_poids_total, item_volume_total, total_units,
			nomenclature, urgent,
			num_contrat, num_poste_contrat
			FROM edit_po_detail
			WHERE num_po = ?
			AND num_ligne_transport = ?
			AND num_ligne_detail = ?
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cherche_infos_edit_po_detail] : ", $DBI::errstr, "\n";

    $requete->execute( $num_po, $num_transport, $num_detail );

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $cle ( keys %{ $$res_curseur1[0] } ) {
                ${ $$res_curseur1[0] }{$cle} =~ s/^\s+//;
                ${ $$res_curseur1[0] }{$cle} =~ s/\s+$//;
            }
        }

        return $$res_curseur1[0];
    }
}
#</SUB>
#===================================================================================================================================
#<SUB NAME = 'FCS_cherche_infos_edit_po_transport'>
#<COMMENT>
# fonction qui retourne un hach de tous les champs de la table edit_po_transport pour un num_po donne et une ligne de transport:
# parametres d'entree:
# - handle sur la base
# - num_po
# - num_ligne
#fonction retournant:
#	une ref sur un hach qui a pour cle les champs de la table edit_po_transport
#</COMMENT>
sub FCS_cherche_infos_edit_po_transport {

    # on recupere les parametres:
    my $hdl_base  = shift @_;
    my $num_po    = shift @_;
    my $num_ligne = shift @_;

    # on padde le num_po et la ligne de transport:
    $num_po    =~ s/^\s+//;
    $num_po    =~ s/\s+$//;
    $num_ligne =~ s/^\s+//;
    $num_ligne =~ s/\s+$//;

    my $requeteSQL = qq{SELECT num_po, num_ligne_transport,
			CASE WHEN (date_min = '' OR date_min IS NULL) THEN NULL
			ELSE
			TO_CHAR(TO_DATE(date_min, 'YYYYMMDD'), '$FCS_format_date_out')
			END
			AS date_min_fr,
			date_min,
			CASE WHEN (date_max = '' OR date_max IS NULL) THEN NULL
			ELSE
			TO_CHAR(TO_DATE(date_max, 'YYYYMMDD'), '$FCS_format_date_out')
			END
			AS date_max_fr,
			date_max,
			add1_enlevement, add2_enlevement, cpville_enlevement,
			pays_enlevement, nom_contact_enlevement, tel_contact_enlevement, fax_contact_enlevement, mail_contact_enlevement,
			CASE WHEN (date_arrivee_entrepot = '' OR date_arrivee_entrepot IS NULL) THEN NULL
			ELSE
			TO_CHAR(TO_DATE(date_arrivee_entrepot, 'YYYYMMDD'), '$FCS_format_date_out')
			END
			AS date_arrivee_entrepot_fr,
			date_arrivee_entrepot,
			rs_arrivee_entrepot, cl_arrivee_entrepot, add1_arrivee_entrepot, add2_arrivee_entrepot,
			cpville_arrivee_entrepot, pays_arrivee_entrepot, valeur_totale, monnaie, poids_total, volume_total, mer_linerterm,
			equipement, mer_port_embarquement, mer_nom_port_embarquement, mer_port_arrivee, mer_nom_port_arrivee, 
			mer_date_arrivee, certifiant_id1, certifiant_id2, certifiant_add1, certifiant_add2,
			certifiant_cpville, certifiant_pays, certificate_bill_of_lading, certificate_lettre_voiture, certificate_packing_list, certificate_invoice,
			certificate_insurance, certificate_origin, certificate_origin_form_a, certificate_properties, certificate_origin_textile, certificate_export_textile, certificate_export_license,
			certificate_fumigation, certificate_inspection, route_equipement, route_mode, route_ftl_ltl, air_aeroport_embarquement,
			air_aeroport_arrivee, air_date_arrivee, certificate_airway_bill,
			id_place_of_receipt
			FROM edit_po_transport
			WHERE num_po = ?
			AND num_ligne_transport = ?
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cherche_infos_edit_po_transport] : ", $DBI::errstr, "\n";

    $requete->execute( $num_po, $num_ligne );

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $cle ( keys %{ $$res_curseur1[0] } ) {
                ${ $$res_curseur1[0] }{$cle} =~ s/^\s+//;
                ${ $$res_curseur1[0] }{$cle} =~ s/\s+$//;
            }
        }

        return $$res_curseur1[0];
    }
}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cherche_infos_edit_po_header'>
#<COMMENT>
# fonction qui retourne un hach de tous les champs de la table edit_po_header pour un num_po donne:
# parametres d'entree:
# - handle sur la base
# - num_po
#fonction retournant:
#	une ref sur un hach qui a pour cle les champs de la table edit_po_header
#</COMMENT>
sub FCS_cherche_infos_edit_po_header {

    # on recupere les parametres:
    my $hdl_base = shift @_;
    my $num_po   = shift @_;

    # on padde le num_po:
    $num_po =~ s/^\s+//;
    $num_po =~ s/\s+$//;

    my $requeteSQL = qq{SELECT p.num_po, 
			CASE WHEN (p.date_creation_po = '' OR p.date_creation_po IS NULL) THEN NULL
			ELSE
			TO_CHAR(TO_DATE(p.date_creation_po, 'YYYYMMDD'), '$FCS_format_date_out')
			END
  			AS date_creation_fr,
			p.date_creation_po, 
			CASE	WHEN (p.date_annulation_po = '' OR p.date_annulation_po IS NULL) THEN NULL
			ELSE
				TO_CHAR(TO_DATE(p.date_annulation_po, 'YYYYMMDD'), '$FCS_format_date_out') 
			END
			AS date_annulation_fr,
			p.date_annulation_po,
			p.num_lc,
			CASE WHEN (p.date_lc = '' OR p.date_lc IS NULL) THEN NULL
			ELSE
			TO_CHAR(TO_DATE(p.date_lc, 'YYYYMMDD'), '$FCS_format_date_out')  
			END
			AS date_lc_fr,
			p.date_lc,
			CASE WHEN (p.date_validation = '' OR p.date_validation IS NULL) THEN NULL
			ELSE
			TO_CHAR(TO_DATE(p.date_validation, 'YYYYMMDD'), '$FCS_format_date_out')  
			END
			AS date_validation_fr, 
			p.date_validation,
			p.code_acheteur, p.id_acheteur, p.div_acheteur, p.incoterm, 
			p.reglement, p.mode_transport, p.code_fournisseur,
			p.id_fournisseur, p.add1_fournisseur, p.add2_fournisseur,
			p.cp_ville_fournisseur, p.pays_fournisseur, p.nom_contact,
			p.tel_contact, p.fax_contact, p.mail_contact, p.code_transporteur,
			ri.libelle AS lib_incoterm, rt.nom AS lib_mode_transport ,
			p.uo_number
			FROM edit_po_header p
			LEFT OUTER JOIN ref_incoterm ri
			ON p.incoterm = ri.code
			LEFT OUTER JOIN ref_transport rt
			ON p.mode_transport = rt.code
			WHERE p.num_po = ?					
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cherche_infos_edit_po_header] : ", $DBI::errstr, "\n";

    $requete->execute($num_po);

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $cle ( keys %{ $$res_curseur1[0] } ) {
                ${ $$res_curseur1[0] }{$cle} =~ s/^\s+//;
                ${ $$res_curseur1[0] }{$cle} =~ s/\s+$//;
            }
        }

        return $$res_curseur1[0];
    }
}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_incoterm'>
#<COMMENT>
# fonction qui cree un tableau contenant les libelles de la table ref_incoterm
# parametres d'entree:
# - handle sur la base
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: code et libelle
#</COMMENT>
sub FCS_cree_tab_incoterm {

    # on recupere le handle sur la base:
    my $hdl_base = shift @_;

    my $requeteSQL = qq{SELECT code AS code_incoterm, libelle 
				FROM ref_incoterm 
				ORDER BY code					
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cree_tab_incoterm] : ", $DBI::errstr, "\n";

    $requete->execute();

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $ligne (@$res_curseur1) {

                foreach my $cle ( keys %$ligne ) {
                    $$ligne{$cle} =~ s/^\s+//;
                    $$ligne{$cle} =~ s/\s+$//;
                }
            }
        }

        return $res_curseur1;
    }

}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_transport'>
#<COMMENT>
# fonction qui cree un tableau contenant les libelles de la table ref_transport
# parametres d'entree:
# - handle sur la base
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: code et nom
#</COMMENT>
sub FCS_cree_tab_transport {

    # on recupere le handle sur la base:
    my $hdl_base = shift @_;

    my $requeteSQL = qq{SELECT code AS code_transport, nom 
				FROM ref_transport 
				ORDER BY code					
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cree_tab_transport] : ", $DBI::errstr, "\n";

    $requete->execute();

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $ligne (@$res_curseur1) {

                foreach my $cle ( keys %$ligne ) {
                    $$ligne{$cle} =~ s/^\s+//;
                    $$ligne{$cle} =~ s/\s+$//;
                }
            }
        }

        return $res_curseur1;
    }
}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_pays'>
#<COMMENT>
# fonction qui cree un tableau contenant les libelles de la table ref_pays
# parametres d'entree:
# - handle sur la base
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: code et nom
#</COMMENT>
sub FCS_cree_tab_pays {

    # on recupere le handle sur la base:
    my $hdl_base = shift @_;

    my $requeteSQL = qq{SELECT code as code, 
	                 nom AS nom				 
				FROM ref_pays 
				ORDER BY nom					
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cree_tab_pays] : ", $DBI::errstr, "\n";

    $requete->execute();

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $ligne (@$res_curseur1) {

                foreach my $cle ( keys %$ligne ) {
                    $$ligne{$cle} =~ s/^\s+//;
                    $$ligne{$cle} =~ s/\s+$//;
                }
            }
        }

        return $res_curseur1;
    }
}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_division'>
#<COMMENT>
# fonction qui cree un tableau contenant les divisions acheteur (departements)
# parametres d'entree:
# - handle sur la base
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: division
#</COMMENT>
sub FCS_cree_tab_division {

    # on recupere le handle sur la base:
    my $hdl_base = shift @_;

    my $requeteSQL = qq{SELECT division AS division 
				FROM ref_division
				ORDER BY division					
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cree_tab_division] : ", $DBI::errstr, "\n";

    $requete->execute();

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $ligne (@$res_curseur1) {

                foreach my $cle ( keys %$ligne ) {
                    $$ligne{$cle} =~ s/^\s+//;
                    $$ligne{$cle} =~ s/\s+$//;
                }
            }
        }

        return $res_curseur1;
    }
}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_entrepot'>
#<COMMENT>
# fonction qui cree un tableau contenant les libelles de la table ref_entrepot
# parametres d'entree:
# - handle sur la base
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: code, ville et pays
#</COMMENT>
sub FCS_cree_tab_entrepot {

    # on recupere le handle sur la base:
    my $hdl_base = shift @_;

    my $requeteSQL = qq{SELECT code_entrepot AS code_entrepot, cpville_arrivee_entrepot || pays_arrivee_entrepot as libelle
				FROM ref_entrepot
				ORDER BY code_entrepot					
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cree_tab_entrepot] : ", $DBI::errstr, "\n";
    $requete->execute();

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $ligne (@$res_curseur1) {
                foreach my $cle ( keys %$ligne ) {
                    $$ligne{$cle} =~ s/^\s+//;
                    $$ligne{$cle} =~ s/\s+$//;
                }
            }
        }

        return $res_curseur1;
    }
}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_port'>
#<COMMENT>
# fonction qui cree un tableau contenant les libelles de la table ref_port
# parametres d'entree:
# - handle sur la base
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: code_pays, code_port et nom
#</COMMENT>
sub FCS_cree_tab_port {

    # on recupere le handle sur la base:
    my $hdl_base = shift @_;

    my $requeteSQL = qq{SELECT code_pays, code_port, nom_port 
				FROM ref_ports
				ORDER BY code_pays, code_port					
			};

    my $requete = $hdl_base->prepare($requeteSQL) or print STDERR "[FCS_cree_tab_port] : ", $DBI::errstr, "\n";

    $requete->execute();

    if ( $hdl_base->errstr ne undef ) {    # ERREUR EXECUTION SQL
        $requete->finish;
        return undef;
    }
    else {
        my $res_curseur1 = $requete->fetchall_arrayref( {} );
        $requete->finish;

        if ( scalar @$res_curseur1 > 0 ) {

            foreach my $ligne (@$res_curseur1) {

                foreach my $cle ( keys %$ligne ) {
                    $$ligne{$cle} =~ s/^\s+//;
                    $$ligne{$cle} =~ s/\s+$//;
                }
            }
        }

        return $res_curseur1;
    }
}
#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_linerterm'>
#<COMMENT>
# fonction qui cree un tableau contenant les libelles et codes des linerterms
# parametres d'entree:
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: code_linerterm et nom_linerterm
#</COMMENT>
sub FCS_cree_tab_linerterm {

    my @tab_retour = ();

    foreach my $cle ( keys %hash_code_linerterm2libelle ) {
        my %hach_temp = ();
        $hach_temp{code_linerterm} = $cle;
        $hach_temp{nom_linerterm}  = $hash_code_linerterm2libelle{$cle};
        push @tab_retour, \%hach_temp;
    }

    return \@tab_retour;
}

#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_mode_transport_route'>
#<COMMENT>
# fonction qui cree un tableau contenant les libelles et codes des modes de transport route
# parametres d'entree:
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: route_mode_code et route_mode_lib
#</COMMENT>
sub FCS_cree_tab_mode_transport_route {

    my @tab_retour = ();

    foreach my $cle ( keys %fcs_hach_route_mode ) {
        my %hach_temp = ();
        $hach_temp{route_mode_code} = $cle;
        $hach_temp{route_mode_lib}  = $fcs_hach_route_mode{$cle};
        push @tab_retour, \%hach_temp;
    }

    return \@tab_retour;
}

#</SUB>

#===================================================================================================================================
#<SUB NAME = 'FCS_cree_tab_ftl_ltl'>
#<COMMENT>
# fonction qui cree un tableau contenant les codes et libelles des ftl_ltl
# parametres d'entree:
#fonction retournant:
#	une ref sur un tableau de hach qui a pour cle: route_ftl_ltl_code et route_ftl_ltl_lib
#</COMMENT>
sub FCS_cree_tab_ftl_ltl {

    my @tab_retour = ();

    foreach my $cle ( keys %fcs_hach_ftl_ltl ) {
        my %hach_temp = ();
        $hach_temp{route_ftl_ltl_code} = $cle;
        $hach_temp{route_ftl_ltl_lib}  = $fcs_hach_ftl_ltl{$cle};
        push @tab_retour, \%hach_temp;
    }

    return \@tab_retour;
}

#</SUB>

#
1;
