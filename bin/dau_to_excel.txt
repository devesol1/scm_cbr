
-- LA COTATION ORIGINALE
SELECT
TRIM(du.uo_num) as uo,
du.id_status as id_status,
TRIM(rds.designation) as statut,
TRIM(dpr.contact_ada) as contact,
TRIM(dpr.po_root_num) as po_root,
TRIM(dpr.dpt) as dpt,
db.is_not_waiting_invoice as factavenir,
du.date_open as moisouvert,
du.date_resulted as moissolde,
du.date_confirmed as moisconfirm,
du.date_close as moiscloture,
TRIM(dp.po_num) as po,
dp.id_export_level as level,
TRIM(db.bl_num) as bl,
CASE 
WHEN TRIM(dbi.cost_kind) = 'EGAL DUTIES & TAXES'
        OR TRIM(dbi.cost_kind) = 'DUTIES COSTS'
    OR TRIM(dbi.cost_kind) = 'CUSTOMS FEES' THEN 'CUSTOMS'
WHEN TRIM(dbi.cost_kind) = 'POST ACHEMINEMENT' THEN 'FREIGHT'
ELSE TRIM(dbi.cost_kind)
END as cost_kind,
TRIM(dbi.invoice_num) as invoice,
rdc.designation as currency,
dbi.exchange_rate as rate,
TRIM(db.date_arrival) as date_arrived_pod,
MAX(dbi.invoice_amount) as cost,
MAX(ds.import_dpt_fees_rate_by_shpt) as is_import_dpt_fees_rate_by_shpt,
SUM(ds.sku_pcs) as pcs,
SUM(ds.sku_cbm * ds.sku_ctns) as cbm 
FROM dau_bl_invoice dbi
LEFT JOIN dau_bl db
ON db.id = dbi.id_bl
LEFT JOIN dau_po dp
ON dp.id = db.id_po
LEFT JOIN dau_po_root dpr
ON dpr.id = dp.id_po_root
LEFT JOIN dau_uo du
ON du.id = dpr.id_uo
LEFT JOIN ref_dau_status rds
ON rds.id = du.id_status
LEFT JOIN ref_dau_currency rdc
ON rdc.id = dbi.id_currency
LEFT JOIN dau_sku ds
ON ds.id_bl = dbi.id_bl
WHERE dp.id_export_level = '1'
AND TRIM(dbi.invoice_num) = ''
AND du.uo_num IS NOT NULL 
AND du.uo_num <> '0'
AND du.uo_num <> '23612'
AND du.uo_num LIKE '%'
AND TO_CHAR(TO_DATE(du.date_open, 'YYYYMMDD'), 'YYYY') >= TO_CHAR(NOW()- interval '2 year', 'YYYY')

GROUP BY du.uo_num, du.id_status, rds.designation, dpr.contact_ada, dpr.po_root_num, dpr.dpt, db.is_not_waiting_invoice, du.date_open, du.date_resulted, du.date_confirmed, du.date_close, dp.po_num, dp.id_export_level, db.bl_num, dbi.cost_kind, dbi.invoice_num, rdc.designation, dbi.exchange_rate, db.date_arrival

-- L'AMENDE (CHECKING) 
UNION
SELECT
TRIM(du.uo_num) as uo,
du.id_status as id_status,
TRIM(rds.designation) as statut,
TRIM(dpr.contact_ada) as contact,
TRIM(dpr.po_root_num) as po_root,
TRIM(dpr.dpt) as dpt,
db.is_not_waiting_invoice as factavenir,
du.date_open as moisouvert,
du.date_resulted as moissolde,
du.date_confirmed as moisconfirm,
du.date_close as moiscloture,
TRIM(dp.po_num) as po,
dp.id_export_level as level,
TRIM(db.bl_num) as bl,
CASE 
WHEN TRIM(dbi.cost_kind) = 'EGAL DUTIES & TAXES'
OR TRIM(dbi.cost_kind) = 'DUTIES COSTS'
OR TRIM(dbi.cost_kind) = 'CUSTOMS FEES' THEN 'CUSTOMS'
WHEN TRIM(dbi.cost_kind) = 'POST ACHEMINEMENT' THEN 'FREIGHT'
ELSE TRIM(dbi.cost_kind)
END as cost_kind,
TRIM(dbi.invoice_num) as invoice,
rdc.designation as currency,
dbi.exchange_rate as rate,
TRIM(db.date_arrival) as date_arrived_pod,
MAX(dbi.invoice_amount) as cost,
MAX(ds.import_dpt_fees_rate_by_shpt) as is_import_dpt_fees_rate_by_shpt ,
SUM(ds.sku_pcs) as pcs,
SUM(ds.sku_cbm * ds.sku_ctns) as cbm 
FROM dau_bl_invoice dbi
LEFT JOIN dau_bl db
ON db.id = dbi.id_bl
LEFT JOIN dau_po dp
ON dp.id = db.id_po
LEFT JOIN dau_po_root dpr
ON dpr.id = dp.id_po_root
LEFT JOIN dau_uo du
ON du.id = dpr.id_uo
LEFT JOIN ref_dau_status rds
ON rds.id = du.id_status
LEFT JOIN ref_dau_currency rdc
ON rdc.id = dbi.id_currency
LEFT JOIN dau_sku ds
ON ds.id_bl = dbi.id_bl
WHERE dp.id_export_level = '2'
AND TRIM(dbi.invoice_num) = ''
AND du.uo_num IS NOT NULL 
AND du.uo_num <> '0'
AND du.uo_num <> '23612'
AND du.uo_num LIKE '%'
AND TO_CHAR(TO_DATE(du.date_open, 'YYYYMMDD'), 'YYYY') >= TO_CHAR(NOW()- interval '2 year', 'YYYY')

GROUP BY du.uo_num, du.id_status, rds.designation, dpr.contact_ada, dpr.po_root_num, dpr.dpt, db.is_not_waiting_invoice, du.date_open, du.date_resulted, du.date_confirmed, du.date_close, dp.po_num, dp.id_export_level, db.bl_num, dbi.cost_kind, dbi.invoice_num, rdc.designation, dbi.exchange_rate, db.date_arrival

-- L'ENGAGE (EMBARQUE)
UNION
SELECT
TRIM(du.uo_num) as uo,
du.id_status as id_status,
TRIM(rds.designation) as statut,
TRIM(dpr.contact_ada) as contact,
TRIM(dpr.po_root_num) as po_root,
TRIM(dpr.dpt) as dpt,
db.is_not_waiting_invoice as factavenir,
du.date_open as moisouvert,
du.date_resulted as moissolde,
du.date_confirmed as moisconfirm,
du.date_close as moiscloture,
TRIM(dp.po_num) as po,
dp.id_export_level as level,
TRIM(db.bl_num) as bl,
CASE 
WHEN TRIM(dbi.cost_kind) = 'EGAL DUTIES & TAXES'
        OR TRIM(dbi.cost_kind) = 'DUTIES COSTS'
    OR TRIM(dbi.cost_kind) = 'CUSTOMS FEES' THEN 'CUSTOMS'
WHEN TRIM(dbi.cost_kind) = 'POST ACHEMINEMENT' THEN 'FREIGHT'
ELSE TRIM(dbi.cost_kind)
END as cost_kind,
TRIM(dbi.invoice_num) as invoice,
rdc.designation as currency,
dbi.exchange_rate as rate,
TRIM(db.date_arrival) as date_arrived_pod,
MAX(dbi.invoice_amount) as cost,
MAX(ds.import_dpt_fees_rate_by_shpt) as is_import_dpt_fees_rate_by_shpt ,
SUM(ds.sku_pcs) as pcs,
SUM(ds.sku_cbm * ds.sku_ctns) as cbm
FROM dau_bl_invoice dbi
LEFT JOIN dau_bl db
ON db.id = dbi.id_bl
LEFT JOIN dau_po dp
ON dp.id = db.id_po
LEFT JOIN dau_po_root dpr
ON dpr.id = dp.id_po_root
LEFT JOIN dau_uo du
ON du.id = dpr.id_uo
LEFT JOIN ref_dau_status rds
ON rds.id = du.id_status
LEFT JOIN ref_dau_currency rdc
ON rdc.id = dbi.id_currency
LEFT JOIN dau_sku ds
ON ds.id_bl = dbi.id_bl
WHERE dp.id_export_level = '3'
AND TRIM(dbi.invoice_num) = ''
-- Supprim� le 16 Novembre 2012, Demande Laetitia Durand, on prend le flottant donc d�s que dans le DAU
--AND TRIM(db.date_arrival) < 20170927 
--AND TRIM(db.date_arrival) > 0
--AND ( TRIM(substr(db.date_arrival,1,6)) > '200912' OR TRIM(db.date_arrival) = 0 )
AND du.uo_num IS NOT NULL 
AND du.uo_num <> '0'
AND du.uo_num <> '23612'
AND du.uo_num LIKE '%'
AND TO_CHAR(TO_DATE(du.date_open, 'YYYYMMDD'), 'YYYY') >= TO_CHAR(NOW()- interval '2 year', 'YYYY')

GROUP BY du.uo_num, du.id_status, rds.designation, dpr.contact_ada, dpr.po_root_num, dpr.dpt, db.is_not_waiting_invoice, du.date_open, du.date_resulted, du.date_confirmed, du.date_close, dp.po_num, dp.id_export_level, db.bl_num, dbi.cost_kind, dbi.invoice_num, rdc.designation, dbi.exchange_rate, db.date_arrival

-- LE FACTURE
UNION
SELECT
TRIM(du.uo_num) as uo,
du.id_status as id_status,
TRIM(rds.designation) as statut,
TRIM(dpr.contact_ada) as contact,
TRIM(dpr.po_root_num) as po_root,
TRIM(dpr.dpt) as dpt,
db.is_not_waiting_invoice as factavenir,
du.date_open as moisouvert,
du.date_resulted as moissolde,
du.date_confirmed as moisconfirm,
du.date_close as moiscloture,
TRIM(dp.po_num) as po,
dp.id_export_level as level,
TRIM(db.bl_num) as bl,
CASE WHEN TRIM(dbi.cost_kind) = 'EGAL DUTIES & TAXES'
        OR TRIM(dbi.cost_kind) = 'DUTIES COSTS'
    OR TRIM(dbi.cost_kind) = 'CUSTOMS FEES' THEN 'CUSTOMS'
WHEN TRIM(dbi.cost_kind) = 'POST ACHEMINEMENT' THEN 'FREIGHT'
ELSE TRIM(dbi.cost_kind)
END as cost_kind,
TRIM(dbi.invoice_num) as invoice,
rdc.designation as currency,
dbi.exchange_rate as rate,
TRIM(db.date_arrival) as date_arrived_pod,
SUM(dbi.invoice_amount) as cost,
ds.max_is_import_dpt_fees_rate_by_shpt as is_import_dpt_fees_rate_by_shpt,
ds.sum_sku_pcs as pcs, 
ds.sum_sku_cbm  as cbm
FROM dau_uo du
LEFT JOIN dau_po_root dpr
ON du.id = dpr.id_uo
LEFT JOIN dau_po dp
ON dpr.id = dp.id_po_root
LEFT JOIN dau_bl db
ON dp.id = db.id_po
LEFT JOIN  dau_bl_invoice dbi
ON db.id = dbi.id_bl
LEFT JOIN ref_dau_status rds
ON rds.id = du.id_status
LEFT JOIN ref_dau_currency rdc
ON rdc.id = dbi.id_currency
LEFT JOIN ( 
	SELECT 
    ds.id_bl,
	MAX(ds.import_dpt_fees_rate_by_shpt) as max_is_import_dpt_fees_rate_by_shpt ,
	SUM(ds.sku_pcs) as sum_sku_pcs,
	SUM(ds.sku_cbm * ds.sku_ctns) as sum_sku_cbm
	FROM dau_sku as ds
	GROUP BY ds.id_bl
) as ds
ON db.id = ds.id_bl
WHERE dp.id_export_level = '3'
AND TRIM(dbi.invoice_num) <> ''
AND du.uo_num IS NOT NULL 
AND du.uo_num <> '0' 
AND du.uo_num <> '23612'
AND du.uo_num LIKE '%'
AND TO_CHAR(TO_DATE(du.date_open, 'YYYYMMDD'), 'YYYY') >= TO_CHAR(NOW()- interval '2 year', 'YYYY')

GROUP BY du.uo_num, du.id_status, du.date_open, du.date_resulted, du.date_confirmed, du.date_close, dpr.contact_ada, dpr.po_root_num, dpr.dpt, db.is_not_waiting_invoice, dp.po_num, dp.id_export_level, db.bl_num, db.date_arrival,dbi.cost_kind, dbi.invoice_num, dbi.exchange_rate, ds.m