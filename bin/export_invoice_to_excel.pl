#!/usr/bin/perl

use strict;
use CGI;
use DBI;
use MIME::Lite;
use Spreadsheet::WriteExcel;
use POSIX qw(strftime);

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv( __FILE__, 'PATH_CBR_ROOT_DIR' ) . 'params';
use File::FindLib esolGetEnv( __FILE__, 'PATH_CBR_ROOT_DIR' ) . 'lib';
use conf_scm_cbr;

my $dbug = 0;

my ( $dbh, $acces );
my $is_web;
my $file_name;
my $log_msg;

my $uo;
my $po;
my $bl;
my $invoice;
my $contact;
my $currency;
my $is_cfs;

# Tentative de connexion à la base:

(
    $dbh = DBI->connect(
        esolGetEnv( __FILE__, 'BDD_CBR_DSN' ),
        esolGetEnv( __FILE__, 'BDD_USER' ),
        esolGetEnv( __FILE__, 'BDD_PASSWORD' ),
        { AutoCommit => 1 }
    )
) or print "Connexion base impossible.";

#====================
# INITIALISATION DE LA FEUILLE EXCEL =
#====================

my $export_path = esolGetEnv( __FILE__, 'PATH_CBR_ROOT_DIR' ) . "/bin/stats/";

if ( $file_name eq '' ) {
    $file_name = "export_invoice_";
    $file_name .= ( strftime "%Y", localtime );
    $file_name .= ( strftime "%m", localtime );
    $file_name .= ( strftime "%d", localtime ) . "-";
    $file_name .= ( strftime "%H", localtime );
    $file_name .= ( strftime "%M", localtime );
    $file_name .= ".xls";
}
my $file_path = $export_path . $file_name;

my $recipient = 'system@e-solutions.tm.fr';
my $copy      = "";
my $forwarder = 'no-reply@fcsystem.com';

&init;
&main;

sub main {

    my $workbook = Spreadsheet::WriteExcel->new($file_path);
    &add_worksheet_invoice( $dbh, $workbook, 'INVOICE' );
    $workbook->close();

    #	&send_export($recipient, $forwarder, $copy, $file_path) if(!$is_web);
}

sub add_worksheet_invoice {
    my ( $dbh, $which_workbook, $worksheet_name ) = @_;

    my $worksheet = $which_workbook->add_worksheet($worksheet_name);
    $worksheet->hide_zero();

    $worksheet->freeze_panes( 2, 3 );

    $worksheet->set_column( 'A:B', 8 );
    $worksheet->set_column( 'C:C', 20 );
    $worksheet->set_column( 'D:D', 8 );
    $worksheet->set_column( 'E:G', 20 );
    $worksheet->set_column( 'I:I', 15 );
    $worksheet->set_column( 'L:L', 15 );

    my $sqlr = "
	SELECT 
	rdc.designation as monnaie,
	dbi.exchange_rate,
	dbi.cost_kind,
    TO_CHAR(CAST(dateheure AS TIMESTAMP), 'MM-YYYY') as mois_de_saisie, 
	du.uo_num as num_uo, 
    dp.po_num as num_po, 
	db.bl_num as bl, 
	rdi.designation as print_issuer,
	
    SUBSTR(dpr.contact_ada, 1, 1)||SUBSTR(SPLIT_PART(dpr.contact_ada, ' ', 2), 1, 1) as initial_gestionnaire, 
    dbi.invoice_num as facture, 
    CASE
		WHEN dbi.id_currency = '1' THEN dbi.invoice_amount
		ELSE dbi.invoice_amount*dbi.exchange_rate
	END as montant_cumule_po_shpt, 
	CASE
		WHEN dbi.ct_date IS NOT NULL AND LENGTH(dbi.ct_date) > 0 THEN TO_CHAR(TO_DATE(dbi.ct_date, 'YYYYMMDD'), 'DD/MM/YYYY') 
	END as etd,
    dbi.ct_num as ct_num, 
	TO_CHAR(CAST(dateheure AS TIMESTAMP), 'DD/MM/YYYY') as date_de_saisie, 
    dbi.cbm as cbm, 
	dbi.ct_type as ct_type, 

	''
	FROM dau_uo as du
	LEFT JOIN dau_po_root as dpr
	ON du.id = dpr.id_uo
	LEFT JOIN dau_po as dp
	ON dpr.id = dp.id_po_root
	LEFT JOIN dau_bl as db
	ON dp.id = db.id_po
	LEFT JOIN dau_bl_invoice as dbi
	ON db.id = dbi.id_bl
	LEFT JOIN ref_dau_issuers as rdi
	ON dbi.id_issuer = rdi.id::text
	LEFT JOIN ref_dau_currency as rdc
	ON dbi.id_currency = rdc.id
	WHERE 1=1
--    AND dbi.ct_date IS NOT NULL
--    AND LENGTH(dbi.ct_date)> 0
    AND LENGTH(invoice_num)>0
    AND dateheure IS NOT NULL
	AND TO_CHAR(TO_DATE(dbi.dateheure, 'YYYY-MM-DD'), 'YYYY') >= TO_CHAR(NOW()-INTERVAL '1 years', 'YYYY')

	AND ('$uo' = '' OR TRIM(LOWER(du.uo_num)) LIKE TRIM(LOWER('$uo'))||'%' )
	AND ('$po' = '' OR TRIM(LOWER(dp.po_num)) LIKE TRIM(LOWER('$po'))||'%' )
	AND ('$bl' = '' OR TRIM(LOWER(db.bl_num)) LIKE TRIM(LOWER('$bl'))||'%' )
	AND ('$invoice' = '' OR TRIM(LOWER(dbi.invoice_num)) LIKE TRIM(LOWER('$invoice'))||'%' )
	AND ('$contact' = '' OR TRIM(LOWER(dpr.contact_ada)) LIKE '%'||TRIM(LOWER('$contact'))||'%' )
	AND ('$currency' = '' OR TRIM(LOWER(rdc.designation)) LIKE TRIM(LOWER('$currency'))||'%' )
	AND ('$is_cfs' = '' OR LENGTH(TRIM(dbi.ct_num)) > 0 )
	
	ORDER BY TO_CHAR(CAST(dateheure AS TIMESTAMP), 'YYYYMMDD')

	;
	";
    my $rs = $dbh->prepare($sqlr);
    $rs->execute();

    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . "\n" . $sqlr;
        $rs->finish;
        $dbh->disconnect;
        exit;
    }

    my $format = $which_workbook->add_format();
    $format->set_format_properties( bold => 1, rotation => '45', border => 3 );
    my $format_cells = $which_workbook->add_format();
    $format_cells->set_format_properties( border => 1 );

    my $col = 0;
    $worksheet->write_string( 0, $col++, "Facture",            $format );
    $worksheet->write_string( 0, $col++, "Date de saisie",     $format );
    $worksheet->write_string( 0, $col++, "Mois de saisie",     $format );
    $worksheet->write_string( 0, $col++, "UO",                 $format );
    $worksheet->write_string( 0, $col++, "PO",                 $format );
    $worksheet->write_string( 0, $col++, "Gestionnaire",       $format );
    $worksheet->write_string( 0, $col++, "Prestataire",        $format );
    $worksheet->write_string( 0, $col++, "BL",                 $format );
    $worksheet->write_string( 0, $col++, "Famille de Cout",    $format );
    $worksheet->write_string( 0, $col++, "Montant Facture",    $format );
    $worksheet->write_string( 0, $col++, "Monnaie",            $format );
    $worksheet->write_string( 0, $col++, "Taux de conversion", $format );
    $worksheet->write_string( 0, $col++, "ATD Container",      $format );
    $worksheet->write_string( 0, $col++, "Container",          $format );
    $worksheet->write_string( 0, $col++, "CBM container",      $format );
    $worksheet->write_string( 0, $col++, "Type container",     $format );

    for ( my $row = 1 ; my $data = $rs->fetchrow_hashref ; $row++ ) {
        my $col = 0;

        $worksheet->write_string( $row, $col++, $data->{'facture'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'date_de_saisie'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'mois_de_saisie'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'num_uo'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'num_po'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'initial_gestionnaire'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'print_issuer'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'bl'}, $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'cost_kind'},
            $format_cells );
        $worksheet->write_string( $row, $col++,
            $data->{'montant_cumule_po_shpt'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'monnaie'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'exchange_rate'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'etd'}, $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'ct_num'},
            $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'cbm'}, $format_cells );
        $worksheet->write_string( $row, $col++, $data->{'ct_type'},
            $format_cells );

    }

    $rs->finish;
}

#################
#Envoi du fichier
#################

sub send_export {

    my $send_result = "";
    my ( $recipient, $forwarder, $copy, $file_path ) = @_;

    if ( $file_name ne "" && $recipient ne "" ) {

        # On expédie le document
        # On prépare le message e-mail

        my $message = "
Bonjour, <br />

Vous trouverez ci-joint à ce courrier l'export des flux. <br />
Cordialement. <br /><br />
<B>CLOUD SCM  with  E-SOLUTIONS sas</B><br/>
7 rue Poullain Duparc 35000 Rennes (F)<br/>
Tél :   +33 (0)230 0231 60   or  +33 (0)950 2488 70<br/>
<a href='http://www.fcsystem.com'>http://www.fcsystem.com</a>
	";

        my $mime_msg = MIME::Lite->new(
            From    => $forwarder,
            To      => $recipient,
            Cc      => $copy,
            Subject => 'Extraction des flux portuaires',
            Type    => 'TEXT/html',
            Data    => $message
        ) or print STDERR ("Erreur lors de la création de MIME body: $!\n");

        # On attache le fichier dispo à l'e-mail

        $mime_msg->attach(
            Type    => 'BINARY',
            Path    => $file_path,
            ReadNow => 1
          )
          or print STDERR (
            "Erreur lors de l'attachement du fichier export : $!\n");

        my $error;
        if ( $mime_msg->send_by_smtp( esolGetEnv( __FILE__, 'SMTP_SERVER' ) ) )
        {
            $error = 0;
        }
        else {
            $error = 1;
        }

        if ($error) {
            $send_result .= "File export has not been sent to $recipient <br>";
            $send_result .= "You can download it to following link : <br>";
            $send_result .=
              "<a href='" . $file_path . "' target='_blank'>File export</a>";
            print STDERR
"[$0] ERREUR envoi export $file_name par e-mail pour $recipient , demandeur : yrgroup\@fcsystem.com \n";
        }
        else {
            $send_result .= "File export has been sent to $recipient";

            #		`rm -rf $file_path`;
        }

        #	print $file_path."\n";

    }

    #	print $send_result."\n";

}

##########################
#   SCRIPTS DE PASSAGE DE PARAMETRES #
##########################

sub init(@ARGV) {
    $log_msg .= "Debut du programme :" . `date` . "\n";
    if ( scalar @ARGV eq 0 ) {

        #                &init_error();
    }
    else {
        for ( my $i ; $i < scalar @ARGV ; $i++ ) {
            if ( get_arg( $ARGV[$i] ) eq 'w' ) {
                $is_web = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'f' ) {
                $file_name = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'u' ) {
                $uo = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'p' ) {
                $po = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'b' ) {
                $bl = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'i' ) {
                $invoice = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'c' ) {
                $contact = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'm' ) {
                $currency = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 's' ) {
                $is_cfs = $ARGV[ $i + 1 ];
            }
            elsif ( get_arg( $ARGV[$i] ) eq 'help' ) {
                &init_error();
            }
        }
    }
}

sub init_error() {
    $log_msg .= "
usage : export_invoice_to_excel.pl -w is_web 
";
    &exit_function;
}

sub get_arg() {
    my ($str_to_return) = @_;
    if ( index( $str_to_return, '-' ) > -1 ) {
        $str_to_return = substr( $str_to_return, 1, length($str_to_return) );
    }
    else {
        $str_to_return = 0;
    }
    return ($str_to_return);
}

sub exit_function() {
    my $fichier_log = "./activ_deal.log";
    open LOGFILE, ">> $fichier_log" or die "Can't open $fichier_log";
    $log_msg .= "\nFin du programme.\n";
    if ($dbug) {
        print $log_msg;
    }
    else {
        print LOGFILE $log_msg;

    }
    close LOGFILE;
    exit;
}

