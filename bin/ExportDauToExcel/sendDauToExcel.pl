#!/usr/bin/perl

use strict;

use CGI;
use MIME::Lite;
use Class::Date qw(:errors now);

my $fichier = '/home/tmp/dauToExcel.xls';

my $tmp_date = now;
my $jour = sprintf( "%02d", $tmp_date->day ). '/' . sprintf( "%02d", $tmp_date->month ) . '/'. $tmp_date->year;

my $from = 'no-reply@fcsystem.com';

my $to;
$to .= 'vincent.legeay@yrnet.com,';
$to .= 'madleen.duret@yrnet.com,';

my $copy = 'system@e-solutions.tm.fr';
my $subject = "[CBR] Extraction DAU";

my $message = "
Bonjour,

Veuillez trouver en piece attachee à cet email, l'extraction  mensuelle du DAU effectuee ce matin (1H)
Cordialement,

CLOUD SCM  with  E-SOLUTIONS sas
7 rue Poullain Duparc 35000 Rennes (F)
Tel :   +33 (0)230 0231 60   or  +33 (0)950 2488 70
http://www.fcsystem.com
";

my $mime_msg = MIME::Lite->new(
                        From    => '$from',
                        To      => '$to',
                        Cc      => '$copy',
                        Subject => '$subject' ,
                        Type    => 'TEXT',
                        Data    => $message
)
or print STDERR ("Erreur lors de la creation de MIME body: $!\n");

# On attache le fichier dispo e l'e-mail
$mime_msg->attach(
                        Type => 'BINARY',
                        Path => $fichier,
                        ReadNow  => 1
)
or print STDERR ("Erreur lors de l'attachement du fichier excel !\n");

my $error;
if($mime_msg->send_by_smtp(esolGetEnv(__FILE__, 'SMTP_SERVER'))) {
  $error = 0;
} else {
  $error = 1;
}

if ($error) {
  print STDERR ("Erreur lors de l'envoi du mail !!!\n");
} else {
  print STDERR ("Le mail a ete envoye.\n");
}

