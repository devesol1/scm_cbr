#!/usr/bin/perl

use strict;

use CGI;
use MIME::Lite;
use Class::Date qw(:errors now);

my $fichier = esolGetEnv(__FILE__, 'PATH_ROOT_CBR').'/bin/dau_to_excel.xls';
system "rm -rf $fichier";

system "wget -O ".esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau_to_excel.xls 'http://cbryr.fcsystem.com/dau_to_excell.psp?TO_EXCELL=1&IS_BATCH=1'";

my $tmp_date = now;
my $jour = sprintf( "%02d", $tmp_date->day ). '/' . sprintf( "%02d", $tmp_date->month ) . '/'. $tmp_date->year;

my $message = "
Bonjour,

Veuillez trouver en pièce attachée à cet email, l'extraction  mensuelle du DAU effectuée ce matin (1H) concernant les POs arrivés au port avant le ";
$message .= $jour;
$message .= "

Cordialement,

CLOUD SCM  with  E-SOLUTIONS sas
7 rue Poullain Duparc 35000 Rennes (F)
Tél :   +33 (0)230 0231 60   or  +33 (0)950 2488 70
http://www.fcsystem.com
";

my $to;
$to .= 'valerie.hervy@yrnet.com,';
$to .= 'madleen.duret@yrnet.com,';
$to .= 'jeanne.peron@yrnet.com,';
$to .= 'regine.reynaud@yrnet.com'; 

my $mime_msg = MIME::Lite->new(
                        From    => 'no-reply@fcsystem.com',
                        To      => $to,
                        Cc      => 'system@e-solutions.tm.fr',
                        Subject => '[YROCHER] Extraction DAU ',
                        Type    => 'TEXT',
                        Data    => $message
)
or print STDERR ("Erreur lors de la création de MIME body: $!\n");

# On attache le fichier dispo é l'e-mail
$mime_msg->attach(
                        Type => 'BINARY',
                        Path => $fichier,
                        ReadNow  => 1
)
or print STDERR ("Erreur lors de l'attachement du fichier excel !\n");

my $error;
if($mime_msg->send_by_smtp(esolGetEnv(__FILE__, 'SMTP_SERVER'))) {
  $error = 0;
} else {
  $error = 1;
}

if ($error) {
  print STDERR ("Erreur lors de l'envoi du mail !!!\n");
} else {
  print STDERR ("Le mail a été envoyé.\n");
}

