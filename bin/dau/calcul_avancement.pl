#!/usr/bin/perl

use strict;

use DBI;
use Class::Date qw(:errors date now);

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $dau_directory = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR') . "bin/dau/";
my $fichier_log = $dau_directory . "calcul_avancement.log";
open( FICHIER_LOG, ">> $fichier_log" ) || die "Impossible ouvrir fichier log !\n";
my $tmp_date = now;
print FICHIER_LOG "\n\nDEBUT TRAITEMENT de l'avancement du $tmp_date";

my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
if ( $dbh eq undef ) {
  print FICHIER_LOG "\nERROR Connexion base COT_YR";
  close FICHIER_LOG;
  exit;
}

my $sqlr = " SELECT value FROM ref_dau_settings WHERE name = 'rate_po_solde' ";
my $rq = $dbh->prepare( $sqlr );
$rq->execute;
if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
  print FICHIER_LOG "\nError : . $rq";
  $rq->finish;
  close FICHIER_LOG;
  exit;
}
my $rate_po_solde = $rq->fetchrow;
$rq->finish;

my $old_autocommit = $dbh->{AutoCommit};
if ( $old_autocommit == 1 ) {
  $dbh->{AutoCommit} = 0;
}

my $requo = " SELECT
		dlc.id_uo as id_uo,
		dlc.fic_num as fic_num,
		dlc.id as id_fic,
		SUM(dls.total_q_pcs) as qt_ordered
		FROM dau_list_shpts dls
		LEFT JOIN dau_list_cotations dlc
		ON dlc.id = dls.id_cotation
		LEFT JOIN dau_list_uos dlu
		ON dlu.id = dlc.id_uo
		WHERE (dlc.id_status = '2' OR dlc.id_status = '3')
		AND dlc.is_fcs_tracking IS FALSE
		GROUP BY dlc.id_uo, dlc.fic_num, dlc.id
		ORDER BY dlc.id_uo, dlc.fic_num, dlc.id
";
my $rs = $dbh->prepare( $requo );

my $reqpo = "  SELECT
	       SUM(dls.total_q_pcs) as qt_stripped
	       FROM dau_list_shpts dls
	       LEFT JOIN dau_list_cotations dlc
	       ON dlc.id = dls.id_cotation
	       WHERE dlc.fic_num = ?
	       AND dlc.is_fcs_tracking IS TRUE
";
my $rspo = $dbh->prepare( $reqpo );

$rs->execute;
if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
  print FICHIER_LOG "\nError : . $requo";
  $rs->finish;
  $dbh->rollback;
  $dbh->disconnect;
  close FICHIER_LOG;
  exit;
}

my $upuo = " UPDATE dau_list_uos
		SET percent_advanced = ?
		WHERE id = ?
";
my $supuo = $dbh->prepare($upuo);

my $upfic = " UPDATE dau_list_cotations
		SET percent_advanced = ?
		WHERE fic_num = ?
		AND is_fcs_tracking IS FALSE 
";
my $supfic = $dbh->prepare($upfic);

my $upstatutfic = " UPDATE dau_list_cotations
		SET id_status = '3' 
		WHERE fic_num = ?
";
my $supstatutfic = $dbh->prepare($upstatutfic);

my $sum_uo_ordered = 0;
my $sum_uo_stripped = 0;
my $id_uo = 0;
my $id_fic = 0;
my $fic_num = '';
my $qt_ordered = 0;
my $qt_stripped = 0;
my $advanced_uo = 0;
my $advanced_fic = 0;
my $current_id_uo = 0;
while ( my $tableau = $rs->fetchrow_hashref ) {
  $id_uo = $tableau->{'id_uo'};
  $fic_num = $tableau->{'fic_num'};
  $id_fic = $tableau->{'id_fic'};
  $qt_ordered = $tableau->{'qt_ordered'};

  $rspo->execute($fic_num);
  if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
    print FICHIER_LOG "\nError : . $requo";
    $rspo->finish;
    $rs->finish;
    $dbh->rollback;
    $dbh->disconnect;
    close FICHIER_LOG;
    exit;
  }
  $qt_stripped = $rspo->fetchrow;
  $rspo->finish;
  if ( $current_id_uo ne $id_uo ) {
    if ( $current_id_uo ne 0 ) {
      # ON UDDATE l'UO
      if ( $sum_uo_ordered ne 0 ) {
        $advanced_uo = $sum_uo_stripped / $sum_uo_ordered * 100;
      } else {
        $advanced_uo = 0;
      }
      $supuo->execute( $advanced_uo, $current_id_uo );
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nError : . $supuo";
        $supuo->finish;
        $rs->finish;
        $dbh->rollback;
        $dbh->disconnect;
        close FICHIER_LOG;
        exit;
      }
      $supuo->finish;
      $sum_uo_ordered = 0;
      $sum_uo_stripped = 0;
    }
  }

  # ON UDDATE lA FIC
  my $for_statut_stripped = 0;
  if ( $qt_ordered ne 0 ) {
    $advanced_fic = $qt_stripped / $qt_ordered * 100;
    $for_statut_stripped = $qt_stripped + ( $qt_ordered * $rate_po_solde / 100 ); 
  } else {
    $advanced_fic = 0;
  }
  # LE POURCENTAGE D'AVANCEMENT
  # ===========================
  $supfic->execute( $advanced_fic, $fic_num );
  if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
    print FICHIER_LOG "\nError : . $supfic";
    $supfic->finish;
    $rs->finish;
    $dbh->rollback;
    $dbh->disconnect;
    close FICHIER_LOG;
    exit;
  }
  $supfic->finish;

  # LE STATUT
  # =========
  if ( $for_statut_stripped >= $qt_ordered ) {
    $supstatutfic->execute( $fic_num );
    if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
      print FICHIER_LOG "\nError : . $supstatutfic";
      $supstatutfic->finish;
      $rs->finish;
      $dbh->rollback;
      $dbh->disconnect;
      close FICHIER_LOG;
      exit;
    }
    $supstatutfic->finish;
  }  

  $current_id_uo = $id_uo;
  $sum_uo_ordered = $sum_uo_ordered + $qt_ordered;
  $sum_uo_stripped = $sum_uo_stripped + $qt_stripped;
}
$rs->finish;

if ( $current_id_uo ne 0 ) {
  # ON UDDATE l'UO
  if ( $sum_uo_ordered ne 0 ) {
    $advanced_uo = $sum_uo_stripped / $sum_uo_ordered * 100;
  } else {
    $advanced_uo = 0;
  }
  $supuo->execute( $advanced_uo, $current_id_uo);
  if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
    print FICHIER_LOG "\nError : . $supuo";
    $supuo->finish;
    $dbh->rollback;
    $dbh->disconnect;
    close FICHIER_LOG;
    exit;
  }
  $supuo->finish;
}

$dbh->commit;
$dbh->{AutoCommit} = $old_autocommit;
$dbh->disconnect;
print FICHIER_LOG "\nFIN TRAITEMENT de l'avancement\n";
close FICHIER_LOG;


