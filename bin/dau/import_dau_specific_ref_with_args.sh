#!/bin/bash

scriptFile=${BASH_SOURCE[0]}
scriptPath=$( cd -P $( dirname $scriptFile ) && pwd )

cmd=( cd ${scriptPath} )
${cmd[@]}

./01_fcs_extract_dau.pl -p $1
./02_fcs_import_dau.pl
./03_qf_extract_dau.pl -p $1 -d 0
./04_import_charges.pl -x QF_EXTRACT_COTATION.XML
./04_import_charges.pl -x QF_EXTRACT_CHECKING.XML
./04_import_charges.pl -x QF_EXTRACT_ARRIVED_POD.XML
./05_import_floating.pl -x FCS_EXTRACT_ARRIVED_POD_DAU.XML -s 4

