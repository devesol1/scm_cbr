#!/usr/bin/perl
use strict;
use DBI;
use POSIX qw(strftime);
use Class::Date qw(:errors now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;
use XML::Writer;
use IO::File;
use MIME::Lite;
use MIME::QuotedPrint;
use MIME::Base64;

print "\n 05_import_floating.pl @ARGV ".now." \n";

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $dbug = 1;
my $log_msg;

my $xml_file;
my $po_to_dau;

my $dau_bin_directory = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau/";
my $src_directory = $dau_bin_directory."/src/";
my $done_directory = $dau_bin_directory."/done/";
my $dest_directory = $dau_bin_directory."/dest/";
my $file_log = $dau_bin_directory . "integration_floating.log";
open( FILE_LOG, "> $file_log" ) || die "Impossible ouvrir fichier log !\n";
my $tmp_date = now;
print FILE_LOG "\n\nDEBUT TRAITEMENT du $tmp_date";

######################################################"""
# usage : 05_import_floating.pl -x file.xml -s 4 (s: n'autorise l'integration pays que pour les po commençant par l'argument 4)
######################################################"""


&init();
#$xml_file = "EXPORT_FLOATING_20090908.xml";
#$xml_file = "FCS_EXTRACT_SAILING_DAU.XML";



my $xml_content = XML::XPath->new(filename => $src_directory.$xml_file);
&main();

sub main {

#	&purge_dau_floating( $xml_content, $dbug );
	&maj_dau_floating( $xml_content, $dbug );
	    my $date_log = now;
        rename ($src_directory.$xml_file , $done_directory.$xml_file.$date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day ).sprintf( "%02d", $date_log->hour ). sprintf( "%02d", $date_log->min )) or die "impossible de déplacer $src_directory$xml_file vers $done_directory$xml_file";
        $tmp_date = now;
	print FILE_LOG "\n\nFIN TRAITEMENT du $tmp_date";
	close FILE_LOG;

# ON ENVOI LE DIAG PAR MAIL
my $mime_msg = MIME::Lite->new(
  From    => 'YR <no-reply@fcsystem.com>',
  Subject => 'Rapport intégration prélèvements pays pour DAU',
  To      => 'system@e-solutions.tm.fr',
  Type    => 'multipart/mixed'
) or print STDERR ("Error on creating email!\n");
$mime_msg->attach(
  Type => 'TEXT',
  Path => $dau_bin_directory . 'integration_floating.log',
  ReadNow  => 1,
  Filename => $dau_bin_directory . 'integration_floating.log'
) or print STDERR ("Error on attachment email!\n");
$mime_msg->send_by_smtp(esolGetEnv(__FILE__, 'SMTP_SERVER')) or print STDERR ("Error on send email!\n");
}

###########################
# Fonctions et procédures #
###########################

sub purge_dau_floating{
	my ( $xml_content, $dbug ) = @_;
	my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	my $sqlr = "
        DELETE FROM dau_floating
        ;
	";

#	print $sqlr."\n" if ($dbug);
	my $rs = $dbh->prepare( $sqlr );
	$rs->execute;
	if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
	#  print "\n $dbh->errstr \n  $sqlr" if ( $dbug );
	  $rs->finish;
	  $dbh->rollback;
	  $dbh->disconnect;
	  exit;
	}

	$rs->finish;
	$dbh->disconnect;
	
	return 0;	
			
}


sub maj_dau_floating(){
	my ( $xml_content, $dbug ) = @_;

	my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );

	my $sfind = " 
		SELECT df.is_changed
		FROM dau_floating df
		LEFT JOIN dau_sku ds
		ON ds.id = df.id_sku
		LEFT JOIN dau_bl db
		ON db.id = ds.id_bl
		LEFT JOIN dau_po dp
		ON dp.id = db.id_po
		LEFT JOIN dau_po_root dpr
		ON dpr.id = dp.id_po_root
		LEFT JOIN dau_uo du
		on du.id = dpr.id_uo
		WHERE du.uo_num = ?
		AND dpr.po_root_num = ?
		AND dp.po_num = ?
		AND db.bl_num = ?
		AND ds.sku_num = ?
		AND ds.sku_size = ?
		AND ds.sku_color = ?
		AND df.pays_reso = ?
		AND df.date_pays_reso = ?
	";
	my $rfind = $dbh->prepare($sfind);


	my $supdate = " 
		UPDATE dau_floating SET
		sku_pcs = ?,
		sku_pcs_deducted = ?,
		date_deducted = ?
		WHERE id = (
			SELECT id_floating
			FROM dau_floating_full as dif
			WHERE uo_num = ?
			AND po_root_num = ?
			AND po_num = ?
			AND id_export_level = ( SELECT MAX(id) FROM ref_dau_export_level )
			AND bl_num = ?
			AND sku_num = ?
			AND sku_size = ?
			AND sku_color = ?
			AND pays_reso = ?
			AND date_pays_reso = ?
		)
	";
	my $rupdate = $dbh->prepare( $supdate );

	my $scontroleinsert = " 
		SELECT id_sku FROM dau_sku_full
		WHERE uo_num = ?
		AND po_root_num = ?
		AND po_num = ?
		AND bl_num = ?
		AND id_export_level = ( SELECT MAX(id) FROM ref_dau_export_level )
		AND sku_num = ?
		AND sku_size = ?
		AND sku_color = ?
	";
	my $rcontroleinsert = $dbh->prepare( $scontroleinsert );

	my $sinsert = " 
		INSERT INTO dau_floating
		( id_sku, pays_reso, date_pays_reso, 
		  sku_pcs, sku_pcs_deducted, date_deducted, 
		  price_fob1, id_currency_fob1, exchange_rate_fob1, 
		  price_fob2, id_currency_fob2, exchange_rate_fob2, 
		  price_hub, id_currency_hub, exchange_rate_hub, 
		  id_currency_invoiced, exchange_rate_invoiced 
		)
		SELECT 
		(
			SELECT id_sku FROM dau_sku_full
			WHERE uo_num = ?
			AND po_root_num = ?
			AND po_num = ?
			AND bl_num = ?
			AND id_export_level = ( SELECT MAX(id) FROM ref_dau_export_level )
			AND sku_num = ?
			AND sku_size = ?
			AND sku_color = ?
		) as id_sku, 
		?, 
		?, 
		?,
		?,
		'0',
		MAX(price_fob1), 
		MAX(id_currency), 
		MAX(exchange_rate_fob), 
		MAX(price_fob2), 
		MAX(id_currency),
		MAX(exchange_rate_fob),
		MAX(price_hub),
		MAx(id_currency), 
		MAX(exchange_rate),
		MAX(id_currency), 
		MAX(exchange_rate)
		FROM dau_sku_full as dsf
		WHERE uo_num = ?
		AND po_root_num = ?
		AND sku_num = ?
		AND sku_size = ?
		AND sku_color = ?
		AND id_export_level = ( 
			SELECT MIN(id_export_level) 
			FROM dau_sku_full 
			WHERE uo_num = ?
			AND po_root_num = ?
			AND po_num = ?
			AND bl_num = ?
			AND sku_num = ?
			AND sku_size = ?
			AND sku_color = ?
		)
		GROUP BY po_root_num
	";

	my $rinsert = $dbh->prepare( $sinsert );

	my $floating_nodeset = $xml_content->find('/dau/uo/po_root/po/bl/sku/floating');
	foreach my $floating_node ($floating_nodeset->get_nodelist) {
		my $sqlr;
		my $uo_num = $floating_node->findvalue('../../../../../@num');
		my $po_root_num = uc($floating_node->findvalue('../../../../@num'));
		my $po_num = uc($floating_node->findvalue('../../../@num'));
		my $bl_num = uc($floating_node->findvalue('../../@num'));
		
		#my $is_po_to_dau = substr($po_root_num,0,1);		
		#next if($is_po_to_dau ne $po_to_dau);
		
		my $dates_nodeset = $floating_node->findnodes('../../dates');
		my ($etd, $eta, $date_arrival );
		foreach my $dates_node ($dates_nodeset->get_nodelist) {
			$etd = $dates_node->findvalue('etd');
			$eta = $dates_node->findvalue('eta');
			$date_arrival = $dates_node->findvalue('arrival');
			$date_arrival =~ s/ //g;
			$date_arrival = 0 if ( $date_arrival eq '');
			
			$sqlr = "
				UPDATE dau_bl SET
				bl_num = '$bl_num', 
				etd = '$etd', 
				eta = '$eta', 
				date_arrival = '$date_arrival'
				WHERE id = (
					SELECT id_bl
					FROM dau_sku_full as dsf
					WHERE uo_num = '$uo_num'
					AND po_root_num = '$po_root_num'
					AND po_num = '$po_num'
					AND bl_num = '$bl_num'
					AND id_export_level = '3'
					GROUP BY id_bl
				)
				;
			";
			my $rs = $dbh->prepare( $sqlr );
			print "\n$sqlr"if ($dbug);
			$rs->execute;
			if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
				print FILE_LOG "\n$sqlr";
				$rs->finish;
				$dbh->rollback;
				$dbh->disconnect;
				exit;
			}
			my $tmp_date = now;
			print FILE_LOG "\n".$tmp_date;
			print FILE_LOG "\n\nUpdate dau_bl : $uo_num, $po_root_num, $po_num, $bl_num : etd=$etd, eta=$eta, date_arrival=$date_arrival"; 
			$rs->finish;
		}

		my $sku_num = uc($floating_node->findvalue('../@num'));
		my $sku_size = uc($floating_node->findvalue('../@sku_size'));
		my $sku_color = uc($floating_node->findvalue('../@sku_color'));

		my $pays_reso = $floating_node->findvalue('@pays_reso');
		#$pays_reso =~ s/\-//g;
	    print "pays_reso $pays_reso" if($dbug);
		
		
		my $date_pays_reso = $floating_node->findvalue('@date');
		$date_pays_reso = '0' if($date_pays_reso eq '');
		my $sku_pcs = $floating_node->findvalue('.');

		my $tmp_date = now;
		print FILE_LOG "\n".$tmp_date;
		print FILE_LOG  "\n$uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color, $pays_reso, $date_pays_reso, $sku_pcs";
		print "\nsfind : " if($dbug) ;       
		print "\n$sfind" if($dbug) ;       
		print "\n$uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color, $pays_reso, $date_pays_reso" if($dbug) ;

#		$rfind->execute($uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color, $pays_reso, $date_pays_reso );
# CD20180309 Utilisation de sqlr sans prepare... visiblement plus rapide.
		my $sqlr = " 
		SELECT df.is_changed
		FROM dau_floating df
		LEFT JOIN dau_sku ds
		ON ds.id = df.id_sku
		LEFT JOIN dau_bl db
		ON db.id = ds.id_bl
		LEFT JOIN dau_po dp
		ON dp.id = db.id_po
		LEFT JOIN dau_po_root dpr
		ON dpr.id = dp.id_po_root
		LEFT JOIN dau_uo du
		on du.id = dpr.id_uo
		WHERE du.uo_num = '".$uo_num."'
		AND dpr.po_root_num = '".$po_root_num."'
		AND dp.po_num = '".$po_num."'
		AND db.bl_num = '".$bl_num."'
		AND ds.sku_num = '".$sku_num."'
		AND ds.sku_size = '".$sku_size."'
		AND ds.sku_color = '".$sku_color."'
		--AND df.pays_reso = '".$pays_reso."'
		AND regexp_replace(df.pays_reso, '[^a-zA-Z]', '', 'g') = regexp_replace('".$pays_reso."', '[^a-zA-Z]', '', 'g')
		AND df.date_pays_reso = '".$date_pays_reso."'
		;
	";
#	print  $sfind;
		my $rfind = $dbh->prepare($sqlr );
		
		print "sqlr_change ".$sqlr if ($dbug);
		$rfind->execute();
		
		

		if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
			print FILE_LOG "\n$sfind";
			$rfind->finish;
			$dbh->rollback;
			$dbh->disconnect;
			exit;
		}
		
		my $insert = 0;
		my $update = 0;
		if ( $rfind->rows == 0 ) { 
			# PAS d'enregistrement sur la clé : uo,po_root,po,bl,sku,size,color,pays,date pays
			# ON INSERT
			$insert = 1; 
			print FILE_LOG "\n ===> ON INSERT";
		} else { 
			$update = 1;
			while( my @result = $rfind->fetchrow ) {
				if ( $result[0] eq '1' ) { 
					# UN enregistrement a été modifié sur la clé : uo,po_root,po,bl,sku,size,color,pays,date pays
					# ON NE FAIT RIEN
					$update = 0; 
				}
				# AUCUN enregistrement n'a été modifié sur la clé : uo,po_root,po,bl,sku,size,color,pays,date pays
				# ON UPDATE
			}
			if ( $update eq '1' ) { 
				print FILE_LOG "\n ===> ON UPDATE"; 
			} else { 
				print FILE_LOG "\n ===> PAS d'UPDATE : enregistrement modifiéé par utilisateur"; 
			}
		}
		$rfind->finish;
		 
		print "\ninsert $insert\n" if($dbug);
		print "update $update\n" if($dbug);
		 
		if ( $insert ) {
			print "scontroleinsert : \n $scontroleinsert \n";
			print "$uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color\n" if($dbug) ;
			$rcontroleinsert->execute( $uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color );
			if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
				print FILE_LOG "\n$scontroleinsert";
				$rcontroleinsert->finish;
				$dbh->rollback;
				$dbh->disconnect;
				exit;
			}
			if ( $rcontroleinsert->rows == 0 ) {
				print FILE_LOG "\n =======> INSERTION IMPOSSIBLE : clé non trouvée !!!";
				print "
					SELECT id_sku FROM dau_sku_full
					WHERE uo_num = '$uo_num'
					AND po_root_num = '$po_root_num'
					AND po_num = '$po_num'
					AND bl_num = '$bl_num'
					AND id_export_level = ( SELECT MAX(id) FROM ref_dau_export_level )
					AND sku_num = '$sku_num'
					AND sku_size = '$sku_size'
					AND sku_color = '$sku_color'
					;
				";
			}
			$rcontroleinsert->finish;

			$rinsert->execute( $uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color, $pays_reso, $date_pays_reso, $sku_pcs, $sku_pcs, $uo_num, $po_root_num, $sku_num, $sku_size, $sku_color, $uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color );
			print "\n" if($dbug);
			print $sinsert if($dbug);
			print "\n" if($dbug);
			print ( $uo_num," ", $po_root_num," ", $po_num," ", $bl_num," ", $sku_num," ", $sku_size," ", $sku_color," ", $pays_reso," ", $date_pays_reso," ", $sku_pcs," ", $sku_pcs," ", $uo_num," ", $po_root_num," ", $sku_num," ", $sku_size," ", $sku_color," ", $uo_num," ", $po_root_num," ", $po_num," ", $bl_num," ", $sku_num," ", $sku_size," ", $sku_color ) if($dbug);
			print "\n" if($dbug);
			if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
				print FILE_LOG "\n$sinsert";
				$rinsert->finish;
				$dbh->rollback;
				$dbh->disconnect;
				exit;
			}
			$rinsert->finish;
		}

		if ( $update ) {
			$rupdate->execute( $sku_pcs, $sku_pcs, $date_arrival, $uo_num, $po_root_num, $po_num, $bl_num, $sku_num, $sku_size, $sku_color, $pays_reso, $date_pays_reso );
			if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
				print FILE_LOG "\n$supdate";
				$rupdate->finish;
				$dbh->rollback;
				$dbh->disconnect;
				exit;
			}
			$rupdate->finish;
		}

	}

	$dbh->disconnect;

	return 0;
	
}


######################################
#   SCRIPTS DE PASSAGE DE PARAMETRES #
######################################

sub init(@ARGV){
		$log_msg .= "Debut du programme :".`date`."\n";
		if(scalar @ARGV eq 0) {
				&init_error();
		} else {
				for ( my $i = 0 ; $i <= scalar @ARGV ; $i++ ){
								if (get_arg($ARGV[$i]) eq 'x' ){
									$xml_file = $ARGV[$i+1];
								}elsif (get_arg($ARGV[$i]) eq 's' ){
							        $po_to_dau = $ARGV[$i+1];
								} elsif (get_arg($ARGV[$i]) eq 'help' ){
									&init_error();
						}


				}
		}
		if ($xml_file eq '') {
		  &init_error(); 
		}
}

sub init_error(){
	$log_msg .= "
			usage : import_floating.pl -x xml_file 
	";
		&exit_function;
}

sub get_arg(){
		my ($str_to_return) = @_;
		if (index( $str_to_return, '-' ) > -1){
		  $str_to_return = substr($str_to_return , 1, length($str_to_return));
		} else {
		  $str_to_return = 0
		}
		return ($str_to_return);
}

sub exit_function(){
	my $fichier_log = $dau_bin_directory."/import_floating.log";
	open LOGFILE, ">> $fichier_log" or die "Can't open $fichier_log";
		$log_msg .= "\nFin du programme.\n";
	if ($dbug) {
	  print $log_msg;
	} else {
	  print LOGFILE $log_msg;
	}
	close LOGFILE;
		exit;
}

