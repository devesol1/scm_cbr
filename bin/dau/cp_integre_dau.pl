#!/usr/bin/perl

use strict;
use CGI;
use DBI;
use HTML::Template;
use POSIX qw(strftime);
use Class::Date qw(:errors now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;

use MIME::Lite;

use XML::Writer;
use IO::File;

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $DBUG = 0;

( my $DBH_CBR = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or die print "Connexion base cbr impossible." ;
( my $DBH_COT = DBI->connect( esolGetEnv(__FILE__, 'BDD_QF_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or die print "Connexion base cotyr impossible." ;
( my $DBH_FCS = DBI->connect( esolGetEnv(__FILE__, 'BDD_FCS_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or die print "Connexion base fcs impossible." ;

my $BIN_DIRECTORY = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR') . "bin/dau/";
my $SRC_DIRECTORY = $BIN_DIRECTORY . "src/";
my $LOG_FILE_PATH = $BIN_DIRECTORY . 'cp_integre_dau.log';
my $MAD_ESD_DELAY = '7 day'; # Time limit between MAD and date esd

my $log_msg;
my $xml_file;
my ($NUM_PO, $export_level);

&init;

#my @list_quotation_z_to_integrate = &get_list_quotation_z_to_integrate($DBH_FCS, $DBUG);
#my @list_quotation_e_to_integrate = &get_list_quotation_e_to_integrate($DBH_FCS, $DBUG);

&main();

sub main {	
	&purge_dau();
	&vExtractFromQF();
	&vImportToCbr();	
}

$DBH_COT->disconnect;
$DBH_FCS->disconnect;
$DBH_CBR->disconnect;

###########################
# Fonctions et procédures #
###########################

sub vExtractFromQF{
	my ( $department, $contact, $importer, $num_uo, $incoterm, $mode_transport, $linerterm ) = &get_po_detail_from_fcs ();

	if ($export_level eq '1')
	{
		&qf_extract_dau( $num_uo, $department, $importer, $contact, '1', $incoterm, $mode_transport, $linerterm);
    }
	elsif ($export_level eq '3')
	{
		&qf_extract_dau( $num_uo, $department, $importer, $contact, '3', $incoterm, $mode_transport, $linerterm);
	}
	elsif ($export_level eq '5')
	{
		&extract_arrived_pod_dau( $num_uo );
	}
	else 
	{
		&qf_extract_dau( $num_uo, $department, $importer, $contact, '1', $incoterm, $mode_transport, $linerterm);
		&qf_extract_dau( $num_uo, $department, $importer, $contact, '3', $incoterm, $mode_transport, $linerterm);
		&extract_arrived_pod_dau( $num_uo );		
	}
	
}

sub vImportToCbr{
	my $sCommande;

	if ($export_level eq '1')
	{
		$sCommande = $BIN_DIRECTORY.'04_import_charges.pl -x QF_EXTRACT_COTATION.XML';
		&vExecCommande($sCommande); 	
    }
	elsif ($export_level eq '3')
	{
		$sCommande = $BIN_DIRECTORY.'04_import_charges.pl -x QF_EXTRACT_ARRIVED_POD.XML';
		&vExecCommande($sCommande); 	
	}
	elsif ($export_level eq '5')
	{
		$sCommande = $BIN_DIRECTORY.'05_import_floating.pl -x FCS_EXTRACT_ARRIVED_POD_DAU.XML';
		&vExecCommande($sCommande); 	
	}
	else 
	{
		$sCommande = $BIN_DIRECTORY.'04_import_charges.pl -x QF_EXTRACT_COTATION.XML';
		&vExecCommande($sCommande); 	
		$sCommande = $BIN_DIRECTORY.'04_import_charges.pl -x QF_EXTRACT_ARRIVED_POD.XML';
		&vExecCommande($sCommande); 	
		$sCommande = $BIN_DIRECTORY.'05_import_floating.pl -x FCS_EXTRACT_ARRIVED_POD_DAU.XML';
		&vExecCommande($sCommande); 	
	}
	
}
	
	
sub vExecCommande {
	my ($sCommande) = @_;
	my $iResult;
	my $sResult;
	my $sNow = (strftime '%Y%m%d - %H:%M:%S', localtime);
	open( LOG, '>> '.$LOG_FILE_PATH );
	print LOG "\n";
	print LOG $sNow;
	print LOG "\n";
	print LOG $sCommande;
	print LOG "\n";
	$iResult = system($sCommande);
	if($iResult eq 0)
	{
		$sResult = 'SUCCESS';
	} 
	else 
	{
		$sResult = 'FAIL : '.$iResult;
	}
	print LOG $sResult;
	print LOG "\n";		
	close LOG;
	
}

	
sub purge_dau{
	
	my $sqlr = "
		DELETE FROM dau_floating
		WHERE id IN (
			SELECT df.id FROM dau_floating as df
			LEFT JOIN dau_sku as ds
			ON df.id_sku = ds.id
			LEFT JOIN dau_bl as db
			ON ds.id_bl = db.id
			LEFT JOIN dau_po as dp
			ON db.id_po = dp.id
			LEFT JOIN dau_po_root as dpr
			ON dp.id_po_root = dpr.id
			WHERE dpr.po_root_num = SPLIT_PART(UPPER('".$NUM_PO."'), '_', 1)	
			AND df.is_changed <> 1
		)
		;
	";
	
	print "<pre>".$sqlr if($DBUG);
	
	my $rs = $DBH_CBR->prepare($sqlr);
	$rs->execute;
	if ( $DBH_CBR->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n-- Error : ". ($DBH_CBR->errstr) ." $sqlr" if ( $DBUG );
		$rs->finish;
		$DBH_CBR->disconnect;
		exit;
	}	
	$rs->finish;
	
	my $sqlr_dau_sku = "
		DELETE FROM dau_sku
		WHERE id IN (
			SELECT ds.id 
			FROM dau_floating as df
			LEFT JOIN dau_sku as ds
			ON df.id_sku = ds.id
			LEFT JOIN dau_bl as db
			ON ds.id_bl = db.id
			LEFT JOIN dau_po as dp
			ON db.id_po = dp.id
			LEFT JOIN dau_po_root as dpr
			ON dp.id_po_root = dpr.id
			WHERE dpr.po_root_num = SPLIT_PART(UPPER('".$NUM_PO."'), '_', 1)	
			AND df.is_changed <> 1
		)
		;
		";
	print "<pre>".$sqlr_dau_sku if($DBUG);
	
	my $rs_dau_sku = $DBH_CBR->prepare($sqlr_dau_sku);
	$rs_dau_sku->execute;
	if ( $DBH_CBR->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n-- Error : ". ($DBH_CBR->errstr) ." $sqlr_dau_sku" if ( $DBUG );
		$rs_dau_sku->finish;
		$DBH_CBR->disconnect;
		exit;
	}	
	$rs_dau_sku->finish;
	
		
    my $sqlr_dau_bl = "
		DELETE FROM dau_bl
		WHERE id IN (
			SELECT db.id 
			FROM dau_floating as df
			LEFT JOIN dau_sku as ds
			ON df.id_sku = ds.id
			LEFT JOIN dau_bl as db
			ON ds.id_bl = db.id
			LEFT JOIN dau_po as dp
			ON db.id_po = dp.id
			LEFT JOIN dau_po_root as dpr
			ON dp.id_po_root = dpr.id
			WHERE dpr.po_root_num = SPLIT_PART(UPPER('".$NUM_PO."'), '_', 1)	
			AND df.is_changed <> 1
		);
		";
	print "<pre>".$sqlr_dau_bl if ($DBUG);
	
	my $rs_dau_bl = $DBH_CBR->prepare($sqlr_dau_bl);
	$rs_dau_bl->execute;
	if ( $DBH_CBR->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n-- Error : ". ($DBH_CBR->errstr) ." $sqlr_dau_bl" if ( $DBUG );
		$rs_dau_bl->finish;
		$DBH_CBR->disconnect;
		exit;
	}	
	$rs_dau_bl->finish;
	
	
	my $sqlr_dau_po = "
		
		DELETE FROM dau_po
		WHERE id IN (
			SELECT dp.id 
			FROM dau_floating as df
			LEFT JOIN dau_sku as ds
			ON df.id_sku = ds.id
			LEFT JOIN dau_bl as db
			ON ds.id_bl = db.id
			LEFT JOIN dau_po as dp
			ON db.id_po = dp.id
			LEFT JOIN dau_po_root as dpr
			ON dp.id_po_root = dpr.id
			WHERE dpr.po_root_num = SPLIT_PART(UPPER('".$NUM_PO."'), '_', 1)	
			AND df.is_changed <> 1
		);
		";
	
	print "<pre>".$sqlr_dau_po if ($DBUG);
	
	my $rs_dau_po = $DBH_CBR->prepare($sqlr_dau_po);
	$rs_dau_po->execute;
	if ( $DBH_CBR->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n-- Error : ". ($DBH_CBR->errstr) ." $sqlr_dau_po" if ( $DBUG );
		$rs_dau_po->finish;
		$DBH_CBR->disconnect;
		exit;
	}	
	$rs_dau_po->finish;
	
	my $sqlr_dau_po_root = "
		DELETE FROM dau_po_root
		WHERE id IN (
			SELECT dpr.id 
			FROM dau_floating as df
			LEFT JOIN dau_sku as ds
			ON df.id_sku = ds.id
			LEFT JOIN dau_bl as db
			ON ds.id_bl = db.id
			LEFT JOIN dau_po as dp
			ON db.id_po = dp.id
			LEFT JOIN dau_po_root as dpr
			ON dp.id_po_root = dpr.id
			WHERE dpr.po_root_num = SPLIT_PART(UPPER('".$NUM_PO."'), '_', 1)	
			AND df.is_changed <> 1
		);
	";
	
	print "<pre>".$sqlr_dau_po_root if($DBUG);
	
	my $rs_dau_po_root = $DBH_CBR->prepare($sqlr_dau_po_root);
	$rs_dau_po_root->execute;
	if ( $DBH_CBR->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n-- Error : ". ($DBH_CBR->errstr) ." $sqlr_dau_po_root" if ( $DBUG );
		$rs_dau_po_root->finish;
		$DBH_CBR->disconnect;
		exit;
	}	
	$rs_dau_po_root->finish;
	
}	

sub qf_extract_dau{

    my ( $num_uo, $department, $importer, $contact, $export_level , $incoterm, $mode_transport, $linerterm) = @_;
  
	my $xml_file;
	$xml_file = "QF_EXTRACT_COTATION.XML" if ($export_level eq 1);
	$xml_file = "QF_EXTRACT_CHECKING.XML" if ($export_level eq 2);
	$xml_file = "QF_EXTRACT_ARRIVED_POD.XML" if ($export_level eq 3);
	my $xml_file = $SRC_DIRECTORY . $xml_file;
    
    my $find_fic = "";
	$find_fic = " SUBSTR(imh.fic_num,11,1) <> '~'" if ($export_level eq 1);
	$find_fic = " SUBSTR(imh.fic_num,11,1) <> '~'" if ($export_level eq 3);
 
 
	#####################################################""
	# LES REQUETES GENERIQUES
	# =======================
	# Pour la cotation on prend le taux archivé au calcul de cette cotation
	#####################################################

	my $exchange_rate_courant = 1;

	my $sqlrate = " SELECT ch.usd_to_euro_rate as usd_to_euro_rate,
	CASE WHEN ch.usd_to_euro_fob_rate IS NULL then ch.usd_to_euro_rate
	ELSE ch.usd_to_euro_fob_rate END as usd_to_euro_fob_rate
	FROM cotation_header ch
	LEFT JOIN logistic_routing_scenarios lrs
	ON lrs.id = ch.id_scenario
	LEFT JOIN input_model_header imh
	ON imh.id = lrs.id_input_model
	WHERE SUBSTR(imh.fic_num,1,10) = UPPER(?)
	AND SUBSTR(imh.fic_num,11,1) <> '~'
	AND SUBSTR(imh.fic_num,11,1) <> '#'
	AND lrs.date_fcs IS NOT NULL
	AND TRIM(lrs.date_fcs) <> ''
	";
	my $rqrate = $DBH_COT->prepare( $sqlrate );

	my $seqctn = "
	SELECT
	rck.code_fcs as type,
	lre.nb_container
	FROM logistic_routings_equipement lre
	LEFT JOIN ref_container_kinds rck
	ON rck.id = lre.id_container_kinds
	WHERE lre.id_shpt = ?
	";
	my $reqctn = $DBH_COT->prepare( $seqctn );


	my $seqsku = "

	SELECT
	hp.sku as sku,
	'' as size,
	'' as color,
	hp.description as designation,
	hp.pcs_per_master as pcs_per_ctn,
	hp.master_length as packing_long,
	hp.master_large as packing_large,
	hp.master_height as packing_height,
	hp.master_weight as packing_weight,
	hp.unit_cost as prix_fob1,
	0 as marge,
	hp.id as id_scenario,
	0 as id_sku,
	SUM(hpsh.quantity) as pcs

	FROM hp 
	LEFT JOIN hp_shpts hpsh
	ON hpsh.id_hp = hp.id
	WHERE hpsh.id = ?
	AND substr(hp.fic_num,1,3) = '414'
	GROUP BY 
	hp.sku,hp.description,hp.pcs_per_master,hp.master_length,
	hp.master_large,hp.master_height,hp.master_weight,
	hp.unit_cost,hp.id;
	";

	my $reqsku = $DBH_COT->prepare( $seqsku );

	my $seqpricetothub = "
	SELECT
	MAX(unit_purchase_price) as unit_purchase_price,
	SUM(csf.q_pcs) as pcs,
	SUM(csf.total_cost_hub_price) as tot_prix_hub,
	SUM(csf.total_cost_price) as tot_prix
	FROM cotation_sku_fnd csf
	LEFT JOIN cotation_header ch
	ON ch.id = csf.id_header
	WHERE ch.id_scenario = ?
	AND csf.id_sku = ?
	";
	my $reqpricetothub = $DBH_COT->prepare( $seqpricetothub );

	my $seqcost = " 
	SELECT
	rloc.cost_kind as type,
	SUM(cd.cost) as cost
	FROM cotation_detail cd
	LEFT JOIN cotation_header as ch
	ON ch.id=cd.id_header
	LEFT JOIN ref_list_of_charges rloc
	ON rloc.id = cd.id_charge
	WHERE ch.id_scenario = ?
	AND cd.id_shpt = ?
	GROUP BY rloc.cost_kind, cd.id_shpt
    ORDER BY cd.id_shpt
	;
	";
    my $reqcost = $DBH_COT->prepare( $seqcost );

	# Pour l'engagé, on sort un poste de charge "FREIGHT_POL" comprenant VM+FREIGHT+BAF+CAF
	# pour calculer les écarts taux de change dans le DAU
    my $seqcost_couverture = "
	SELECT
	'FREIGHT_POL' as type,
	SUM(cd.cost) as cost
	FROM cotation_detail cd
	LEFT JOIN cotation_header as ch
	ON ch.id=cd.id_header
	LEFT JOIN ref_list_of_charges rloc
	ON rloc.id = cd.id_charge
	WHERE ch.id_scenario = ?
	AND ( rloc.code = 'S_VM' OR rloc.code = 'S_FR' OR rloc.code = 'S_BA' OR rloc.code = 'S_CA' OR rloc.code = 'S_RF' )

	";
    my $reqcost_couverture = $DBH_COT->prepare( $seqcost_couverture );

    #################################################" 
	 
  
    my $output = new IO::File (">".$xml_file);
    print output "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
    my $writer = new XML::Writer(OUTPUT => $output);
    $writer->startTag("dau");

  # ON RECUPERE Les FICs dans COTATION
  # ==================================

    my $exchange_rate = 1;
    my $exchange_rate_fob = 1;
    print FICHIER_LOG "\n====> PO $NUM_PO";

    $writer->startTag("uo",
					"num" => $num_uo 
		   );	

    $writer->startTag("po_root",
                        "num" => substr(uc$NUM_PO,0,10) 
      );
    $writer->startTag("dpt");
    $writer->characters($department);
    $writer->endTag("dpt");
    $writer->startTag("contact");
    $writer->characters($contact);
    $writer->endTag("contact");
    $writer->startTag("importer");
    $writer->characters($importer);
    $writer->endTag("importer");

    if ($exchange_rate == 0 ) { $exchange_rate = 1; }
    if ($exchange_rate_fob == 0 ) { $exchange_rate_fob = $exchange_rate ; }
    

	my $sql = "
	SELECT
	hp.fic_num,
    '".$export_level."' as id_export_level,
    TRIM(UPPER( SUBSTR(rhr.country,1,2)||SUBSTR(replace(rhr.city,' ',''),1,3))) as pod,
    '".$mode_transport."' as id_transport_mode,
    hpsh.ref_loading,
    hp.devise as currency,
    '".$linerterm."' as linerterm,
    imh.id as id_input_model,
    hp.id as id_scenario,
    hpsh.shpt_num as shpt_num,
    -- to_char((hpsh.mad - interval '21 day'),'YYYYMMDD') as esd, --- XP 20170816 Modification of the time limit between MAD and date esd 21 day -> 7 day
	to_char((hpsh.mad - interval '".$MAD_ESD_DELAY."'),'YYYYMMDD') as esd,
    hpsh.id as id_shpt,
    '".$incoterm."' as incoterm

    FROM hp_shpts hpsh
    LEFT JOIN hp 
    ON hp.id = hpsh.id_hp
    LEFT JOIN input_model_header imh
	ON (SUBSTR(imh.fic_num,1,10) = hp.fic_num";
	$sql.=" AND SUBSTR(imh.fic_num,1,11) = 'Z') " if ( $export_level eq '1');
	$sql.=" AND SUBSTR(imh.fic_num,1,11) = 'E') " if ( $export_level eq '3');
	$sql.="
    LEFT JOIN ref_hp_region as rhr
    ON rhr.name = hp.destination_code
    WHERE UPPER(SUBSTR(hp.fic_num,1,10)) = UPPER(SUBSTR('$NUM_PO',1,10)) ";

	
	$sql.=" AND hp.status='Z'" if ( $export_level eq '1');
	$sql.=" AND hp.status='E'" if ( $export_level eq '3');

	$sql .= " ORDER BY esd;";
	
    my $rql = $DBH_COT->prepare( $sql );
	print "\n".$sql."\n" if ($DBUG);
    $rql->execute();
    if ( $DBH_COT->errstr ne undef ) { # Erreur SQL
		print "\n $DBH_COT->errstr \n $sql  \n";
      print FICHIER_LOG "\nERROR : $sql";
      close FICHIER_LOG;
      $rql->finish;
      $DBH_COT->disconnect;
      exit;
    }
  
  my $last_po;
	while ( my $data = $rql->fetchrow_hashref) {
        
		my $po = '';
		my $bl = '';
		
		my $status_po = "";
	

		if ($export_level eq '1'){
			$status_po = 'Z' ;
			my $nb_shpt = &get_nb_shpt($DBH_COT, $NUM_PO, $status_po, $DBUG);
			$po = substr(uc$NUM_PO, 0, 10).'_'.$data->{'shpt_num'};
			$bl = $po;
		}
		
		if ($export_level eq '3'){
			$status_po = 'E' ;
			$po = $data->{'fic_num'};
			$bl = $data->{'ref_loading'};
		}
			
		if( $last_po ne $po ){
			if( $last_po ne '' ){
				  $writer->endTag("po");			
			}
		
			$last_po = $po;
			$writer->startTag("po",
								"num" => $po,
								"id_export_level" => $export_level,
								"pod" => $data->{'pod'},
								"id_transport_mode" => $data->{'id_transport_mode'}
			);
		}
  
		# Recherche du taux de couverture freight
		#========================================
		my $scouverture_freight = "
		 SELECT max(cd.rate_couverture) as rate_freight
		 FROM cotation_detail cd
		 LEFT JOIN cotation_header ch
		 ON ch.id = cd.id_header
		 WHERE ch.id_scenario = $data->{'id_scenario'}
		";
		my $rcouverture_freight = $DBH_COT->prepare( $scouverture_freight );
		print "\n scouverture_freight : ".$scouverture_freight."\n" if ($DBUG);
		$rcouverture_freight->execute();
		my $rate_freight = $rcouverture_freight->fetchrow;
		$rate_freight = 1 if ($rate_freight eq '');

		print "\n rate_freight : ".$rate_freight."\n" if ($DBUG);
		$rcouverture_freight->finish;
        my $taux_exchange_fob = $exchange_rate_fob;
		
        if ( uc(substr($data->{'currency'},0,3)) ne 'USD' && uc(substr($data->{'currency'},0,3)) ne 'EUR' ) {
            $taux_exchange_fob = $exchange_rate / $exchange_rate_fob; # EUR-AUTRE MONNAIE
            $exchange_rate_fob = $exchange_rate;
        } 
        $writer->startTag("bl",
                            "num" => $bl,
                            "currency" => $data->{'currency'},
                            "exchange_rate" => $exchange_rate,
                            "exchange_rate_fob" => $taux_exchange_fob,
			    "linerterm" => $data->{'linerterm'},
			    "rate_freight" => $rate_freight
        ); 
          
		 
	    # ON RECHERCHE LES SKUS
	    # =====================
	    print "\n [$data->{'id_shpt'} ] \n ".$seqsku."\n" if ($DBUG);
  	    $reqsku->execute( $data->{'id_shpt'} );
		
	    if ( $DBH_COT->errstr ne undef ) { # Erreur SQL
		print "\n $DBH_COT->errstr \n $seqsku [$data->{'id_shpt'}]  \n";
	    print FICHIER_LOG "\nERROR : $seqsku";
	    close FICHIER_LOG;
	    $reqsku->finish;
	    $rql->finish;
	    $DBH_COT->disconnect;
	    exit;
	    }
	    my $last_sku = '';
	    while ( my $data_sku = $reqsku->fetchrow_hashref ) {
		
			if ( $last_sku ne $data_sku->{'sku'} && $last_sku ne '' ) {
				  $writer->endTag("sku");
			}
		
			if ( $last_sku ne $data_sku->{'sku'} ) {
			
				  my $seqfees = " 
				   SELECT
				  ( SELECT DISTINCT ON (comment)
				   comment FROM cotation_detail cd
				   LEFT JOIN cotation_header ch
				   ON ch.id = cd.id_header
				   LEFT JOIN logistic_routing_scenarios lrs
				   ON lrs.id = ch.id_scenario
				   LEFT JOIN input_model_header imh
				   ON imh.id = lrs.id_input_model
				   LEFT JOIN ref_list_of_charges rc
				   ON rc.id = cd.id_charge
				   LEFT JOIN hp 
				   ON lrs.id=hp.id
				   WHERE SUBSTR(imh.fic_num,1,10) = UPPER(SUBSTR('$NUM_PO',1,10))
				   AND $find_fic";
				   $seqfees .= "AND hp.status = 'Z'" if ( $export_level eq '1');
				   $seqfees .= "AND hp.status = 'E'" if ( $export_level eq '3');
				   $seqfees .= "
				   AND rc.code = 'S_ID'
				 ) as import_dpt_fees_rate,
				 ( SELECT DISTINCT ON (comment)
				   comment FROM cotation_detail cd
				   LEFT JOIN cotation_header ch
				   ON ch.id = cd.id_header
				   LEFT JOIN logistic_routing_scenarios lrs
				   ON lrs.id = ch.id_scenario
				   LEFT JOIN input_model_header imh
				   ON imh.id = lrs.id_input_model
				   LEFT JOIN ref_list_of_charges rc
				   ON rc.id = cd.id_charge
				   LEFT JOIN hp 
				   ON lrs.id=hp.id
				   WHERE SUBSTR(imh.fic_num,1,10) = UPPER(SUBSTR('$NUM_PO',1,10))
				   AND $find_fic";
				   $seqfees .= "AND hp.status = 'Z'" if ( $export_level eq '1');
				   $seqfees .= "AND hp.status = 'E'" if ( $export_level eq '3');
				   $seqfees .= "
				   AND rc.code = 'S_QC'
				  ) as qc_dpt_fees_rate,
				  ( SELECT DISTINCT ON (comment)
				  comment FROM cotation_detail cd
				  LEFT JOIN cotation_header ch
				  ON ch.id = cd.id_header
				  LEFT JOIN logistic_routing_scenarios lrs
				  ON lrs.id = ch.id_scenario
				  LEFT JOIN input_model_header imh
				  ON imh.id = lrs.id_input_model
				  LEFT JOIN ref_list_of_charges rc
				  ON rc.id = cd.id_charge
				  LEFT JOIN hp 
				  ON lrs.id=hp.id
				  WHERE SUBSTR(imh.fic_num,1,10) = UPPER(SUBSTR('$NUM_PO',1,10))
				  AND $find_fic";
				  $seqfees .= "AND hp.status = 'Z'" if ( $export_level eq '1');
				  $seqfees .= "AND hp.status = 'E'" if ( $export_level eq '3');
				  $seqfees .= "
				  AND rc.code = 'S_IR'
				) as import_risk_fees_rate,
				( SELECT DISTINCT ON (comment)
				  comment FROM cotation_detail cd
				  LEFT JOIN cotation_header ch
				  ON ch.id = cd.id_header
				  LEFT JOIN logistic_routing_scenarios lrs
				  ON lrs.id = ch.id_scenario
				  LEFT JOIN input_model_header imh
				  ON imh.id = lrs.id_input_model
				  LEFT JOIN ref_list_of_charges rc
				  ON rc.id = cd.id_charge
				  LEFT JOIN hp 
				  ON lrs.id=hp.id
				  WHERE SUBSTR(imh.fic_num,1,10) = UPPER(SUBSTR('$NUM_PO',1,10))
				  AND $find_fic
				  ";
				  $seqfees .= "AND hp.status = 'Z'" if ( $export_level eq '1');
				  $seqfees .= "AND hp.status = 'E'" if ( $export_level eq '3');
				  $seqfees .= "
				  AND rc.code = 'S_PF'
				) as purchase_fees_rate,
				( SELECT DISTINCT ON (comment)
				  comment FROM cotation_detail cd
				  LEFT JOIN cotation_header ch
				  ON ch.id = cd.id_header
				  LEFT JOIN logistic_routing_scenarios lrs
				  ON lrs.id = ch.id_scenario
				  LEFT JOIN input_model_header imh
				  ON imh.id = lrs.id_input_model
				  LEFT JOIN ref_list_of_charges rc
				  ON rc.id = cd.id_charge
				  LEFT JOIN hp ON lrs.id=hp.id
                  WHERE SUBSTR(imh.fic_num,1,10) = UPPER(SUBSTR('$NUM_PO',1,10))				  AND $find_fic";
				  $seqfees .= "AND hp.status = 'Z'" if ( $export_level eq '1');
				  $seqfees .= "AND hp.status = 'E'" if ( $export_level eq '3');
				  $seqfees .= "
				  AND rc.code = 'S_OF'
				  ) as other_fees_rate
				  ";
				 my $reqfees = $DBH_COT->prepare( $seqfees );
				 print " \n seqfees : ".$seqfees."\n" if ($DBUG);
				 $reqfees->execute();
		
				if ( $DBH_COT->errstr ne undef ) { # Erreur SQL
					print "\n $DBH_COT->errstr \n $seqfees  \n";
					print FICHIER_LOG "\nERROR : $seqfees";
					close FICHIER_LOG;
					$reqfees->finish;
					$reqsku->finish;
					$rql->finish;
					$DBH_COT->disconnect;
					exit;
				}
				my $fees = $reqfees->fetchrow_hashref;
				my $tx_import_fees = $fees->{'import_dpt_fees_rate'};
				my $tx_qc_fees = $fees->{'qc_dpt_fees_rate'} ;
				my $tx_import_risk_fees = $fees->{'import_risk_fees_rate'} ;
				my $tx_purchase_fees = $fees->{'purchase_fees_rate'} ;
				my $tx_other_fees = $fees->{'other_fees_rate'};
				$reqfees->finish;

				my $ctns = int($data_sku->{'pcs'} / $data_sku->{'pcs_per_ctn'} );
				my $cbm = ( $data_sku->{'packing_long'} * $data_sku->{'packing_large'} * $data_sku->{'packing_height'} / 1000000 );
				$cbm = 1 if (int($cbm) < 1);
				$writer->startTag("sku",
										"num" => $data_sku->{'sku'},
										"sku_size" => $data_sku->{'size'},
										"sku_color" => $data_sku->{'color'}
			    );
				$writer->startTag("designation");
				$writer->characters($data_sku->{'designation'});
				$writer->endTag("designation");
				$writer->startTag("pcs");
				$writer->characters($data_sku->{'pcs'});
				$writer->endTag("pcs");
				$writer->startTag("ctns");
				$writer->characters($ctns);
				$writer->endTag("ctns");
				$writer->startTag("cbm");
				$writer->characters($cbm);
				$writer->endTag("cbm");
				$writer->startTag("kgs");
				$writer->characters($data_sku->{'packing_weight'});
				$writer->endTag("kgs");
				$writer->startTag("prices");
				$writer->startTag("fob1");
				$writer->characters(($data_sku->{'prix_fob1'} / $exchange_rate_fob) / $exchange_rate_courant);
				$writer->endTag("fob1");
				$writer->startTag("fob2");
				$writer->characters( (($data_sku->{'prix_fob1'} + $data_sku->{'marge'} ) / $exchange_rate_fob) / $exchange_rate_courant);
				$writer->endTag("fob2");
              
				print "\n [$data_sku->{'id_scenario'}, $data_sku->{'id_sku'} ] \n ".$seqpricetothub."\n" if ($DBUG);
				$reqpricetothub->execute( $data_sku->{'id_scenario'}, $data_sku->{'id_sku'} );
		  
				if ( $DBH_COT->errstr ne undef ) { # Erreur SQL
					print "\n $DBH_COT->errstr \n $seqsku [$data_sku->{'id_scenario'}, $data_sku->{'id_sku'}]  \n";
					print FICHIER_LOG "\nERROR : $seqsku";
					close FICHIER_LOG;
					$reqsku->finish;
					$rql->finish;
					$DBH_COT->disconnect;
					exit;
				}
				
				my $unit_price_hub = 0;
				my $unit_price = 0;
				# ATTENTION, DON'T TOUCH
				
	            while ( my $data_hub_price = $reqpricetothub->fetchrow_hashref ) {
				
	                   if ( $data_hub_price->{'pcs'} > 0 ) {
							  my $tot_purchase_price = ( $data_hub_price->{'unit_purchase_price'} / $exchange_rate_courant) * $data_hub_price->{'pcs'} ;
							  my $total_transport_hub = ($data_hub_price->{'tot_prix_hub'} / $exchange_rate_courant) - $tot_purchase_price;
							  my $total_transport = ($data_hub_price->{'tot_prix'} / $exchange_rate_courant) - $tot_purchase_price;
					
					          $unit_price_hub = (($data_hub_price->{'unit_purchase_price'} / $exchange_rate_fob)) / $exchange_rate_courant + ($total_transport_hub / $data_hub_price->{'pcs'} / $exchange_rate);
					          $unit_price = ($data_hub_price->{'unit_purchase_price'} / $exchange_rate_fob) / $exchange_rate_courant + ($total_transport / $data_hub_price->{'pcs'} / $exchange_rate);

					           #$unit_price_hub = $data_hub_price->{'tot_prix_hub'} / $data_hub_price->{'pcs'}; 
					           #$unit_price = $data_hub_price->{'tot_prix'} / $data_hub_price->{'pcs'}; 
							   
							  $unit_price_hub = &get_hub_price($data->{'id_scenario'}, $DBUG, $DBH_COT);
		                }
	            }
	            $reqpricetothub->finish;
	      
				$writer->startTag("hub");
				if ( substr($department,0,4) eq 'Ind_' ) {
					$writer->characters($unit_price);
				}
				else {
					$writer->characters($unit_price_hub);
				}
			    $writer->endTag("hub");
			    $writer->startTag("tx_import_fees");
			    $writer->characters($tx_import_fees);
			    $writer->endTag("tx_import_fees");
			    $writer->startTag("tx_qc_fees");
			    $writer->characters($tx_qc_fees);
			    $writer->endTag("tx_qc_fees");
			    $writer->startTag("tx_import_risk_fees");
			    $writer->characters($tx_import_risk_fees);
			    $writer->endTag("tx_import_risk_fees");
			    $writer->startTag("tx_purchase_fees");
			    $writer->characters($tx_purchase_fees);
			    $writer->endTag("tx_purchase_fees");
			    $writer->startTag("tx_other_fees");
			    $writer->characters($tx_other_fees);
			    $writer->endTag("tx_other_fees");
			    $writer->endTag("prices");

				$writer->endTag("sku");
				$last_sku = '';
	        }
	    }
	    $reqsku->finish;
        
          # ON RECHERCHE LES COUTS
          # ======================
		print "\n [$data->{'id_shpt'} ] \n ".$seqcost."\n" if ($DBUG);
		
		$reqcost->execute( $data->{'id_scenario'}, $data->{'id_shpt'} );
		
		
	    if ( $DBH_COT->errstr ne undef ) { # Erreur SQL
		print "\n $DBH_COT->errstr \n $seqcost [$data->{'id_shpt'}]  \n";
	    print FICHIER_LOG "\nERROR : $seqcost";
	    close FICHIER_LOG;
	    $reqcost->finish;
	    $rql->finish;
	    $DBH_COT->disconnect;
	    exit;
	    }
		while ( my $data_cost = $reqcost->fetchrow_hashref ) {

			print "<br>".$data_cost->{'type'}."<br>" if ($DBUG);
			$writer->startTag("invoice",
			"cost_kind" => $data_cost->{'type'}
			);
			$writer->characters(($data_cost->{'cost'} / $exchange_rate) / $exchange_rate_courant);
			print "cost: ".(($data_cost->{'cost'} / $exchange_rate) / $exchange_rate_courant) if ($DBUG);
			print "exchange_rate_courant: $exchange_rate_courant " if ($DBUG);
			$writer->endTag("invoice");

		}
		$reqcost->finish;
        
		$writer->endTag("bl");
  }
        $writer->endTag("po");

  $writer->endTag("po_root");
  $writer->endTag("uo");
  $writer->endTag("dau");
  $writer->end();
  #  $output->close();
}



sub extract_arrived_pod_dau{

  my ( $num_uo ) = @_;
	# 3eme PARTIE l'ARRIVED POD pour DAU (PRELEVEMENTS PAYS)
	# ==================================
	
	my $src_file_arrived_pod_dau = "FCS_EXTRACT_ARRIVED_POD_DAU.XML";	
	my $xml_file = $SRC_DIRECTORY . $src_file_arrived_pod_dau;
	if (-e $xml_file){
	  unlink($xml_file) or print FICHIER_LOG 'erreur dans la suppression du fichier XML '.$src_file_arrived_pod_dau."\n";
	}
	my $sqlr;
	

	$sqlr = "
		--CD20170809 La requete suivante est désactivée car les données de prélèvemment pays doivent provvenir du split FCS pas de QF...
		SELECT
		'".$num_uo."' as uo,
		SUBSTR(LOWER(hp.fic_num), 1,10) as num_po_root,
		fic_num as num_po, 
		hpsh.ref_loading as bl,


		'road' as transport,
		hp.sku as sku,
		'' as size,
		'' as color,
		UPPER(TRIM(rhr.country)) as pays_reso,


		hpsh.delivery_rdv_date,
		hpsh.delivery_real_date,
		hpsh.quantity as qty

		FROM  hp

		LEFT JOIN hp_shpts as hpsh
		ON hpsh.id_hp=hp.id
		LEFT JOIN ref_hp_region as rhr
		ON rhr.name = hp.destination_code

		WHERE SUBSTR(LOWER(hp.fic_num), 1,10) = SPLIT_PART(LOWER('".$NUM_PO."'), '_', '1')
		AND hp.status='E'

		;
	";
	
	
	my $sqlr2 = "
		SELECT
        DISTINCT ON (uo, num_po_root, num_po, bl, sku, pays_reso, delivery_rdv_date, delivery_real_date, qty)
		ph.uo_number as uo,
		SPLIT_PART(LOWER(ph.num_po), '_', '1') as num_po_root,
		ph.num_po as num_po, 
        cl.ref as bl,
		'road' as transport,
		clps.sku_id as sku, 
        '' as size,
		'' as color,
		
        CASE 
        	WHEN UPPER( clpsdf.entrepot ) LIKE 'LOG%' THEN 'FRANCE'
            WHEN CHAR_LENGTH(rsg.groupe) IS NOT NULL THEN rsg.groupe
            ELSE clpsdf.entrepot
        END as pays_reso,
        clpsdf.entrepot,
        clpsdf.sous_pays, 
        rsg.cpc,
        rsg.groupe, 
        rsg.name, 
		clpsdf.estimed_date_arrived as delivery_rdv_date, 
		clpsdf.estimed_date_arrived as delivery_real_date, 
        
        clpsdf.quantites as qty, 
        
		cldf.ada_check_time,
        ''
		FROM po_header as ph
        LEFT JOIN cp_loading_po_sku as clps
        ON ph.num_po = clps.num_po
        LEFT JOIN cp_loading as cl
        ON clps.idloading = cl.idloading
        LEFT JOIN cp_loading_detail_fnd as cldf
        ON cldf.idloading = cl.idloading
        LEFT JOIN cp_loading_po_sku_detail_fnd as clpsdf
        ON clps.idloading_po_sku = clpsdf.idloading_po_sku
        LEFT JOIN ref_sous_groupe  as rsg
        ON clpsdf.entrepot = rsg.cpc
        
		WHERE SPLIT_PART(LOWER(ph.num_po), '_', '1') = SPLIT_PART(LOWER('".$NUM_PO."'), '_', '1')
		AND cldf.ada_check_time IS NOT NULL
        AND clpsdf.quantites > 0
        ORDER BY ph.num_po, cl.ref
	";		
	
 	
	

	print $sqlr if($DBUG);
	
	my $rs = $DBH_COT->prepare( $sqlr );
	$rs->execute();
	if ( $DBH_COT->errstr ne undef ) { # Erreur SQL
	  print FICHIER_LOG "\nERROR : ".$sqlr;
	  close FICHIER_LOG;
	  $rs->finish;
	  $DBH_COT->disconnect;
	   exit;
	}

	my $nb_po_root = $rs->rows;
	print FICHIER_LOG "\n==> $nb_po_root POs ARRIVED à traiter";

	  my $output = new IO::File (">".$xml_file);
	  print $output "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
	  my $writer = new XML::Writer(OUTPUT => $output);
	  $writer->startTag("dau");

	  my $key_uo = '';
	  my $NUM_PO_root = '';
	  my $key_po = '';
	  my $key_bl = '';
	  my $key_sku = '';

	while ( my $data = $rs->fetchrow_hashref) {
    if ( $key_uo ne $data->{'uo'} && $key_uo ne '' ) {
      $writer->endTag("sku");
      $writer->endTag("bl");
      $writer->endTag("po");
      $writer->endTag("po_root");
      $writer->endTag("uo");
    }
    if ( $key_uo ne $data->{'uo'} ) {
      $writer->startTag("uo",
                        "num" => $data->{'uo'}
      );
      $key_uo = $data->{'uo'};
      $NUM_PO_root = '';
      $key_po = '';
      $key_bl = '';
      $key_sku = '';
    }

    if ( $NUM_PO_root ne $data->{'num_po_root'} && $NUM_PO_root ne '' ) {
      $writer->endTag("sku");
      $writer->endTag("bl");
      $writer->endTag("po");
      $writer->endTag("po_root");
    }

    if ( $NUM_PO_root ne $data->{'num_po_root'} ) {
      $writer->startTag("po_root",
                        "num" => $data->{'num_po_root'}
      );
      $NUM_PO_root = $data->{'num_po_root'};
      $key_po = '';
      $key_bl = '';
      $key_sku = '';
    }

    if ( $key_po ne $data->{'num_po'} && $key_po ne '' ) {
        $writer->endTag("sku");
        $writer->endTag("bl");
        $writer->endTag("po");
    }
    if ( $key_po ne $data->{'num_po'} ) {
      $writer->startTag("po",
                        "num" => $data->{'num_po'}
      );
      $key_po = $data->{'num_po'};
      $key_bl = '';
      $key_sku = '';
    }

    if ( $key_bl ne $data->{'bl'} && $key_bl ne '' ) {
      $writer->endTag("sku");
      $writer->endTag("bl");
    }
    if ( $key_bl ne $data->{'bl'} ) {
      $writer->startTag("bl",
                        "num" => $data->{'bl'},
                        "transport_mode" => $data->{'transport'}
      );
      $writer->startTag("dates");
      $writer->startTag("eta");
      $writer->characters(&check_date($data->{'delivery_rdv_date'}));
      $writer->endTag("eta");
      $writer->startTag("etd");
      $writer->characters(&check_date($data->{'delivery_rdv_date'}));
      $writer->endTag("etd");
      $writer->startTag("arrival");
      $writer->characters(&check_date($data->{'delivery_real_date'}));
      $writer->endTag("arrival");
      $writer->endTag("dates");
      $key_bl = $data->{'bl'};
      $key_sku = '';

    }
                        

    if ( $key_sku ne ( $data->{'sku'} . $data->{'size'} .  $data->{'color'} ) && $key_sku ne '' ) {
      $writer->endTag("sku");
    }
    if ( $key_sku ne ( $data->{'sku'} . $data->{'size'} .  $data->{'color'} ) ) {
      $writer->startTag("sku",
			"num" => $data->{'sku'},
                        "sku_size" => $data->{'size'},
                        "sku_color" => $data->{'color'}
      );
      $key_sku = $data->{'sku'} . $data->{'size'} .  $data->{'color'};
    }
			
    if ( $data->{'pays_reso'} ne '' ) {
      $writer->startTag("floating",
                      "pays_reso" => $data->{'pays_reso'},
                      "date" => $data->{'date'}
      );
    }
    else {
      $writer->startTag("floating",
                      "pays_reso" => 'All',
                      "date" => ''
      );
    }
    $writer->characters($data->{'qty'});
    $writer->endTag("floating");
  }
  $writer->endTag("sku");
  $writer->endTag("bl");
  $writer->endTag("po");
  $writer->endTag("po_root");
  $writer->endTag("uo");
  $writer->endTag("dau");
  $writer->end();
  $output->close();

	$rs->finish;

	$DBH_COT->disconnect;
	  
 }
	  

sub get_po_detail_from_fcs {

	my $department = "";
	my $contact = "";
	my $importer = "";
	my $num_uo = "";
	my $incoterm = "" ;
	my $mode_transport = "";
	my $linerterm = "";
	my $sqlr = "    
		SELECT
		DISTINCT ON (uo_number)
		ph.uo_number,
		TRIM(ph.div_acheteur) as department,
		TRIM(ph.code_fournisseur) as importer,
		UPPER(SUBSTRING(TRIM(u.prenom) from 1 for 1)||'.'||TRIM(u.nom)) as contact,
		ph.incoterm,
		ph.mode_transport,
		CASE
			WHEN CHAR_LENGTH(TRIM(pt.mer_linerterm)) > 0 THEN CAST(pt.mer_linerterm as NUMERIC)
			ELSE 1
		END as linerterm
		FROM po_header as ph
		LEFT JOIN pre_order as po
		ON SPLIT_PART(UPPER(po.num_po), '_', '1') = SPLIT_PART(UPPER(ph.num_po), '_', '1')
		LEFT JOIN utilisateurs as u
		ON u.login = po.assigned_ada	
		LEFT JOIN po_transport as pt
		ON pt.num_po = ph.num_po			 			   
		WHERE LOWER(SPLIT_PART(ph.num_po, '_', 1))=SPLIT_PART(LOWER('".$NUM_PO."'), '_', 1)
		;
	";
my $rs = $DBH_FCS->prepare($sqlr);
$rs->execute;

	print "<pre>".$sqlr if ($DBUG);
	
if ( $DBH_FCS->errstr ne undef ) {    # ERREUR EXECUTION SQL
	print $DBH_FCS->errstr."<br>".$sqlr;
	$rs->finish;
	$DBH_FCS->disconnect;
	exit;
}


	while (my $data = $rs->fetchrow_hashref){	
	
	$department = $data->{'department'} ;
	$contact = $data->{'contact'} ;
	$importer = $data->{'importer'} ;
	$num_uo = $data->{'uo_number'} ;
	$incoterm = $data->{'incoterm'} ;
	$mode_transport = $data->{'mode_transport'} ;
	$linerterm = $data->{'linerterm'} ;
	}
	
$rs->finish;

return $department, $contact, $importer, $num_uo, $incoterm, $mode_transport, $linerterm ;
}

sub get_nb_shpt{

	my ($status) = @_;
	
	my $sqlr = "
		SELECT COUNT(*)
		FROM hp_shpts hps
		LEFT JOIN hp h
		ON h.id = hps.id_hp
		WHERE LOWER(h.fic_num) = LOWER('".$NUM_PO."')
		AND h.status = '".$status."'
	";

	print "\n".$sqlr."\n" if ($DBUG);
	
	my $rs = $DBH_COT->prepare($sqlr); 
	$rs->execute();
	
	if ( $DBH_COT->errstr ne undef ) {
		print $DBH_COT->errstr.":<br><pre>".$sqlr; 
		$rs->finish;
		exit;
    }
    my $data = $rs->fetchrow_hashref;
    my $nb_shpt = $data->{'count'};
	
	return $nb_shpt;
}

	
sub check_date {
	
	my ($date) = @_;
	my $date_return = "";
	 
	if ($date =~ /(\d\d\d\d)-(\d\d)-(\d\d)/) {  # reconnaît le format YYYY-MM-DD
		my $YYYY = $1;
		my $MM = $2;
		my $DD = $3;	  
		$date_return = $YYYY.$MM.$DD if( $YYYY ne '0001'); #Date Year Null format	  
	}	 
	 return $date_return; 	
}
	
sub get_list_quotation_z_to_integrate {

  my @list_quotation_z_to_integrate =();

  
	my $sqlr = " 
	SELECT
    ph.num_po
	FROM po_header as ph
    WHERE to_date(ph.date_validation,'YYYYMMDD') = CURRENT_DATE
	";
	print $sqlr."<br>" if ($DBUG);
	my $rs = $DBH_FCS->prepare($sqlr);
	$rs->execute;
	
	if ( $DBH_FCS->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print $DBH_FCS->errstr."<pre>".$sqlr;
		$rs->finish;
		$DBH_FCS->disconnect;
		exit;
	}
	
	while ( my $data = $rs->fetchrow_hashref ){
	push (@list_quotation_z_to_integrate, $data->{'num_po'}) if ($data->{'num_po'});
	}
	$rs->finish;
	
	@list_quotation_z_to_integrate = do { my %seen; grep { !$seen{$_}++ } @list_quotation_z_to_integrate };
	
	return @list_quotation_z_to_integrate;
}	

sub get_list_quotation_e_to_integrate {

  my @list_quotation_e_to_integrate =();
 
  my $sqlr = " 

  	SELECT
	clps.num_po
	FROM cp_loading as cl
	LEFT JOIN cp_loading_detail_fnd as cldf
	ON cl.idloading = cldf.idloading
    LEFT JOIN cp_loading_po_sku as clps
	ON cl.idloading = clps.idloading
    WHERE age(current_timestamp , cldf.ada_check_time ) < interval '1 day'
   
	";
	print $sqlr."<br>" if ($DBUG);
	my $rs = $DBH_FCS->prepare($sqlr);
	$rs->execute;
	
	if ( $DBH_FCS->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print $DBH_FCS->errstr."<pre>".$sqlr;
		$rs->finish;
		$DBH_FCS->disconnect;
		exit;
	}
	
	while ( my $data = $rs->fetchrow_hashref ){
	push (@list_quotation_e_to_integrate, $data->{'num_po'}) if ($data->{'num_po'});
	}
	$rs->finish;
	
	@list_quotation_e_to_integrate = do { my %seen; grep { !$seen{$_}++ } @list_quotation_e_to_integrate };
	
	return @list_quotation_e_to_integrate;
}		



sub get_hub_price {

	my ($sc_id, $dbug, $dbh) = @_;	
	my $dHubPrice;
	
	my $sqlr = "
	SELECT 

	TO_CHAR((total_transport_cost_price/q_pcs), '9 999 999 990D999')as unit_transport_price,
	TO_CHAR( ( unit_purchase_price+(total_transport_cost_price/q_pcs)), '9 999 999 990D999') as total_unit_price,
	TO_CHAR( ( ( unit_purchase_price*q_pcs)+total_transport_cost_price), '9999999990') as total_price,
	total_transport_cost_price
	 
	FROM (

		SELECT

		imh.fic_num as fic_num,	 
		ch.id_scenario as sc_id, 
		ch.id_scenario, 
		cf.q_pcs as q_pcs,
		cf.unit_purchase_price as unit_purchase_price,
		cost_charge.total_cost_transport as total_transport_cost_price
		
		FROM cotation_sku_fnd as cf
		LEFT JOIN ref_fnds as rf
		ON rf.id = cf.id_fnd
		LEFT JOIN ref_pays as rp
		ON rf.code_country = rp.code
		LEFT JOIN cotation_header as ch
		ON cf.id_header = ch.id
		LEFT JOIN logistic_routing_scenarios as lrs
		ON lrs.id=ch.id_scenario
		LEFT JOIN input_model_header as imh
		ON imh.id=lrs.id_input_model
		LEFT JOIN ref_currency as rc
		ON SUBSTR(imh.currency, 1,3) = LOWER(rc.code)
		LEFT JOIN hp 
		ON hp.id=ch.id_scenario
		LEFT JOIN(
		SELECT
		ch.id_scenario,
		SUM(cd.cost) AS total_cost_transport
		FROM cotation_header ch
		LEFT JOIN cotation_detail cd
		ON cd.id_header = ch.id
		LEFT JOIN ref_list_of_charges rc
		ON cd.id_charge = rc.id
		LEFT JOIN ref_transport_mode as rtm
		ON UPPER(rtm.designation ) = 'ROAD'
		
		WHERE 1=1
		GROUP BY ch.id_scenario 
		) as cost_charge
		ON cost_charge.id_scenario=ch.id_scenario  
	) as sqlr
	
	WHERE id_scenario = '$sc_id'
	;
	";

	print "<PRE>".$sqlr if ($dbug);
	my $rs = $dbh->prepare($sqlr);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr.":<br><pre>".$sqlr; 
		$rs->finish;
		exit;
	}

	while ( my $data = $rs->fetchrow_hashref ) {
		
		$dHubPrice = $data->{'total_unit_price'};
	}
	
  return $dHubPrice;
}


sub init(@ARGV){
	$log_msg .= "Debut du programme :".`date`."\n";
	if(scalar @ARGV eq 0) {
			&init_error();
	} else {
		for ( my $i ; $i < scalar @ARGV ; $i++ ){
			if (get_arg($ARGV[$i]) eq 'p' ){
				$NUM_PO = $ARGV[$i+1];
			}
			if (get_arg($ARGV[$i]) eq 'e' ){
				$export_level = $ARGV[$i+1];
			}
			
			if (get_arg($ARGV[$i]) eq 'help' ){
				&init_error();
			}
		}
	}
	if ($NUM_PO eq '') {
		&init_error(); 
	}
}

sub init_error(){
        $log_msg .= "
		usage : cp_integre_dau.pl -p num_po -e export_level 
		export_level in (1,3,5) ";
        &exit_function;
}

sub get_arg(){
        my ($str_to_return) = @_;
        if (index( $str_to_return, '-' ) > -1){
                $str_to_return = substr($str_to_return , 1, length($str_to_return));
        } else {
                $str_to_return = 0
        }
        return ($str_to_return);
}

sub exit_function(){
        $log_msg .= "\nFin du programme.\n";
			if ($DBUG) {
				print $log_msg;
			}
        exit;
}
	