#!/usr/bin/perl

use strict;

use MIME::Lite;
use CGI;
use DBI;
use Class::Date qw(:errors date localdate gmdate now);
use POSIX qw(strftime mktime);
use HTML::Template;

my $dbug = 0;

(
    my $dbh_qf= DBI->connect(
        esolGetEnv( __FILE__, 'BDD_QF_DSN' ),
        esolGetEnv( __FILE__, 'BDD_USER' ),
        esolGetEnv( __FILE__, 'BDD_PASSWORD' ),
        { AutoCommit => 1 }
    )
) or die print "Connexion base cotyr impossible.";
(
    my $dbh_fcs = DBI->connect(
        esolGetEnv( __FILE__, 'BDD_FCS_DSN' ),
        esolGetEnv( __FILE__, 'BDD_USER' ),
        esolGetEnv( __FILE__, 'BDD_PASSWORD' ),
        { AutoCommit => 1 }
    )
) or die print "Connexion base fcs impossible.";
(
    my $dbh_cbr = DBI->connect(
        esolGetEnv( __FILE__, 'BDD_CBR_DSN' ),
        esolGetEnv( __FILE__, 'BDD_USER' ),
        esolGetEnv( __FILE__, 'BDD_PASSWORD' ),
        { AutoCommit => 1 }
    )
) or die print "Connexion base CBR impossible.";

my $fic_num;
my @loop = ();
@loop = &get_loop_fic_num_waiting_to_be_in_cbr( $dbh_qf, $dbug );
foreach my $data (@loop) {

    $fic_num = $data->{'fic_num'};
    &send_packing_to_fcs( $dbh_qf, $dbh_fcs, $fic_num, $dbug );
    &send_price_to_fcs( $dbh_qf, $dbh_fcs, $fic_num, $dbug );
    &rename_fic_num( $dbh_qf, $fic_num, $dbug );
    &send_to_dau( substr( $fic_num, 0, 12 ), $dbug );
    &register_validated_to_dau( $dbh_cbr, substr( $fic_num, 0, 10 ), $dbug );
    &send_email_to_binome( $dbh_fcs, $fic_num, $dbug );
    &update_cintyr( $dbh_qf, $fic_num, $dbug );
}

$dbh_qf->disconnect;
$dbh_fcs->disconnect;
$dbh_cbr->disconnect;

sub send_email_to_binome {
    my ( $dbh, $which_fic_num, $dbug ) = @_;

    my $to_email;
    my $copy_email;
    my $from_email;

    $from_email = 'no-reply@fcsystem.com';
    $to_email .= &get_to_email( $dbug, $dbh, substr( $which_fic_num, 0, 10 ) );
    $copy_email = 'valerie.hervy@yrnet.com,';
    $copy_email .= 'system@e-solutions.tm.fr';

    if ( $dbug ) {
        $to_email = 'system@e-solutions.tm.fr';
    }

    my $subject     = "WEB4 Validated Cint'YR FIC# $fic_num";
    my $msg_content = "
	Hello
	<br>
	Cintyr calcul relaunch has just been validated for FIC# $fic_num .
	<br>	
	Best regards.
	";
    &send_mail( $from_email, $to_email, $copy_email, $subject, $msg_content,
        $dbug );

}

sub register_validated_to_dau {
    my ( $dbh, $which_fic_num, $dbug ) = @_;

    my $sqlr = "
	INSERT INTO cintyr_validated (po_root_num) VALUES ('$which_fic_num');
	";

    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . ":<br><pre>" . $sqlr;
        $rs->finish;
        exit;
    }
    $rs->finish;

}

sub send_price_to_fcs {
    my ( $dbh_qf, $dbh_fcs, $fic_num, $dbug ) = @_;
    my @loop = ();
    @loop = &get_prices_from_cot_yr( $dbh_qf, $fic_num );
    &update_prices_into_fcs( $dbh_fcs, $dbug, @loop );
}

sub get_loop_fic_num_waiting_to_be_in_cbr {
    my ( $dbh, $dbug ) = @_;

    my $sqlr = "
	SELECT
	fic_num
	FROM cintyr
	WHERE ts_validate IS NOT NULL
	AND ts_in_cbr IS NULL
	";

    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . ":<br><pre>" . $sqlr;
        $rs->finish;
        exit;
    }

    my @loop = ();
    while ( my $data = $rs->fetchrow_hashref ) {
        my %hash_value = ();
        $hash_value{'fic_num'} = $data->{'fic_num'};
        push( @loop, \%hash_value );
    }
    $rs->finish;

    return (@loop);

}

sub get_prices_from_cot_yr {
    my ( $dbh, $fic_num, $dbug ) = @_;

    my $sqlr = "
	SELECT
	SUBSTR(LOWER(fic_num), 1 ,10) as fic_num, 
	sku_num, 
	CAST(REPLACE(total_hub_unit_price, ',', '.') AS FLOAT) as total_hub_unit_price, 
	CAST(REPLACE(total_unit_price, ',', '.') AS FLOAT) as total_unit_price, 
	print_fnd
	FROM (

		SELECT 
		fic_num, 
		sku_num, 
		CASE
		WHEN '1'='1'  AND LOWER(currency) IN ('usd', 'eur') THEN TO_CHAR( ( (unit_purchase_price/usd_to_euro_fob_rate)+( (total_hub_transport_cost_price/q_pcs)/usd_to_euro_rate) ), '9 999 999 990D999')
		WHEN '1'='1'  AND LOWER(currency) NOT IN ('usd', 'eur')  THEN TO_CHAR( ( (unit_purchase_price/usd_to_euro_rate)+( (total_hub_transport_cost_price/q_pcs)/usd_to_euro_rate) ), '9 999 999 990D999')
		WHEN LOWER(currency) NOT LIKE 'eur%' THEN TO_CHAR( ( unit_purchase_price+(total_hub_transport_cost_price/q_pcs)), '9 999 999 990D999')
		ELSE TO_CHAR( ( (unit_purchase_price/usd_to_euro_rate)+( ( total_hub_transport_cost_price/q_pcs)/usd_to_euro_rate) ), '9 999 999 990D999')
		END as total_hub_unit_price,
		CASE
		WHEN '1'='1'  AND LOWER(currency) IN ('usd', 'eur') THEN TO_CHAR( ( (unit_purchase_price/usd_to_euro_fob_rate)+( (total_transport_cost_price/q_pcs)/usd_to_euro_rate) ), '9 999 999 990D999')
		WHEN '1'='1'  AND LOWER(currency) NOT IN ('usd', 'eur') THEN TO_CHAR( ( (unit_purchase_price/usd_to_euro_rate)+( (total_transport_cost_price/q_pcs)/usd_to_euro_rate) ), '9 999 999 990D999')
		WHEN LOWER(currency) NOT LIKE 'eur%' THEN TO_CHAR( ( unit_purchase_price+(total_transport_cost_price/q_pcs)), '9 999 999 990D999')
		ELSE TO_CHAR( ( (unit_purchase_price/usd_to_euro_rate)+( ( total_transport_cost_price/q_pcs)/usd_to_euro_rate) ), '9 999 999 990D999')
		END as total_unit_price,
		id_fnd,
		print_fnd, 

		''
		FROM (

			SELECT
			imh.id as imh_id,
			imh.fic_num as fic_num,	
			ims.id as ims_id, 
			ims.sku_num as sku_num, 
			ch.id_scenario as sc_id, 
			ch.id_scenario, 
			cf.id_sku as sku_id,
			ims.designation || ' (' || ims.sku_num || ') ' || ims.size || ' ' || ims.color as print_sku_designation,
			cf.q_pcs as q_pcs,
			CASE
			WHEN ims.other_marge > 0 THEN cf.unit_purchase_price + ims.other_marge
			ELSE cf.unit_purchase_price
			END as unit_purchase_price,
			CASE
			WHEN ims.other_marge > 0 THEN ( (cf.total_cost_price-(cf.q_pcs*(cf.unit_purchase_price+ims.other_marge) ) ) /cf.q_pcs)
			ELSE ( (cf.total_cost_price-(cf.q_pcs*(cf.unit_purchase_price) ) ) /cf.q_pcs)
			END as unit_transport_price,
			CASE
			WHEN ims.other_marge > 0 THEN ( (cf.total_cost_hub_price-(cf.q_pcs*(cf.unit_purchase_price+ims.other_marge) ) ) /cf.q_pcs)
			ELSE ( (cf.total_cost_hub_price-(cf.q_pcs*(cf.unit_purchase_price) ) ) /cf.q_pcs)
			END as unit_transport_hub_price,
			CASE
			WHEN ims.other_marge > 0 THEN  (cf.total_cost_hub_price-(cf.q_pcs*(cf.unit_purchase_price+ims.other_marge) ) ) 
			ELSE (cf.total_cost_hub_price-(cf.q_pcs*(cf.unit_purchase_price) ) ) 
			END as total_transport_hub_price,
			CASE
			WHEN ims.other_marge > 0 THEN (cf.total_cost_hub_price-(cf.q_pcs*(cf.unit_purchase_price+ims.other_marge) ) )
			ELSE (cf.total_cost_hub_price-(cf.q_pcs*(cf.unit_purchase_price) ) )
			END as total_hub_transport_cost_price,
			CASE
			WHEN ims.other_marge > 0 THEN (cf.total_cost_price-(cf.q_pcs*(cf.unit_purchase_price+ims.other_marge) ) )
			ELSE (cf.total_cost_price-(cf.q_pcs*(cf.unit_purchase_price) ) )
			END as total_transport_cost_price,
			cf.total_cost_price,
			cf.total_cost_hub_price as total_cost_hub_price,
			cf.id_fnd,
			rf.designation as print_fnd, 
			SUBSTR(imh.currency, 1,3) as currency, 
			CAST( ch.usd_to_euro_rate AS REAL) as usd_to_euro_rate,
			--		CAST( (SELECT usd_to_euro_rate FROM ref_global_parameters) AS REAL) as usd_to_euro_rate,
			/*
			CASE
			WHEN CAST('0'||ch.usd_to_euro_fob_rate AS FLOAT) <> 0 THEN CAST( ch.usd_to_euro_fob_rate AS REAL)
			ELSE CAST( ch.usd_to_euro_rate AS REAL)
			END as usd_to_euro_fob_rate,
			*/
			CASE
			WHEN LOWER(SUBSTR(imh.currency, 1,3)) IN ('usd', 'eur') THEN CAST(ch.usd_to_euro_fob_rate as FLOAT)
			ELSE CAST((CAST(usd_to_euro_rate as FLOAT)/CAST(ch.usd_to_euro_fob_rate as FLOAT)*100) AS INTEGER)/100.0
			END as usd_to_euro_fob_rate, 
			CASE 
			WHEN SUBSTR(imh.currency, 1,3) IS NULL OR SUBSTR(imh.currency, 1,3) = '' THEN '$(USD)'
			WHEN LOWER(SUBSTR(imh.currency, 1,3)) IN ('eur') THEN ''
			ELSE rc.symbole||'('||rc.code||')'
			END as print_fob_currency, 

			''
			FROM cotation_sku_fnd as cf
			LEFT JOIN input_model_skus as ims
			ON cf.id_sku = ims.id
			LEFT JOIN input_model_header as imh
			ON ims.id_header = imh.id
			LEFT JOIN ref_currency as rc
			ON SUBSTR(imh.currency, 1,3) = LOWER(rc.code)
			LEFT JOIN ref_fnds as rf
			ON rf.id = cf.id_fnd
			LEFT JOIN cotation_header as ch
			ON cf.id_header = ch.id

		) as main

	) as main
	WHERE fic_num  = '$fic_num'
	GROUP BY 
	SUBSTR(LOWER(fic_num), 1 ,10), 
	sku_num, 
	total_hub_unit_price, 
	total_unit_price, 
	print_fnd
	ORDER BY fic_num, sku_num, print_fnd
	;

	";

    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . ":<br><pre>" . $sqlr;
        $rs->finish;
        exit;
    }

    my @loop = ();
    my $num_po;
    while ( my $data = $rs->fetchrow_hashref ) {
        my %hash_value = ();
        $hash_value{'fic_num'}              = $data->{'fic_num'};
        $hash_value{'sku_num'}              = $data->{'sku_num'};
        $hash_value{'total_hub_unit_price'} = $data->{'total_hub_unit_price'};
        $hash_value{'total_unit_price'}     = $data->{'total_unit_price'};
        $hash_value{'print_fnd'}            = $data->{'print_fnd'};
        push( @loop, \%hash_value );
    }
    $rs->finish;

    return (@loop);

}

sub update_prices_into_fcs {
    my ( $dbh, $dbug, @loop ) = @_;

    my ( $fic_num, $sku_num, $total_hub_unit_price, $total_unit_price,
        $print_fnd );
    foreach my $data (@loop) {

        $fic_num              = $data->{'fic_num'};
        $sku_num              = $data->{'sku_num'};
        $total_hub_unit_price = $data->{'total_hub_unit_price'};
        $total_unit_price     = $data->{'total_unit_price'};
        $print_fnd            = $data->{'print_fnd'};

        my $sqlr = "
		UPDATE po_detail_fnd
		SET prix_vente = (
			SELECT 
			DISTINCT ON ( SPLIT_PART(num_po, '_', 1))
			CASE 
				WHEN LOWER(div_acheteur) LIKE 'ind%' THEN CAST('$total_unit_price' as FLOAT)
				ELSE CAST('$total_hub_unit_price' as FLOAT)
			END
			FROM po_header 
			WHERE SPLIT_PART(num_po, '_', 1) = '$fic_num'
		)
		WHERE SPLIT_PART(num_po, '_', 1) = '$fic_num'
		AND sku_id = '$sku_num'
		AND entrepot = (SELECT code_entrepot FROM ref_entrepot WHERE is_split IS TRUE AND cot_fnd = '$print_fnd'||(
		SELECT DISTINCT ON (SPLIT_PART(num_po, '_', 1)) 
			CASE 
			WHEN div_acheteur LIKE '%VAD' THEN 'VAD'
			WHEN div_acheteur LIKE '%VPM' THEN 'VPM'
			ELSE ''
		END as reseau
		FROM po_header 
		WHERE SPLIT_PART(num_po, '_', 1) = '$fic_num'
		)
		)
		;
		";

        print "<pre>" . $sqlr if ($dbug);
        my $rs = $dbh->prepare($sqlr);
        $rs->execute();
        if ( $dbh->errstr ne undef ) {
            print $dbh->errstr . ":<br><pre>" . $sqlr;
            $rs->finish;
            exit;
        }

    }
}

sub send_packing_to_fcs {
    my ( $dbh_qf, $dbh_fcs, $fic_num, $dbug ) = @_;
    my @loop = ();
    @loop = &get_packing_from_cot_yr( $dbh_qf, $fic_num );
    &update_packing_into_fcs_necessary( $dbh_fcs, $dbug, @loop );
}

sub update_packing_into_fcs_necessary {
    my ( $dbh, $dbug, @loop ) = @_;

    my ( $shpt_num, $sku_num, $packing_long, $packing_large, $packing_height,
        $packing_pcs_per_ctn, $packing_weight, $q_ctn );
    foreach my $data (@loop) {

        $shpt_num            = $data->{'shpt_num'};
        $sku_num             = $data->{'sku_num'};
        $packing_long        = $data->{'packing_long'};
        $packing_large       = $data->{'packing_large'};
        $packing_height      = $data->{'packing_height'};
        $packing_pcs_per_ctn = $data->{'packing_pcs_per_ctn'};
        $packing_weight      = $data->{'packing_weight'};
        $q_ctn               = $data->{'q_ctn'};

        if ( $packing_long ne 1 && $packing_large ne 1 && $packing_height ne 1 )
        {

            my $sqlr_update_carton_details = "
			
			UPDATE carton_details
			SET etd_eta = 'ORIGIN'
			WHERE etd_eta = ''
			AND num_po NOT IN (
				SELECT
				num_po
				FROM carton_details
				WHERE etd_eta = 'ORIGIN'
				AND num_po = '$shpt_num' 			
				AND sku_id = '$sku_num'
			)
			AND num_po = '$shpt_num' 			
			AND sku_id = '$sku_num'
			;";

            print "<pre>" . $sqlr_update_carton_details if ($dbug);

            my $rs_update_carton_details =
              $dbh->prepare($sqlr_update_carton_details);
            $rs_update_carton_details->execute();
            if ( $dbh->errstr ne undef ) {
                print $dbh->errstr . ":<br><pre>" . $sqlr_update_carton_details;
                $rs_update_carton_details->finish;
                exit;
            }

            my $sqlr_carton_details = "
			DELETE FROM carton_details 
			WHERE etd_eta = ''
			AND num_po = '$shpt_num' 			
			AND sku_id = '$sku_num'
			;
            ";

            print "<pre>" . $sqlr_carton_details if ($dbug);

            my $rs_carton_details = $dbh->prepare($sqlr_carton_details);
            $rs_carton_details->execute();
            if ( $dbh->errstr ne undef ) {
                print $dbh->errstr . ":<br><pre>" . $sqlr_carton_details;
                $rs_carton_details->finish;
                exit;
            }

            my $sqlr_insert_carton_details = "
			INSERT INTO carton_details
			( num_po, lt, sku_id, etd_eta, q_cartons_forcasted, pcs_per_master, carton_size, carton_weight )
			SELECT 
			'$shpt_num', 
			'1',
			'$sku_num', 
			'', 
			'$q_ctn', 
			'$packing_pcs_per_ctn', 
			'$packing_long'||'X'||'$packing_large'||'X'||'$packing_height', 
			'$packing_weight'
			;
			";

            print "<pre>" . $sqlr_insert_carton_details if ($dbug);

            my $rs_insert_carton_details =
              $dbh->prepare($sqlr_insert_carton_details);
            $rs_insert_carton_details->execute();
            if ( $dbh->errstr ne undef ) {
                print $dbh->errstr . ":<br><pre>" . $sqlr_insert_carton_details;
                $rs_insert_carton_details->finish;
                exit;
            }

        }

    }

}

sub get_packing_from_cot_yr {

    my ( $dbh, $fic_num ) = @_;

    my $sqlr = "

SELECT 
    LOWER(SPLIT_PART(lrsh.shpt_num, '__', 1)) as shpt_num,
	ims.sku_num, 
	ims.id as sku_id, 
	ims.packing_height as packing_height, 
	ims.packing_large as packing_large, 
	ims.packing_long as packing_long, 
	ims.packing_pcs_per_ctn as packing_pcs_per_ctn, 
	ims.packing_weight as packing_weight,
	SUM(lrd.q_pcs) as q_pcs, 
    SUM(lrd.q_pcs)/ims.packing_pcs_per_ctn as q_ctn
    FROM logistic_routings_detail as lrd
    LEFT JOIN logistic_routings_shpts as lrsh
    ON lrd.id_shpt = lrsh.id
    LEFT JOIN input_model_sku_fnds as imsf
    ON imsf.id = lrd.id_input_model_sku_fnd
    LEFT JOIN input_model_skus as ims
    ON ims.id = imsf.id_sku

    LEFT JOIN input_model_header as imh
    ON ims.id_header = imh.id
	WHERE imh.fic_num  = '$fic_num'
	AND imsf.id IS NOT NULL
    GROUP BY 
    imh.fic_num,
	ims.sku_num, 
	ims.id, 
	ims.packing_height, 
    ims.packing_large, 
    ims.packing_long, 
    ims.packing_pcs_per_ctn, 
    ims.packing_weight, 
    lrsh.shpt_num
	ORDER BY  lrsh.shpt_num, ims.sku_num
	;
	";

    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . ":<br><pre>" . $sqlr;
        $rs->finish;
        exit;
    }

    my @loop = ();
    my $num_po;
    my ( $packing_long, $packing_large, $packing_height );
    while ( my $data = $rs->fetchrow_hashref ) {
        my %hash_value = ();
        $hash_value{'shpt_num'}            = $data->{'shpt_num'};
        $hash_value{'sku_num'}             = $data->{'sku_num'};
        $hash_value{'packing_long'}        = $data->{'packing_long'};
        $hash_value{'packing_large'}       = $data->{'packing_large'};
        $hash_value{'packing_height'}      = $data->{'packing_height'};
        $hash_value{'packing_pcs_per_ctn'} = $data->{'packing_pcs_per_ctn'};
        $hash_value{'packing_weight'}      = $data->{'packing_weight'};
        $hash_value{'q_ctn'}               = $data->{'q_ctn'};
        push( @loop, \%hash_value );
    }
    $rs->finish;

    return (@loop);
}

sub send_to_dau {
    my ( $fic_num, $dbug ) = @_;
    my $str_to_execute;
    $str_to_execute .= "
	"
      . esolGetEnv( __FILE__, 'PATH_CBR_ROOT_DIR' )
      . "/bin/dau/03_qf_extract_dau.pl -p $fic_num -e 2 
	"
      . esolGetEnv( __FILE__, 'PATH_CBR_ROOT_DIR' )
      . "/bin/dau/04_import_charges.pl -x QF_EXTRACT_CHECKING.XML
	";
    print $str_to_execute if ($dbug);
    print $str_to_execute;
    system($str_to_execute) == 0 or die print "system $str_to_execute failed?";
}

sub rename_fic_num {
    my ( $dbh, $fic_num, $dbug ) = @_;
    my $sqlr_sauvegarde = "	
	-- ce qui suit permet de sauvegarder les différents calcul d'engagés.
	UPDATE input_model_header
	SET fic_num = SUBSTR('$fic_num', 1, 12 )||'_bck'||TO_CHAR(NOW(), 'YYYYMMDD-HH24MI') 
	WHERE fic_num = SUBSTR('$fic_num', 1, 12 )
	;";

    print $sqlr_sauvegarde if ($dbug);

    my $rs_sauvegarde = $dbh->prepare($sqlr_sauvegarde);
    $rs_sauvegarde->execute();
    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . ":<br><pre>" . $sqlr_sauvegarde;
        $rs_sauvegarde->finish;
        exit;
    }
    $rs_sauvegarde->finish;

    my $sqlr = "
	-- ce qui suit permet de renommer la ~1B en ~1
	UPDATE input_model_header
	SET fic_num = SUBSTR('$fic_num', 1, 12 ) 
	WHERE fic_num = '$fic_num'
	;
	";

    print $sqlr if ($dbug);

    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . ":<br><pre>" . $sqlr;
        $rs->finish;
        exit;
    }
    $rs->finish;
}

sub update_cintyr {
    my ( $dbh, $fic_num, $dbug ) = @_;

    my $sqlr = "
	
	UPDATE cintyr
	SET ts_in_cbr = NOW()
	WHERE fic_num = '$fic_num'
	;
	";

    print $sqlr if ($dbug);

    my $rs = $dbh->prepare($sqlr);
    $rs->execute();
    if ( $dbh->errstr ne undef ) {
        print $dbh->errstr . ":<br><pre>" . $sqlr;
        $rs->finish;
        exit;
    }
    $rs->finish;

}

sub get_to_email {

    my ( $dbug, $dbh, $fic_num ) = @_;
    my $sqlr = "

			   SELECT
			   CASE
	           WHEN aph.contact_ada IS NULL OR LENGTH(aph.contact_ada)=0 THEN km.sender
	           ELSE aph.contact_ada
               END as contact_ada,
			   poau.login,
			   groupe.email_menbers as email_to
			   
			   FROM ada_po_header as aph
               
			   LEFT JOIN kdo_main as km
			   ON TRIM(aph.num_po) = km.num_po_root
               
			   LEFT JOIN kdo_importers as ki
			   ON km.id_kdo_importer = ki.id
               
			   LEFT JOIN pre_order_ada_user as poau
			   ON LOWER(substring(km.sender from 1 FOR 1)|| split_part(km.sender,' ',2))=poau.login
			 
			   LEFT JOIN (
					SELECT  
					id_group,login 
					FROM pre_order_ada_user 
				    ) as id_groupe
				ON poau.login = id_groupe.login
										
				LEFT JOIN (
						SELECT  
						id_group, 
						array_agg(pror_user.login) as menbers,
						array_agg(pror_user.phone_num) as phone_num,
						array_agg(TRIM(ut.email)) as email_menbers
						FROM pre_order_ada_user as pror_user
						LEFT JOIN utilisateurs ut
						ON pror_user.login = ut.login
						GROUP BY id_group
					) as groupe
					 ON groupe.id_group=id_groupe.id_group
						   
               WHERE LOWER(aph.num_po)=LOWER('$fic_num') 				
			  ";

    my $rs = $dbh->prepare($sqlr);
    $rs->execute;

    if ($dbug) {
        print "<pre>" . $sqlr;
    }

    if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print $dbh->errstr . "<br>" . $sqlr;
        $rs->finish;
        $dbh->disconnect;
        exit;
    }

    my $recipient;

    while ( my $data = $rs->fetchrow_hashref ) {

        $recipient = join( ",", @{ $data->{'email_to'} } );
    }

    $rs->finish;

    return $recipient;
}

sub send_mail {
    my ( $from, $to, $copy, $subject, $msg, $dbug ) = @_;
    my $log_msg;

    $from = 'no-reply@fcsystem.com';

    my $mime_msg = MIME::Lite->new(
        From    => 'no-reply@fcsystem.com',
        To      => 'system@e-solutions.tm.fr',
        Cc      => 'system@e-solutions.tm.fr',
        Subject => $subject,
        Type    => 'text/html',
        Data    => $msg
    ) or $log_msg .= "Erreur lors de la création de MIME body: $!\n";

    $log_msg .= "envoi du message : \n";
    $log_msg .= " à $to et $copy\n";
    $log_msg .= "Sujet : " . $subject . "\n";

    if ( $mime_msg->send_by_smtp( esolGetEnv( __FILE__, 'SMTP_SERVER' ) ) ) {
        $log_msg .= "OK";
    }
    else {
        $log_msg .= "KO";
    }
    $log_msg .= "\n";
    print $log_msg if ($dbug);
}
