#!/usr/bin/perl

use strict;

use DBI;
use POSIX qw(strftime);
use Class::Date qw(:errors now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;

use XML::Writer;
use IO::File;


# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $dbug = 0;

my $dau_bin_directory = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau/";

my $log_msg;
my $NUM_PO;

my $status_mouvemente_time_limit = '6 month'; #Time beyond which the status is changed from 'mouvementé' to 'soldé'

&init();
&update_statut_solde_cp($dbug);



###########################
# Fonctions et procédures #
###########################

sub update_statut_solde_cp{

	my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	my $dbh_fcs = DBI->connect( esolGetEnv(__FILE__, 'BDD_FCS_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	
	my $sqlr = " --get_list_cp_to_update_statut_mouvementé_to_soldé
	
					SELECT
					cp.uo_number,
					array_agg(cp.status),
					bool_and(cp.status = 5 OR cp.status = 6 ) as is_status_ok, 
					bool_or(cp.status = 4 AND (to_date(substr(cp.max_delivery_real_date_time,1,8), 'YYYYMMDD') < current_date- interval '6 month')) as is_time_limit_reached

					FROM (
								SELECT
								
									ph.num_po,
									ph.uo_number,
									CASE
										WHEN 1=0 THEN 1
										WHEN  sum_detail.ctn_nb IS NULL THEN 2
										WHEN (pd.total_units-(sum_detail.ctn_nb*cd.pcs_per_ctn)) < 0 AND TO_TIMESTAMP(sum_detail.max_loading_estimated_date_time, 'YYYYMMDDHH24MI')> NOW() THEN 3
										WHEN  (pd.total_units-(sum_detail.ctn_nb*cd.pcs_per_ctn)) < 0 AND TO_TIMESTAMP(sum_detail.max_loading_estimated_date_time, 'YYYYMMDDHH24MI')< NOW()  THEN 5
										WHEN (pd.total_units-(sum_detail.ctn_nb*cd.pcs_per_ctn)) <= 0 AND sum_detail.max_delivery_real_date_time IS NOT NULL  THEN 6
										WHEN pcs_received IS NOT NULL AND pcs_received > 0 THEN 4
										WHEN (pd.total_units-(sum_detail.ctn_nb*cd.pcs_per_ctn)) >= 0 THEN 3
										ELSE 2
									END as status,
									sum_detail.max_delivery_real_date_time

									FROM po_header as ph
									
									LEFT JOIN ref_transport as rt
									ON ph.mode_transport = rt.code

									LEFT JOIN po_transport as pt
									ON ph.num_po = pt.num_po
									
									LEFT JOIN  po_detail as pd
									ON pd.num_po = ph.num_po
									
									LEFT JOIN (
										SELECT 
										DISTINCT ON (cd.num_po, cd.sku_id)
										cd.id_carton_detail,
										cd.etd_eta, 
										cd.num_po, 
										cd.sku_id, 
										cd.pcs_per_master as pcs_per_ctn
										FROM carton_details as cd
										LEFT JOIN (
											SELECT
											MAX(cd.id_carton_detail) as id_carton_detail,
											cd.num_po, 
											cd.sku_id
											FROM 
											carton_details  as cd
											GROUP BY 
											cd.num_po, 
											cd.sku_id
										) as max_cd
										 ON cd.num_po = max_cd.num_po
										 AND cd.sku_id = max_cd.sku_id
										 AND cd.id_carton_detail = max_cd.id_carton_detail 
										LEFT JOIN po_header  as ph
										ON cd.num_po = ph.num_po
										LEFT JOIN ref_transport as rt
										ON ph.mode_transport = rt.code
										 
										WHERE 1=1 
										AND max_cd.num_po IS NOT NULL
										AND LOWER(rt.nom) = 'road'

									) as cd
									ON pd.num_po = cd.num_po
									AND pd.set_article_fournisseur = cd.sku_id
									LEFT JOIN outer_detail as od
									ON cd.id_carton_detail = od.id_master
									LEFT JOIN (
										SELECT
										clps.num_po,
										clps.sku_id, 
										SUM(clpsd.pallet_nb) as pallet_nb, 
										SUM(clpsd.pallet_nb*clpsd.pallet_ctn_per) as ctn_nb, 
										MIN(cl.loading_estimated_date||cl.loading_estimated_time) as min_loading_estimated_date_time,
										MAX(cl.loading_estimated_date||cl.loading_estimated_time) as max_loading_estimated_date_time, 
										MIN(cl.delivery_real_date||cl.delivery_real_time) as min_delivery_real_date_time,
										MAX(cl.delivery_real_date||cl.delivery_real_time) as max_delivery_real_date_time,
										MIN(clps.pcs_received) as pcs_received,
										
										''

										FROM cp_loading_po_sku as clps
										LEFT JOIN cp_loading_po_sku_detail as clpsd
										ON clps.idloading_po_sku = clpsd.idloading_po_sku
										LEFT JOIN cp_loading as cl
										ON cl.idloading = clps.idloading
										WHERE 1=1
										AND cl.is_enabled = TRUE
										GROUP BY clps.num_po, clps.sku_id
									) as sum_detail

									ON pd.num_po = sum_detail.num_po
									AND pd.set_article_fournisseur = sum_detail.sku_id
									LEFT JOIN pre_order as po
									ON SPLIT_PART(LOWER(ph.num_po), '_', 1) = LOWER(po.num_po)
									WHERE 1=1
									AND LOWER(rt.nom) = 'road'
									AND po.is_off_panel IS TRUE
									AND CHAR_LENGTH(cd.num_po) > 0
					  ) as cp
					  LEFT JOIN cp_status as cps
					  ON cps.id =cp.status
					  WHERE cp.status = 5
					  OR cp.status = 6
					  OR ( cp.status = 4 AND (to_date(substr(cp.max_delivery_real_date_time,1,8), 'YYYYMMDD') < current_date- interval '$status_mouvemente_time_limit'))
					  
					  GROUP BY cp.uo_number
					 
	";

	print "\n $sqlr \n";
	
	my $rs = $dbh_fcs->prepare( $sqlr );
	$rs->execute;
	if ( $dbh_fcs->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n ".$dbh_fcs->errstr." \n $sqlr \n";
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
	my $list_uo_full_arrived;
	
	while ( my $data = $rs->fetchrow_hashref ) {
			$list_uo_full_arrived .= "'$data->{'uo_number'}', " if ( $data->{'is_status_ok'}  || $data->{'is_time_limit_reached'}) ;
	}
	$list_uo_full_arrived = substr($list_uo_full_arrived, 0, length($list_uo_full_arrived)-2 );
	
	my $sqlr = "
		UPDATE dau_uo
		SET id_status  = '30',
		date_resulted = CAST(TO_CHAR(NOW(), 'YYYYMMDD') as INTEGER)
		WHERE id_status = '20'
		AND uo_num IN ($list_uo_full_arrived);
	";
	my $rs = $dbh->prepare( $sqlr );
	
	$rs->execute;
	if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n ".$dbh->errstr." \n $sqlr \n";
		$rs->finish;
		$dbh->disconnect;
		exit;
	}

	print $sqlr."\n" if ($dbug);

	$rs->finish;
	$dbh->disconnect;
	$dbh_fcs->disconnect;
	
	return 0;	
	
}

######################################
#   SCRIPTS DE PASSAGE DE PARAMETRES #
######################################

sub init(@ARGV){

	$log_msg .= "Debut du programme :".`date`."\n";

	for ( my $i ; $i < scalar @ARGV ; $i++ ){
	
			if (get_arg($ARGV[$i]) eq 'p' ){
				$NUM_PO = $ARGV[$i+1];
			}
		
			if (get_arg($ARGV[$i]) eq 'help' ){
				&init_error();
			}
			
	}
	
	if ($NUM_PO){

		$log_msg .=  "\n cp_dau_status_update.pl -p ".$NUM_PO." ".now." \n" ;
	}
	else{
	
		$log_msg .= "\n cp_dau_status_update.pl ".now." \n" ;
	}
	
}

sub init_error(){
	$log_msg .= "
			usage : cp_dau_status_update.pl -p num_po
	";
		&exit_function;
}

sub get_arg(){
		my ($str_to_return) = @_;
		if (index( $str_to_return, '-' ) > -1){
		  $str_to_return = substr($str_to_return , 1, length($str_to_return));
		} else {
		  $str_to_return = 0
		}
		return ($str_to_return);
}

sub exit_function(){
	my $fichier_log = $dau_bin_directory."/cp_dau_status_update.log";
	open LOGFILE, ">> $fichier_log" or die "Can't open $fichier_log";
		$log_msg .= "\nFin du programme.\n";
	if ($dbug) {
	  print $log_msg;
	} else {
	  print LOGFILE $log_msg;
	}
	close LOGFILE;
		exit;
}

