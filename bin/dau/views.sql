DROP VIEW "public"."dau_bl_full" CASCADE;
DROP VIEW public.tdb_lb_cost;
DROP VIEW public.tdb_lb_container;

CREATE VIEW "public"."dau_bl_full" (
    id_uo,
    uo_num,
    uo_id_status,
    uo_comment,
    uo_comment_extra_cost,
    uo_comment_ca,
    print_uo_status,
	date_open, 
	print_date_open, 
	date_resulted, 
    print_date_resulted, 
    date_confirmed, 
    print_date_confirmed, 
    date_close, 
    print_date_close, 
    id_po_root,
    po_root_num,
    contact_ada,
    importer,
    dpt,
    dpr_id_status,
    print_po_root_status,
    po_root_comment,
    id_po,
    po_num,
    pod,
    print_pod,
    id_export_level,
    print_export_level,
    id_transport_mode,
    print_transport_mode,
    po_comment,
    id_bl,
    bl_num,
    id_linerterm,
    print_linerterm,
    etd,
    print_etd,
    eta,
    print_eta,
    date_arrival,
    print_date_arrival,
    print_order,
    is_not_waiting_invoice)
AS
SELECT
du.id as id_uo,
du.uo_num, 
du.id_status as uo_id_status, 
du.comment as uo_comment,
du.comment_extra_cost as uo_comment_extra_cost,
du.comment_ca as uo_comment_ca,
du_s.designation as print_uo_status, 
du.date_open, 
CASE
    WHEN du.date_open =0 THEN ''
    ELSE TO_CHAR(TO_DATE(du.date_open, 'YYYYMMDD'), 'DD/MM/YYYY') 
END as print_date_open, 
du.date_resulted, 
CASE
    WHEN du.date_resulted =0 THEN ''
    ELSE TO_CHAR(TO_DATE(du.date_resulted, 'YYYYMMDD'), 'DD/MM/YYYY') 
END as print_date_resulted, 
du.date_confirmed, 
CASE
    WHEN du.date_confirmed =0 THEN ''
    ELSE TO_CHAR(TO_DATE(du.date_confirmed, 'YYYYMMDD'), 'DD/MM/YYYY') 
END as print_date_confirmed, 
du.date_close, 
CASE
    WHEN du.date_close =0 THEN ''
    ELSE TO_CHAR(TO_DATE(du.date_close, 'YYYYMMDD'), 'DD/MM/YYYY') 
END as print_date_close, 
dpr.id as id_po_root,
dpr.po_root_num, 
dpr.contact_ada, 
dpr.importer, 
dpr.dpt, 
dpr.id_status as dpr_id_status, 
dpr_s.designation as print_po_root_status, 
dpr.comment as po_root_comment,
dp.id as id_po,
dp.po_num, 
dp.pod, 
pod.name||' ('|| dp.pod|| ' )' as print_pod, 
dp.id_export_level, 
export_level.designation as print_export_level,
dp.id_transport_mode, 
transport_mode.designation as print_transport_mode,
dp.comment as po_comment,
db.id as id_bl,
db.bl_num,
db.id_linerterm,
rl.designation as print_linerterm,
db.etd, 
CASE
    WHEN db.etd =0 THEN ''
    ELSE TO_CHAR(TO_DATE(db.etd, 'YYYYMMDD'), 'DD/MM/YYYY') 
END as print_etd, 
db.eta, 
CASE
    WHEN db.eta =0 THEN ''
    ELSE TO_CHAR(TO_DATE(db.eta, 'YYYYMMDD'), 'DD/MM/YYYY') 
END as print_eta, 
db.date_arrival, 
CASE
    WHEN db.date_arrival =0 THEN ''
    ELSE TO_CHAR(TO_DATE(db.date_arrival, 'YYYYMMDD'), 'DD/MM/YYYY') 
END as print_date_arrival, 
db.print_order, 
db.is_not_waiting_invoice

FROM dau_uo as du
LEFT JOIN ref_dau_status as du_s
ON du.id_status = du_s.id
LEFT JOIN dau_po_root as dpr
ON du.id = dpr.id_uo
LEFT JOIN ref_dau_status as dpr_s
ON dpr.id_status = dpr_s.id
LEFT JOIN dau_po as dp
ON dpr.id = dp.id_po_root
LEFT JOIN ref_dau_export_level as export_level
ON dp.id_export_level = export_level.id
LEFT JOIN ref_dau_transport_mode as transport_mode
ON dp.id_transport_mode = transport_mode.id
LEFT JOIN ports as pod
ON dp.pod = pod.label
LEFT JOIN dau_bl as db
ON dp.id = db.id_po
LEFT JOIN ref_linerterms as rl
ON db.id_linerterm = rl.id;


CREATE OR REPLACE VIEW "public"."dau_sku_full" (
    id_uo,
    uo_num,
    uo_id_status,
    uo_comment,
    uo_comment_extra_cost,
    uo_comment_ca,
    print_uo_status,
    date_open,
    print_date_open,
    date_resulted,
    print_date_resulted,
    date_confirmed,
    print_date_confirmed,
    date_close,
    print_date_close,
    id_po_root,
    po_root_num,
    contact_ada,
    importer,
    dpt,
    dpr_id_status,
    print_po_root_status,
    po_root_comment,
    id_po,
    po_num,
    pod,
    print_pod,
    id_export_level,
    print_export_level,
    id_transport_mode,
    print_transport_mode,
    po_comment,
    id_bl,
    bl_num,
	id_linerterm, 
	print_linerterm,
    etd,
    print_etd,
    eta,
    print_eta,
    date_arrival,
    print_date_arrival,
    print_order,
    is_not_waiting_invoice,
    id_sku,
    sku_num,
    sku_size,
    sku_color,
    sku_designation,
    sku_pcs,
    sku_ctns,
    sku_cbm,
    sku_kgs,
    price_fob1,
    usd_price_fob1,
    price_fob2,
    usd_price_fob2,
    price_hub,
    usd_price_hub,
    id_currency,
    currency,
    print_html_currency,
    exchange_rate,
	"type",
	qc_fees_rate,
	import_fees_rate,
	import_risk_fees_rate,
	purchase_fees_rate,
	other_fees_rate, 
	exchange_rate_fob
	)
AS
SELECT dbf.id_uo, dbf.uo_num, dbf.uo_id_status, dbf.uo_comment, dbf.uo_comment_extra_cost, dbf.uo_comment_ca, dbf.print_uo_status,
    dbf.date_open, dbf.print_date_open, dbf.date_resulted,
    dbf.print_date_resulted, dbf.date_confirmed, dbf.print_date_confirmed,
    dbf.date_close, dbf.print_date_close, dbf.id_po_root, dbf.po_root_num,
    dbf.contact_ada, dbf.importer, dbf.dpt, dbf.dpr_id_status,
    dbf.print_po_root_status, dbf.po_root_comment, dbf.id_po, dbf.po_num,
    dbf.pod, dbf.print_pod, dbf.id_export_level, dbf.print_export_level,
    dbf.id_transport_mode, dbf.print_transport_mode, dbf.po_comment, dbf.id_bl,
    dbf.bl_num, 
    dbf.id_linerterm, 
	dbf.print_linerterm,
	dbf.etd,dbf.print_etd, dbf.eta, dbf.print_eta,
    dbf.date_arrival, dbf.print_date_arrival, dbf.print_order,
    dbf.is_not_waiting_invoice, ds.id AS id_sku, ds.sku_num, ds.sku_size,
    ds.sku_color, ds.sku_designation, ds.sku_pcs, ds.sku_ctns, ds.sku_cbm,
    ds.sku_kgs, ds.price_fob1, 
	CASE 
		WHEN currency.designation = 'usd' THEN ds.price_fob1 
		ELSE (ds.price_fob1 * ds.exchange_rate_fob) 
	END AS usd_price_fob1, 
	ds.price_fob2, 
	CASE 
		WHEN currency.designation = 'usd' THEN ds.price_fob2 
		ELSE (ds.price_fob2 * ds.exchange_rate_fob) 
	END AS usd_price_fob2, 
	ds.price_hub, 
	CASE 
	WHEN currency.designation = 'usd' THEN ds.price_hub 
	ELSE (ds.price_hub * ds.exchange_rate) 
	END AS usd_price_hub, 
	ds.id_currency, 
	currency.designation AS currency,
    currency.print_html AS print_html_currency, ds.exchange_rate, 
	ds."type",
	ds.qc_fees_rate,
	ds.import_fees_rate,
	ds.import_risk_fees_rate,
	ds.purchase_fees_rate,
	ds.other_fees_rate, 
	ds.exchange_rate_fob

	FROM ((dau_bl_full dbf LEFT JOIN dau_sku ds ON ((dbf.id_bl = ds.id_bl))) LEFT
    JOIN ref_dau_currency currency ON ((ds.id_currency = currency.id)));


CREATE OR REPLACE VIEW "public"."dau_floating_full" (
    id_uo,
    uo_num,
    uo_id_status,
    uo_comment, 
    uo_comment_extra_cost, 
    uo_comment_ca, 
	print_uo_status,
    date_open,
    print_date_open,
    date_resulted,
    print_date_resulted,
    date_confirmed,
    print_date_confirmed,
    date_close,
    print_date_close,
    id_po_root,
    po_root_num,
    contact_ada,
    importer,
    dpt,
    dpr_id_status,
    print_po_root_status,
    po_root_comment,
    id_po,
    po_num,
    pod,
    print_pod,
    id_export_level,
    print_export_level,
    id_transport_mode,
    print_transport_mode,
    po_comment,
    id_bl,
    bl_num,
    etd,
    print_etd,
    eta,
    print_eta,
    date_arrival,
    print_date_arrival,
    print_order,
    is_not_waiting_invoice,
    id_sku,
    sku_num,
    sku_size,
    sku_color,
    sku_designation,
    origin_price_fob1,
    origin_usd_price_fob1,
    origin_price_fob2,
    origin_usd_price_fob2,
    origin_price_hub,
    origin_usd_price_hub,
    origin_id_currency,
    origin_currency,
    origin_print_html_currency,
    origin_exchange_rate,
    "type",
    sku_pcs_total,
    id_floating,
    pays_reso,
    date_pays_reso,
    print_date_pays_reso,
    sku_pcs_floating,
    sku_pcs_deducted,
    date_deducted,
    print_date_deducted,
    print_month_deducted,
    is_deducted,
    date_invoiced,
    print_date_invoiced,
    print_month_invoiced,
    is_invoiced,
    price_fob1,
    id_currency_fob1,
    currency_fob1,
    print_html_currency_fob1,
    exchange_rate_fob1,
    usd_price_fob1,
    is_changed_id_currency_fob1,
    is_changed_exchange_rate_fob1,
    is_changed_price_fob1,
    price_fob2,
    id_currency_fob2,
    currency_fob2,
    print_html_currency_fob2,
    exchange_rate_fob2,
    usd_price_fob2,
    is_changed_id_currency_fob2,
    is_changed_exchange_rate_fob2,
    is_changed_price_fob2,
    price_hub,
    id_currency_hub,
    currency_hub,
    print_html_currency_hub,
    exchange_rate_hub,
    usd_price_hub,
    is_changed_id_currency_hub,
    is_changed_exchange_rate_hub,
    is_changed_price_hub,
    price_invoiced,
    id_currency_invoiced,
    currency_invoiced,
    print_html_currency_invoiced,
    exchange_rate_invoiced,
    usd_price_invoiced, 
	is_changed)
AS
SELECT dsf.id_uo, dsf.uo_num, dsf.uo_id_status, dsf.uo_comment, dsf.uo_comment_extra_cost, dsf.uo_comment_ca, dsf.print_uo_status,
    dsf.date_open, dsf.print_date_open, dsf.date_resulted,
    dsf.print_date_resulted, dsf.date_confirmed, dsf.print_date_confirmed,
    dsf.date_close, dsf.print_date_close, dsf.id_po_root, dsf.po_root_num,
    dsf.contact_ada, dsf.importer, dsf.dpt, dsf.dpr_id_status,
    dsf.print_po_root_status, dsf.po_root_comment, dsf.id_po, dsf.po_num,
    dsf.pod, dsf.print_pod, dsf.id_export_level, dsf.print_export_level,
    dsf.id_transport_mode, dsf.print_transport_mode, dsf.po_comment, dsf.id_bl,
    dsf.bl_num, dsf.etd, dsf.print_etd, dsf.eta, dsf.print_eta,
    dsf.date_arrival, dsf.print_date_arrival, dsf.print_order,
    dsf.is_not_waiting_invoice, dsf.id_sku, dsf.sku_num, dsf.sku_size,
    dsf.sku_color, dsf.sku_designation, dsf.price_fob1 AS origin_price_fob1,
    dsf.usd_price_fob1 AS origin_usd_price_fob1, dsf.price_fob2 AS
    origin_price_fob2, dsf.usd_price_fob2 AS origin_usd_price_fob2,
    dsf.price_hub AS origin_price_hub, dsf.usd_price_hub AS
    origin_usd_price_hub, dsf.id_currency AS origin_id_currency, dsf.currency
    AS origin_currency, dsf.print_html_currency AS origin_print_html_currency,
    dsf.exchange_rate AS origin_exchange_rate, dsf."type", dsf.sku_pcs AS
    sku_pcs_total, df.id AS id_floating, df.pays_reso, df.date_pays_reso,
    to_char(to_date(df.date_pays_reso, 'YYYYMMDD'), 'DD/MM/YYYY') AS print_date_pays_reso, 
	df.sku_pcs AS sku_pcs_floating, df.sku_pcs_deducted, df.date_deducted,
    to_char(to_date(df.date_deducted, 'YYYYMMDD'), 'DD/MM/YYYY') AS print_date_deducted,
    to_char(to_date(df.date_deducted, 'YYYYMMDD'), 'YYMM') AS print_month_deducted, df.is_deducted,
    df.date_invoiced, 
	to_char(to_date(df.date_invoiced, 'YYYYMMDD'), 'DD/MM/YYYY') AS print_date_invoiced, 
	to_char(to_date(df.date_invoiced,'YYYYMMDD'), 'YYMM') AS print_month_invoiced, df.is_invoiced, df.price_fob1, df.id_currency_fob1,
    currency_fob1.designation AS currency_fob1, currency_fob1.print_html AS
    print_html_currency_fob1, df.exchange_rate_fob1, 
	CASE 
		WHEN currency_fob1.designation = 'usd' THEN df.price_fob1 
		ELSE (df.price_fob1 * df.exchange_rate_fob1) 
	END AS usd_price_fob1, 
	CASE 
		WHEN (currency_fob1.designation = 'usd') AND (dsf.usd_price_fob1 <> df.price_fob1) THEN 1 
		WHEN (currency_fob1.designation <> 'usd') AND (dsf.usd_price_fob1 <> (df.price_fob1 * df.exchange_rate_fob1)) THEN 1 
		ELSE 0 
	END AS is_changed_id_currency_fob1,
    CASE 
		WHEN (dsf.id_currency <> df.id_currency_fob1) THEN 1 
		ELSE 0 
	END AS is_changed_exchange_rate_fob1, 
	CASE 
		WHEN (dsf.exchange_rate <> df.exchange_rate_fob1) THEN 1 
		ELSE 0 
	END AS is_changed_price_fob1,
    df.price_fob2, df.id_currency_fob2, currency_fob2.designation AS
    currency_fob2, currency_fob2.print_html AS print_html_currency_fob2,
    df.exchange_rate_fob2, 
	CASE 
	WHEN currency_fob2.designation = 'usd' THEN df.price_fob2 
	ELSE (df.price_fob2 * df.exchange_rate_fob2) 
	END AS usd_price_fob2, 
	CASE 
	WHEN (currency_fob2.designation = 'usd') AND (dsf.usd_price_fob2 <> df.price_fob2) THEN 1 
	WHEN (currency_fob2.designation <> 'usd') AND (dsf.usd_price_fob2 <> (df.price_fob2 * df.exchange_rate_fob2) ) THEN 1 
	ELSE 0 
	END AS is_changed_id_currency_fob2,
    CASE 
	WHEN (dsf.id_currency <> df.id_currency_fob2) THEN 1 
	ELSE 0 
	END AS is_changed_exchange_rate_fob2, 
	CASE 
	WHEN (dsf.exchange_rate <> df.exchange_rate_fob2) THEN 1 
	ELSE 0 
	END AS is_changed_price_fob2,
    df.price_hub, df.id_currency_hub, currency_hub.designation AS currency_hub,
    currency_hub.print_html AS print_html_currency_hub, df.exchange_rate_hub,
    CASE 
	WHEN currency_hub.designation = 'usd' THEN df.price_hub 
	ELSE (df.price_hub * df.exchange_rate_hub) 
	END AS usd_price_hub, 
	CASE 
	WHEN (currency_hub.designation = 'usd') AND (dsf.usd_price_hub <> df.price_hub) THEN 1 
	WHEN ( currency_hub.designation <> 'usd') AND (dsf.usd_price_hub  <> (df.price_hub * df.exchange_rate_hub)) THEN 1 
	ELSE 0 
	END AS is_changed_id_currency_hub, 
	CASE 
	WHEN (dsf.id_currency <> df.id_currency_hub) THEN 1 
	ELSE 0 
	END AS is_changed_exchange_rate_hub, 
	CASE
    WHEN (dsf.exchange_rate <> df.exchange_rate_hub) THEN 1 
	ELSE 0 
	END AS
    is_changed_price_hub, df.price_invoiced, df.id_currency_invoiced,
    currency_invoiced.designation AS currency_invoiced,
    currency_invoiced.print_html AS print_html_currency_invoiced,
    df.exchange_rate_invoiced, 
	CASE 
	WHEN currency_invoiced.designation = 'usd' THEN df.price_invoiced 
	ELSE (df.price_invoiced * df.exchange_rate_invoiced) 
	END AS usd_price_invoiced, 
	is_changed
FROM (((((dau_sku_full dsf LEFT JOIN dau_floating df ON ((dsf.id_sku =
    df.id_sku))) LEFT JOIN ref_dau_currency currency_fob1 ON
    ((df.id_currency_fob1 = currency_fob1.id))) LEFT JOIN ref_dau_currency
    currency_fob2 ON ((df.id_currency_fob2 = currency_fob2.id))) LEFT JOIN
    ref_dau_currency currency_hub ON ((df.id_currency_hub = currency_hub.id)))
    LEFT JOIN ref_dau_currency currency_invoiced ON ((df.id_currency_invoiced =
    currency_invoiced.id)))
WHERE (dsf.id_export_level = (
    SELECT max(ref_dau_export_level.id) AS max
    FROM ref_dau_export_level
    )
	AND df.id IS NOT NULL);


CREATE OR REPLACE VIEW "public"."dau_invoice_full" (
    id_uo,
    uo_num,
    uo_id_status,
    uo_comment, 
    uo_comment_extra_cost, 
    uo_comment_ca, 
	print_uo_status,
    date_open,
    print_date_open,
    date_resulted,
    print_date_resulted,
    date_confirmed,
    print_date_confirmed,
    date_close,
    print_date_close,
    id_po_root,
    po_root_num,
    contact_ada,
    importer,
    dpt,
    dpr_id_status,
    print_po_root_status,
    po_root_comment,
    id_po,
    po_num,
    pod,
    print_pod,
    id_export_level,
    print_export_level,
    id_transport_mode,
    print_transport_mode,
    po_comment,
    id_bl,
    bl_num,
	id_linerterm, 
	print_linerterm,
    etd,
    print_etd,
    eta,
    print_eta,
    date_arrival,
    print_date_arrival,
    print_order,
    is_not_waiting_invoice,
    id_bl_invoice,
    invoice_num,
    cost_kind,
    bl_invoice_amount,
    id_currency_bl_invoice,
    currency_bl_invoice,
    print_html_currency_bl_invoice,
    exchange_rate_bl_invoice,
    print_bl_invoice_amount, 
	id_issuer, 
	issuer, 
	comment)
AS
SELECT dbf.id_uo, dbf.uo_num, dbf.uo_id_status, dbf.uo_comment, dbf.uo_comment_extra_cost, dbf.uo_comment_ca, dbf.print_uo_status,
    dbf.date_open, dbf.print_date_open, dbf.date_resulted,
    dbf.print_date_resulted, dbf.date_confirmed, dbf.print_date_confirmed,
    dbf.date_close, dbf.print_date_close, dbf.id_po_root, dbf.po_root_num,
    dbf.contact_ada, dbf.importer, dbf.dpt, dbf.dpr_id_status,
    dbf.print_po_root_status, dbf.po_root_comment, dbf.id_po, dbf.po_num,
    dbf.pod, dbf.print_pod, dbf.id_export_level, dbf.print_export_level,
    dbf.id_transport_mode, dbf.print_transport_mode, dbf.po_comment, dbf.id_bl,
    dbf.bl_num, 	dbf.id_linerterm, 
	dbf.print_linerterm,
dbf.etd, dbf.print_etd, dbf.eta, dbf.print_eta,
    dbf.date_arrival, dbf.print_date_arrival, dbf.print_order,
    dbf.is_not_waiting_invoice, dbi.id AS id_bl_invoice, dbi.invoice_num,
    dbi.cost_kind, dbi.invoice_amount AS bl_invoice_amount, dbi.id_currency AS
    id_currency_bl_invoice, currency_invoice.designation AS
    currency_bl_invoice, currency_invoice.print_html AS
    print_html_currency_bl_invoice, dbi.exchange_rate AS
    exchange_rate_bl_invoice, 
	CASE 
		WHEN currency_invoice.designation = 'usd' THEN (dbi.invoice_amount * dbi.exchange_rate) ||' '|| currency_invoice.print_html
		ELSE dbi.invoice_amount|| ' '||currency_invoice.print_html
	END AS print_bl_invoice_amount, 
	dbi.id_issuer as id_issuer,
	rdi.designation as issuer, 
	dbi.comment
FROM (((dau_bl_full dbf LEFT JOIN dau_bl_invoice dbi ON ((dbf.id_bl =
    dbi.id_bl))) LEFT JOIN ref_dau_issuers rdi ON ((dbi.id_issuer = rdi.id)))
    LEFT JOIN ref_dau_currency currency_invoice ON ((dbi.id_currency =
    currency_invoice.id)));


CREATE OR REPLACE VIEW "public"."dau_ct_full" (
    id_uo,
    uo_num,
    uo_id_status,
	uo_comment, 
	uo_comment_extra_cost, 
	uo_comment_ca, 
    print_uo_status,
    date_open,
    print_date_open,
    date_resulted,
    print_date_resulted,
    date_confirmed,
    print_date_confirmed,
    date_close,
    print_date_close,
    id_po_root,
    po_root_num,
    contact_ada,
    importer,
    dpt,
    dpr_id_status,
    print_po_root_status,
    po_root_comment,
    id_po,
    po_num,
    pod,
    print_pod,
    id_export_level,
    print_export_level,
    id_transport_mode,
    print_transport_mode,
    po_comment,
    id_bl,
    bl_num,
	id_linerterm, 
	print_linerterm,
    etd,
    print_etd,
    eta,
    print_eta,
    date_arrival,
    print_date_arrival,
    print_order,
    is_not_waiting_invoice,
    ct_num,
    id_ct_types,
    print_short_ct_type,
    print_long_ct_type,
    cbm_ct_type)
AS
SELECT dbf.id_uo, dbf.uo_num, dbf.uo_id_status, dbf.uo_comment, dbf.uo_comment_extra_cost, dbf.uo_comment_ca, dbf.print_uo_status,
    dbf.date_open, dbf.print_date_open, dbf.date_resulted,
    dbf.print_date_resulted, dbf.date_confirmed, dbf.print_date_confirmed,
    dbf.date_close, dbf.print_date_close, dbf.id_po_root, dbf.po_root_num,
    dbf.contact_ada, dbf.importer, dbf.dpt, dbf.dpr_id_status,
    dbf.print_po_root_status, dbf.po_root_comment, dbf.id_po, dbf.po_num,
    dbf.pod, dbf.print_pod, dbf.id_export_level, dbf.print_export_level,
    dbf.id_transport_mode, dbf.print_transport_mode, dbf.po_comment, dbf.id_bl,
    dbf.bl_num, 	dbf.id_linerterm, 
	dbf.print_linerterm,
dbf.etd, dbf.print_etd, dbf.eta, dbf.print_eta,
    dbf.date_arrival, dbf.print_date_arrival, dbf.print_order,
    dbf.is_not_waiting_invoice, dc.ct_num, dc.id_ct_types, lct.label AS
    print_short_ct_type, lct.name AS print_long_ct_type, lct.cbm AS cbm_ct_type
FROM ((dau_bl_full dbf LEFT JOIN dau_ct dc ON ((dbf.id_bl = dc.id_bl))) LEFT
    JOIN list_ct_types lct ON ((dc.id_ct_types = lct.ct_type_id)));


CREATE OR REPLACE VIEW "public"."tdb_lb_cost" 
AS
SELECT ftlmv.po_shpt,
ftlmv.supplier,
ftlmv.manufacturer,
ftlmv.transport_mode,
ftlmv.fwd,
ftlmv.pol,
ftlmv.pod,
ftlmv.esd AS us_esd,
ftlmv.etd AS us_etd,
ftlmv.eta AS us_eta,
to_char(to_date(ftlmv.esd,'YYYYMMDD'),'DD/MM/YYYY') AS fr_esd,
to_char(to_date(ftlmv.etd,'YYYYMMDD'),'DD/MM/YYYY') AS fr_etd,
to_char(to_date(ftlmv.eta,'YYYYMMDD'),'DD/MM/YYYY') AS fr_eta,
to_char(to_date(ftlmv.esd,'YYYYMMDD'),'YYYYMM') AS year_month_esd,
CASE 
	WHEN length(ftlmv.etd) = 0 THEN '' 
	ELSE to_char(to_date(ftlmv.etd, 'YYYYMMDD'),'YYYYMM') 
END AS year_month_etd,
to_char(to_date(ftlmv.eta, 'YYYYMMDD'), 'YYYYMM') AS year_month_eta,
ftlmv.bl_hawb,
ftlmv.carrier_airline,
ftlmv.vessel,
CASE 
	WHEN (ftlcv.linerterm IS NOT NULL) THEN ftlcv.linerterm  
	ELSE ftlmv.origin_linerterm 
END AS linerterm,
quoted.freight AS quoted_freight,
engaged.freight AS engaged_freight,
shipped.freight AS shipped_freight,
invoiced.freight AS invoiced_freight 
FROM fcs_tracing_lb_main_view ftlmv 
LEFT JOIN (
	SELECT 
	rs.po_shpt,
	rs.etd,
	rs.eta,
	rs.linerterm FROM (
		SELECT 
		fcs_tracing_lb_container_view.po_shpt,
		fcs_tracing_lb_container_view.etd,
		fcs_tracing_lb_container_view.eta,
		CASE 
			WHEN lower(fcs_tracing_lb_container_view.type_container) = 'lcl' THEN 'LCL' 
			ELSE 'FCL' 
		END AS linerterm 
		FROM fcs_tracing_lb_container_view 
		GROUP BY 
		fcs_tracing_lb_container_view.po_shpt,
		fcs_tracing_lb_container_view.etd,
		fcs_tracing_lb_container_view.eta,
		fcs_tracing_lb_container_view.type_container
	) as rs 
	GROUP BY rs.po_shpt,
	rs.etd,
	rs.eta,
	rs.linerterm
) as ftlcv 
ON ftlmv.po_shpt = ftlcv.po_shpt 
AND ftlmv.etd = ftlcv.etd
AND ftlmv.eta = ftlcv.eta

LEFT JOIN (
	SELECT dif.po_num AS num_po,
	dif.bl_num,
	sum(dif.bl_invoice_amount) AS freight,
	'' 
	FROM dau_invoice_full dif 
	WHERE dif.id_export_level = 1 
	AND length(dif.invoice_num) = 0
	AND dif.cost_kind = 'FREIGHT'
	GROUP BY 
	dif.po_num,
	dif.bl_num
) as quoted 
ON ftlmv.po_shpt = quoted.num_po
LEFT JOIN (
	SELECT dif.po_num AS num_po,
	dif.bl_num,
	sum(dif.bl_invoice_amount) AS freight,
	'' 
	FROM dau_invoice_full dif 
	WHERE dif.id_export_level = 2 
	AND length(dif.invoice_num) = 0
	AND dif.cost_kind = 'FREIGHT'
	GROUP BY 
	dif.po_num,
	dif.bl_num
) as engaged 
ON ftlmv.po_shpt = engaged.num_po
LEFT JOIN (
	SELECT dif.po_num AS num_po,
	dif.bl_num,
	sum(dif.bl_invoice_amount) AS freight,
	'' 
	FROM dau_invoice_full dif 
	WHERE dif.id_export_level = 3 
	AND length(dif.invoice_num) = 0
	AND dif.cost_kind = 'FREIGHT' 
	GROUP BY dif.po_num,
	dif.bl_num
) as shipped 
ON ftlmv.po_shpt = shipped.num_po
AND ftlmv.bl_hawb = shipped.bl_num
LEFT JOIN (
	SELECT dif.po_num AS num_po,
	dif.bl_num,
	sum(dif.bl_invoice_amount) AS freight,
	'' 
	FROM dau_invoice_full dif 
	WHERE dif.id_export_level = 3
	AND length(dif.invoice_num) > 0
	AND dif.cost_kind = 'FREIGHT'
	GROUP BY 
	dif.po_num,
	dif.bl_num
) as invoiced 
ON ftlmv.po_shpt = invoiced.num_po 
AND ftlmv.bl_hawb = invoiced.bl_num 
WHERE engaged.num_po IS NOT NULL 
ORDER BY ftlmv.po_shpt, ftlmv.bl_hawb
;

CREATE OR REPLACE VIEW "public"."tdb_lb_container" AS
SELECT 
ftlmv.po_shpt,
ftlmv.supplier,
ftlmv.manufacturer,
ftlmv.transport_mode,
ftlmv.fwd,
ftlmv.pol,
ftlmv.pod,
ftlmv.esd AS us_esd,
ftlmv.etd AS us_etd,
ftlmv.eta AS us_eta,
to_char(to_date(ftlmv.esd,'YYYYMMDD'),'DD/MM/YYYY') AS fr_esd,
to_char(to_date(ftlmv.etd,'YYYYMMDD'),'DD/MM/YYYY') AS fr_etd,
to_char(to_date(ftlmv.eta,'YYYYMMDD'),'DD/MM/YYYY') AS fr_eta,
to_char(to_date(ftlmv.esd,'YYYYMMDD'),'YYYYMM') AS year_month_esd,
to_char(to_date(ftlmv.etd,'YYYYMMDD'),'YYYYMM') AS year_month_etd,
to_char(to_date(ftlmv.eta,'YYYYMMDD'),'YYYYMM') AS year_month_eta,
ftlmv.bl_hawb,
ftlmv.carrier_airline,
ftlmv.vessel,
ftlcv.type_container,
ftlcv.container,
ftlcv.po_container_cbm AS cbm,
ftlcv.po_container_weight AS weight 
FROM fcs_tracing_lb_main_view ftlmv 
LEFT JOIN fcs_tracing_lb_container_view ftlcv 
ON ftlmv.po_shpt = ftlcv.po_shpt 
AND ftlmv.etd = ftlcv.etd 
AND ftlmv.eta = ftlcv.eta 
WHERE ftlcv.container IS NOT NULL 
ORDER BY ftlmv.po_shpt,
ftlcv.type_container;

