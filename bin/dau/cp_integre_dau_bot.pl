#!/usr/bin/perl

use strict;
use DBI;
use POSIX qw(strftime);

my $DBUG = 0;

# ENVIRONNEMENT SPECIFIQUE

use esol::env;
use lib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params/';
use lib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'/lib/';
use conf_scm_cbr;
use lib_scm_cbr;

my @aLoop;

my $BIN_DIRECTORY = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR') . '/bin/dau/';


my ( $DBH, $DBH_FCS );
( $DBH = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or print 'Connexion base impossible.';
( $DBH_FCS = DBI->connect( esolGetEnv(__FILE__, 'BDD_FCS_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or print 'Connexion base impossible.';

my $LOG_FILE_PATH = $BIN_DIRECTORY . 'cp_integre_dau_bot.log';
		
&main();


sub main{
	
	my @aLoop;	
	@aLoop = &aGetLoopNumPoRef();
	&vUpdateCpNumPoRef(@aLoop);
	@aLoop = &aGetLoopToTreat();
	&vExecLoopToTreat(@aLoop);
	
	my $sCommande = $BIN_DIRECTORY . 'cp_dau_status_update.pl';
	&vExecCommande($sCommande); 
}


sub vExecLoopToTreat {
	my (@aLoop ) = @_;
	my $sCommande;
	my $sUrl;
	foreach my $hData (@aLoop) {
		my %hValue = ();
		$hValue{num_po_root} = $hData->{'num_po_root'};
		$hValue{concat_ref} = $hData->{'concat_ref'};
		
		$sCommande = $BIN_DIRECTORY . 'cp_integre_dau.pl '.' -p '.  $hValue{num_po_root};
		if($hValue{concat_ref} eq ''){
			$sCommande .= ' -e 1';
		}
		&vExecCommande($sCommande); 	
	}
}


sub vExecCommande {
	my ($sCommande) = @_;
	my $iResult;
	my $sResult;
	my $sNow = (strftime '%Y%m%d - %H:%M:%S', localtime);
	open( LOG, '>> '.$LOG_FILE_PATH );
	print LOG "\n";
	print LOG $sNow;
	print LOG "\n";
	print LOG $sCommande;
	print LOG "\n";
	$iResult = system($sCommande);
	if($iResult eq 0)
	{
		$sResult = 'SUCCESS';
	} 
	else 
	{
		$sResult = 'FAIL : '.$iResult;
	}
	print LOG $sResult;
	print LOG "\n";		
	close LOG;
	
}



sub aGetLoopNumPoRef{
	my $sSqlr = "	
	SELECT 
	DISTINCT ON (ph.num_po, cl.ref)
	ph.num_po as num_po, 
	CASE
    	WHEN clpsdf.idloading_po_sku IS NOT NULL THEN cl.ref
        ELSE NULL
    END as ref
	FROM po_header as ph
	LEFT JOIN cp_loading_po_sku as clps
	ON ph.num_po = clps.num_po
	LEFT JOIN cp_loading as cl
	ON clps.idloading = cl.idloading
	LEFT JOIN cp_loading_po_sku_detail_fnd as clpsdf
	ON clps.idloading_po_sku = clpsdf.idloading_po_sku
    LEFT JOIN pre_order as po
    ON SPLIT_PART(UPPER(ph.num_po), '_', 1) = po.num_po
    LEFT JOIN ref_transport as rt
    ON ph.mode_transport = rt.code
	WHERE 1=1
    AND UPPER(rt.nom) = 'ROAD'
    AND po.is_off_panel IS TRUE
	ORDER BY ph.num_po, cl.ref
	;
	";
	
	print $sSqlr if($DBUG);
	
	my $rs = $DBH_FCS->prepare($sSqlr);
	$rs->execute();

	while ( my $hData = $rs->fetchrow_hashref ) {
		my %hValue = ();
		$hValue{num_po} = $hData->{'num_po'};
		$hValue{ref} = $hData->{'ref'};
		push (@aLoop, \%hValue );
			
	}
	$rs->finish;
	
	return @aLoop;

}

sub vUpdateCpNumPoRef {
	my (@aLoop ) = @_;

	my $sSqlr = "
	DROP TABLE IF EXISTS cp_num_po_ref CASCADE;

	CREATE TABLE cp_num_po_ref (
		num_po VARCHAR,
		ref VARCHAR
	);
	";
	
	foreach my $hData (@aLoop) {
		my %hValue = ();
		$hValue{num_po} = $hData->{'num_po'};
		$hValue{ref} = $hData->{'ref'};
		$sSqlr .= "
		INSERT INTO cp_num_po_ref
		( num_po, ref )
		SELECT 
		'".$hValue{num_po}."', 
		'".$hValue{ref}."'
		;
		";
	}
	
	print $sSqlr;# if($DBUG);;
	
	$DBH->do($sSqlr);

}


sub aGetLoopToTreat {

	my @aLoop;

	my $sSqlr = " 
		SELECT 
		num_po_root, 
		-- CD20170810 la ligne compliqué ci-dessous permet de supprimer les ',' en trop
		SUBSTRING(CONCAT(ref||',')  from '[,]*(CP.+[0-9]),') as concat_ref, 
		CONCAT(ref||',') as concat_ref_original
		FROM
		( 
			SELECT
			DISTINCT ON (num_po_root, ref)
			UPPER(SPLIT_PART(cnpr.num_po, '_', '1')) as num_po_root,
			cnpr.ref as ref,
			dp.po_num as dp_num_po, 
			db.bl_num as db_bl_num,
			'' 
			FROM cp_num_po_ref as cnpr
			LEFT JOIN dau_po as dp
			ON SPLIT_PART(LOWER(dp.po_num), '_', '1') = SPLIT_PART(LOWER(cnpr.num_po), '_', '1')
			LEFT JOIN dau_bl as db
			ON dp.id = db.id_po
			AND LOWER(db.bl_num) = LOWER(cnpr.ref)
			WHERE 1=1
			AND ( dp.id IS NULL
				OR db.id IS NULL
			)
			ORDER BY num_po_root, ref
		) as main
		GROUP BY
		num_po_root
		;
	";
	
	print $sSqlr if($DBUG);;
	
	my $rs = $DBH->prepare($sSqlr);
	$rs->execute();

	while ( my $hData = $rs->fetchrow_hashref ) {
		my %hValue = ();
		$hValue{num_po_root} = $hData->{'num_po_root'};
		$hValue{concat_ref} = $hData->{'concat_ref'};
		push (@aLoop, \%hValue );
			
	}
	$rs->finish;
	
	return @aLoop;
 
}

