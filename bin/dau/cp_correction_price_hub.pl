#!/usr/bin/perl

use strict;
use CGI;
use DBI;
use HTML::Template;
use POSIX qw(strftime);
use Class::Date qw(:errors now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;

use MIME::Lite;

use XML::Writer;
use IO::File;

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $DBUG = 0;

( my $DBH_CBR = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or die print "Connexion base cbr impossible." ;
( my $DBH_COT = DBI->connect( esolGetEnv(__FILE__, 'BDD_QF_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or die print "Connexion base cotyr impossible." ;
( my $DBH_FCS = DBI->connect( esolGetEnv(__FILE__, 'BDD_FCS_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD') ) ) or die print "Connexion base fcs impossible." ;

my $NUM_PO;

&init;

&main();

sub main {	
	my $dHubPrice = &get_hub_price ($NUM_PO, $DBUG, $DBH_COT);
    $dHubPrice =~ tr/,/./;
	print " dHubPrice $dHubPrice ";
	&update_hub_price_cotation_A ($dHubPrice, $NUM_PO, $DBUG, $DBH_CBR) if ($dHubPrice);
}

$DBH_COT->disconnect;
$DBH_FCS->disconnect;
$DBH_CBR->disconnect;

###########################
# Fonctions et procédures #
###########################

sub get_hub_price {

	my ($num_po, $DBUG, $dbh) = @_;	
	my $dHubPrice;
	$num_po = $num_po.'A';
	my $sqlr = "
	SELECT 

	TO_CHAR((total_transport_cost_price/q_pcs), '9 999 999 990D999')as unit_transport_price,
	TRIM(TO_CHAR( ( unit_purchase_price+(total_transport_cost_price/q_pcs)), '9 999 999 990D999')) as total_unit_price,
	TO_CHAR( ( ( unit_purchase_price*q_pcs)+total_transport_cost_price), '9999999990') as total_price,
	total_transport_cost_price
	 
	FROM (

		SELECT

		imh.fic_num as fic_num,	 
		ch.id_scenario as sc_id, 
		ch.id_scenario, 
		cf.q_pcs as q_pcs,
		cf.unit_purchase_price as unit_purchase_price,
		cost_charge.total_cost_transport as total_transport_cost_price
		
		FROM cotation_sku_fnd as cf
		LEFT JOIN ref_fnds as rf
		ON rf.id = cf.id_fnd
		LEFT JOIN ref_pays as rp
		ON rf.code_country = rp.code
		LEFT JOIN cotation_header as ch
		ON cf.id_header = ch.id
		LEFT JOIN logistic_routing_scenarios as lrs
		ON lrs.id=ch.id_scenario
		LEFT JOIN input_model_header as imh
		ON imh.id=lrs.id_input_model
		LEFT JOIN ref_currency as rc
		ON SUBSTR(imh.currency, 1,3) = LOWER(rc.code)
		LEFT JOIN hp 
		ON hp.id=ch.id_scenario
		LEFT JOIN(
		SELECT
		ch.id_scenario,
		SUM(cd.cost) AS total_cost_transport
		FROM cotation_header ch
		LEFT JOIN cotation_detail cd
		ON cd.id_header = ch.id
		LEFT JOIN ref_list_of_charges rc
		ON cd.id_charge = rc.id
		LEFT JOIN ref_transport_mode as rtm
		ON UPPER(rtm.designation ) = 'ROAD'
		
		WHERE 1=1
		GROUP BY ch.id_scenario 
		) as cost_charge
		ON cost_charge.id_scenario=ch.id_scenario  
	) as sqlr
	
	WHERE fic_num = '$num_po'
	;
	";

	print "<PRE>".$sqlr if ($DBUG);
	my $rs = $dbh->prepare($sqlr);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr.":<br><pre>".$sqlr; 
		$rs->finish;
		exit;
	}

	while ( my $data = $rs->fetchrow_hashref ) {
		
		$dHubPrice = $data->{'total_unit_price'};
	}
	
  return $dHubPrice;
}


sub update_hub_price_cotation_A {

	my ($dHubPrice, $num_po, $DBUG, $dbh) = @_;	
    my $sqlr = "
	UPDATE dau_sku
	set price_hub = $dHubPrice
	WHERE id IN (
	
	SELECT

	ds.id

	FROM dau_po_root as dpr
	LEFT JOIN dau_po as dp
	ON dp.id_po_root = dpr.id
	LEFT JOIN dau_bl as dbl
	ON dbl.id_po = dp.id
	LEFT JOIN dau_sku as ds
	ON ds.id_bl = dbl.id
	WHERE dpr.po_root_num = '$num_po'
	AND dp.id_export_level ='1'
	
	)"
    ;
	
	print "<pre>".$sqlr if( $DBUG);

	$dbh->do($sqlr) or die "Couldn't execute sql: $sqlr $dbh->errstr+";
	if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
		&FCS_AfficheErreur( "authentification.psp", "POST", '', $dbh->errstr."<pre>".$sqlr );
		$dbh->disconnect;
		exit;
	}
	
}

sub init(@ARGV){

	if(scalar @ARGV eq 0) {
			&init_error();
	} else {
		for ( my $i ; $i < scalar @ARGV ; $i++ ){
			if (get_arg($ARGV[$i]) eq 'p' ){
				$NUM_PO = $ARGV[$i+1];
			}
			
			if (get_arg($ARGV[$i]) eq 'help' ){
				&init_error();
			}
		}
	}
	if ($NUM_PO eq '') {
		&init_error(); 
	}
}

sub init_error(){
        print "
		usage : cp_correction_price_hub.pl -p num_po 
		export_level in (1,3,5) ";
        &exit_function;
}

sub get_arg(){
        my ($str_to_return) = @_;
        if (index( $str_to_return, '-' ) > -1){
                $str_to_return = substr($str_to_return , 1, length($str_to_return));
        } else {
                $str_to_return = 0
        }
        return ($str_to_return);
}

sub exit_function(){
        exit;
}
	