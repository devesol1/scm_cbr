#!/usr/bin/perl

use strict;

use DBI;
use POSIX qw(strftime);
use Class::Date qw(:errors now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;

use XML::Writer;
use IO::File;


# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $dbug = 0;
my $log_msg;

my $xml_file;

my $dau_bin_directory = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau/";
my $src_directory = $dau_bin_directory."/src/";
my $done_directory = $dau_bin_directory."/done/";
my $dest_directory = $dau_bin_directory."/dest/";

&init();
#$xml_file = "EXPORT_COTATION_20090908.xml";
#$xml_file = "QF_EXTRACT_COTATION.XML";

print "\n 04_import_charges.pl @ARGV ".now." \n";


my $xml_content = XML::XPath->new(filename => $src_directory.$xml_file);
&main();
&update_statut_solde($dbug);

sub main {
	open LOGFILE, ">> ".esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau/import_charges.log" or die "Can't open ".esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau/import_charges.log";

	#&purge_dau_po( $xml_content, $dbug );

	my $date = strftime("%Y/%m/%d %H:%M:%S", localtime);
	print LOGFILE $date.' - START sub maj_dau_po xml_file : '.$xml_file."\n"; 
		&maj_dau_po( $xml_content, $dbug );
	my $date = strftime("%Y/%m/%d %H:%M:%S", localtime);
	print LOGFILE $date.' - END sub maj_dau_po xml_file : '.$xml_file."\n"; 
    my $date_log = now;
	rename ($src_directory.$xml_file , $done_directory.$xml_file.$date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day ).sprintf( "%02d", $date_log->hour ). sprintf( "%02d", $date_log->min )) or die "impossible de déplacer $src_directory$xml_file vers $done_directory$xml_file";

	my $date = strftime("%Y/%m/%d %H:%M:%S", localtime);
	print LOGFILE $date.' - START sub update_dau_uo_status xml_file : '.$xml_file."\n"; 
		&update_dau_uo_status($dbug);
		&update_statut_solde($dbug);
	my $date = strftime("%Y/%m/%d %H:%M:%S", localtime);
	print LOGFILE $date.' - END sub update_dau_uo_status xml_file : '.$xml_file."\n"; 

#	&synchronize_quot_and_waited(1);
#	if(!$dbug){
#		rename ($src_directory.$xml_file , $done_directory.$xml_file) or die "impossible de déplacer $src_directory$xml_file vers $done_directory$xml_file";
#	}

	print LOGFILE "\n"; 
	close(LOGFILE);

}

###########################
# Fonctions et procédures #
###########################

sub purge_dau_po{
	my ( $xml_content, $dbug ) = @_;
	my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	my $old_autocommit = $dbh->{AutoCommit};
	if ( $old_autocommit == 1 ) {
		$dbh->{AutoCommit} = 0;
	}
	my $sqlr = "
	
		--CD20131106 ce qui suit permet de supprimer les données dans le dau 
		--en cas d'amendement du nombre de shipments
		DELETE
		FROM dau_po
		WHERE 
		SPLIT_PART(po_num, '_', 1)||SPLIT_PART(po_num, '-', 2) IN
		(
			SELECT
			SPLIT_PART(po_num, '_', 1)||SPLIT_PART(po_num, '-', 2)
			FROM 
			dau_po
			WHERE id IN (
				SELECT MIN(id)
				FROM dau_po
				WHERE SPLIT_PART(po_num, '_', 1) IN (
					SELECT
					po_num
					FROM (
						SELECT
						SPLIT_PART(po_num, '_', 1)as po_num,
						count(*) as my_count 
						FROM(
							SELECT  
							SPLIT_PART(po_num, '_', 1)||SPLIT_PART(po_num, '-', 2) as po_num
							FROM dau_po 
							WHERE id_export_level = 1
							GROUP BY SPLIT_PART(po_num, '_', 1)||SPLIT_PART(po_num, '-', 2)
						) as main
						GROUP BY SPLIT_PART(po_num, '_', 1)
					) as main
					WHERE my_count > 1
				)
				GROUP BY SPLIT_PART(po_num, '_', 1)
			)
		);


        DELETE FROM dau_bl
        WHERE id_po NOT IN ( SELECT id FROM dau_po )
        ;
        DELETE FROM dau_sku
        WHERE id_bl NOT IN ( SELECT id FROM dau_bl )
        ;
        DELETE FROM dau_ct
        WHERE id_bl NOT IN ( SELECT id FROM dau_bl )
        ;
        DELETE FROM dau_bl_invoice
        WHERE id_bl NOT IN ( SELECT id FROM dau_bl )
        ;
        
		DELETE FROM dau_uo
        WHERE id NOT IN ( 
              SELECT id_uo FROM dau_bl_full as dbf
         )
         ;
                         
		DELETE FROM dau_po_root
		WHERE id_uo NOT IN ( SELECT id FROM dau_uo )
		;
	";

	print $sqlr."\n" if ($dbug);
	my $rs = $dbh->prepare( $sqlr );
	$rs->execute;
	if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n ".$dbh->errstr." \n $sqlr \n";
		$rs->finish;
		$dbh->rollback;
		$dbh->disconnect;
		exit;
	}

	$dbh->commit;
	$rs->finish;
	$dbh->{AutoCommit} = $old_autocommit;
	$dbh->disconnect;
	
	return 0;	
			
}

sub update_statut_solde{
	my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	my $dbh_fcs = DBI->connect( esolGetEnv(__FILE__, 'BDD_FCS_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	
	my $sqlr = "
		SELECT 
		ordered.uo_number,
		ordered_qty, 
		shipped_qty

		 
		FROM (
			SELECT
			ph.uo_number, 
			SUM(pd.total_units) as ordered_qty
			FROM po_detail as pd
			LEFT JOIN po_header as ph
			ON pd.num_po = ph.num_po
			GROUP BY ph.uo_number
		) as ordered
		LEFT JOIN(
			SELECT
			uo_number,
			SUM(shipped_qty) as shipped_qty
			FROM (
				SELECT 
				ph.uo_number, 
				SUM(tcdm.sku_pc) as shipped_qty,
				SUM(tcdm.stripped_sku) as stripped_qty
				FROM tracing_container_detail_mer as tcdm
				LEFT JOIN tracing_container_po_mer as tcpm
				ON tcdm.num_po = tcpm.num_po
				AND tcpm.container = tcdm.container
				LEFT JOIN po_header as ph
				ON tcdm.num_po = ph.num_po
				WHERE tcpm.statut = 'F'
				AND TO_DATE(tcpm.etd, 'YYYYMMDD') > NOW() - interval '1 year'
				GROUP BY ph.uo_number
				UNION
				SELECT 
				ph.uo_number, 
				SUM(tapd.item_pcs) as shipped_qty,
				SUM(tapd.item_pcs) as stripped_qty
				FROM tracing_air_po_hawb as taph
				LEFT JOIN po_header as ph
				ON taph.num_po = ph.num_po
				LEFT JOIN tracing_air_po_detail as tapd
				ON tapd.id_tracing_air_po_hawb = taph.id_tracing_air_po_hawb
				WHERE taph.statut = 'F'
				AND TO_DATE(taph.aeroport_etd, 'YYYYMMDD') > NOW() - interval '1 year'
				GROUP BY ph.uo_number
			) as stripped
			GROUP BY uo_number
		) as stripped
		ON ordered.uo_number = stripped.uo_number

		WHERE ordered_qty<=shipped_qty
		AND ordered.uo_number <> ''
		AND shipped_qty IS NOT NULL
        
		ORDER BY ordered.uo_number
	";

	print "\n $sqlr \n" if($dbug);
	
	my $rs = $dbh_fcs->prepare( $sqlr );
	$rs->execute;
	if ( $dbh_fcs->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n ".$dbh_fcs->errstr." \n $sqlr \n";
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
	my $list_uo_full_arrived;
	
	while ( my $data = $rs->fetchrow_hashref ) {
			$list_uo_full_arrived .= "'$data->{'uo_number'}', ";
	}
	$list_uo_full_arrived = substr($list_uo_full_arrived, 0, length($list_uo_full_arrived)-2 );
	
	my $sqlr = "
		UPDATE dau_uo
		SET id_status  = '30',
		date_resulted = CAST(TO_CHAR(NOW(), 'YYYYMMDD') as INTEGER)
		WHERE id_status = '20'
		AND uo_num IN ($list_uo_full_arrived);
	";
	my $rs = $dbh->prepare( $sqlr );
	
	$rs->execute;
	if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n ".$dbh->errstr." \n $sqlr \n";
		$rs->finish;
		$dbh->disconnect;
		exit;
	}

	print $sqlr."\n" if ($dbug);

	$rs->finish;
	$dbh->disconnect;
	$dbh_fcs->disconnect;
	
	return 0;	
	
}

sub update_dau_uo_status(){
	my ( $xml_content, $dbug ) = @_;

	my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	my $old_autocommit = $dbh->{AutoCommit};
	if ( $old_autocommit == 1 ) {
		$dbh->{AutoCommit} = 0;
	}

	my $sqlr = "
	-- Remise à Zéro
	--UPDATE dau_uo SET id_status = 0;
	 
	-- Gestion du statut ouvert
	UPDATE dau_uo SET id_status = ( SELECT id FROM ref_dau_status WHERE LOWER(designation)=LOWER('Ouvert') )
	WHERE id_status < ( SELECT id FROM ref_dau_status WHERE LOWER(designation)=LOWER('Mouvementé') )
	AND id NOT IN 
	(
	SELECT DISTINCT id_uo
	FROM dau_bl_full
	WHERE id_export_level >='3'
	)
	;

	-- Gestion du statut Mouvementé
	UPDATE dau_uo SET id_status = ( SELECT id FROM ref_dau_status WHERE LOWER(designation)=LOWER('Mouvementé') )
	WHERE id_status < ( SELECT id FROM ref_dau_status WHERE LOWER(designation)=LOWER('Soldé') )
	AND id IN 
	(
	SELECT DISTINCT id_uo
	FROM dau_bl_full
	WHERE id_export_level >='3'
	)
	;

	-- Gestion du statut Soldé
	UPDATE dau_uo SET id_status = ( SELECT id FROM ref_dau_status WHERE LOWER(designation)=LOWER('Soldé') ), 
	date_resulted = CAST(TO_CHAR(NOW(), 'YYYYMMDD') as INTEGER)
--    SELECT id, dau_uo.uo_num
--    FROM dau_uo 
    WHERE id_status < ( SELECT id FROM ref_dau_status WHERE LOWER(designation)=LOWER('Confirmé') )
	AND date_resulted = '0'

	AND id IN (
		SELECT
		DISTINCT id_uo
		FROM (

		SELECT
		ordered.id_uo, 
		CASE
			WHEN sku_pcs_ordered IS NULL OR sku_pcs_arrived IS NULL THEN NULL
			ELSE (sku_pcs_ordered-sku_pcs_arrived)
		END as sku_pcs_waited, 
		sku_pcs_ordered, 
		sku_pcs_arrived,
        uo_num_arrived,

         
		'' 
		FROM (
			SELECT id_uo,
            uo_num as uo_num_ordered,
			SUM(sku_pcs) as sku_pcs_ordered
			FROM dau_sku_full
			WHERE id_export_level ='2' 
			GROUP BY id_uo,uo_num
		) as ordered
		LEFT JOIN (
			SELECT id_uo,
            uo_num as uo_num_arrived,
			SUM(sku_pcs) as sku_pcs_arrived
			FROM dau_sku_full
			WHERE id_export_level ='3' 
			AND date_arrival > '0'
			AND date_arrival <= TO_CHAR(NOW(), 'YYYYMMDD')
			GROUP BY id_uo,uo_num
		) as arrived
		ON ordered.id_uo = arrived.id_uo
		) as sqlr
 
		WHERE sku_pcs_waited IS NOT NULL
		AND sku_pcs_waited = 0
        AND uo_num_arrived NOT IN (
          SELECT
          uo_num
          FROM(
          SELECT
          ftsa.statut,
          dauo.uo_num
          FROM fcs_tracing_sea_air as ftsa
          LEFT JOIN dau_uo as dauo
          ON ftsa.uo_number = dauo.uo_num
          GROUP BY dauo.uo_num,ftsa.statut
         ) as ftsa
         WHERE ftsa.statut IN ('D','S','P','R')
         GROUP BY uo_num,statut
       ) 
        

	);

	";
	
	my $rs = $dbh->prepare( $sqlr );
	$rs->execute;
	if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n ".$dbh->errstr." \n $sqlr \n";
		$rs->finish;
		$dbh->rollback;
		$dbh->disconnect;
		exit;
	}
	print $sqlr."\n" if ($dbug);

	$dbh->commit;
	$rs->finish;

	$dbh->{AutoCommit} = $old_autocommit;
	$dbh->disconnect;
	
	return 0;	
	
}

sub maj_dau_po(){
	my ( $xml_content, $dbug ) = @_;

	my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
	my $old_autocommit = $dbh->{AutoCommit};
	if ( $old_autocommit == 1 ) {
		$dbh->{AutoCommit} = 0;
	}

	my $nodeset = $xml_content->find('/dau/uo');
	foreach my $node ($nodeset->get_nodelist) {
		my $uo_num = $node->findvalue('@num');
		my $sqlr ="
			INSERT INTO dau_uo ( uo_num )
			SELECT 
			'$uo_num' 
			WHERE 0 = (
				SELECT COUNT(id)
				FROM dau_uo
				WHERE uo_num = '$uo_num'
			)
			;
		";
        print $sqlr."\n" if ($dbug);
		
		my $rs = $dbh->prepare( $sqlr );
		$rs->execute;
		
		my $po_root_nodeset = $node->findnodes('po_root');

		foreach my $po_root_node ($po_root_nodeset->get_nodelist) {
			my $po_root_num = uc($po_root_node->findvalue('@num'));
			my $dpt = $po_root_node->findvalue('dpt');
			my $importer = $po_root_node->findvalue('importer');
			my $contact = $po_root_node->findvalue('contact');
			$contact =~ s/é/e/g;
			$contact =~ s/è/e/g;
			$contact =~ s/ê/e/g;
			$contact =~ s/à/a/g;
			$contact =~ s/ç/c/g;
			$contact =~ s/\'/\ /g;
			
			
			$sqlr = "
			UPDATE dau_po_root SET
			dpt = '$dpt',
			contact_ada = '$contact',
			importer = '$importer'
			WHERE id = (
				SELECT id_po_root
				FROM dau_sku_full as dsf
				WHERE uo_num = '$uo_num'
				AND po_root_num = '$po_root_num'
				GROUP BY id_po_root
			)
			;";
			
			$rs = $dbh->prepare( $sqlr );
		    $rs->execute;
			
			$sqlr = "
			INSERT INTO dau_po_root
			( id_uo, po_root_num, importer, dpt, contact_ada )
			SELECT 
			du.id, 
			'$po_root_num', 
			'$importer', 
			'$dpt', 
			'$contact'
			FROM dau_uo as du
			WHERE du.uo_num = '$uo_num'
			AND 0=(
				SELECT COUNT(*)
				FROM dau_sku_full as dsf
				WHERE uo_num = '$uo_num'
				AND po_root_num = '$po_root_num'
			)
			GROUP BY du.id
			;
				
			";
			
			$rs = $dbh->prepare( $sqlr );
		    $rs->execute;
			
			my $po_nodeset = $po_root_node->findnodes('po');
            print $sqlr."\n" if ($dbug);
			foreach my $po_node ($po_nodeset->get_nodelist) {
				my $po_num = uc($po_node->findvalue('@num'));

				my $id_export_level = $po_node->findvalue('@id_export_level');
# ON CONTROLE QUE le PO_ROOT n'a pas été compacté (amendement simplifié)
if ( $id_export_level eq '2' && length($po_num) == 14){ 
        my $del_po = substr($po_num,0,10);
        my $char_13 = substr($po_num,13,1);
	$sqlr = " 
		DELETE FROM dau_po
		WHERE SPLIT_PART(po_num,'_', 1) = '$del_po'
		AND SUBSTR(po_num,14,1) <> '$char_13'
		AND ( id_export_level = '2' OR id_export_level = '3' )
	; ";
	
	$rs = $dbh->prepare( $sqlr );
	$rs->execute;
	
	$sqlr = "
        DELETE FROM dau_bl
        WHERE id_po NOT IN ( SELECT id FROM dau_po )
        ;";
		
	$rs = $dbh->prepare( $sqlr );
	$rs->execute;	
		
	$sqlr = "
        DELETE FROM dau_sku
        WHERE id_bl NOT IN ( SELECT id FROM dau_bl )
        ;";
		
	$rs = $dbh->prepare( $sqlr );
	$rs->execute;
	
	$sqlr = "
        DELETE FROM dau_ct
        WHERE id_bl NOT IN ( SELECT id FROM dau_bl )
        ;
	";
	
	$rs = $dbh->prepare( $sqlr );
	$rs->execute;
	
	
}
				if($id_export_level eq '1'){
					$po_num =~ s/(.*)[A-Z](\_.*$)/\1\2/g;
					my $nb_shpt = $po_nodeset->get_nodelist;
					if($nb_shpt > 1){
						$po_num.= "-".$nb_shpt;
					} else {
						$po_num =~ s/(.*)(\_.*$)/\1/g;
					}
				}
				
				my $pod = $po_node->findvalue('@pod');
				my $id_transport_mode = $po_node->findvalue('@id_transport_mode');
				
				$sqlr = "
				UPDATE dau_po SET
				pod = '$pod',
				id_transport_mode = '$id_transport_mode'
				
				WHERE id = (
					SELECT id_po
					FROM dau_sku_full as dsf
					WHERE uo_num = '$uo_num'
					AND po_root_num = '$po_root_num'
					AND po_num = '$po_num'
					AND id_export_level = '$id_export_level'
					GROUP BY id_po
				)
				;";
				
				$rs = $dbh->prepare( $sqlr );
	            $rs->execute;
				
				$sqlr = "
				INSERT INTO dau_po
				( id_po_root, po_num, pod, id_export_level, id_transport_mode )
				SELECT 
				id_po_root, 
				'$po_num', 
				'$pod', 
				'$id_export_level',
				'$id_transport_mode'
				FROM dau_sku_full as dsf
				WHERE uo_num = '$uo_num'
				AND po_root_num = '$po_root_num'
				AND 0=(
					SELECT COUNT(*)
					FROM dau_sku_full as dsf
					WHERE uo_num = '$uo_num'
					AND po_root_num = '$po_root_num'
					AND po_num = '$po_num'
					AND id_export_level = '$id_export_level'
				)
				GROUP BY id_po_root
				;
				";
				$rs = $dbh->prepare( $sqlr );
	            $rs->execute;
                print $sqlr."\n" if ($dbug);
				
				my $bl_nodeset = $po_node->findnodes('bl');
				my $print_order = 0;
				foreach my $bl_node ($bl_nodeset->get_nodelist) {
					my $currency = $bl_node->findvalue('@currency');
					my $exchange_rate = $bl_node->findvalue('@exchange_rate');
					my $exchange_rate_fob = $bl_node->findvalue('@exchange_rate_fob');
					my $rate_freight = $bl_node->findvalue('@rate_freight');
					$rate_freight = $exchange_rate if ($rate_freight eq '');
					
					my $bl_num = uc($bl_node->findvalue('@num'));
					my $id_linerterm = $bl_node->findvalue('@linerterm');
					if($id_export_level eq '1'){
						$bl_num = $po_num;
					}
					
					my $dates_nodeset = $bl_node->findnodes('dates');
					my ($etd, $eta, $date_arrival );
					foreach my $dates_node ($dates_nodeset->get_nodelist) {
						$etd = $dates_node->findvalue('etd');
						$eta = $dates_node->findvalue('eta');
						$date_arrival = $dates_node->findvalue('arrival');
					}
					$print_order += 1;

					$sqlr = "
					UPDATE dau_bl SET
					bl_num = '$bl_num', 
					id_linerterm = '$id_linerterm'
					WHERE id = (
						SELECT id_bl
						FROM dau_sku_full as dsf
						WHERE uo_num = '$uo_num'
						AND po_root_num = '$po_root_num'
						AND po_num = '$po_num'
						AND bl_num = '$bl_num'
						AND id_export_level = '$id_export_level'
						GROUP BY id_bl
					)
					;";
					
					print $sqlr."\n" if ($dbug);
					$rs = $dbh->prepare( $sqlr );
	                $rs->execute;
					
					
					$sqlr = "
					
					INSERT INTO dau_bl
					( id_po, bl_num, id_linerterm, print_order )
					SELECT 
					id_po, 
					'$bl_num',
					'$id_linerterm', 
					'$print_order'
					FROM dau_sku_full as dsf
					WHERE uo_num = '$uo_num'
					AND po_root_num = '$po_root_num'
					AND po_num = '$po_num'
					AND 0=(
						SELECT COUNT(*)
						FROM dau_sku_full as dsf
						WHERE uo_num = '$uo_num'
						AND po_root_num = '$po_root_num'
						AND po_num = '$po_num'
						AND bl_num = '$bl_num'
						AND id_export_level = '$id_export_level'
					)
					AND id_export_level = '$id_export_level'
					GROUP BY id_po
					;
						
					";
                    print $sqlr."\n" if ($dbug);
					$rs = $dbh->prepare( $sqlr );
	                $rs->execute; 
					 
					my $ct_nodeset = $bl_node->findnodes('ct');
					$sqlr = "
					DELETE FROM dau_ct
					WHERE id_bl = (
						SELECT id_bl
						FROM dau_ct_full as dcf
						WHERE uo_num = '$uo_num'
						AND po_root_num = '$po_root_num'
						AND po_num = '$po_num'
						AND bl_num = '$bl_num'
						AND id_export_level = '$id_export_level'
						GROUP BY id_bl
					)
					;
					";
                    $rs = $dbh->prepare( $sqlr );
	                $rs->execute;
					
					foreach my $ct_node ($ct_nodeset->get_nodelist) {
						my $ct_num = uc($ct_node->findvalue('@num'));
						my $ct_type = $ct_node->findvalue('@type');

						$sqlr = "
						INSERT INTO dau_ct
						( id_bl, ct_num, id_ct_types )
						SELECT 
						id_bl, 
						'$ct_num', 
						( SELECT ct_type_id FROM list_ct_types WHERE label = '$ct_type' ) as id_ct_types
						FROM dau_ct_full as dcf
						WHERE uo_num = '$uo_num'
						AND po_root_num = '$po_root_num'
						AND po_num = '$po_num'
						AND bl_num = '$bl_num'
						AND id_export_level = '$id_export_level'
						GROUP BY id_bl
						;
							
						";
						print $sqlr."\n" if ($dbug);
						$rs = $dbh->prepare( $sqlr );
	                    $rs->execute;
					}

					my $sku_nodeset = $bl_node->findnodes('sku');
					foreach my $sku_node ($sku_nodeset->get_nodelist) {
						my $sku_num = uc($sku_node->findvalue('@num'));
						my $sku_size = uc($sku_node->findvalue('@sku_size'));
						my $sku_color = uc($sku_node->findvalue('@sku_color'));
						my $sku_designation = $sku_node->findvalue('designation');
						my $sku_pcs = $sku_node->findvalue('pcs');
						my $sku_ctns = $sku_node->findvalue('ctns');
						my $sku_cbm = $sku_node->findvalue('cbm');
						my $sku_kgs = $sku_node->findvalue('kgs');

						my $prices_nodeset = $sku_node->findnodes('prices');
						my ($price_fob1, $price_fob2, $price_hub, $tx_import_fees, $tx_import_dpt_fees_by_shpt,$tx_qc_fees, $tx_import_risk_fees, $tx_purchase_fees, $tx_exchange_rate_fees,$tx_other_fees  );
						foreach my $prices_node ($prices_nodeset->get_nodelist) {
							$price_fob1 = $prices_node->findvalue('fob1');
							$price_fob2 = $prices_node->findvalue('fob2');
							$price_hub = $prices_node->findvalue('hub');

							$tx_import_fees = $prices_node->findvalue('tx_import_fees');
							$tx_import_dpt_fees_by_shpt=$prices_node->findvalue('tx_import_dpt_fees_by_shpt');
							$tx_qc_fees = $prices_node->findvalue('tx_qc_fees');
							$tx_import_risk_fees = $prices_node->findvalue('tx_import_risk_fees');
							$tx_purchase_fees = $prices_node->findvalue('tx_purchase_fees');
                            $tx_exchange_rate_fees = $prices_node->findvalue('tx_exchange_rate_fees');

							$tx_other_fees = $prices_node->findvalue('tx_other_fees');
							
							$tx_import_fees = '0' if ($tx_import_fees eq '');
							$tx_import_dpt_fees_by_shpt = '0' if ($tx_import_dpt_fees_by_shpt eq '');
							$tx_qc_fees = '0' if ($tx_qc_fees eq '');
							$tx_import_risk_fees = '0' if ($tx_import_risk_fees eq '');
							$tx_purchase_fees = '0' if ($tx_purchase_fees eq '');
                            $tx_exchange_rate_fees = '0' if ($tx_exchange_rate_fees eq '');
							$tx_other_fees = '0' if ($tx_other_fees eq '');
						}

						$sqlr = "
						UPDATE dau_sku SET
						sku_designation = '$sku_designation',
						sku_pcs = '$sku_pcs', 
						sku_ctns = '$sku_ctns', 
						sku_cbm = '$sku_cbm', 
						sku_kgs = '$sku_kgs',
						price_fob1 = '$price_fob1', 
						price_fob2 = '$price_fob2', 
						price_hub = CAST(REPLACE(TRIM('$price_hub'), ',', '.') AS NUMERIC), 
						id_currency = (SELECT id FROM ref_dau_currency WHERE designation = '$currency'), 
						exchange_rate = '$exchange_rate',
						import_fees_rate = '$tx_import_fees',
						qc_fees_rate = '$tx_qc_fees', 
						import_risk_fees_rate = '$tx_import_risk_fees',
						purchase_fees_rate = '$tx_purchase_fees',
						other_fees_rate = '$tx_other_fees', 
						exchange_rate_fob = '$exchange_rate_fob',
                        exchange_rate_fees = '$tx_exchange_rate_fees',
						import_dpt_fees_rate_by_shpt='$tx_import_dpt_fees_by_shpt'
						WHERE id = (
							SELECT id_sku
							FROM dau_sku_full as dsf
							WHERE uo_num = '$uo_num'
							AND po_root_num = '$po_root_num'
							AND po_num = '$po_num'
							AND bl_num = '$bl_num'
							AND sku_num = '$sku_num'
							AND sku_size = '$sku_size'
							AND sku_color = '$sku_color'
							AND id_export_level = '$id_export_level'
							GROUP BY id_sku
						)
						;";
						print $sqlr."\n" if ($dbug);
						$rs = $dbh->prepare( $sqlr );
	                    $rs->execute;
						
						$sqlr = "
						INSERT INTO dau_sku
						( id_bl, sku_num, sku_size, sku_color, sku_designation, 
						  sku_pcs, sku_ctns, sku_cbm, sku_kgs, 
						  price_fob1, price_fob2, price_hub, id_currency, exchange_rate, import_fees_rate, qc_fees_rate, import_risk_fees_rate, purchase_fees_rate, other_fees_rate, exchange_rate_fob,exchange_rate_fees,import_dpt_fees_rate_by_shpt
 
						 )
						SELECT 
						id_bl, 
						'$sku_num', 
						'$sku_size', 
						'$sku_color', 
						'$sku_designation',
						'$sku_pcs', 
						'$sku_ctns', 
						'$sku_cbm', 
						'$sku_kgs',
						'$price_fob1', 
						'$price_fob2', 
						CAST(REPLACE(TRIM('$price_hub'), ',', '.') AS NUMERIC), 
						(SELECT id FROM ref_dau_currency WHERE designation = '$currency') as id_currency, 
						'$exchange_rate' as echange_rate, 
						'$tx_import_fees' , 
						'$tx_qc_fees', 
						'$tx_import_risk_fees' , 
						'$tx_purchase_fees' , 
						'$tx_other_fees', 
						'$exchange_rate_fob', 
                        '$tx_exchange_rate_fees',
						'$tx_import_dpt_fees_by_shpt'

						FROM dau_sku_full as dsf
						WHERE uo_num = '$uo_num'
						AND po_root_num = '$po_root_num'
						AND po_num = '$po_num'
						AND bl_num = '$bl_num'
						AND 0=(
							SELECT COUNT(*)
							FROM dau_sku_full as dsf
							WHERE uo_num = '$uo_num'
							AND po_root_num = '$po_root_num'
							AND po_num = '$po_num'
							AND bl_num = '$bl_num'
							AND sku_num = '$sku_num'
							AND sku_size = '$sku_size'
							AND sku_color = '$sku_color'
							AND id_export_level = '$id_export_level'
						)
						AND id_export_level = '$id_export_level'
						GROUP BY id_bl
						;
							
						";
						print $sqlr."\n" if ($dbug);
                        $rs = $dbh->prepare( $sqlr );
	                    $rs->execute;
					}

					my $invoice_nodeset = $bl_node->findnodes('invoice');
						$sqlr = "
						DELETE FROM dau_bl_invoice
						WHERE id IN (
							SELECT id_bl_invoice
							FROM dau_invoice_full as dif
							WHERE uo_num = '$uo_num'
							AND po_root_num = '$po_root_num'
							AND po_num = '$po_num'
							AND bl_num = '$bl_num'
							AND LENGTH(invoice_num) = 0
							AND id_export_level = '$id_export_level'
							GROUP BY id_bl_invoice
						)
						;
					";
					$rs = $dbh->prepare( $sqlr );
	                $rs->execute;

					foreach my $invoice_node ($invoice_nodeset->get_nodelist) {
						my $cost_kind = uc($invoice_node->findvalue('@cost_kind'));
						my $invoice_amount = $invoice_node->findvalue('.');


						$sqlr = "
						INSERT INTO dau_bl_invoice
						( id_bl, cost_kind, invoice_amount, id_currency, exchange_rate, rate_freight )
						SELECT 
						id_bl, 
						'$cost_kind',
						'$invoice_amount',
						(SELECT id FROM ref_dau_currency WHERE designation = '$currency') as id_currency,
						'$exchange_rate',
						'$rate_freight'
						FROM dau_invoice_full as dif
						WHERE uo_num = '$uo_num'
						AND po_root_num = '$po_root_num'
						AND po_num = '$po_num'
						AND bl_num = '$bl_num'
						AND 0=(
							SELECT COUNT(*)
							FROM dau_invoice_full as dif
							WHERE uo_num = '$uo_num'
							AND po_root_num = '$po_root_num'
							AND po_num = '$po_num'
							AND bl_num = '$bl_num'
							AND cost_kind = '$cost_kind'
							AND LENGTH(invoice_num) = 0
							AND id_export_level = '$id_export_level'
						)
						AND id_export_level = '$id_export_level'
						GROUP BY id_bl
						;
							
						";
						
						print $sqlr."\n" if ($dbug);
						
						$rs = $dbh->prepare( $sqlr );
	                    $rs->execute;
						
					}
					
				}

			}
			
				
			if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
				print "\n". $dbh->errstr." \n $sqlr \n";
				$rs->finish;
				$dbh->rollback;
				$dbh->disconnect;
				exit;
			}
			print "\n sqlr \n" if ($dbug);

			$dbh->commit;
			$rs->finish;
			
		}
	
	}

	$dbh->{AutoCommit} = $old_autocommit;
	$dbh->disconnect;
	
	return 0;	
	
}


######################################
#   SCRIPTS DE PASSAGE DE PARAMETRES #
######################################

sub init(@ARGV){
		$log_msg .= "Debut du programme :".`date`."\n";
		if(scalar @ARGV eq 0) {
#				&init_error();
		} else {
				for ( my $i = 0 ; $i <= scalar @ARGV ; $i++ ){
								if (get_arg($ARGV[$i]) eq 'x' ){
									$xml_file = $ARGV[$i+1];
								} elsif (get_arg($ARGV[$i]) eq 'help' ){
									&init_error();
						}


				}
		}
		if ($xml_file eq '') {
#		  &init_error(); 
		}
}

sub init_error(){
	$log_msg .= "
			usage : import_charges.pl -x xml_file 
	";
		&exit_function;
}

sub get_arg(){
		my ($str_to_return) = @_;
		if (index( $str_to_return, '-' ) > -1){
		  $str_to_return = substr($str_to_return , 1, length($str_to_return));
		} else {
		  $str_to_return = 0
		}
		return ($str_to_return);
}

sub exit_function(){
	my $fichier_log = $dau_bin_directory."/import_charges.log";
	open LOGFILE, ">> $fichier_log" or die "Can't open $fichier_log";
		$log_msg .= "\nFin du programme.\n";
	if ($dbug) {
	  print $log_msg;
	} else {
	  print LOGFILE $log_msg;
	}
	close LOGFILE;
		exit;
}

