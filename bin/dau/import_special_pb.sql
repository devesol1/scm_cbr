SELECT
DISTINCT ON (list_po_invoice.po_num)
--main.invoice_num, 
'/home/fcs/cbryrocher/bin/dau/01_fcs_extract_dau.pl -p ',

' '||list_po_invoice.po_num,
'retour', 
'/home/fcs/cbryrocher/bin/dau/02_fcs_import_dau.pl',
'retour',
'/home/fcs/cbryrocher/bin/dau/03_qf_extract_dau.pl -p ',
' '||list_po_invoice.po_num,
' -d 0', 
'retour', 
'/home/fcs/cbryrocher/bin/dau/04_import_charges.pl -x QF_EXTRACT_ARRIVED_POD.XML'
'retour', 
'echo '||list_po_invoice.po_num||' >> /home/fcs/cbryrocher/bin/dau/import_special_pb.log ',
'retourretour', 
''
--list_po_invoice.dateheure_validated
FROM (

SELECT
invoice_num, 
CONCAT(';'||po_num||';') as concat_num_po, 
COUNT(*) as my_count
FROM (
SELECT
dbei.invoice_num, dbei.po_num
FROM dau_bl_e_invoice as dbei
--WHERE dbei.invoice_num LIKE 'FT283521%'
GROUP BY dbei.invoice_num, dbei.po_num
) as main
GROUP BY invoice_num
) as main
LEFT JOIN (
SELECT
dbei.invoice_num, 
dbei.po_num, 
MIN(dbei.dateheure_received) as dateheure_received, 
MIN(dbei.dateheure_validated) as dateheure_validated
FROM dau_bl_e_invoice as dbei
--WHERE dbei.invoice_num LIKE 'FT283521%'
GROUP BY dbei.invoice_num, dbei.po_num

) as list_po_invoice
ON list_po_invoice.invoice_num = main.invoice_num
WHERE my_count > 1
AND main.concat_num_po LIKE '%;4%'
AND main.concat_num_po LIKE '%;pb%'
AND list_po_invoice.po_num LIKE 'pb%'
AND TO_CHAR(list_po_invoice.dateheure_validated, 'YYYYMMDD')>= '20160101'
--ORDER BY TO_CHAR(list_po_invoice.dateheure_validated, 'YYYYMMDD')