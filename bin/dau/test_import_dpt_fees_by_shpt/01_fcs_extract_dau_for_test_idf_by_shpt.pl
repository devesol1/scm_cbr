#!/usr/bin/perl
#===========================================================================================
# Ce programme génère 3 fichiers XML:
# 1- un fichier FCS_EXTRACT_CHECKING.XML qui permettra de calculer dans Quot@Freight les couts
#    des charges de tous les POs qui sont CHECKING.
#    ON prend les POs qui ont été checkés depuis x jours
#    VU le 9 Septembre 2009 : Pour le DAU, a l'export "CHECKING" on utilise pas les coûts HUB-CPC
#    donc pour cette extraction nous n'avons pas de détail CPC et en import pour calcul des coûts
#    on prendra un CPC quelconque afin de construire des FICs standard, mais les coûts
#    Hub-ce CPC ne seront pas utilisés.
#    REMODIFIE le 21 SEPTEMBRE : on travaille au split
# 2- un fichier FCS_EXTRACT_ARRIVED_POD.XML qui permettra de calculer dans Quot@Freight les couts
#    des charges de tous les BLs qui sont entre "ARRIVED_POD" et "ARRIVED_FND", ceci pour contourner
#    la possibilité de passer du statut "ARRIVED_POD" au statut "ARRIVED_FND" dans la même journée.
#    MOFIFIE le 12/11/2009 : on prend tous les Pos sailing pour le maritime et sailing_arrived AOD pour l'aérien
# Ces 2 fichiers sont destinés à l'application QUOT@FREIGHT
#
#
# 3- un fichier FCS_EXTRACT_ARRIVED_POD_DAU.XML qui donnera pour les BLs arrivés ao port, la ventilation pays-reseau.
#    MOFIFIE le 12/11/2009 : on prend tous les Pos sailing pour le maritime et sailing_arrived AOD pour l'aérien
# Ce fichier est destiné à l'application CBR
#
# ENSUITE :
# 	- il envoie via ftp les fichiers sur la machine QUOT@FREIGHT et CBR
#===========================================================================================
#=============================================================================
# MODIFICATIONS : CFS/CY
# - XP  20/12/2013 Ajout de la fonction get_cbm_charge qui permet de calculer la ~1 en proratisant
# le container via une regle de 3 afin de remplir uniformémént les containers
#=============================================================================
# MODIFICATIONS : CFS/CY
# - XP  04/02/2014 Modification de la fonction get_cbm_charge, pour le PO courant, la régle de la cotation est utilisé
# cad 20' pour le hors Piramal et 40' pour le Piramal
#=============================================================================

use strict;

use DBI;
use POSIX;
use Class::Date qw(:errors date now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;
use XML::Writer;
use IO::File;
use Net::FTP;
use MIME::Lite;
use MIME::QuotedPrint;
use MIME::Base64;

print "\n 01_fcs_extract_dau.pl @ARGV ".now." \n";


# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;
use fcs_lib;
use fcs_fonctions_globales;

my $dbug = 1;
my $specific_uo = '';
my $specific_po = '';
my $specific_export_level = '';
my $is_cintyr = '';
my $log_msg;
&init();

my $bin_directory  = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau/";
my $dau_directory = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR') . "bin/dau/";
my $src_directory = $dau_directory . "src/";

my $src_file_checking = "FCS_EXTRACT_CHECKING.XML";
my $src_file_arrived_pod = "FCS_EXTRACT_ARRIVED_POD.XML";
my $src_file_arrived_pod_dau = "FCS_EXTRACT_ARRIVED_POD_DAU.XML";
my $fichier_log = $dau_directory . "fcs_extract_dau.log";
open( FICHIER_LOG, "> $fichier_log" ) || die "Impossible ouvrir fichier log !\n";
my $date_log = now;

print FICHIER_LOG "\n\nDEBUT TRAITEMENT du ".$date_log;

my @list_container =(20,40); #liste des containers possibles dans CFS/CY


# Tentative de connexion à la base FCS :
# ======================================
#XP 10/01
#my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_FCS_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
if ( $dbh eq undef ) {
  print FICHIER_LOG "\nERROR Connexion base FCS";
  close FICHIER_LOG;
  exit;
}
my $dbh_cot = DBI->connect( esolGetEnv(__FILE__, 'BDD_QF_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
if ( $dbh_cot eq undef ) {
  print FICHIER_LOG "\nERROR Connexion base Cot_yr";
  close FICHIER_LOG;
  exit;
}

# 1ere PARTIE le CHECKING
#========================
my $xml_file = $src_directory . $src_file_checking;

if (-e $xml_file){
  unlink($xml_file) or print FICHIER_LOG "erreur dans la suppression du fichier XML $src_file_checking\n";
}


my $requete = "  
SELECT
SPLIT_PART(ph.num_po,'_', 1) as fic,
TRIM(ph.num_po) as po,
CASE
  WHEN (ph.mode_transport = '1') THEN 'SEA'
  ELSE 'AIR'
END as transport,
TRIM(ph.uo_number) as uo,
TRIM(ph.div_acheteur) as dpt,
TRIM(ph.code_fournisseur) as fournisseur,
ph.incoterm as incoterm,
rl.designation as linerterm,
pt.date_min as etd,
CASE
  WHEN (ph.mode_transport = '1') THEN pt.mer_port_embarquement
  ELSE pt.air_aeroport_embarquement
END as pol,
CASE
  WHEN (ph.mode_transport = '1') THEN pt.mer_port_arrivee
  ELSE pt.air_aeroport_arrivee
END as pod,
TRIM(pt.equipement) as equipment,
TRIM(hub.cot_fnd) as hub,
pt.date_arrivee_entrepot as mad_hub,
pt.monnaie as currency,

TRIM(pd.set_article_acheteur) as sku,
TRIM(pd.taille) as size,
TRIM(pd.couleur) as color,
TRIM(pd.libelle_set_article) as name,
pd.unit_cost as cost,
pd.master_units as ctn,
pd.item_volume_total as cbm,
pd.item_poids_total as kgs,
pd.total_units as qty

FROM po_detail pd
LEFT JOIN po_transport pt
ON pt.num_po = pd.num_po
LEFT JOIN po_header ph
ON ph.num_po = pd.num_po
LEFT JOIN ref_entrepot hub
ON hub.code_entrepot = pt.rs_arrivee_entrepot
LEFT JOIN ref_linerterms rl
ON rl.id::text = pt.mer_linerterm

-- ON NE PREND QUE SI ON A UN UO
WHERE ph.uo_number is NOT NULL
AND TRIM(ph.uo_number) <> ''
-- ON NE PREND QUE SI ON A UN HUB COTATION
AND TRIM(hub.cot_fnd) <> ''
AND ( ph.mode_transport = '1' OR ph.mode_transport = '4' )
AND TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) <> '4102576r35'

";
if ( $specific_uo eq '' && $specific_po eq '' ) {
$requete .= "
AND TRIM(ph.date_validation) >  TO_CHAR((CURRENT_DATE-5), 'YYYYMMDD')
";
} else {
$requete .= "
AND ( '$specific_po' = '' OR TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) LIKE SPLIT_PART(LOWER('$specific_po'), '_', 1) ||'%' )
AND ( '$specific_uo' = '' OR TRIM(ph.uo_number) = '$specific_uo' )
";
}
$requete .= "
ORDER BY po, sku, size, color
";

print $requete if ($dbug);

my $result = $dbh->prepare( $requete );
$result->execute();
if ( $dbh->errstr ne undef ) { # Erreur SQL
  print FICHIER_LOG "\nERROR : $requete";
  close FICHIER_LOG;
  $result->finish;
  $dbh->disconnect;
  exit;
}

my $nb_po_root = $result->rows;
print FICHIER_LOG "\n==> $nb_po_root POs CHECKING à traiter";

if ( $nb_po_root > 0 && ( $is_cintyr ||  $specific_export_level eq '2' || $specific_export_level eq '') ) {
  my $output = new IO::File (">".$xml_file);
  print $output "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
  my $writer = new XML::Writer(OUTPUT => $output);
  $writer->startTag("fics");

  my $key_fic = '';
  my $key_po = '';
  my $key_sku = '';
	while ( my $tableau = $result->fetchrow_hashref) {
	  
		my $linerterm = 'FCL';
		
		if ( $tableau->{'linerterm'} eq 'LCL' ) {
			$linerterm = 'LCL';
		}
		
		if ( $tableau->{'linerterm'} eq 'CFS/CY' ) {
			$linerterm = 'CFS/CY';
		}
		
		my $ctn = $tableau->{'ctn'};
		if ( $ctn eq '0' || $ctn eq 'Null' || $ctn eq '' ) { $ctn = '1'; }
		my $ctn_long = $tableau->{'cbm'} * 1000000 / $ctn;
		my $ctn_large = 1;
		my $ctn_height = 1;
		my $ctn_weight = $tableau->{'kgs'} / $ctn;
		my $pcs_per_ctn = int( $tableau->{'qty'} / $ctn); 

		if ( $key_fic ne $tableau->{'fic'} && $key_fic ne '' ) {
			$writer->endTag("sku");
			$writer->endTag("shpt");
			$writer->endTag("fic");
		}
		my $fic = uc($tableau->{'fic'}).'~1';
		$fic .='B' if($is_cintyr);
		
		if ( $key_fic ne $tableau->{'fic'} ) {
			$writer->startTag(
				"fic",
				"fic" => $fic,
				"uo"  => $tableau->{'uo'},
				"dpt" => $tableau->{'dpt'}
			);
			$key_fic = $tableau->{'fic'};
			$key_po = '';
			$key_sku = '';
		}

		if ( $key_po ne $tableau->{'po'} && $key_po ne '' ) {
		  $writer->endTag("sku");
		  $writer->endTag("shpt");
		}
		if ( $key_po ne $tableau->{'po'} ) {
			$writer->startTag("shpt",
				"transport" => $tableau->{'transport'},
				"id" => uc($tableau->{'po'})."__". uc($tableau->{'po'}),
				"incoterm" => $tableau->{'incoterm'},
				"pol" => $tableau->{'pol'},
				"pod" => $tableau->{'pod'},
				"etd" => $tableau->{'etd'},
				"linerterm" => $linerterm,
				"hub" => $tableau->{'hub'},
				"mad_hub" => $tableau->{'mad_hub'},
				"currency" => $tableau->{'currency'}
			);
			$key_po = $tableau->{'po'};
			$key_sku = '';

			if ( $tableau->{'transport'} eq 'SEA' && $tableau->{'equipment'} ne '' ) {
				my @tab = split(";" , $tableau->{'equipment'} );
				
				for ( my $i = 0 ; $i < scalar @tab ; $i++ ) {
					my @type_qty = split("x" , $tab[$i]);
					if($linerterm eq 'CFS/CY'){
						$writer->startTag(
							"equipment",
							"type" => $type_qty[0],
							"qty" => $type_qty[1],
							"cbm_charge"=>&get_cbm_charge($dbh,$dbh_cot,$tableau->{'po'}, $type_qty[1], $type_qty[0],$tableau->{'fournisseur'})
						);
					} else {
						$writer->startTag(
							"equipment",
							"type" => $type_qty[0],
							"qty" => $type_qty[1]
						);
					}
					$writer->endTag("equipment");
				}
			}
		}

		if ( $key_sku ne ( $tableau->{'sku'} . $tableau->{'size'} .  $tableau->{'color'} ) && $key_sku ne '' ) {
		  $writer->endTag("sku");
		}
		if ( $key_sku ne ( $tableau->{'sku'} . $tableau->{'size'} .  $tableau->{'color'} ) ) {
			my $libelle_sku =  $tableau->{'name'};
			$libelle_sku =~ s/é/e/g;
			$libelle_sku =~ s/è/e/g;
			$libelle_sku =~ s/à/a/g;
			$libelle_sku =~ s/ç/c/g;
			$libelle_sku =~ s/\'/\ /g;

			$writer->startTag(
				"sku",
				"id" => $tableau->{'sku'},
				"size" => $tableau->{'size'},
				"color" => $tableau->{'color'},
				"name" => $libelle_sku,
				"price" => $tableau->{'cost'},
				"ctn" => $ctn,
				"ctn_long" => $ctn_long,
				"ctn_large" => $ctn_large,
				"ctn_height" => $ctn_height,
				"ctn_weight" => $ctn_weight,
				"pcs_per_ctn" => $pcs_per_ctn,
				"qty" => $tableau->{'qty'}
			);
			$key_sku = $tableau->{'sku'} . $tableau->{'size'} . $tableau->{'color'};

			# ON RECHERCHE pour CHAQUE SKU, TOUTES LES INFORMATIONS permettant de créer:
			# - input_model_sku_fnds
			# - logistic_routings_detail
			my $sqlfnd = " 
				SELECT
				TRIM(re.delivery_platform) as cot_cpc,
				pdf.estimed_date_arrived as mad_cpc,
				SUM(pdf.quantites) as qty
				FROM po_detail_fnd pdf
				LEFT JOIN ref_entrepot re
				ON re.code_entrepot = pdf.entrepot
				WHERE pdf.num_po = ?
				AND pdf.sku_id = ?
				AND pdf.sku_size = ?
				AND pdf.sku_color = ?
				AND TRIM(re.delivery_platform) <> ''
				GROUP by re.delivery_platform, pdf.estimed_date_arrived
			";
			my $reqfnd = $dbh->prepare( $sqlfnd );
			$reqfnd->execute( $tableau->{'po'},$tableau->{'sku'}, $tableau->{'size'}, $tableau->{'color'}  );
		  
			print $sqlfnd."\n".$tableau->{'po'}.",".$tableau->{'sku'}.",". $tableau->{'size'}.",". $tableau->{'color'}."\n";
			if ( $dbh->errstr ne undef ) { # Erreur SQL
				print FICHIER_LOG "\nERROR : $sqlfnd";
				close FICHIER_LOG;
				$reqfnd->finish;
				$result->finish;
				$dbh->disconnect;
				exit;
			}
			while ( my $tabfnd = $reqfnd->fetchrow_hashref ) {
				my $cot_cpc = $tabfnd->{'cot_cpc'};
				my $mad_cpc = $tabfnd->{'mad_cpc'};
				my $qty = $tabfnd->{'qty'};
				$writer->startTag(
					"cpc",
					"cpc" => $cot_cpc,
					"mad_cpc" => $mad_cpc,
					"qty" => $qty
				);
				print "cpc : ".$cot_cpc."\n";
				print "mad_cpc : ".$mad_cpc."\n";
				print "qty : ".$qty."\n";
				$writer->endTag("cpc");
			}
		}
	
	}
	$writer->endTag("sku");
	$writer->endTag("shpt");
	$writer->endTag("fic");
	$writer->endTag("fics");
	$writer->end();
	$output->close();
}
$result->finish;

# 2de PARTIE l'ARRIVED_POD
#=========================
my $xml_file = $src_directory . $src_file_arrived_pod;

if (-e $xml_file){
  unlink($xml_file) or print FICHIER_LOG "erreur dans la suppression du fichier XML $src_file_checking\n";
}


my $requete = "  
SELECT
DISTINCT ON (tcpm.num_po, tcpm.bill_of_lading)
SPLIT_PART(tcpm.num_po,'_', 1) as po_root,
TRIM(tcpm.num_po) as po,
'SEA' as transport,
TRIM(ph.uo_number) as uo,
TRIM(ph.div_acheteur) as dpt,
TRIM(tcpm.bill_of_lading) as bl,
CASE WHEN TRIM(tcpm.incoterm) = '' THEN ph.incoterm
ELSE tcpm.incoterm
END as incoterm,
--ph.incoterm as incoterm,
tcpm.pol as pol,
tcpm.pod as pod,
tcpm.etd as etd,
TRIM(tcpm.linerterm) as linerterm,
-- ON RECUPERE LES CODES FND FCS ET COTATION
re.cot_fnd as cot_fnd,
pt.date_arrivee_entrepot as mad,
pt.monnaie as currency,
TRIM(ph.code_fournisseur) as fournisseur,
( SELECT count(*)
 FROM tracing_container_po_mer t
 WHERE t.bill_of_lading = tcpm.bill_of_lading
 AND t.num_po <> tcpm.num_po
 AND (t.statut = 'S' OR t.statut = 'P' OR t.statut = 'D' OR t.statut = 'R' OR t.statut = 'F')
 GROUP BY t.bill_of_lading
) as other_po


FROM tracing_container_po_mer tcpm
LEFT JOIN po_header ph
ON ph.num_po = tcpm.num_po
LEFT JOIN po_transport pt
ON pt.num_po = ph.num_po
LEFT JOIN ref_entrepot re
ON re.code_entrepot = pt.rs_arrivee_entrepot
LEFT JOIN floating_split_bl fsb
ON fsb.bl = tcpm.bill_of_lading
WHERE ph.uo_number is NOT NULL
AND TRIM(ph.uo_number) <> ''
";
if ( $specific_uo eq '' && $specific_po eq '' ) {
$requete .= "
AND (
      (tcpm.statut IN ('S', 'P', 'D', 'R')
      OR
      (tcpm.statut = 'F' AND SPLIT_PART(tcpm.num_po,'_', 1)
        IN ( SELECT  SPLIT_PART(num_po, '_', 1) FROM tracing_container_po_mer
             WHERE statut IN ('S', 'P', 'D', 'R')
	     UNION 
	     SELECT  SPLIT_PART(num_po, '_', 1) FROM tracing_air_po_hawb
	     WHERE statut IN ('S', 'P', 'D', 'R')
           )
      )
   )
 ) 
";
} else {
$requete .= " 
AND ( '$specific_po' = '' OR TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) = SPLIT_PART(LOWER('$specific_po'), '_', 1) )
AND ( '$specific_uo' = '' OR TRIM(ph.uo_number) = '$specific_uo' )
-- POUR prendre tout le flottant et plus
AND ( tcpm.statut = 'S' OR tcpm.statut = 'P' OR tcpm.statut = 'D' OR tcpm.statut = 'R' OR tcpm.statut = 'F')
";
}
$requete .= "
-- ON NE PREND QUE SI IL Y A DU FLOTTANT POUR LE MARITIME
AND (fsb.id::text) <> ''
-- ON NE PREND QUE SI ON A UN ENTEPOT COTATION
AND TRIM(re.cot_fnd) <> ''

UNION
SELECT
DISTINCT ON (taph.num_po, taph.awb)
SPLIT_PART(taph.num_po,'_', 1) as po_root,
TRIM(taph.num_po) as po,
'AIR' as transport,
TRIM(ph.uo_number) as uo,
TRIM(ph.div_acheteur) as dpt,
TRIM(taph.awb) as bl,
ph.incoterm as incoterm,
taph.aol as pol,
taph.aod as pod,
taph.aeroport_etd as etd,
'LCL' as linerterm,
-- ON RECUPERE LES CODES FND FCS ET COTATION
re.cot_fnd as cot_fnd,
pt.date_arrivee_entrepot as mad,
pt.monnaie as currency,
TRIM(ph.code_fournisseur) as fournisseur,
'0' as other_po


FROM tracing_air_po_hawb taph
LEFT JOIN po_header ph
ON ph.num_po = taph.num_po
LEFT JOIN po_transport pt
ON pt.num_po = ph.num_po
LEFT JOIN ref_entrepot re
ON re.code_entrepot = pt.rs_arrivee_entrepot
WHERE ph.uo_number is NOT NULL
AND TRIM(ph.uo_number) <> ''
";
if ( $specific_uo eq '' && $specific_po eq '' ) {
$requete .= "
AND (
      (taph.statut IN ('S', 'P', 'D','R') )
      OR
      (taph.statut = 'F' AND SPLIT_PART(taph.num_po,'_', 1)
        IN ( SELECT  SPLIT_PART(num_po, '_', 1) FROM tracing_air_po_hawb
             WHERE statut IN ('S', 'P', 'D', 'R')
	     UNION
	     SELECT  SPLIT_PART(num_po, '_', 1) FROM tracing_container_po_mer
	     WHERE statut IN ('S', 'P', 'D', 'R')

           )
      )

)
";
} else {
$requete .= "
AND ( '$specific_po' = '' OR TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) = SPLIT_PART(LOWER('$specific_po'), '_', 1) )
AND ( '$specific_uo' = '' OR TRIM(ph.uo_number) = '$specific_uo' )
-- POUR prendre tout le flottant et plus
AND ( taph.statut = 'S' OR taph.statut = 'P' OR taph.statut = 'D' OR taph.statut = 'R' OR taph.statut = 'F')
AND TRIM(re.cot_fnd) <> ''
";
}
$requete .= "
ORDER BY po_root, bl, etd
";

print $requete if ($dbug);
my $result = $dbh->prepare( $requete );
$result->execute();
if ( $dbh->errstr ne undef ) { # Erreur SQL
  print FICHIER_LOG "\nERROR : $requete";
  close FICHIER_LOG;
  $result->finish;
  $dbh->disconnect;
  exit;
}
my $fournissseur;
my @loop_fic = ();
my $nb_po_root = $result->rows;
print FICHIER_LOG "\n==> $nb_po_root POs ARRIVED_POD à traiter";

while ( my $tableau = $result->fetchrow_hashref) {
  push (@loop_fic, $tableau->{'po_root'});
  push (@loop_fic, $tableau->{'po'});
  push (@loop_fic, $tableau->{'transport'});
  push (@loop_fic, $tableau->{'uo'});
  push (@loop_fic, $tableau->{'dpt'});
  push (@loop_fic, $tableau->{'bl'});
  push (@loop_fic, $tableau->{'incoterm'});
  push (@loop_fic, $tableau->{'pol'});
  push (@loop_fic, $tableau->{'pod'});
  push (@loop_fic, $tableau->{'etd'});
  push (@loop_fic, $tableau->{'linerterm'});
  push (@loop_fic, $tableau->{'cot_fnd'});
  push (@loop_fic, $tableau->{'mad'});
  push (@loop_fic, $tableau->{'currency'});
  push (@loop_fic, $tableau->{'fournisseur'});
  push (@loop_fic, $tableau->{'other_po'});
}
$result->finish;

# ON lance le TRAITEMENT si la liste n'est pas vide
# =================================================
if ( $nb_po_root > 0 && ( $specific_export_level eq '3' || $specific_export_level eq '')) {
  my $output = new IO::File (">".$xml_file);
  print $output "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
  my $writer = new XML::Writer(OUTPUT => $output);
  $writer->startTag("fics");

  my $j = 0;
  my $last_fic = '';
  for ( my $i = 0; $i < $nb_po_root; $i++ ) {
    my $fic = $loop_fic[$j].'~2';
	$fic .='B' if($is_cintyr);
	$j++;
    my $po = $loop_fic[$j]; $j++;
    my $transport = $loop_fic[$j]; $j++;
    my $uo = $loop_fic[$j]; $j++;
    my $dpt = $loop_fic[$j]; $j++;
    my $bl = $loop_fic[$j]; $j++;
    my $incoterm = $loop_fic[$j]; $j++;
    my $pol = $loop_fic[$j]; $j++;
    my $pod = $loop_fic[$j]; $j++;
    my $etd = $loop_fic[$j]; $j++;
		
	my $linerterm = 'FCL';
	
    if ( $loop_fic[$j] eq 'LCL') {
      $linerterm = $loop_fic[$j];
    }
 
    if ($loop_fic[$j] eq 'CFS/CY' || $loop_fic[$j] eq 'CFSCY')
	{
	$linerterm='CFS/CY';
	}
 
	
    # AJOUTE le 3 Juillet 2012, gestion du CFA
	

		#if ( $incoterm == '02' ) { $linerterm = 'FCL'; } # FCA
		$j++;
		my $cot_fnd = $loop_fic[$j]; $j++;
		my $mad= $loop_fic[$j]; $j++;
		my $currency= $loop_fic[$j]; $j++;
		my $fournissseur=$loop_fic[$j]; $j++;
        
		my $is_other_po_in_bl = $loop_fic[$j]; $j++;
		if ($is_other_po_in_bl eq '' || $is_other_po_in_bl eq 'Null' || $is_other_po_in_bl eq '0') {}
		else  { # CONSO FOURNISSEUR		  
		  if ($incoterm ne '02' && $linerterm ne 'CFS/CY' )  { $linerterm = 'FCL'; $incoterm = '02'; }
		}
  
	
    my $po_root = substr($fic,0,10);

    if ( $last_fic ne $fic && $last_fic ne '') {
      $writer->endTag("fic");
    }

    if ( $last_fic ne $fic ) {
      $writer->startTag("fic",
                        "fic" => uc($fic),
                        "uo" => $uo,
                        "dpt" => $dpt
      );
      $last_fic = $fic;
    }

    $writer->startTag("shpt",
                       "transport" => $transport,
                       "id" => $po."__".$bl,
                       "incoterm" => $incoterm,
                       "pol" => $pol,
                       "pod" => $pod,
                       "etd" => $etd,
                       "linerterm" => $linerterm,
                       "hub" => $cot_fnd,
                       "mad_hub" => $mad,
                       "currency" => $currency
    );

    # ON RECHERCHE pour CHAQUE BL, LES EQUIPEMENTS si FCL
    #
    if ( $linerterm eq 'FCL' || $linerterm eq 'CFS/CY' ) {
		my $sqlcharge= "
		SELECT
		sum(tcdm.volume)
		FROM tracing_container_detail_mer tcdm
		WHERE tcdm.container in ( SELECT
			tcpm.container
			FROM tracing_container_po_mer tcpm
			WHERE TRIM(tcpm.num_po) = ?
			AND tcpm.bill_of_lading = ?
			AND tcpm.type_container = ?
		)
		";

		if ( $linerterm eq 'CFS/CY' && $fournissseur eq 'Piramal')
		{
		  $sqlcharge= "
					SELECT
						sum(tcdm.volume) as volume_totale,
						sum(tcdm.sku_ctn) as nb_pallets
						FROM tracing_container_detail_mer tcdm
						WHERE tcdm.container in ( SELECT
							tcpm.container
							FROM tracing_container_po_mer tcpm
							WHERE TRIM(tcpm.num_po) = ?
							AND tcpm.bill_of_lading = ?
							AND tcpm.type_container = ?
						)"
							
		}

      my $sql = " SELECT
					TRIM(tcpm.type_container) as type,
				    count(*) as qty
					FROM tracing_container_po_mer tcpm
					WHERE TRIM(tcpm.num_po) = ?
					AND tcpm.bill_of_lading = ?
				    GROUP BY tcpm.type_container 
				    ORDER BY tcpm.type_container
                ";
      my $req = $dbh->prepare( $sql );
      $req->execute( $po, $bl);
      if ( $dbh->errstr ne undef ) { # Erreur SQL
        print FICHIER_LOG "\nERROR : $sql";
        close FICHIER_LOG;
        $req->finish;
        $dbh->disconnect;
        exit;
      }
      if ( $req->rows > 0 ) { 
      	while ( my $tabct = $req->fetchrow_hashref ) {
          my $reqcharge = $dbh->prepare( $sqlcharge );
          $reqcharge->execute( $po, $bl, $tabct->{'type'} );
          if ( $dbh->errstr ne undef ) { # Erreur SQL
            print FICHIER_LOG "\nERROR : $sqlcharge";
            close FICHIER_LOG;
            $reqcharge->finish;
            $req->finish;
            $dbh->disconnect;
            exit;
          }
		  
        my $cbm_charge;
		if ( $linerterm eq 'CFS/CY' && $fournissseur eq 'Piramal')
			{
			
				my $volume_totale;
				my $nb_pallets;
				my $nb_pallets_ctn;
				
				while ( my $data = $reqcharge->fetchrow_hashref ) {
						$volume_totale = $data->{'volume_totale'};
						$nb_pallets = $data->{'nb_pallets'};
						}
						
					if ($tabct->{'type'} eq '40')
					{$nb_pallets_ctn=40;}
					if ($tabct->{'type'} eq '20')
					{$nb_pallets_ctn=20;}
							
					 $cbm_charge = ($volume_totale*$nb_pallets_ctn*$tabct->{'qty'})/$nb_pallets;
                     print "\n ~2  \n" if($dbug);
					 print " volume_totale $volume_totale " if($dbug);
					 print " nb_pallets_ctn $nb_pallets_ctn " if($dbug);
					 print " tabct->{'qty'}  $tabct->{'qty'} " if($dbug);
					 print  " nb_pallets  $nb_pallets " if($dbug);
					 print  "cbm_charge $cbm_charge " if($dbug);
				}
			
		    #if ( $linerterm ne'CFS/CY' && $fournissseur ne 'Piramal')
			else
			{
			  $cbm_charge = $reqcharge->fetchrow;
			  print "\n ~2  \n" if($dbug);
			  print  "cbm_charge $cbm_charge " if($dbug);
			}
		  
          $reqcharge->finish;

		  
		  
      	  $writer->startTag("equipment",
	                    "type" => $tabct->{'type'},
	  		    "qty" => $tabct->{'qty'},
	  		    "cbm_charge" => $cbm_charge
                           );
          $writer->endTag("equipment");
		  
        }
        $req->finish;
      } 
    }

    # ON RECHERCHE pour CHAQUE PO_ROOT/BL, TOUTES LES INFORMATIONS permettant de créer:
    # - input_model_header
    # - input_model_skus
    # - logistic_routing_scenarios
    # - logistic_routing_shpt
    # - logistic_routing_price
    # =========================================================
    my $sql = '';
    if ( $transport eq 'SEA' ) {
      $sql = " SELECT
    		TRIM(tcdm.sku_id) as sku,
    		TRIM(tcdm.sku_size) as size,
    		TRIM(tcdm.sku_color) as color,
    		TRIM(tcdm.sku_name) as name,
		pd.unit_cost as price,
    		SUM(tcdm.sku_pc) as pcs,
    		SUM(tcdm.sku_ctn) as ctn,
		-- ON PREND LES VOLUMES ET POIDS DANS tracing_container_detail_mer car il peut y yavoir plusieurs types de carton
		-- on recalculera à partir de ces donnees :
		-- le volume ( volume total / nb carton ) = longueur * largeur = 1 * hauteur = 1 
		-- le poids ( poids total / nb carton )
		SUM(tcdm.volume) as cbm,
		SUM(tcdm.poids) as kgs
    		FROM tracing_container_detail_mer tcdm
    		LEFT JOIN tracing_container_po_mer tcpm
    		ON tcpm.num_po = tcdm.num_po
    		AND tcpm.container = tcdm.container
		LEFT JOIN po_detail pd
		ON pd.num_po = tcdm.num_po
		AND pd.set_article_acheteur = tcdm.sku_id
		AND pd.taille = tcdm.sku_size
		AND pd.couleur = tcdm.sku_color
    		WHERE TRIM(tcdm.num_po) = ?
    		AND tcpm.bill_of_lading = ?
		GROUP BY sku, size, color, name, price
      ";
    }
    if ( $transport eq 'AIR' ) {
      $sql = " SELECT
    		TRIM(tapd.item_buyer_code) as sku,
    		TRIM(tapd.item_size) as size,
    		TRIM(tapd.item_color) as color,
    		TRIM(pd.libelle_set_article) as name,
		pd.unit_cost as price,
    		SUM(tapd.item_pcs) as pcs,
    		SUM(tapd.item_ctn) as ctn,
		-- ON PREND LES VOLUMES ET POIDS DANS tracing_air_po_detail_mer car il peut y yavoir plusieurs types de carton
		-- on recalculera à partir de ces donnees :
		-- le volume ( volume total / nb carton ) = longueur * largeur = 1 * hauteur = 1 
		-- le poids ( poids total / nb carton )
		SUM(tapd.item_cbm) as cbm,
		SUM(tapd.item_kgs) as kgs
    		FROM tracing_air_po_detail tapd
    		LEFT JOIN tracing_air_po_hawb taph
    		ON taph.id_tracing_air_po_hawb = tapd.id_tracing_air_po_hawb
			AND tapd.item_pcs > 0

		LEFT JOIN po_detail pd
		ON pd.num_po = taph.num_po
		AND pd.set_article_acheteur = tapd.item_buyer_code
		AND pd.taille = tapd.item_size
		AND pd.couleur = tapd.item_color
    		WHERE TRIM(taph.num_po) = ?
    		AND taph.awb = ?
		GROUP BY sku, size, color, name, price
      ";
    }

	
	print "$sql \n $po, $bl \n" if($dbug);
    my $req = $dbh->prepare( $sql );
    $req->execute( $po, $bl);
    if ( $dbh->errstr ne undef ) { # Erreur SQL
      print FICHIER_LOG "\nERROR : $sql";
      close FICHIER_LOG;
      $req->finish;
      $dbh->disconnect;
      exit;
    }

    my $last_sku_key = '';
    while ( my $tableau = $req->fetchrow_hashref ) {
      my $sku = $tableau->{'sku'};
      my $size = $tableau->{'size'};
      my $color = $tableau->{'color'};
      my $sku_key = $sku.$size.$color;
      my $name = $tableau->{'name'};
      $name =~ s/é/e/g;
      $name =~ s/è/e/g;
      $name =~ s/à/a/g;
      $name =~ s/ç/c/g;
      $name =~ s/\'/\ /g;
      my $price = $tableau->{'price'};
      my $pcs = $tableau->{'pcs'};
      my $ctn = $tableau->{'ctn'};
      my $cbm = $tableau->{'cbm'};
      my $kgs = $tableau->{'kgs'};
	  print "ctn $ctn\n" if($dbug);
      if ( $ctn eq '0' || $ctn eq 'Null' || $ctn eq '' ) { $ctn = '1'; }
      my $ctn_long = $cbm * 1000000 / $ctn;
      my $ctn_large = 1;
      my $ctn_height = 1;
      my $ctn_weight = $kgs / $ctn;
      my $pcs_per_ctn = int( $pcs / $ctn);

		print "$last_sku_key ne $sku_key \n" if($dbug);
      if ( $last_sku_key ne $sku_key && $last_sku_key ne '') {
        $writer->endTag("sku");
      }
      if ( $last_sku_key ne $sku_key ) {
        $writer->startTag("sku",
                          "id" => $sku,
                          "size" => $size,
                          "color" => $color,
                          "name" => $name,
                          "price" => $price,
                          "ctn" => $ctn,
                          "ctn_long" => $ctn_long,
                          "ctn_large" => $ctn_large,
                          "ctn_height" => $ctn_height,
                          "ctn_weight" => $ctn_weight,
			  "pcs_per_ctn" => $pcs_per_ctn,
                          "qty" => $pcs
        );
      }

      # ON RECHERCHE pour CHAQUE SKU, TOUTES LES INFORMATIONS permettant de créer:
      # - input_model_sku_fnds
      # - logistic_routings_detail
      # UNIQUEMENT pour le MARITIME, JAMAIS pour l'AERIEN
      my $sqlfnd ='';
      my $reqfnd ='';
      if ( $transport eq 'SEA' ) {
        $sqlfnd = " SELECT
 		  -- ON RECUPERE LES CODES FND COTATION
      		  TRIM(re.delivery_platform) as cot_cpc,
		  fsf.estimed_date_arrived as mad_cpc,
		  SUM(fsf.qty) as qty
      		  FROM floating_split_fnd fsf
		  LEFT JOIN floating_split_sku fss
		  ON fss.id = fsf.id_sku
		  LEFT JOIN floating_split_bl fsb
		  ON fsb.id = fss.id_bl
		  LEFT JOIN ref_entrepot re
		  ON re.code_entrepot = fsf.fnd
		  WHERE fsb.bl = ?
		  AND TRIM(fss.po) = ?
		  AND fss.sku_id = ?
		  AND fss.sku_size = ?
		  AND fss.sku_color = ?
		  AND TRIM(re.delivery_platform) <> ''
		  GROUP by re.delivery_platform, fsf.estimed_date_arrived 
        ";
      }
      if ( $transport eq 'AIR' ) {
        $sqlfnd = " SELECT
		TRIM(re.delivery_platform) as cot_cpc,
                fsf.estimed_date_arrived as mad_cpc,
		SUM(fsf.qty) as qty
		FROM floating_split_fnd fsf
		LEFT JOIN floating_split_sku fss
		ON fss.id = fsf.id_sku
		LEFT JOIN floating_split_bl fsb
		ON fsb.id = fss.id_bl
		LEFT JOIN ref_entrepot re
		ON re.code_entrepot = fsf.fnd
		WHERE SUBSTR(fsb.bl,5,LENGTH(fsb.bl)) = ? 
		AND TRIM(fss.po) = ?
		AND fss.sku_id = ?
		AND fss.sku_size = ?
		AND fss.sku_color = ?
		AND TRIM(re.delivery_platform) <> ''
		GROUP by re.delivery_platform, fsf.estimed_date_arrived
        ";
		
      }
		print "noeud cpc arrived_pod \n  $sqlfnd \n $bl, $po, $sku, $size, $color \n" if($dbug);
        $reqfnd = $dbh->prepare( $sqlfnd );
        $reqfnd->execute( $bl, $po, $sku, $size, $color );
      if ( $dbh->errstr ne undef ) { # Erreur SQL
        print FICHIER_LOG "\nERROR : $sqlfnd";
        close FICHIER_LOG;
        $reqfnd->finish;
        $req->finish;
        $dbh->disconnect;
        exit;
      }

      while ( my $tabfnd = $reqfnd->fetchrow_hashref ) {
        my $cot_cpc = $tabfnd->{'cot_cpc'};
		print "cot_cpc $cot_cpc \n" if($dbug);
        my $mad_cpc = $tabfnd->{'mad_cpc'};
        my $qty = $tabfnd->{'qty'};
        $writer->startTag("cpc",
                          "cpc" => $cot_cpc,
                          "mad_cpc" => $mad_cpc,
                          "qty" => $qty
        );
        $writer->endTag("cpc");
      }
      $reqfnd->finish;
      $writer->endTag("sku");
    }
    $req->finish;

    $writer->endTag("shpt");

  }
  $writer->endTag("fic");
  $writer->endTag("fics");
  $writer->end();
  $output->close();

}

my $sqlct = "
SELECT type_container, container
FROM tracing_container_po_mer
WHERE num_po = lower(?)
AND bill_of_lading = ?
";
my $rqlct = $dbh->prepare( $sqlct );

# 3eme PARTIE l'ARRIVED POD pour DAU (PRELEVEMENTS PAYS)
# ==================================
my $xml_file = $src_directory . $src_file_arrived_pod_dau;
if (-e $xml_file){
  unlink($xml_file) or print FICHIER_LOG "erreur dans la suppression du fichier XML $src_file_arrived_pod_dau\n";
}
# Cette requete concerne les cadeaux, on fait un regoupement pays/madpays
# Quand on traitera les produits industriels, il faudra faire une seconde requete en prenant en compte les pays/madpays
my $sql = "
SELECT
ph.uo_number as uo,
UPPER(SPLIT_PART(ph.num_po,'_', 1)) as fic,
UPPER(ph.num_po) as po,
TRIM(fsb.bl) as bl,
etd,
eta,
arrival,
ph.mode_transport as transport,
fss.sku_id as sku,
fss.sku_size as size,
fss.sku_color as color,
SUBSTR('',1,0) as pays_reso,
SUBSTR('',1,0) as date,
SUM(fsf.qty) as qty
FROM po_header as ph
LEFT JOIN po_transport as pt
ON ph.num_po = pt.num_po
LEFT JOIN ref_entrepot re
ON re.code_entrepot = pt.rs_arrivee_entrepot
LEFT JOIN (
  SELECT DISTINCT
    bill_of_lading as bl, num_po, etd, eta, 
	CASE 
	WHEN arrival_pod IS NOT NULL AND LENGTH(arrival_pod)> 0 THEN arrival_pod 
	WHEN fnd_arrival_date IS NOT NULL AND LENGTH(fnd_arrival_date)> 0 THEN fnd_arrival_date 
	END as arrival
    FROM tracing_container_po_mer
    WHERE statut IN ('S', 'P', 'D', 'R')
    -- ajoute le 12/11/2012 pour SPDR saisies le même jour
    OR fnd_arrival_date >= (TO_CHAR(NOW()- interval '30 day', 'YYYYMMDD') )
) as tracing
 ON ph.num_po = tracing.num_po
LEFT JOIN floating_split_bl fsb
ON tracing.bl = fsb.bl
LEFT JOIN floating_split_sku fss
ON fss.id_bl = fsb.id
AND fss.po = ph.num_po
LEFT JOIN floating_split_fnd fsf
ON fsf.id_sku = fss.id
WHERE LENGTH(TRIM(ph.uo_number)) > 0
AND ( '$specific_uo' = '' OR TRIM(ph.uo_number) = '$specific_uo' )
AND ( '$specific_po' = '' OR TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) = SPLIT_PART(LOWER('$specific_po'), '_', 1) )
AND SUBSTR(ph.div_acheteur,1,4) <> 'Ind_'
AND fsb.id IS NOT NULL
AND fss.sku_id IS NOT NULL
AND (SUBSTR(ph.num_po,1,2) <> 'pb')
-- ON NE PREND QUE SI ON A UN ENTEPOT COTATION
AND TRIM(re.cot_fnd) <> ''
GROUP BY uo, ph.num_po, transport, etd, eta, arrival, fsb.bl, sku, size, color

UNION
SELECT
ph.uo_number as uo,
UPPER(SPLIT_PART(ph.num_po,'_', 1)) as fic,
UPPER(ph.num_po) as po,
TRIM(fsb.bl) as bl,
etd,
eta,
arrival,
ph.mode_transport as transport,
fss.sku_id as sku,
fss.sku_size as size,
fss.sku_color as color,
fsf.fnd as pays_reso,
fsf.estimed_date_arrived as date,
SUM(fsf.qty) as qty
FROM po_header as ph
LEFT JOIN po_transport as pt
ON ph.num_po = pt.num_po
LEFT JOIN ref_entrepot re
ON re.code_entrepot = pt.rs_arrivee_entrepot
LEFT JOIN (
  SELECT DISTINCT
    bill_of_lading as bl, num_po, etd, eta, 
	CASE 
	WHEN arrival_pod IS NOT NULL AND LENGTH(arrival_pod)> 0 THEN arrival_pod 
	WHEN fnd_arrival_date IS NOT NULL AND LENGTH(fnd_arrival_date)> 0 THEN fnd_arrival_date 
	END as arrival
    FROM tracing_container_po_mer
    WHERE statut IN ('S', 'P', 'D', 'R')
    -- ajoute le 12/11/2012 pour SPDR saisies le même jour
    OR fnd_arrival_date >= (TO_CHAR(NOW()- interval '30 day', 'YYYYMMDD') )
  ) as tracing
ON ph.num_po = tracing.num_po
LEFT JOIN floating_split_bl fsb
ON tracing.bl = fsb.bl
LEFT JOIN floating_split_sku fss
ON fss.id_bl = fsb.id
AND fss.po = ph.num_po
LEFT JOIN floating_split_fnd fsf
ON fsf.id_sku = fss.id
WHERE LENGTH(TRIM(ph.uo_number)) > 0
AND ( '$specific_uo' = '' OR TRIM(ph.uo_number) = '$specific_uo' )
AND ( '$specific_po' = '' OR TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) = SPLIT_PART(LOWER('$specific_po'), '_', 1) )
AND SUBSTR(ph.div_acheteur,1,4) = 'Ind_'
AND fsb.id IS NOT NULL
AND fss.sku_id IS NOT NULL
AND (SUBSTR(ph.num_po,1,2) <> 'pb')
-- ON NE PREND QUE SI ON A UN ENTEPOT COTATION
AND TRIM(re.cot_fnd) <> ''

GROUP BY uo, ph.num_po, transport, etd, eta, arrival, fsb.bl, sku, size, color, fnd, estimed_date_arrived

UNION
SELECT
ph.uo_number as uo,
UPPER(SPLIT_PART(ph.num_po,'_', 1)) as fic,
UPPER(ph.num_po) as po,
SUBSTR(TRIM(fsb.bl),5,LENGTH(fsb.bl)) as bl,
etd,
eta,
arrival,
'4' as transport,
fss.sku_id as sku,
fss.sku_size as size,
fss.sku_color as color,
SUBSTR('',1,0) as pays_reso,
SUBSTR('',1,0) as date,
SUM(fsf.qty) as qty
FROM po_header as ph
LEFT JOIN po_transport as pt
ON ph.num_po = pt.num_po
LEFT JOIN ref_entrepot re
ON re.code_entrepot = pt.rs_arrivee_entrepot
LEFT JOIN (
  SELECT DISTINCT
    awb as bl, num_po, aeroport_etd as etd, aeroport_eta as eta, aeroport_date_arrivee as arrival
    FROM tracing_air_po_hawb
    WHERE statut IN ('S', 'P', 'D', 'R')
  ) as tracing
ON ph.num_po = tracing.num_po
LEFT JOIN floating_split_bl fsb
ON tracing.bl = SUBSTR(fsb.bl,5,LENGTH(fsb.bl))
LEFT JOIN floating_split_sku fss
ON fss.id_bl = fsb.id
AND fss.po = ph.num_po
LEFT JOIN floating_split_fnd fsf
ON fsf.id_sku = fss.id
WHERE LENGTH(TRIM(ph.uo_number)) > 0
AND ( '$specific_uo' = '' OR TRIM(ph.uo_number) = '$specific_uo' )
AND ( '$specific_po' = '' OR TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) = SPLIT_PART(LOWER('$specific_po'), '_', 1) )
AND SUBSTR(ph.div_acheteur,1,4) <> 'Ind_'
AND fsb.id IS NOT NULL
AND fss.sku_id IS NOT NULL
AND (SUBSTR(ph.num_po,1,2) <> 'pb')
-- ON NE PREND QUE SI ON A UN ENTEPOT COTATION
AND TRIM(re.cot_fnd) <> ''

GROUP BY uo, ph.num_po, transport, etd, eta, arrival, fsb.bl, sku, size, color

UNION
SELECT
ph.uo_number as uo,
UPPER(SPLIT_PART(ph.num_po,'_', 1)) as fic,
UPPER(ph.num_po) as po,
SUBSTR(TRIM(fsb.bl),5,LENGTH(fsb.bl)) as bl,
etd,
eta,
arrival,
'4' as transport,
fss.sku_id as sku,
fss.sku_size as size,
fss.sku_color as color,
fsf.fnd as pays_reso,
fsf.estimed_date_arrived as date,
SUM(fsf.qty) as qty
FROM po_header as ph
LEFT JOIN po_transport as pt
ON ph.num_po = pt.num_po
LEFT JOIN ref_entrepot re
ON re.code_entrepot = pt.rs_arrivee_entrepot
LEFT JOIN (
  SELECT DISTINCT
    awb as bl, num_po, aeroport_etd as etd, aeroport_eta as eta, aeroport_date_arrivee as arrival
    FROM tracing_air_po_hawb
    WHERE statut IN ('S', 'P', 'D', 'R')
  ) as tracing
ON ph.num_po = tracing.num_po
LEFT JOIN floating_split_bl fsb
ON tracing.bl = SUBSTR(fsb.bl,5,LENGTH(fsb.bl))
LEFT JOIN floating_split_sku fss
ON fss.id_bl = fsb.id
AND fss.po = ph.num_po
LEFT JOIN floating_split_fnd fsf
ON fsf.id_sku = fss.id
WHERE LENGTH(TRIM(ph.uo_number)) > 0
AND ( '$specific_uo' = '' OR TRIM(ph.uo_number) = '$specific_uo' )
AND ( '$specific_po' = '' OR TRIM(SPLIT_PART(LOWER(ph.num_po), '_', 1)) = SPLIT_PART(LOWER('$specific_po'), '_', 1) )
AND SUBSTR(ph.div_acheteur,1,4) = 'Ind_'
AND fsb.id IS NOT NULL
AND fss.sku_id IS NOT NULL
AND (SUBSTR(ph.num_po,1,2) <> 'pb')
-- ON NE PREND QUE SI ON A UN ENTEPOT COTATION
AND TRIM(re.cot_fnd) <> ''

GROUP BY uo, ph.num_po, transport, etd, eta, arrival, fsb.bl, sku, size, color, fnd, estimed_date_arrived

ORDER BY uo, fic, po, bl, sku, size, color, pays_reso, date
";

my $result = $dbh->prepare( $sql );
$result->execute();
if ( $dbh->errstr ne undef ) { # Erreur SQL
  print FICHIER_LOG "\nERROR : $sql";
  close FICHIER_LOG;
  $result->finish;
  $dbh->disconnect;
   exit;
}

my $nb_po_root = $result->rows;
print FICHIER_LOG "\n==> $nb_po_root POs ARRIVED à traiter";

if ( $nb_po_root > 0 && $specific_export_level eq '') {
  my $output = new IO::File (">".$xml_file);
  print $output "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
  my $writer = new XML::Writer(OUTPUT => $output);
  $writer->startTag("dau");

  my $key_uo = '';
  my $key_fic = '';
  my $key_po = '';
  my $key_bl = '';
  my $key_sku = '';

  while ( my $tableau = $result->fetchrow_hashref) {
    if ( $key_uo ne $tableau->{'uo'} && $key_uo ne '' ) {
      $writer->endTag("sku");
      $writer->endTag("bl");
      $writer->endTag("po");
      $writer->endTag("po_root");
      $writer->endTag("uo");
    }
    if ( $key_uo ne $tableau->{'uo'} ) {
      $writer->startTag("uo",
                        "num" => $tableau->{'uo'}
      );
      $key_uo = $tableau->{'uo'};
      $key_fic = '';
      $key_po = '';
      $key_bl = '';
      $key_sku = '';
    }

    if ( $key_fic ne $tableau->{'fic'} && $key_fic ne '' ) {
      $writer->endTag("sku");
      $writer->endTag("bl");
      $writer->endTag("po");
      $writer->endTag("po_root");
    }
    if ( $key_fic ne $tableau->{'fic'} ) {
      $writer->startTag("po_root",
                        "num" => $tableau->{'fic'}
      );
      $key_fic = $tableau->{'fic'};
      $key_po = '';
      $key_bl = '';
      $key_sku = '';
    }

    if ( $key_po ne $tableau->{'po'} && $key_po ne '' ) {
        $writer->endTag("sku");
        $writer->endTag("bl");
        $writer->endTag("po");
    }
    if ( $key_po ne $tableau->{'po'} ) {
      $writer->startTag("po",
                        "num" => $tableau->{'po'}
      );
      $key_po = $tableau->{'po'};
      $key_bl = '';
      $key_sku = '';
    }

    if ( $key_bl ne $tableau->{'bl'} && $key_bl ne '' ) {
      $writer->endTag("sku");
      $writer->endTag("bl");
    }
    if ( $key_bl ne $tableau->{'bl'} ) {
      $writer->startTag("bl",
                        "num" => $tableau->{'bl'},
                        "transport_mode" => $tableau->{'transport'}
      );
      $writer->startTag("dates");
      $writer->startTag("eta");
      $writer->characters($tableau->{'eta'});
      $writer->endTag("eta");
      $writer->startTag("etd");
      $writer->characters($tableau->{'etd'});
      $writer->endTag("etd");
      $writer->startTag("arrival");
      $writer->characters($tableau->{'arrival'});
      $writer->endTag("arrival");
      $writer->endTag("dates");
      $key_bl = $tableau->{'bl'};
      $key_sku = '';

      $rqlct->execute( $tableau->{'po'}, $tableau->{'bl'} );
      if ( $dbh->errstr ne undef ) { # Erreur SQL
        print FICHIER_LOG "\nERROR : $sqlct";
        close FICHIER_LOG;
        $rqlct->finish;
        $result->finish;
        $dbh->disconnect;
        exit;
      }
      while ( my @tabct = $rqlct->fetchrow ) {
        $writer->startTag("equipment",
                          "type" => $tabct[0], 
                          "num" => $tabct[1] 
        );
        $writer->endTag("equipment");
      } 
    }
                        

    if ( $key_sku ne ( $tableau->{'sku'} . $tableau->{'size'} .  $tableau->{'color'} ) && $key_sku ne '' ) {
      $writer->endTag("sku");
    }
    if ( $key_sku ne ( $tableau->{'sku'} . $tableau->{'size'} .  $tableau->{'color'} ) ) {
      $writer->startTag("sku",
			"num" => $tableau->{'sku'},
                        "sku_size" => $tableau->{'size'},
                        "sku_color" => $tableau->{'color'}
      );
      $key_sku = $tableau->{'sku'} . $tableau->{'size'} .  $tableau->{'color'};
    }
			
    if ( $tableau->{'pays_reso'} ne '' ) {
      $writer->startTag("floating",
                      "pays_reso" => $tableau->{'pays_reso'},
                      "date" => $tableau->{'date'}
      );
    }
    else {
      $writer->startTag("floating",
                      "pays_reso" => 'All',
                      "date" => ''
      );
    }
    $writer->characters($tableau->{'qty'});
    $writer->endTag("floating");
  }
  $writer->endTag("sku");
  $writer->endTag("bl");
  $writer->endTag("po");
  $writer->endTag("po_root");
  $writer->endTag("uo");
  $writer->endTag("dau");
  $writer->end();
  $output->close();
}
$result->finish;

$dbh->disconnect;


# Déplacement des fichiers traités
# ================================


$date_log = now;
$date_log = now;
print FICHIER_LOG "\nFIN TRAITEMENT à $date_log\n";
close FICHIER_LOG;



# ON ENVOI LE DIAG PAR MAIL
my $mime_msg = MIME::Lite->new(
  From    => 'YR <no-reply@fcsystem.com>',
  Subject => 'Rapport extraction FCS pour DAU',
  To      => 'support@fcsystem.com',
  Type    => 'multipart/mixed'
) or print STDERR ("Error on creating email!\n");
$mime_msg->attach(
  Type => 'TEXT',
  Path => $dau_directory . 'fcs_extract_dau.log',
  ReadNow  => 1,
  Filename => $dau_directory . 'fcs_extract_dau.log'
) or print STDERR ("Error on attachment email!\n");

$mime_msg->send_by_smtp(esolGetEnv(__FILE__, 'SMTP_SERVER')) or print STDERR ("Error on send email!\n");

sub get_file_size(){
  my ($file_directory, $file_name ) = @_;
  my $size=0;
  if(-e $file_directory.$file_name){
    open(FILE,  $file_directory.$file_name) || print FICHIER_LOG "\nimpossible d'ouvir le fichier $file_directory.$file_name!";
    seek(FILE,0,2); # se placer a la fin du fichier
    $size=tell(FILE); # obtenir la position de ton curseur
    close (FILE);
  }
  return $size;
}

# get_cbm_charge()  permet de calculer le cbm_charge à passer dans le cas d'un CFS/CY uniquement

sub get_cbm_charge(){

 my ($dbh,$dbh_cot,$po, $nb_current_equipment,$type_current_equipment, $fournisseur) = @_;

 my $sqlcfs="
	SELECT   
            sum(item_volume_total) as volume_total,
            sum(master_units) as nb_pallets, 
			pt.equipement
			FROM po_detail as pd
			LEFT JOIN po_transport as pt
			ON pd.num_po = pt.num_po
            WHERE pd.num_po = '$po'
			GROUP BY pt.equipement
			";
			

			
 my $reqcfs = $dbh->prepare( $sqlcfs );
 $reqcfs->execute();
 
 if ( $dbh->errstr ne undef ) { # Erreur SQL
        print FICHIER_LOG "\nERROR : $sqlcfs";
        close FICHIER_LOG;
        $reqcfs->finish;
        $result->finish;
        $dbh->disconnect;
        exit;
      }

 	my ($volume_total, $nb_pallets, $nb_20, $nb_40, $equipement,$kind_equipement);
	
 while ( my $tabcfs = $reqcfs->fetchrow_hashref ) 
	   {
        $volume_total = $tabcfs->{'volume_total'};
        $nb_pallets = $tabcfs->{'nb_pallets'};
        $equipement = $tabcfs->{'equipement'};
       }
	
	my $cbm_charge;
	
	my @tab = split(";" , $equipement );

		for ( my $i = 0 ; $i < scalar @tab ; $i++ ) {
			  my @type_qty = split("x" , $tab[$i]);
			  if($type_qty[0] eq '20'){
				$nb_20 = $type_qty[1];
			  }		  
			  if($type_qty[0] eq '40'){
				$nb_40 = $type_qty[1];
			  }
		}
	
    #Modification de la fonction : pour les CFS/CY les volumes des containers sont
	# désormais spécifiques et renseignés dans le champ cbm_per_ct_cfs
	# de la table ref_container_kinds
	# il faut donc déterminer le volume correspondant à un CFS/CY
	

	my %hash_list_container_eq_CFS;
	
	foreach my $container_cfs_kind(@list_container)
	{
	
	my $sql_cbm_cfs="
		SELECT   
				cbm_per_ct_cfs as volume_per_ct
				FROM ref_container_kinds 
				WHERE code_fcs = '$container_cfs_kind'
				";
						
	 my $rs_cbm_cfs = $dbh_cot->prepare( $sql_cbm_cfs );
	 $rs_cbm_cfs->execute();
	 print $po.' ';
     print $sql_cbm_cfs;
	 
	 
	 if ( $dbh_cot->errstr ne undef ) { # Erreur SQL
			print FICHIER_LOG "\nERROR : $sql_cbm_cfs";
			close FICHIER_LOG;
			$rs_cbm_cfs->finish;
			$result->finish;
			$dbh_cot->disconnect;
			exit;
		  }

		my $volume_per_ct;
		
		my $CFS_cbm_charge;
		
	 while ( my $tabcfs_cbm = $rs_cbm_cfs->fetchrow_hashref ) 
		   {
			$hash_list_container_eq_CFS{$container_cfs_kind} = $tabcfs_cbm->{'volume_per_ct'};
		   }
	
    }
	
	if($fournisseur eq 'Piramal')
	{
	
    #Modification de la fonction : on impose un 40' hors Piramal et un 20' pour Piramal
	# Le cbm_charge n'est plus calculé aux proratas des équipements et ne peut plus
	# prendre en compte plusieurs type de containers 
	
	################################################
	$type_current_equipment = 40; # eq nb de palettes dans un 40' 
	$nb_current_equipment = floor($nb_pallets / $type_current_equipment);
	$nb_current_equipment +=1 if (($nb_pallets % $type_current_equipment) ne  0);
	#Fin Modif #################################################
	
    $cbm_charge = ($volume_total*($nb_current_equipment*$type_current_equipment)) / $nb_pallets;
		
	}
	else
	{
	my $volume_equipment=0;
	
    #Modification de la fonction : pour les CFS/CY les volumes des containers sont
	# désormais spécifiques et renseignés dans le champ cbm_per_ct_cfs
	# de la table ref_container_kinds
	
	################################################
	#if($type_current_equipment eq '20')
	#  {$volume_equipment=27;}
	#if($type_current_equipment eq '40')
	#  {$volume_equipment=57;}  
	#Fin Modif #################################################  
	  
	
	#$cbm_charge=($nb_current_equipment*$volume_equipment)*($nb_current_equipment*$volume_equipment)/($nb_20*27+$nb_40*57);
	
	$nb_20=1;  #hors piramal on impose un container 20'
	$nb_40=0;
	$nb_current_equipment=1;
	$volume_equipment= $hash_list_container_eq_CFS{20};
	
	$cbm_charge=($nb_current_equipment*$volume_equipment)*($nb_current_equipment*$volume_equipment)/($nb_20*$hash_list_container_eq_CFS{20}+$nb_40*$hash_list_container_eq_CFS{40});
	}
    print " po $po \n" if($dbug);
	print " volume_total $volume_total \n" if($dbug);
	print " nb_pallets $nb_pallets \n" if($dbug);
	print " nb_20 $nb_20 \n" if($dbug);
	print " nb_40 $nb_40 \n" if($dbug);
	print " type_current_equipment $type_current_equipment \n" if($dbug);
	print " nb_current_equipment $nb_current_equipment \n" if($dbug);
	print " cbm_charge $cbm_charge \n" if($dbug);
    print " fournisseur $fournisseur \n" if($dbug);
	print " volume_per_ct 20 \n" if($dbug);
    print " volume_per_ct 20  $hash_list_container_eq_CFS{20} \n" if($dbug);
    print " volume_per_ct 40  $hash_list_container_eq_CFS{40} \n" if($dbug);
	print " \n";
 $dbh_cot->disconnect; 
  
 return $cbm_charge;
 }

 
 ######################################
#   SCRIPTS DE PASSAGE DE PARAMETRES #
######################################

sub init(@ARGV){
		$log_msg .= "Debut du programme :".`date`."\n";
		
		for ( my $i = 0 ; $i <= scalar @ARGV ; $i++ ){
			if (get_arg($ARGV[$i]) eq 'u' ){
				$specific_uo = $ARGV[$i+1];
			}elsif (get_arg($ARGV[$i]) eq 'p' ){
				$specific_po = $ARGV[$i+1];
			}elsif (get_arg($ARGV[$i]) eq 'e' ){
				$specific_export_level = $ARGV[$i+1];
			}elsif (get_arg($ARGV[$i]) eq 'c' ){
				$is_cintyr = $ARGV[$i+1];
			} elsif (get_arg($ARGV[$i]) eq 'help' ){
				&init_error();
			}
		}
}

sub init_error(){
	$log_msg .= "
			usage : 01_fcs_extract_dau.pl -u uo -e export_level ( 2:checking, 3:arrived_pod ) -c ( 1:duplication cintyr)
			";
		&exit_function;
}

sub get_arg(){
		my ($str_to_return) = @_;
		if (index( $str_to_return, '-' ) > -1){
		  $str_to_return = substr($str_to_return , 1, length($str_to_return));
		} else {
		  $str_to_return = 0
		}
		return ($str_to_return);
}

sub exit_function(){
	my $fichier_log = $bin_directory."/dau/fcs_extract_dau.log";
	open LOGFILE, ">> $fichier_log" or die "Can't open $fichier_log";
		$log_msg .= "\nFin du programme.\n";
	if ($dbug) {
	  print $log_msg;
	} else {
	  print LOGFILE $log_msg;
	}
	close LOGFILE;
		exit;
}


