#!/usr/bin/perl
#===========================================================================================
# Ce programme génère 3 fichiers XML:
#
# HISTORIQUE :
# Le 22 MARS 2010 : Pour amendé et engagé on prend les taux USD/EURO dans cotation hader et :
# on ne prend que les ~1 et ~2 qui ont été calculées le jour de l'exécution du programme
#
# Le 4 MAI rajouter extraction des 5 taux de fees
#
# Le 11 Janvier 2012
# Gestion autre monnaie que USD et EURO, dans ce cas il faut sortir le taux fob EURO-AUTRE MONNAIE
# Ex : pour le MXN on a dans cotation le taux transport USD-EURO et le taux fob MXN-USD
# il faut sortir le taux fob EUR-MXN (par exemple : 1.35 / 0.072894168 = 18.52) 

use strict;

use DBI;
use Class::Date qw(:errors date now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;
use XML::Writer;
use IO::File;
use Net::FTP;
use MIME::Lite;
use MIME::QuotedPrint;
use MIME::Base64;

print "\n 03_qf_extract_dau.pl ".now." \n";


# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;
use fcs_lib;
use fcs_fonctions_globales;

my $bin_directory  = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/dau/";

my $traite_cotation = 1;
my $traite_checking = 1;
my $traite_arrived_pod = 1;

my $specific_uo = '';
my $specific_po = '';
my $specific_export_level = '';
my $specific_nb_day='';
######################################################"""
#		usage : qf_extract_dau.pl -u uo -p num_po -e export_level ( 1:cotation, 2:checking, 3:arrived_pod ) -d nb_day
######################################################"""
my $log_msg;
&init();
if( $specific_export_level ne ''){
	$traite_cotation = 0 if( $specific_export_level ne 1);
	$traite_checking = 0 if( $specific_export_level ne 2);
	$traite_arrived_pod = 0 if( $specific_export_level ne 3);
}

my $dbug = '1';

#XP 10/01
my $dau_directory = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR') . "bin/dau/";
my $src_directory = $dau_directory . "src/";
my $fichier_log = $dau_directory . "qf_extract_dau.log";
open( FICHIER_LOG, "> $fichier_log" ) || die "Impossible ouvrir fichier log !\n";
my $tmp_date = now;
my $date_amende_engage = $tmp_date->year . sprintf( "%02d", $tmp_date->month ) . sprintf( "%02d", $tmp_date->day );
my $date_log = now;
print FICHIER_LOG "\n\nDEBUT TRAITEMENT du $date_log";
my $src_file_cotation = "QF_EXTRACT_COTATION.XML";
my $done_file_cotation = $dau_directory . "done/" . "QF_EXTRACT_COTATION." . $date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day );
my $src_file_checking = "QF_EXTRACT_CHECKING.XML";
my $done_file_checking = $dau_directory . "done/" . "QF_EXTRACT_CHECKING." . $date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day );
my $src_file_arrived_pod = "QF_EXTRACT_ARRIVED_POD.XML";
my $done_file_arrived_pod = $dau_directory . "done/" . "QF_EXTRACT_ARRIVED_POD." . $date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day );

# Tentative de connexion à la base FCS :
# ======================================
#XP 10/01
#my $dbh_fcs = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
my $dbh_fcs = DBI->connect( esolGetEnv(__FILE__, 'BDD_FCS_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );

if ( $dbh_fcs eq undef ) {
  print FICHIER_LOG "\nERROR Connexion base FCS";
  close FICHIER_LOG;
  exit;
}
# Tentative de connexion à la COTATION :
# ======================================

my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_QF_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );

if ( $dbh eq undef ) {
  print FICHIER_LOG "\nERROR Connexion base COT_YR";
  close FICHIER_LOG;
  $dbh_fcs->disconnect;
  exit;
}

# LES REQUETES GENERIQUES
# =======================
# Pour la cotation on prend le taux archivé au calcul de cette cotation
# Demande E.T. le 22/9/2009 pour l'amendé et l'attendu on prend le taux de conversion courant
my $sqlratecourant = " SELECT usd_to_euro_rate FROM ref_global_parameters ";
my $rqratecourant = $dbh->prepare( $sqlratecourant );
print "\n sqlratecourant :".$sqlratecourant."\n" if($dbug);
$rqratecourant->execute;
print "\n sqlratecourant :".$sqlratecourant."\n" if($dbug);
if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
  print FICHIER_LOG "\nERROR : $rqratecourant";
  close FICHIER_LOG;
  $rqratecourant->finish;
  $dbh_fcs->disconnect;
  $dbh->disconnect;
  exit;
}
my $exchange_rate_courant = $rqratecourant->fetchrow;
$rqratecourant->finish;


my $sqlrate = " SELECT ch.usd_to_euro_rate as usd_to_euro_rate,
CASE WHEN ch.usd_to_euro_fob_rate IS NULL then ch.usd_to_euro_rate
ELSE ch.usd_to_euro_fob_rate END as usd_to_euro_fob_rate
FROM cotation_header ch
LEFT JOIN logistic_routing_scenarios lrs
ON lrs.id = ch.id_scenario
LEFT JOIN input_model_header imh
ON imh.id = lrs.id_input_model
WHERE SPLIT_PART(imh.fic_num,'_', 1) = UPPER(?)
AND SUBSTR(imh.fic_num,11,1) <> '~'
AND SUBSTR(imh.fic_num,11,1) <> '#'
AND lrs.date_fcs IS NOT NULL
AND TRIM(lrs.date_fcs) <> ''
";


my $rqrate = $dbh->prepare( $sqlrate );

my $seqctn = "
SELECT
rck.code_fcs as type,
lre.nb_container
FROM logistic_routings_equipement lre
LEFT JOIN ref_container_kinds rck
ON rck.id = lre.id_container_kinds
WHERE lre.id_shpt = ?
";
my $reqctn = $dbh->prepare( $seqctn );

my $seqsku = "
SELECT
ims.sku_num as sku,
ims.size as size,
ims.color as color,
ims.designation as designation,
ims.packing_pcs_per_ctn as pcs_per_ctn,
ims.packing_long as packing_long,
ims.packing_large as packing_large,
ims.packing_height as packing_height,
ims.packing_weight as packing_weight,
lrp.unit_price as prix_fob1,
lrp.other_marge as marge,
lrsh.id_scenario as id_scenario,
ims.id as id_sku,

SUM(lrd.q_pcs) as pcs
FROM logistic_routings_detail lrd
LEFT JOIN logistic_routings_shpts lrsh
ON lrsh.id = lrd.id_shpt
LEFT JOIN input_model_sku_fnds imsf
ON imsf.id = lrd.id_input_model_sku_fnd
LEFT JOIN input_model_skus ims
on ims.id = imsf.id_sku
LEFT JOIN logistic_routings_price lrp
ON lrp.id_scenario = lrsh.id_scenario
AND lrp.id_sku = imsf.id_sku
WHERE lrsh.id = ?
GROUP BY ims.sku_num, ims.size, ims.color, ims.designation, ims.packing_pcs_per_ctn,
ims.packing_long, ims.packing_large, ims.packing_height, ims.packing_weight,
lrp.unit_price, lrp.other_marge, lrsh.id_scenario, ims.id
";
my $reqsku = $dbh->prepare( $seqsku );

my $seqpricetothub = "
SELECT
MAX(unit_purchase_price) as unit_purchase_price,
SUM(csf.q_pcs) as pcs,
SUM(csf.total_cost_hub_price) as tot_prix_hub,
SUM(csf.total_cost_price) as tot_prix
FROM cotation_sku_fnd csf
LEFT JOIN cotation_header ch
ON ch.id = csf.id_header
WHERE ch.id_scenario = ?
AND csf.id_sku = ?
";
my $reqpricetothub = $dbh->prepare( $seqpricetothub );

my $seqcost = " 
SELECT
rloc.cost_kind as type,
SUM(cd.cost) as cost
FROM cotation_detail cd
LEFT JOIN ref_list_of_charges rloc
ON rloc.id = cd.id_charge
WHERE cd.id_shpt = ?
GROUP BY rloc.cost_kind
";
my $reqcost = $dbh->prepare( $seqcost );

# Pour l'engagé, on sort un poste de charge "FREIGHT_POL" comprenant VM+FREIGHT+BAF+CAF
# pour calculer les écarts taux de change dans le DAU
my $seqcost_couverture = "
SELECT
'FREIGHT_POL' as type,
SUM(cd.cost) as cost
FROM cotation_detail cd
LEFT JOIN ref_list_of_charges rloc
ON rloc.id = cd.id_charge
WHERE cd.id_shpt = ?
AND ( rloc.code = 'S_VM' OR rloc.code = 'S_FR' OR rloc.code = 'S_BA' OR rloc.code = 'S_CA' )
";
my $reqcost_couverture = $dbh->prepare( $seqcost_couverture );


# =============================
# Extraction COTATION ORIGINALE
# =============================
$tmp_date = $tmp_date - "5D";
my $jour = $tmp_date->year . sprintf( "%02d", $tmp_date->month ) .  sprintf( "%02d", $tmp_date->day );
# ON RECUPERE la liste des POs dans FCS qui ont été créés après la date contenu dans la variable jour
# ===================================================================================================


my $requete = "
 SELECT 
		'$specific_uo' as uo, 
		SPLIT_PART(imh.fic_num,'_', 1) as po_root, 
		rd.designation as departments, 
		imh.buyer_name as contact, 
        rs.designation as importer
		FROM input_model_header as imh 
		LEFT JOIN ref_suppliers as rs
        ON rs.id=imh.id_supplier
        LEFT JOIN ref_departments as rd
        ON rd.id=imh.id_department
		WHERE imh.fic_num = '$specific_po'
        
		GROUP BY imh.fic_num,rd.designation,imh.buyer_name,rs.designation
		ORDER BY imh.fic_num
";



my $result = $dbh->prepare( $requete );
print "\n".$requete."\n" if($dbug);
$result->execute();
if ( $dbh_fcs->errstr ne undef ) { # Erreur SQL
	print "\n $dbh_fcs->errstr \n $requete \n";
  print FICHIER_LOG "\nERROR : $requete";
  close FICHIER_LOG;
  $result->finish;
  $dbh_fcs->disconnect;
  exit;
}



# REQUETE SUR COTATION POUR VERIFIER date_fcs & cotation


my $seq_cot = " SELECT * 
		FROM cotation_header ch
		LEFT JOIN logistic_routing_scenarios lrs
                ON lrs.id = ch.id_scenario
		LEFT JOIN input_model_header imh
                ON imh.id = lrs.id_input_model
		WHERE SPLIT_PART(imh.fic_num,'_', 1) = UPPER(?)
		AND SUBSTR(imh.fic_num,11,1) <> '~'
		AND lrs.date_fcs IS NOT NULL
		AND TRIM(lrs.date_fcs) <> ''
";

my $req_cot = $dbh->prepare( $seq_cot );

my $nb_fic = 0;
my @loop_fic = ();

while ( my $tableau = $result->fetchrow_hashref) {
	print "\n [ $tableau->{'po_root'} ] \n ".$seq_cot."\n" if($dbug);
  $req_cot->execute($tableau->{'po_root'});
  if ( $dbh->errstr ne undef ) { # Erreur SQL
	print "\n $dbh_fcs->errstr \n $seq_cot [$tableau->{'po_root'}]  \n";
    print FICHIER_LOG "\nERROR : $seq_cot [$tableau->{'po_root'}] ";
    close FICHIER_LOG;
    $req_cot->finish;
    $result->finish;
    $dbh_fcs->disconnect;
    $dbh->disconnect;
    exit;
  }
  if ( $req_cot->rows > 0 ) {
    $nb_fic++;
    push (@loop_fic, $tableau->{'uo'});
    push (@loop_fic, $tableau->{'po_root'});
    push (@loop_fic, $tableau->{'department'});
    push (@loop_fic, $tableau->{'contact'});
    push (@loop_fic, $tableau->{'importer'});
  }
  $req_cot->finish;
}
$result->finish;

print FICHIER_LOG "\n==> $nb_fic FICs COTATION à traiter";

# ON CREE LE FICHIER XML SI la liste des POs n'est pas vide
# =========================================================
if ( $traite_cotation ne "1" ) { $nb_fic = 0; }
if ( $nb_fic > 0 ) {
  my $xml_file = $src_directory . $src_file_cotation;
  if (-e $xml_file){
    unlink($xml_file) or die ("erreur dans la suppression des fichiers existants \n");
  }

  my $export_level = "1";
  my $find_fic = " SUBSTR(imh.fic_num,11,1) <> '~'
AND lrs.date_fcs IS NOT NULL
AND TRIM(lrs.date_fcs) <> ''
";
  &traite_liste( $dbh, $dbh_fcs, $nb_fic, $export_level,  $find_fic, $xml_file, @loop_fic );
}

# =====================================
# Extraction COTATION ~1 (CHECKING FCS)
# =====================================


#CD TO_WATCH

print "\n"." Checking --------------------"."\n" if($dbug);

my $requete = " SELECT 
               SPLIT_PART(h.fic_num,'~',1) as po_root
	       FROM input_model_header h
	       LEFT JOIN logistic_routing_scenarios lrs
	       ON lrs.id_input_model = h.id
	       LEFT JOIN cotation_header ch
	       ON ch.id_scenario =lrs.id
	       WHERE 1=1		   
";

if ( $specific_uo eq '' && $specific_po eq '' ) {
$requete .= "
	       AND ch.calcul_date = '$date_amende_engage'
";
}

if ( $specific_po ne '' ) {
$requete .= "
	       AND LOWER(h.fic_num) LIKE SPLIT_PART(LOWER('$specific_po'), '_', 1)||'%'
		   AND ( ch.calcul_date <= to_char((current_date - $specific_nb_day) ,'YYYYMMDD') OR $specific_nb_day IS NULL) 
		   
";
}

if( $traite_cotation eq '1' || $traite_checking eq '1' || $traite_cotation eq '1' ){
	$requete .= "
		AND (
			'$traite_cotation'='0'
			OR
			(
				lrs.date_fcs IS NOT NULL 
				AND LENGTH(TRIM(lrs.date_fcs)) > 0
				AND h.fic_num NOT LIKE '%~%' 
			)
		)
		AND (
			'$traite_checking'='0'
			OR
			h.fic_num LIKE '%~1' 
		)
		AND (
			'$traite_arrived_pod'='0'
			OR
			h.fic_num LIKE '%~2' 
		)

	";
}


my $result = $dbh->prepare( $requete );
print "\n".$requete."\n" if($dbug) ;
$result->execute();
if ( $dbh->errstr ne undef ) { # Erreur SQL
	print "\n $dbh_fcs->errstr \n $requete \n";
  print FICHIER_LOG "\nERROR : $requete";
  close FICHIER_LOG;
  $result->finish;
  $dbh_fcs->disconnect;
  $dbh->disconnect;
  exit;
}

# REQUETE SUR FCS POUR  récupérer : uo, departement et importeur 

my $seq_fcs = "
 SELECT 
		'$specific_uo' as uo, 
		SPLIT_PART(imh.fic_num,'_', 1) as po_root, 
		rd.designation as departments, 
		imh.buyer_name as contact, 
        rs.designation as importer
		FROM input_model_header as imh 
		LEFT JOIN ref_suppliers as rs
        ON rs.id=imh.id_supplier
        LEFT JOIN ref_departments as rd
        ON rd.id=imh.id_department
		WHERE imh.fic_num = '$specific_po'
        
		GROUP BY imh.fic_num,rd.designation,imh.buyer_name,rs.designation
		ORDER BY imh.fic_num
";

my $req_fcs = $dbh->prepare( $seq_fcs );

my $nb_fic = 0;
my @loop_fic = ();
while ( my $tableau = $result->fetchrow_hashref) {
print "\n [$tableau->{'po_root'} ] \n ".$seq_fcs."\n" if($dbug);
  $req_fcs->execute($tableau->{'po_root'});
  if ( $dbh_fcs->errstr ne undef ) { # Erreur SQL
	print "\n $dbh_fcs->errstr \n $seq_fcs [$tableau->{'po_root'}]  \n";
    print FICHIER_LOG "\nERROR : $seq_fcs";
    close FICHIER_LOG;
    $req_fcs->finish;
    $result->finish;
    $dbh_fcs->disconnect;
    $dbh->disconnect;
    exit;
  }
  if ( $req_fcs->rows > 0 ) {
    $nb_fic++;
    my @tab = $req_fcs->fetchrow;
    push (@loop_fic, $tab[0]);
    push (@loop_fic, $tableau->{'po_root'});
    push (@loop_fic, $tab[1]);
    push (@loop_fic, $tab[2]);
    push (@loop_fic, $tab[3]);
  }
  $req_fcs->finish;
}
$result->finish;

print FICHIER_LOG "\n==> $nb_fic FICs CHECKING à traiter";

print "\n nb_fic :".$nb_fic."\n" if($dbug);
if ( $traite_checking ne "1" ) { $nb_fic = 0; }
if ( $nb_fic > 0 ) {
  my $xml_file = $src_directory . $src_file_checking;
	print "\n xml_file :".$xml_file if($dbug);
  if (-e $xml_file){
    unlink($xml_file) or die ("erreur dans la suppression des fichiers existants \n");
  }

  my $export_level = "2";
  my $find_fic = " imh.fic_num LIKE '%~1'";
  &traite_liste( $dbh, $dbh_fcs, $nb_fic, $export_level, $find_fic, $xml_file, @loop_fic );
}

# =====================================
# Extraction COTATION ~2 (ARRIVED PODC FCS)
# =====================================



print "Extraction COTATION ~2"."\n" if ($dbug);


my $requete = " SELECT 
               SPLIT_PART(h.fic_num,'~',1) as po_root
	       FROM input_model_header h
	       LEFT JOIN logistic_routing_scenarios lrs
	       ON lrs.id_input_model = h.id
	       LEFT JOIN cotation_header ch
	       ON ch.id_scenario =lrs.id
	       --WHERE SUBSTR(h.fic_num,11,2) = '~2'
		   WHERE SPLIT_PART(h.fic_num,'~',2) = '2'
";
if ( $specific_uo eq '' && $specific_po eq '' ) {
$requete .= "
	       AND ch.calcul_date = '$date_amende_engage'
";
}
if ( $specific_po ne '' ) {
$requete .= "
	       AND LOWER(h.fic_num) LIKE SPLIT_PART(LOWER('$specific_po'), '_', 1)||'%'
";
}

my $result = $dbh->prepare( $requete );
print "\n".$requete."\n" if($dbug);
$result->execute();
if ( $dbh->errstr ne undef ) { # Erreur SQL
	print "\n $dbh_fcs->errstr \n $requete  \n";
  print FICHIER_LOG "\nERROR : $requete";
  close FICHIER_LOG;
  $result->finish;
  $dbh_fcs->disconnect;
  $dbh->disconnect;
  exit;
}

my $seq_fcs = "
 SELECT 
		'$specific_uo' as uo, 
		SPLIT_PART(imh.fic_num,'_', 1) as po_root, 
		rd.designation as departments, 
		imh.buyer_name as contact, 
        rs.designation as importer
		FROM input_model_header as imh 
		LEFT JOIN ref_suppliers as rs
        ON rs.id=imh.id_supplier
        LEFT JOIN ref_departments as rd
        ON rd.id=imh.id_department
		WHERE imh.fic_num = '$specific_po'
        
		GROUP BY imh.fic_num,rd.designation,imh.buyer_name,rs.designation
		ORDER BY imh.fic_num
";



my $req_fcs = $dbh->prepare( $seq_fcs );


my $nb_fic = 0;
my @loop_fic = ();
while ( my $tableau = $result->fetchrow_hashref) {
	print "\n [$tableau->{'po_root'} ] \n ".$seq_fcs if ($dbug);
	
    $req_fcs->execute($tableau->{'po_root'});
	
  if ( $dbh_fcs->errstr ne undef ) { # Erreur SQL
	
    print FICHIER_LOG "\nERROR : $seq_fcs";
    close FICHIER_LOG;
    $req_fcs->finish;
    $result->finish;
    $dbh_fcs->disconnect;
    $dbh->disconnect;
    exit;
  }
  if ( $req_fcs->rows > 0 ) {
    $nb_fic++;
    my @tab = $req_fcs->fetchrow;
    push (@loop_fic, $tab[0]);
    push (@loop_fic, $tableau->{'po_root'});
    push (@loop_fic, $tab[1]);
    push (@loop_fic, $tab[2]);
    push (@loop_fic, $tab[3]);
  }
  $req_fcs->finish;
}
$result->finish;

print FICHIER_LOG "\n==> $nb_fic FICs ARRIVED_POD à traiter";

if ( $traite_arrived_pod ne "1" ) { $nb_fic = 0; }
if ( $nb_fic > 0 ) {
  my $xml_file = $src_directory . $src_file_arrived_pod;
  if (-e $xml_file){
    unlink($xml_file) or die ("erreur dans la suppression des fichiers existants \n");
  }

  my $export_level = "3";
  my $find_fic = " imh.fic_num LIKE '%~2'";
  &traite_liste( $dbh, $dbh_fcs, $nb_fic, $export_level, $find_fic, $xml_file, @loop_fic );
}

$dbh_fcs->disconnect;
$dbh->disconnect;

# Envoi fichier du calcul de COTATION vers DAU (CBR)
#-------------------------------------------------------
my $size_cotation = &get_file_size ( $src_directory, $src_file_cotation);
my $size_checking = &get_file_size ( $src_directory, $src_file_checking);
my $size_arrived_pod = &get_file_size ( $src_directory, $src_file_arrived_pod);

# DEV: Pas de transfert FTP

if ($size_cotation > 0 ){
$date_log = now;
print FICHIER_LOG "\nFIN TRAITEMENT à $date_log\n";
close FICHIER_LOG;
}
else{
   print FICHIER_LOG "\nLe fichier $src_file_cotation est vide.";
}

if ($size_checking > 0 ){
$date_log = now;
print FICHIER_LOG "\nFIN TRAITEMENT à $date_log\n";
close FICHIER_LOG;
}
else{
   print FICHIER_LOG "\nLe fichier $src_file_checking est vide.";
}

if ($size_arrived_pod > 0 ){

$date_log = now;
print FICHIER_LOG "\nFIN TRAITEMENT à $date_log\n";
close FICHIER_LOG;
}
else{
   print FICHIER_LOG "\nLe fichier $src_file_arrived_pod est vide.";
}

# ON ENVOI LE DIAG PAR MAIL
my $mime_msg = MIME::Lite->new(
  From    => 'YR <no-reply@fcsystem.com>',
  Subject => 'Rapport extraction COTYR pour DAU',
  To      => 'support@fcsystem.com',
  Type    => 'multipart/mixed'
) or print STDERR ("Error on creating email!\n");
$mime_msg->attach(
  Type => 'TEXT',
  Path => $dau_directory . 'qf_extract_dau.log',
  ReadNow  => 1,
  Filename => $dau_directory . 'qf_extract_dau.log'
) or print STDERR ("Error on attachment email!\n");

$mime_msg->send_by_smtp(esolGetEnv(__FILE__, 'SMTP_SERVER')) or print STDERR ("Error on send email!\n");

sub traite_liste() {
  my ( $dbh, $dbh_fcs, $nb_fic, $export_level, $find_fic, $xml_file, @loop_fic ) = @_;
	
	print "traite_liste ( $dbh, $dbh_fcs, $nb_fic, $export_level, $find_fic, $xml_file, @loop_fic )"."\n" if($dbug);
  
  my $output = new IO::File (">".$xml_file);
  print $output "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
  my $writer = new XML::Writer(OUTPUT => $output);
  $writer->startTag("dau");

  # ON RECUPERE Les FICs dans COTATION
  # ==================================
  my $j = 0;
  my $last_uo = ''; 
  my $last_po_root = ''; 

  for ( my $i = 0; $i < $nb_fic; $i++ ) {
    my $uo = $loop_fic[$j]; $j++;
    my $po_root = $loop_fic[$j]; $j++;
    my $department = $loop_fic[$j]; $j++;
    my $contact = $loop_fic[$j]; $j++;
    my $importer = $loop_fic[$j]; $j++;
    my $exchange_rate = 1;
    my $exchange_rate_fob = 1;
    print FICHIER_LOG "\n====> UO $uo	PO $po_root";

    if ( $last_uo ne $uo && $last_uo ne '' ) {
      $writer->endTag("po_root");
      $writer->endTag("uo");
    }
    if ( $last_uo ne $uo ) {
      $writer->startTag("uo",
                        "num" => $uo 
		       );	
      $last_uo = $uo;
      $last_po_root = '';
    }

    if ( $last_po_root ne $po_root && $last_po_root ne '' ) {
      $writer->endTag("po_root");
    }
    if ( $last_po_root ne $po_root ) {
      $writer->startTag("po_root",
                        "num" => $po_root
      );
      $writer->startTag("dpt");
      $writer->characters($department);
      $writer->endTag("dpt");
      $writer->startTag("contact");
      $writer->characters($contact);
      $writer->endTag("contact");
      $writer->startTag("importer");
      $writer->characters($importer);
      $writer->endTag("importer");
      $last_po_root = $po_root;

      # ON RECHERCHE le TAUX de CONVERSION de la FIC intégrée dans FCS
		print "\n [$po_root ] \n ".$sqlrate."\n" if($dbug);
      $rqrate->execute( $po_root);
	  #$rqrate->execute( $po_root,$po_root);
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
		print "\n $dbh_fcs->errstr \n $sqlrate [$po_root]  \n";
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rqrate->finish;
        $dbh->disconnect;
        exit;
      }
      my @tab_exchange_rate = $rqrate->fetchrow;
      $rqrate->finish;
      $exchange_rate = $tab_exchange_rate[0];
      $exchange_rate_fob = $tab_exchange_rate[1];
      # Supprimé le 22 Mars 2010, car on exporte uniquement pour l'amendé et l' engagé 
      # les cotations qui ont été calculées dans la nuit
      if ($exchange_rate == 0 ) { $exchange_rate = 1; }
      if ($exchange_rate_fob == 0 ) { $exchange_rate_fob = $exchange_rate ; }
    }

    my $sql = "
    SELECT 
    imh.fic_num as po,
    '$export_level' as id_export_level,
    rst.country_code||rst.town_code as pod,
    lrsh.id_transport_mode as id_transport_mode,
    lrsh.shpt_num as bl,
    imh.currency as currency,
    lrsh.id_linerterm as linerterm,
    imh.id as id_input_model,
    lrs.id as id_scenario,
    lrsh.id as id_shpt,
    lrsh.esd as esd,
lrs.id as id_scenario,
lrsh.id as id_shpt,
lrsh.id_incoterm as incoterm

    FROM logistic_routings_shpts lrsh
    LEFT JOIN logistic_routing_scenarios lrs
    ON lrs.id = lrsh.id_scenario
    LEFT JOIN input_model_header imh
    ON imh.id = lrs.id_input_model
    LEFT JOIN ref_tarifs_ports rtp
    ON rtp.id = lrsh.id_pod
    LEFT JOIN ref_sailing_terminals rst
    ON rst.id = rtp.id_sailing_terminal
    WHERE 
	( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)
	AND $find_fic
    ORDER BY imh.fic_num, lrs.designation, lrsh.esd
    ";
    my $rql = $dbh->prepare( $sql );
	print "\n".$sql."\n" if($dbug);
    $rql->execute();
    if ( $dbh->errstr ne undef ) { # Erreur SQL
		print "\n $dbh_fcs->errstr \n $sql  \n";
      print FICHIER_LOG "\nERROR : $sql";
      close FICHIER_LOG;
      $rql->finish;
      $dbh_fcs->disconnect;
      $dbh->disconnect;
      exit;
    }

    if ( $rql->rows > 0 ) {
      my $last_po = '';
      my $last_bl = '';
      while ( my $data = $rql->fetchrow_hashref) {
        my $po = '';
		my $bl = '';
        if ( $export_level eq "1" ) {
			$po = $data->{'bl'};
			$bl = $data->{'bl'};
		} else {
			my @tabtmp = split(/__/,$data->{'bl'});
			$po = $tabtmp[0];
			$bl = $tabtmp[1];
		}
        if ( $last_po ne $po && $last_po ne '' ) {
          $writer->endTag("bl");
          $writer->endTag("po");
        }
        if ( $last_po ne $po ) {
          $writer->startTag("po",
                            "num" => $po,
                            "id_export_level" => $data->{'id_export_level'},
                            "pod" => $data->{'pod'},
                            "id_transport_mode" => $data->{'id_transport_mode'}
          );
          $last_po = $po;
          $last_bl = '';
        }
        
        if ( $last_bl ne $bl && $last_bl ne '' ) {
          $writer->endTag("bl");
        }
        if ( $last_bl ne $bl ) {
# Recherche du taux de couverture freight
#========================================
my $scouverture_freight = "
 SELECT max(cd.rate_couverture) as rate_freight
 FROM cotation_detail cd
 LEFT JOIN cotation_header ch
 ON ch.id = cd.id_header
 WHERE ch.id_scenario = $data->{'id_scenario'}
 --AND cd.id_shpt = $data->{'id_shpt'}
";
my $rcouverture_freight = $dbh->prepare( $scouverture_freight );
print "\n scouverture_freight : ".$scouverture_freight."\n" if($dbug);
$rcouverture_freight->execute();
my $rate_freight = $rcouverture_freight->fetchrow;
print "\n rate_freight : ".$rate_freight."\n" if($dbug);
$rcouverture_freight->finish;
          my $taux_exchange_fob = $exchange_rate_fob;
          if ( uc(substr($data->{'currency'},0,3)) ne 'USD' && uc(substr($data->{'currency'},0,3)) ne 'EUR' ) {
            $taux_exchange_fob = $exchange_rate / $exchange_rate_fob; # EUR-AUTRE MONNAIE
            $exchange_rate_fob = $exchange_rate;
          } 
          $writer->startTag("bl",
                            "num" => $bl,
                            "currency" => $data->{'currency'},
                            "exchange_rate" => $exchange_rate,
                            "exchange_rate_fob" => $taux_exchange_fob,
			    "linerterm" => $data->{'linerterm'},
			    "rate_freight" => $rate_freight
          ); 
          $last_bl = $bl;

          if ( $data->{'incoterm'} eq '2' ) { # FCA
	    # On n'envoie pas d'équipements pour que le DAU affiche GROUPAGE
	  }
	  else {
            # ON RECHERCHE LES EQUIPEMENTS
            # ============================
			print "\n [$data->{'id_shpt'} ] \n ".$seqctn."\n" if($dbug);
            $reqctn->execute( $data->{'id_shpt'} );
            if ( $dbh->errstr ne undef ) { # Erreur SQL
				print "\n $dbh_fcs->errstr \n $seqctn [$data->{'id_shpt'}]  \n";
              print FICHIER_LOG "\nERROR : $seqctn";
              close FICHIER_LOG;
              $reqctn->finish;
              $rql->finish;
              $dbh_fcs->disconnect;
              $dbh->disconnect;
              exit;
            }  
            while ( my @tab = $reqctn->fetchrow ) {
	      my $k = 0;
    	      while ( $k < $tab[1] ) {  
                $writer->startTag("ct",
                                "type" => $tab[0]
                ); 
                $writer->endTag("ct");
	        $k++;
	      }
  	    }
            $reqctn->finish;
	  }

          # ON RECHERCHE LES SKUS
          # =====================
		print "\n [$data->{'id_shpt'} ] \n ".$seqsku."\n" if($dbug);
  	  $reqsku->execute( $data->{'id_shpt'} );
	  if ( $dbh->errstr ne undef ) { # Erreur SQL
		print "\n $dbh_fcs->errstr \n $seqsku [$data->{'id_shpt'}]  \n";
	    print FICHIER_LOG "\nERROR : $seqsku";
	    close FICHIER_LOG;
	    $reqsku->finish;
	    $rql->finish;
	    $dbh_fcs->disconnect;
	    $dbh->disconnect;
	    exit;
	  }
	  my $last_sku = '';
	  while ( my $data_sku = $reqsku->fetchrow_hashref ) {
	  print "sku:";
      print	  $data_sku->{'sku'};
	  
            if ( $last_sku ne $data_sku->{'sku'} && $last_sku ne '' ) {
              $writer->endTag("sku");
  	    }
            if ( $last_sku ne $data_sku->{'sku'} ) {
my $seqfees = " 
SELECT
( SELECT DISTINCT ON (comment)
  comment FROM cotation_detail cd
  LEFT JOIN cotation_header ch
  ON ch.id = cd.id_header
  LEFT JOIN logistic_routing_scenarios lrs
  ON lrs.id = ch.id_scenario
  LEFT JOIN input_model_header imh
  ON imh.id = lrs.id_input_model
  LEFT JOIN ref_list_of_charges rc
  ON rc.id = cd.id_charge
  WHERE 
  ( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)
	AND $find_fic
  AND cd.id_shpt = $data->{'id_shpt'}
  AND rc.code = 'S_ID'
) as import_dpt_fees_rate,

( SELECT DISTINCT ON (comment)
  comment FROM cotation_detail cd
  LEFT JOIN cotation_header ch
  ON ch.id = cd.id_header
  LEFT JOIN logistic_routing_scenarios lrs
  ON lrs.id = ch.id_scenario
  LEFT JOIN input_model_header imh
  ON imh.id = lrs.id_input_model
  LEFT JOIN ref_list_of_charges rc
  ON rc.id = cd.id_charge
  WHERE 
  ( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)
	AND $find_fic
  AND cd.id_shpt = $data->{'id_shpt'}
  AND rc.code = 'S_IB'
) as import_dpt_fees_rate_by_shpt,
( SELECT DISTINCT ON (comment)
  comment FROM cotation_detail cd
  LEFT JOIN cotation_header ch
  ON ch.id = cd.id_header
  LEFT JOIN logistic_routing_scenarios lrs
  ON lrs.id = ch.id_scenario
  LEFT JOIN input_model_header imh
  ON imh.id = lrs.id_input_model
  LEFT JOIN ref_list_of_charges rc
  ON rc.id = cd.id_charge
  WHERE 
	( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)  
	AND $find_fic
  AND cd.id_shpt = $data->{'id_shpt'}
  AND rc.code = 'S_QC'
) as qc_dpt_fees_rate,
( SELECT DISTINCT ON (comment)
  comment FROM cotation_detail cd
  LEFT JOIN cotation_header ch
  ON ch.id = cd.id_header
  LEFT JOIN logistic_routing_scenarios lrs
  ON lrs.id = ch.id_scenario
  LEFT JOIN input_model_header imh
  ON imh.id = lrs.id_input_model
  LEFT JOIN ref_list_of_charges rc
  ON rc.id = cd.id_charge
  WHERE 
	( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)  
	AND $find_fic
  AND cd.id_shpt = $data->{'id_shpt'}
  AND rc.code = 'S_IR'
) as import_risk_fees_rate,
( SELECT DISTINCT ON (comment)
  comment FROM cotation_detail cd
  LEFT JOIN cotation_header ch
  ON ch.id = cd.id_header
  LEFT JOIN logistic_routing_scenarios lrs
  ON lrs.id = ch.id_scenario
  LEFT JOIN input_model_header imh
  ON imh.id = lrs.id_input_model
  LEFT JOIN ref_list_of_charges rc
  ON rc.id = cd.id_charge
  WHERE 
	( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)  
	AND $find_fic  
  AND cd.id_shpt = $data->{'id_shpt'}
  AND rc.code = 'S_PF'
) as purchase_fees_rate,

( SELECT DISTINCT ON (comment)
  comment FROM cotation_detail cd
  LEFT JOIN cotation_header ch
  ON ch.id = cd.id_header
  LEFT JOIN logistic_routing_scenarios lrs
  ON lrs.id = ch.id_scenario
  LEFT JOIN input_model_header imh
  ON imh.id = lrs.id_input_model
  LEFT JOIN ref_list_of_charges rc
  ON rc.id = cd.id_charge
  WHERE 
	( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)  
	AND $find_fic  
  AND cd.id_shpt = $data->{'id_shpt'}
  AND rc.code = 'S_ER'
) as exchange_rate_fees,


( SELECT DISTINCT ON (comment)
  comment FROM cotation_detail cd
  LEFT JOIN cotation_header ch
  ON ch.id = cd.id_header
  LEFT JOIN logistic_routing_scenarios lrs
  ON lrs.id = ch.id_scenario
  LEFT JOIN input_model_header imh
  ON imh.id = lrs.id_input_model
  LEFT JOIN ref_list_of_charges rc
  ON rc.id = cd.id_charge
  WHERE 
	( 
    	( imh.fic_num LIKE '%~%' AND SPLIT_PART(imh.fic_num,'~',1)  = '$po_root' )
        OR  ( imh.fic_num NOT LIKE '%~%' AND  SPLIT_PART(imh.fic_num,'_', 1) = '$po_root')
	)  
	AND $find_fic
  AND cd.id_shpt = $data->{'id_shpt'}
  AND rc.code = 'S_OF'
) as other_fees_rate
";
my $reqfees = $dbh->prepare( $seqfees );
		print " \n seqfees : ".$seqfees."\n" if($dbug);

	      $reqfees->execute();
	      if ( $dbh->errstr ne undef ) { # Erreur SQL
			print "\n $dbh_fcs->errstr \n $seqfees  \n";
	        print FICHIER_LOG "\nERROR : $seqfees";
	        close FICHIER_LOG;
	        $reqfees->finish;
	        $reqsku->finish;
	        $rql->finish;
	        $dbh_fcs->disconnect;
	        $dbh->disconnect;
	        exit;
	      }
			my $fees = $reqfees->fetchrow_hashref;
		  my $tx_import_fees = $fees->{'import_dpt_fees_rate'} * 100;
		  my @comment = split /\s+/, $fees->{'import_dpt_fees_rate_by_shpt'};
		  my $tx_import_dpt_fees_by_shpt = $comment[1]*100;
	      my $tx_qc_fees = $fees->{'qc_dpt_fees_rate'} * 100;
	      my $tx_import_risk_fees = $fees->{'import_risk_fees_rate'} * 100;
	      my $tx_purchase_fees = $fees->{'purchase_fees_rate'} * 100;
          my $tx_exchange_rate_fees = $fees->{'exchange_rate_fees'} * 100;    
	      my $tx_other_fees = $fees->{'other_fees_rate' * 100};
	      $reqfees->finish;

	      my $ctns = int($data_sku->{'pcs'} / $data_sku->{'pcs_per_ctn'} )  ;
	      my $cbm = ( $data_sku->{'packing_long'} * $data_sku->{'packing_large'} * $data_sku->{'packing_height'} / 1000000 );
		  #$cbm = 1 if($cbm < 1);
		  print " sku : $data_sku->{'sku'} $data_sku->{'po'}" if($dbug);
              $writer->startTag("sku",
	                        "num" => $data_sku->{'sku'},
	                        "sku_size" => $data_sku->{'size'},
	                        "sku_color" => $data_sku->{'color'}
	      );
              $writer->startTag("designation");
              $writer->characters($data_sku->{'designation'});
              $writer->endTag("designation");

              $writer->startTag("pcs");
              $writer->characters($data_sku->{'pcs'});
              $writer->endTag("pcs");

              $writer->startTag("ctns");
              $writer->characters($ctns);
              $writer->endTag("ctns");

              $writer->startTag("cbm");
              $writer->characters($cbm);
              $writer->endTag("cbm");

              $writer->startTag("kgs");
              $writer->characters($data_sku->{'packing_weight'});
              $writer->endTag("kgs");

              $writer->startTag("prices");
              $writer->startTag("fob1");
              $writer->characters($data_sku->{'prix_fob1'} / $exchange_rate_fob);
              $writer->endTag("fob1");

              $writer->startTag("fob2");
              $writer->characters( ($data_sku->{'prix_fob1'} + $data_sku->{'marge'} ) / $exchange_rate_fob);
              $writer->endTag("fob2");
              
		  print "\n [$data_sku->{'id_scenario'}, $data_sku->{'id_sku'} ] \n ".$seqpricetothub."\n" if($dbug);
	      $reqpricetothub->execute( $data_sku->{'id_scenario'}, $data_sku->{'id_sku'} );
	      if ( $dbh->errstr ne undef ) { # Erreur SQL
			print "\n $dbh_fcs->errstr \n $seqsku [$data_sku->{'id_scenario'}, $data_sku->{'id_sku'}]  \n";
	        print FICHIER_LOG "\nERROR : $seqsku";
	        close FICHIER_LOG;
	        $reqsku->finish;
	        $rql->finish;
	        $dbh_fcs->disconnect;
	        $dbh->disconnect;
	        exit;
	      }
	      my $unit_price_hub = 0;
	      my $unit_price = 0;
	      # ATTENTION, DON'T TOUCH
	      while ( my $data_hub_price = $reqpricetothub->fetchrow_hashref ) {
	        if ( $data_hub_price->{'pcs'} > 0 ) {
	          my $tot_purchase_price = $data_hub_price->{'unit_purchase_price'} * $data_hub_price->{'pcs'} ;
                  my $total_transport_hub = $data_hub_price->{'tot_prix_hub'} - $tot_purchase_price;
                  my $total_transport = $data_hub_price->{'tot_prix'} - $tot_purchase_price;
		
		  $unit_price_hub = ($data_hub_price->{'unit_purchase_price'} / $exchange_rate_fob) + ($total_transport_hub / $data_hub_price->{'pcs'} / $exchange_rate);
		  $unit_price = ($data_hub_price->{'unit_purchase_price'} / $exchange_rate_fob) + ($total_transport / $data_hub_price->{'pcs'} / $exchange_rate);
		}
	      }
	      $reqpricetothub->finish;
	      
              $writer->startTag("hub");
	      if ( substr($department,0,4) eq 'Ind_' ) {
	        $writer->characters($unit_price);
	      }
	      else {
	        $writer->characters($unit_price_hub);
	      }
              $writer->endTag("hub");
              $writer->startTag("tx_import_fees");
              $writer->characters($tx_import_fees);
              $writer->endTag("tx_import_fees");
			  $writer->startTag("tx_import_dpt_fees_by_shpt");
              $writer->characters($tx_import_dpt_fees_by_shpt);
              $writer->endTag("tx_import_dpt_fees_by_shpt");
			  
              $writer->startTag("tx_qc_fees");
              $writer->characters($tx_qc_fees);
              $writer->endTag("tx_qc_fees");
              $writer->startTag("tx_import_risk_fees");
              $writer->characters($tx_import_risk_fees);
              $writer->endTag("tx_import_risk_fees");
              $writer->startTag("tx_purchase_fees");
              $writer->characters($tx_purchase_fees);
              $writer->endTag("tx_purchase_fees");
              $writer->startTag("tx_exchange_rate_fees");
              $writer->characters($tx_exchange_rate_fees);
              $writer->endTag("tx_exchange_rate_fees");
              $writer->startTag("tx_other_fees");
              $writer->characters($tx_other_fees);
              $writer->endTag("tx_other_fees");
              $writer->endTag("prices");

	      $writer->endTag("sku");
	      $last_sku = '';
	    }
	  }
	  $reqsku->finish;
        
          # ON RECHERCHE LES COUTS
          # ======================
		print "\n [$data->{'id_shpt'} ] \n ".$seqcost."\n" if($dbug);
  	  $reqcost->execute( $data->{'id_shpt'} );
	  if ( $dbh->errstr ne undef ) { # Erreur SQL
		print "\n $dbh_fcs->errstr \n $seqcost [$data->{'id_shpt'}]  \n";
	    print FICHIER_LOG "\nERROR : $seqcost";
	    close FICHIER_LOG;
	    $reqcost->finish;
	    $rql->finish;
	    $dbh_fcs->disconnect;
	    $dbh->disconnect;
	    exit;
	  }
	  while ( my $data_cost = $reqcost->fetchrow_hashref ) {
            $writer->startTag("invoice",
	                      "cost_kind" => $data_cost->{'type'}
	    );
            $writer->characters($data_cost->{'cost'} / $exchange_rate);
            $writer->endTag("invoice");
	  }
	  $reqcost->finish;

	  # Pour l'engagé, on sort un poste de charge "FREIGHT_POL" comprenant VM+FREIGHT+BAF+CAF
	  # pour calculer les écarts taux de change dans le DAU
	  if ( $export_level eq '3' ) {
		print "\n [$data->{'id_shpt'} ] \n ".$seqcost_couverture."\n" if($dbug);
  	  $reqcost_couverture->execute( $data->{'id_shpt'} );
	  if ( $dbh->errstr ne undef ) { # Erreur SQL
		print "\n $dbh_fcs->errstr \n $seqcost_couverture [$data->{'id_shpt'}]  \n";
	    print FICHIER_LOG "\nERROR : $seqcost_couverture";
	    close FICHIER_LOG;
	    $reqcost_couverture->finish;
	    $rql->finish;
	    $dbh_fcs->disconnect;
	    $dbh->disconnect;
	    exit;
	  }
	  while ( my $data_cost = $reqcost_couverture->fetchrow_hashref ) {
            $writer->startTag("invoice",
	                      "cost_kind" => $data_cost->{'type'}
	    );
            $writer->characters($data_cost->{'cost'} / $exchange_rate);
            $writer->endTag("invoice");
	  }
	  $reqcost_couverture->finish;
          }
        }
      }
      $rql->finish;
      $writer->endTag("bl");
      $writer->endTag("po");
    }
  }
  $writer->endTag("po_root");
  $writer->endTag("uo");
  $writer->endTag("dau");
  $writer->end();
  $output->close();
}

sub get_file_size(){
  my ($file_directory, $file_name ) = @_;
  my $size=0;
  if(-e $file_directory.$file_name){
    open(FILE,  $file_directory.$file_name) || print FICHIER_LOG "\nimpossible d'ouvir le fichier $file_directory.$file_name!";
    seek(FILE,0,2); # se placer a la fin du fichier
    $size=tell(FILE); # obtenir la position de ton curseur
    close (FILE);
  }
  return $size;
}
			   

######################################
#   SCRIPTS DE PASSAGE DE PARAMETRES #
######################################

sub init(@ARGV){
		$log_msg .= "Debut du programme :".`date`."\n";
		for ( my $i = 0 ; $i <= scalar @ARGV ; $i++ ){
						if (get_arg($ARGV[$i]) eq 'u' ){
							$specific_uo = $ARGV[$i+1];
						}elsif (get_arg($ARGV[$i]) eq 'p' ){
							$specific_po = $ARGV[$i+1];
						}elsif (get_arg($ARGV[$i]) eq 'e' ){
							$specific_export_level = $ARGV[$i+1]; 
						}elsif (get_arg($ARGV[$i]) eq 'd' ){
							$specific_nb_day = $ARGV[$i+1];
						}elsif (get_arg($ARGV[$i]) eq 'help' ){
							&init_error();
				}


		}
}

sub init_error(){
	$log_msg .= "
			usage : qf_extract_dau.pl -u uo -p num_po -e export_level ( 1:cotation, 2:checking, 3:arrived_pod ) -d nb_day

			";
		&exit_function;
}

sub get_arg(){
		my ($str_to_return) = @_;
		if (index( $str_to_return, '-' ) > -1){
		  $str_to_return = substr($str_to_return , 1, length($str_to_return));
		} else {
		  $str_to_return = 0
		}
		return ($str_to_return);
}

sub exit_function(){
	my $fichier_log = $bin_directory."/dau/qf_extraction.log";
	open LOGFILE, ">> $fichier_log" or die "Can't open $fichier_log";
		$log_msg .= "\nFin du programme.\n";
	if ($dbug) {
	  print $log_msg;
	} else {
	  print LOGFILE $log_msg;
	}
	close LOGFILE;
		exit;
}


