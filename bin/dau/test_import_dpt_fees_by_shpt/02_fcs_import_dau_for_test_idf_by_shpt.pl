#!/usr/bin/perl

use strict;

use DBI;
use Class::Date qw(:errors date now);
use File::Copy;
use XML::XPath;
use XML::XPath::XMLParser;
use XML::Writer;
use IO::File;
use MIME::Lite;
use MIME::QuotedPrint;
use MIME::Base64;

print "\n 02_fcs_import_dau.pl ".now." \n";


# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $dbug = 1;

my $dau_directory = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR') . "bin/dau/";
my $src_directory = $dau_directory . "src/";
my $fichier_log = $dau_directory . "fcs_import_dau.log";
my $cmd_cotation = esolGetEnv(__FILE__, 'PATH_QF_ROOT_DIR'). "/bin/cotation.pl ";
open( FICHIER_LOG, "> $fichier_log" ) || die "Impossible ouvrir fichier log !\n";
my $tmp_date = now;

# Tentative de connexion à la base:
my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_QF_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
if ( $dbh eq undef ) {
  print FICHIER_LOG "\nERROR Connexion base COTATION\n";
  close FICHIER_LOG;
  exit;
}

my $old_autocommit = $dbh->{AutoCommit};
if ( $old_autocommit == 1 ) {
  $dbh->{AutoCommit} = 0;
}

# ON RECHERCHE LE TAUX DE CONVERSION USD/EUR
# ==========================================
my $sql = " SELECT usd_to_euro_rate FROM ref_global_parameters ";
my $rq = $dbh->prepare( $sql );
$rq->execute;
if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
  print FICHIER_LOG "\nERROR : $dbh->errstr\n";
  $rq->finish;
  $dbh->rollback;
  $dbh->{AutoCommit} = $old_autocommit;
  $dbh->disconnect;
  exit;
}
my $usd_to_euro = $rq->fetchrow;
$rq->finish;

# RECHERCHE LE TAUX DE CONVERSION FOB
# ===================================


my $sql = " SELECT
            CASE WHEN ( ch.usd_to_euro_fob_rate IS NULL OR ch.usd_to_euro_fob_rate = '' ) THEN ch.usd_to_euro_rate
            ELSE ch.usd_to_euro_fob_rate END
            FROM cotation_header ch
            LEFT JOIN logistic_routing_scenarios lrs
            ON lrs.id = ch.id_scenario
            LEFT JOIN input_model_header imh
            ON imh.id = lrs.id_input_model
            WHERE UPPER(SPLIT_PART(imh.fic_num,'_', 1)) = UPPER(SPLIT_PART(?,'_', 1))
            AND SUBSTR(imh.fic_num,11,1) <> '~'
            AND SUBSTR(imh.fic_num,11,1) <> '#'
            AND LENGTH(TRIM(lrs.date_fcs)) = 8
";

my $rq = $dbh->prepare( $sql );

my @loop_scenarios = ();
my $nb_scenarios = 0;


# DECLARATION DES REQUETES
# ========================
my $smarge = " SELECT lrp.other_marge FROM logistic_routings_price lrp
LEFT JOIN logistic_routing_scenarios lrs
ON lrs.id = lrp.id_scenario
LEFT JOIN input_model_header imh
ON imh.id = lrs.id_input_model
LEFT JOIN input_model_skus ims
ON ims.id_header = imh.id
WHERE TRIM(lrs.user_fcs) != ''
AND lrs.user_fcs IS NOT NULL
AND SPLIT_PART(imh.fic_num,'_', 1) = ? 
AND ims.sku_num = ?
AND ims.size = ?
AND ims.color = ?
";
my $rmarge = $dbh->prepare( $smarge );

my $input_model_header = " INSERT into input_model_header
				( fic_num, id_loading_port, currency, id_department, id_supplier,manufacturer_country_code)
				VALUES ( ?, ?, ?,
				( SELECT id FROM ref_departments WHERE designation = ?),
				( SELECT imh.id_supplier
            			  FROM cotation_header ch
            			  LEFT JOIN logistic_routing_scenarios lrs
            			  ON lrs.id = ch.id_scenario
            			  LEFT JOIN input_model_header imh
            			  ON imh.id = lrs.id_input_model
            			  WHERE UPPER(SPLIT_PART(imh.fic_num,'_', 1)) = UPPER(SPLIT_PART(?,'_', 1))
            			  AND SUBSTR(imh.fic_num,11,1) <> '~'
            			  AND SUBSTR(imh.fic_num,11,1) <> '#'
            			  AND LENGTH(TRIM(lrs.date_fcs)) = 8
				),
                ?)
";
my $rinput_model_header = $dbh->prepare( $input_model_header );

my $id_input_model_header = " SELECT id FROM input_model_header WHERE fic_num = ? ";
my $rid_input_model_header = $dbh->prepare( $id_input_model_header );

my $logistic_routing_scenarios = " INSERT into logistic_routing_scenarios
				( designation, id_input_model)
				VALUES ( 'trial_1', ? )
";
my $rlogistic_routing_scenarios = $dbh->prepare( $logistic_routing_scenarios );
my $id_logistic_routing_scenarios = " SELECT lrs.id FROM logistic_routing_scenarios lrs
				      LEFT JOIN input_model_header imh
				      ON imh.id = lrs.id_input_model
				      WHERE fic_num = ?
";
my $rid_logistic_routing_scenarios = $dbh->prepare( $id_logistic_routing_scenarios );

my $logistic_routings_shpts = " INSERT into logistic_routings_shpts
				( id_scenario, esd, id_transport_mode, id_pol, id_pod, id_intermediate_fnd, id_incoterm, id_linerterm, shpt_num )
				VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )
";
my $rlogistic_routings_shpts = $dbh->prepare( $logistic_routings_shpts );
my $id_logistic_routings_shpts = " SELECT id FROM logistic_routings_shpts
					WHERE id_scenario = ?
					AND esd = ?
					AND id_transport_mode = ?
					AND id_pol = ?
					AND id_pod = ?
					AND id_intermediate_fnd = ?
					AND shpt_num = ?
";
my $rid_logistic_routings_shpts = $dbh->prepare( $id_logistic_routings_shpts );

my $input_model_skus = " INSERT into input_model_skus
				( id_header, designation, size, color, unit_price, packing_long, packing_large, packing_height, packing_weight,  packing_pcs_per_ctn, is_enabled, sku_num )
				VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '1', ? )
";
my $rinput_model_skus = $dbh->prepare( $input_model_skus );
my $id_input_model_skus = " SELECT id FROM input_model_skus
				WHERE id_header = ?
				AND sku_num = ?
				AND size = ?
				ANd color = ?
";
my $rid_input_model_skus = $dbh->prepare( $id_input_model_skus );

my $logistic_routings_detail = " INSERT INTO logistic_routings_detail
				( id_shpt, id_input_model_sku_fnd, q_pcs )
				VALUES ( ?, ?, ? )
";
my $rlogistic_routings_detail = $dbh->prepare ( $logistic_routings_detail );

my $input_model_sku_fnds = " INSERT INTO input_model_sku_fnds
				( id_sku, id_fnd, mad, q_pcs, mad_hub, is_enabled )
				VALUES ( ?, ?, ?, ?, ?, '1' )
";
my $rinput_model_sku_fnds = $dbh->prepare( $input_model_sku_fnds  );
my $id_input_model_sku_fnds = " SELECT id FROM input_model_sku_fnds
				WHERE id_sku = ?
				AND id_fnd = ?
				AND mad = ?
-- CD20140414 on ajoute les quantités dans les critères puisqu'il peut y avoir plusieurs lignes pour un même /fnd/mad dans le cas de plusieurs shpt d'un même po qui voyage ensemble.
 				AND q_pcs = ?
";
my $rid_input_model_sku_fnds = $dbh->prepare( $id_input_model_sku_fnds  );

my $logistic_routings_price = " INSERT INTO logistic_routings_price
				( id_scenario, id_sku, unit_price, other_marge )
				VALUES ( ?, ?, ?, ? )
";
my $rlogistic_routings_price = $dbh->prepare( $logistic_routings_price );

my $logistic_routings_equipement = "INSERT INTO logistic_routings_equipement
					( id_shpt, id_container_kinds, nb_container, cbm_charge )
					VALUES ( ?, ?, ?, ? )
";

my $rlogistic_routings_equipement = $dbh->prepare( $logistic_routings_equipement );

# 1ere PARTIE : CHECKING
# ======================
# Les FICs générées ont comme 11eme caractère le "~1"
# ==================================================
print FICHIER_LOG "\n\nDEBUT TRAITEMENT CHECKING du $tmp_date";
my $xml_file = $src_directory . "FCS_EXTRACT_CHECKING.XML";

print "\n xml_file: $xml_file ".now." \n";

my $done_file = $dau_directory . "done/" . "FCS_EXTRACT_CHECKING." . $tmp_date->year . sprintf( "%02d", $tmp_date->month ) . sprintf( "%02d", $tmp_date->day );
if (-e $xml_file){
  # LIT LE FICHIER
  # ==============
  my $xml_content = XML::XPath->new(filename => $xml_file);

  my $nodeset = $xml_content->find('/fics/fic');
  my $last_fic = '';
  my $last_shpt = '';
  foreach my $node ($nodeset->get_nodelist) {
    my $fic = $node->findvalue('@fic');
    my $dpt = $node->findvalue('@dpt');
    &Efface($dbh, $fic);

# ON RECHERCHE LE TAUX DE CONVERSION FOB
# ======================================
$rq->execute($fic);
#$rq->execute($fic,$fic);
if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
  print FICHIER_LOG "\nERROR : $dbh->errstr\n";
  $rq->finish;
  $dbh->rollback;
  $dbh->{AutoCommit} = $old_autocommit;
  $dbh->disconnect;
  exit;
}
my $usd_to_euro_fob_rate = $rq->fetchrow;
$rq->finish;
if ( $usd_to_euro_fob_rate eq '' ) { $usd_to_euro_fob_rate = $usd_to_euro; }
    print FICHIER_LOG "\nFIC $fic (rate FOB = $usd_to_euro_fob_rate)";

    my $shpt_nodeset = $node->findnodes('./shpt');

    my $id_fic = '';
    my $id_scenario = '';

    foreach my $shpt_node ($shpt_nodeset->get_nodelist) {
      my $shpt = $shpt_node->findvalue('@id');
      print FICHIER_LOG "\n ==========> BL $shpt";
      print FICHIER_LOG "\n ==========> BL $shpt" if($dbug);
      my $pol = $shpt_node->findvalue('@pol');
      my $currency = $shpt_node->findvalue('@currency');
      if ( $currency eq 'EUR' ) { $currency = 'euro'; }
      elsif ( $currency eq 'MXN' ) { $currency = 'mxn'; }
      else { $currency = 'usd'; }
      my $esd = $shpt_node->findvalue('@etd');
      my $transport = $shpt_node->findvalue('@transport');
      my $pod = $shpt_node->findvalue('@pod');
      my $hub = $shpt_node->findvalue('@hub');
      my $mad_hub = $shpt_node->findvalue('@mad_hub');
      my $incoterm = $shpt_node->findvalue('@incoterm');
      my $linerterm = $shpt_node->findvalue('@linerterm');

      my $rid_transport = $dbh->prepare( " SELECT id FROM ref_transport_mode
                                           WHERE UPPER(designation) = '$transport'
      " );
      $rid_transport->execute;
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_transport->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_transport = $rid_transport->fetchrow;
      $rid_transport->finish;

      my $rid_pol = '';
      if ( $transport eq "SEA" ) { 
        $rid_pol = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
     				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pol',1,2)
				   AND rst.town_code = SUBSTR('$pol',3,3)
				   AND rst.is_sea = '1' 
				   AND rtp.id_transport_mode = '1'" );
      }
      if ( $transport eq "AIR" ) { 
        $rid_pol = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
    				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pol',1,2)
				   AND rst.town_code = SUBSTR('$pol',3,3)
				   AND rst.is_air = '1' 
				   AND rtp.id_transport_mode = '4'" );
      }
      $rid_pol->execute;
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_pol->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_pol = $rid_pol->fetchrow;
      $rid_pol->finish;
    
      my $rid_pod = '';
      if ( $transport eq "SEA" ) { 
        $rid_pod = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
    				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pod',1,2)
				   AND rst.town_code = SUBSTR('$pod',3,3)
				   AND rst.is_sea = '1'
				   AND rtp.id_transport_mode = '1'" );
      } 
      if ( $transport eq "AIR" ) { 
        $rid_pod = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
    				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pod',1,2)
				   AND rst.town_code = SUBSTR('$pod',3,3)
				   AND rst.is_air = '1'
				   AND rtp.id_transport_mode = '4'" );
      }
      $rid_pod->execute;
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_pod->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_pod = $rid_pod->fetchrow;
      $rid_pod->finish;

      if ( $last_fic ne $fic ) {
        # ON CREE INPUT_MODEL_HEADER
        # ==========================
		
		my $manufacturer_country_code_from_cotation =&get_manufacturer_country_code_from_cotation( $dbh,$fic,$dbug );
		
        $rinput_model_header->execute( $fic, $pol, $currency, $dpt, $fic ,$manufacturer_country_code_from_cotation);
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rinput_model_header->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $rinput_model_header->finish;

        # ON RECHERCHE l'ID DE LA FIC
        # ===========================
        $rid_input_model_header->execute( $fic );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rid_input_model_header->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $id_fic = $rid_input_model_header->fetchrow;
        $rid_input_model_header->finish;

        # ON CREE LOGISTIC_ROUTING_SCENARIOS
        # ==================================
        $rlogistic_routing_scenarios->execute( $id_fic );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rlogistic_routing_scenarios->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $rlogistic_routing_scenarios->finish;

        # ON RECHERCHE l'ID DU SCENARIO 
        # =============================
        $rid_logistic_routing_scenarios->execute( $fic );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rid_logistic_routing_scenarios->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $id_scenario = $rid_logistic_routing_scenarios->fetchrow;
        $rid_logistic_routing_scenarios->finish;
 
        my %hash = ();
        $hash{ID} = $id_scenario;
        $hash{FIC} = $fic;
        $hash{EXTRACT} = "1";
        push (@loop_scenarios, \%hash);
        $nb_scenarios++;
        $last_fic = $fic;
      }

      # ON RECHERCHE l'ID de L'INCOTERM
      # ===============================
      my $rid_incoterm = $dbh->prepare(" SELECT id FROM ref_incoterms WHERE code_fcs = ? ");
      $rid_incoterm->execute( $incoterm );
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_incoterm->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_incoterm = $rid_incoterm->fetchrow;
      $rid_incoterm->finish;

      # ON RECHERCHE l'ID du LINERTERM
      # ==============================
      my $rid_linerterm = $dbh->prepare(" SELECT id FROM ref_linerterms WHERE designation = ? ");
      $rid_linerterm->execute( $linerterm );
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_linerterm->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_linerterm = $rid_linerterm->fetchrow;
      $rid_linerterm->finish;

      # On recherche l'Id du FND
      # ========================
      my $rid_hub = $dbh->prepare( " SELECT id, is_intermediate, is_final FROM ref_fnds WHERE designation = ? ");
      $rid_hub->execute( $hub);
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_hub->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_hub = 0;
      my $is_intermediate = 0;
      my $is_final = 0;
      while ( my @tab_fnd =  $rid_hub->fetchrow ) {
        if ( $tab_fnd[1] == 1 ) { $is_intermediate = 1; }
        if ( $tab_fnd[2] == 1 ) { $is_final = 1; }
        $id_hub = $tab_fnd[0];
      }
      $rid_hub->finish;

	  print "logistic_routings_shpts :\n $logistic_routings_shpts\n" if($dbug);
	  print "is_intermediate : $is_intermediate \n" if($dbug);
	  print "is_final : $is_final \n" if($dbug);
	  print " $id_scenario, $esd, $id_transport, $id_pol, $id_pod, $id_hub, $id_incoterm, $id_linerterm, $shpt \n" if($dbug);

      if ( $is_intermediate == 0 && $is_final == 0 ) { } # HUB/CPC inconnus dans cotation, JE NE FAIS RIEN 
      else {
        # ON CREE LE SHPT
        # ==============
        $rlogistic_routings_shpts->execute( $id_scenario, $esd, $id_transport, $id_pol, $id_pod, $id_hub, $id_incoterm, $id_linerterm, $shpt); 
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rlogistic_routings_shpts->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $rlogistic_routings_shpts->finish;

        # On recherche l'Id du SHPT
        # =========================
        my $id_shpt = '';
        $rid_logistic_routings_shpts->execute( $id_scenario, $esd, $id_transport, $id_pol, $id_pod, $id_hub, $shpt );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rid_logistic_routings_shpts->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $id_shpt = $rid_logistic_routings_shpts->fetchrow;

        $rid_logistic_routings_shpts->finish;

        my $sku_nodeset = $shpt_node->findnodes('./sku');
        my $id_sku = '';
        foreach my $sku_node ($sku_nodeset->get_nodelist) {
          my $sku = $sku_node->findvalue('@id');
          my $size = $sku_node->findvalue('@size');
          my $color = $sku_node->findvalue('@color');
          my $name = $sku_node->findvalue('@name');
          my $price = eval($sku_node->findvalue('@price'));
          if ( $currency eq 'euro' ) {
            $price = $price * $usd_to_euro_fob_rate;
          }
          if ( $currency eq 'mxn' ) {
            $price = $price * $usd_to_euro_fob_rate;
          }
          my $ctn = eval($sku_node->findvalue('@ctn'));
          my $ctn_long = eval($sku_node->findvalue('@ctn_long'));
          my $ctn_large = eval($sku_node->findvalue('@ctn_large'));
          my $ctn_height = eval($sku_node->findvalue('@ctn_height'));
          my $ctn_weight = eval($sku_node->findvalue('@ctn_weight'));
          my $pcs_per_ctn = eval($sku_node->findvalue('@pcs_per_ctn')); 
          my $qty = eval($sku_node->findvalue('@qty'));

          # On recherche l'Id du SKU
          # ========================
          $rid_input_model_skus->execute( $id_fic, $sku, $size, $color);
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_input_model_skus->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          my $sku_exist = $rid_input_model_skus->rows;
          $id_sku = $rid_input_model_skus->fetchrow;
          $rid_input_model_skus->finish;

          if ( $sku_exist == 0 ) {
            # ON CREE INPUT_MODEL_SKUS
            # ========================
			

            $rinput_model_skus->execute( $id_fic, $name, $size, $color, $price, $ctn_long, $ctn_large, $ctn_height, $ctn_weight, $pcs_per_ctn, $sku );
            print( $id_fic, $name, $size, $color, $price, "]", $ctn_long , "[" , $ctn_large , "[" , $ctn_height, "[", $ctn_weight, $pcs_per_ctn, $sku ) if($dbug);
			print "\n" if($dbug);
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rinput_model_skus->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            $rinput_model_skus->finish;
          }

          # On recherche l'Id du SKU
          # ========================
          $rid_input_model_skus->execute( $id_fic, $sku, $size, $color);
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_input_model_skus->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          $id_sku = $rid_input_model_skus->fetchrow;
          $rid_input_model_skus->finish;

          if ( $sku_exist == 0 ) {
	    # ON RECHERCHE LA MARGE dans le SCENARIO qui a été importé dans FCS
            my $fic_root = substr($fic,0,10);
            $rmarge->execute( $fic_root, $sku, $size, $color );
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rmarge->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            my $marge = $rmarge->fetchrow;
            $rmarge->finish;

            # ON CREE LOGISTIC_ROUTINGS_PRICE
            # ===============================
            $rlogistic_routings_price->execute( $id_scenario, $id_sku, $price, $marge );
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rlogistic_routings_price->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            $rlogistic_routings_price->finish;
          }

          my $cpc_nodeset = $sku_node->findnodes('./cpc');
          foreach my $cpc_node ($cpc_nodeset->get_nodelist) {
            my $cpc = $cpc_node->findvalue('@cpc');
            my $mad_cpc = $cpc_node->findvalue('@mad_cpc');
            my $qty = $cpc_node->findvalue('@qty');

            # On recherche l'Id du FND
            # =======================
            my $rid_hub = $dbh->prepare( " SELECT id FROM ref_fnds WHERE designation = ? AND is_final = '1' ");
            $rid_hub->execute($cpc );
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rid_hub->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            my $id_cpc = $rid_hub->fetchrow;
            $rid_hub->finish;

            if ( $id_cpc > 0 ) { # le CPC existe dans COTATION
				print "\n".$input_model_sku_fnds."\n"."( $id_sku, $id_cpc, $mad_cpc, $qty, $mad_hub )" if($dbug);

              $rinput_model_sku_fnds->execute( $id_sku, $id_cpc, $mad_cpc, $qty, $mad_hub );
              if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
                print FICHIER_LOG "\nERROR : $dbh->errstr\n";
                $rinput_model_sku_fnds->finish;
                $dbh->rollback;
                $dbh->{AutoCommit} = $old_autocommit;
                $dbh->disconnect;
                exit;
              }
              $rinput_model_sku_fnds->finish;
 
              # ON RECHERCHE L'Id de input_model_sku_fnds
              # =========================================
              $rid_input_model_sku_fnds->execute( $id_sku, $id_cpc, $mad_cpc, $qty );
print "\n $id_input_model_sku_fnds \n ( $id_sku, $id_cpc, $mad_cpc  )" if($dbug);
              if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
                print FICHIER_LOG "\nERROR : $dbh->errstr\n";
                $rid_input_model_sku_fnds->finish;
                $dbh->rollback;
                $dbh->{AutoCommit} = $old_autocommit;
                $dbh->disconnect;
                exit;
              }
              my $id_sku_fnd = $rid_input_model_sku_fnds->fetchrow;
              $rid_input_model_sku_fnds->finish;
print "\n $logistic_routings_detail \n ( $id_shpt, $id_sku_fnd, $qty )" if($dbug);
              $rlogistic_routings_detail->execute( $id_shpt, $id_sku_fnd, $qty );
              if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
                print FICHIER_LOG "\nERROR : $dbh->errstr\n";
                $rlogistic_routings_detail->finish;
                $dbh->rollback;
                $dbh->{AutoCommit} = $old_autocommit;
                $dbh->disconnect;
                exit;
              }
              $rlogistic_routings_detail->finish;
            }
          }
        } # foreach sku

        my $Au_Moins_Un_CT = 0;
        my $equipment_nodeset = $shpt_node->findnodes('./equipment');
        foreach my $equipment_node ($equipment_nodeset->get_nodelist) {
          my $ct_type = $equipment_node->findvalue('@type');
          my $ct_qty = $equipment_node->findvalue('@qty');
		  
		  
		  my $cbm_charge='0';
		  if($linerterm eq 'CFS/CY' || $linerterm eq 'CFSCY'){
		  $cbm_charge = $equipment_node->findvalue('@cbm_charge');
		  }		  

          # On recherche l'Id du CT
          # ========================
          my $rid_container = $dbh->prepare( "SELECT id FROM ref_container_kinds WHERE code_fcs = ?" );
          $rid_container->execute( $ct_type);
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_container->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          # Si le container est référencé dans Quot@Freight
          # ===============================================
          if ( $rid_container->rows > 0 ) {
            my $id_container = $rid_container->fetchrow;
            $rid_container->finish;

            $Au_Moins_Un_CT++;
            $rlogistic_routings_equipement->execute( $id_shpt, $id_container, $ct_qty, $cbm_charge );
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rlogistic_routings_equipement->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            $rlogistic_routings_equipement->finish;
          }
        }
        # Si AUCUN CONTAINER on REPOSITIONNE le LINERTERM en LCL
        # ======================================================
        if ( $Au_Moins_Un_CT == 0 && $id_shpt != 0 ) {
          # ON RECHERCHE l'ID du LINERTERM
          # ==============================
          my $rid_linerterm = $dbh->prepare(" SELECT id FROM ref_linerterms WHERE designation = 'LCL' ");
          $rid_linerterm->execute();
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_linerterm->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          my $id_linerterm = $rid_linerterm->fetchrow;
          $rid_linerterm->finish;
	
          my $sajuste = " UPDATE logistic_routings_shpts SET id_linerterm = '$id_linerterm'
			WHERE id = '$id_shpt'
          ";
          my $rajuste = $dbh->prepare( $sajuste ); 
          $rajuste->execute;
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rajuste->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          $rajuste->finish;
	}
      } # de creation shpt
    } # foreach shpt
  } # foreach fic
  move($xml_file, $done_file);
  $tmp_date = now;
  print FICHIER_LOG "\nFIN TRAITEMENT à $tmp_date\n";
}
else {
  print FICHIER_LOG "\nERROR Pas de fichier CHECKING à traiter\n";
}

# 2de PARTIE : ARRIVED_POD
# ========================
# Les FICs générées ont comme 11eme caractère le "~2"
# ==================================================
print FICHIER_LOG "\n\nDEBUT TRAITEMENT ARRIVED_POD du $tmp_date";
my $xml_file = $src_directory . "FCS_EXTRACT_ARRIVED_POD.XML";

print "\n xml_file: $xml_file ".now." \n";

my $done_file = $dau_directory . "done/" . "FCS_EXTRACT_ARRIVED_POD." . $tmp_date->year . sprintf( "%02d", $tmp_date->month ) . sprintf( "%02d", $tmp_date->day );
if (-e $xml_file){
  # LIT LE FICHIER
  # ==============
  my $xml_content = XML::XPath->new(filename => $xml_file);

  my $nodeset = $xml_content->find('/fics/fic');
  my $last_fic = '';
  my $last_shpt = '';
  foreach my $node ($nodeset->get_nodelist) {
    my $fic = $node->findvalue('@fic');
    my $dpt = $node->findvalue('@dpt');
    &Efface($dbh, $fic);
# ON RECHERCHE LE TAUX DE CONVERSION FOB
# ======================================
#$rq->execute($fic,$fic);
$rq->execute($fic);
if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
  print FICHIER_LOG "\nERROR : $dbh->errstr\n";
  $rq->finish;
  $dbh->rollback;
  $dbh->{AutoCommit} = $old_autocommit;
  $dbh->disconnect;
  exit;
}
my $usd_to_euro_fob_rate = $rq->fetchrow;
$rq->finish;
if ( $usd_to_euro_fob_rate eq '' ) { $usd_to_euro_fob_rate = $usd_to_euro; }
    print FICHIER_LOG "\nFIC $fic (rate FOB = $usd_to_euro_fob_rate)";
    my $shpt_nodeset = $node->findnodes('./shpt');
    my $id_fic = '';
    my $id_scenario = '';

    foreach my $shpt_node ($shpt_nodeset->get_nodelist) {
      my $shpt = $shpt_node->findvalue('@id');
      print FICHIER_LOG "\n ==========> BL $shpt";
      my $pol = $shpt_node->findvalue('@pol');
      my $currency = $shpt_node->findvalue('@currency');
      if ( $currency eq 'EUR') { $currency = 'euro'; }
      elsif ( $currency eq 'MXN' ) { $currency = 'mxn'; }
      else { $currency = 'usd'; }
      my $esd = $shpt_node->findvalue('@etd');
      my $transport = $shpt_node->findvalue('@transport');
      my $pod = $shpt_node->findvalue('@pod');
      my $hub = $shpt_node->findvalue('@hub');
      my $mad_hub = $shpt_node->findvalue('@mad_hub');
      my $incoterm = $shpt_node->findvalue('@incoterm');
      my $linerterm = $shpt_node->findvalue('@linerterm');

      my $rid_transport = $dbh->prepare( " SELECT id FROM ref_transport_mode
                                           WHERE UPPER(designation) = '$transport'
      " );
      $rid_transport->execute;
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_transport->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_transport = $rid_transport->fetchrow;
      $rid_transport->finish;

      my $rid_pol = '';
      if ( $transport eq "SEA" ) { 
        $rid_pol = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
     				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pol',1,2)
				   AND rst.town_code = SUBSTR('$pol',3,3)
				   AND rst.is_sea = '1'
				   AND rtp.id_transport_mode = '1'" );
      }
      if ( $transport eq "AIR" ) { 
        $rid_pol = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
    				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pol',1,2)
				   AND rst.town_code = SUBSTR('$pol',3,3)
				   AND rst.is_air = '1'
				   AND rtp.id_transport_mode = '4'" );
      }
      $rid_pol->execute;
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_pol->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_pol = $rid_pol->fetchrow;
      $rid_pol->finish;
    
      my $rid_pod = '';
      if ( $transport eq "SEA" ) { 
        $rid_pod = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
    				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pod',1,2)
				   AND rst.town_code = SUBSTR('$pod',3,3)
				   AND rst.is_sea = '1'
				   AND rtp.id_transport_mode = '1'" );
      }
      if ( $transport eq "AIR" ) { 
        $rid_pod = $dbh->prepare( " SELECT rtp.id FROM ref_tarifs_ports rtp
    				   LEFT JOIN ref_sailing_terminals rst
				   ON rst.id = rtp.id_sailing_terminal
				   WHERE rst.country_code = SUBSTR('$pod',1,2)
				   AND rst.town_code = SUBSTR('$pod',3,3)
				   AND rst.is_air = '1'
				   AND rtp.id_transport_mode = '4'" );
      }
      $rid_pod->execute;
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_pod->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_pod = $rid_pod->fetchrow;
      $rid_pod->finish;

      if ( $last_fic ne $fic ) {
        # ON CREE INPUT_MODEL_HEADER
        # ==========================
		
		my $manufacturer_country_code_from_cotation =&get_manufacturer_country_code_from_cotation( $dbh,$fic,$dbug );
	
        $rinput_model_header->execute( $fic, $pol, $currency, $dpt, $fic, $manufacturer_country_code_from_cotation );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rinput_model_header->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $rinput_model_header->finish;

        # ON RECHERCHE l'ID DE LA FIC
        # ===========================
        $rid_input_model_header->execute( $fic );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rid_input_model_header->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $id_fic = $rid_input_model_header->fetchrow;
        $rid_input_model_header->finish;

        # ON CREE LOGISTIC_ROUTING_SCENARIOS
        # ==================================
        $rlogistic_routing_scenarios->execute( $id_fic );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rlogistic_routing_scenarios->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $rlogistic_routing_scenarios->finish;

        # ON RECHERCHE l'ID DU SCENARIO 
        # =============================
        $rid_logistic_routing_scenarios->execute( $fic );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rid_logistic_routing_scenarios->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $id_scenario = $rid_logistic_routing_scenarios->fetchrow;
        $rid_logistic_routing_scenarios->finish;

        my %hash = ();
        $hash{ID} = $id_scenario;
        $hash{FIC} = $fic;
        $hash{EXTRACT} = "2";
        push (@loop_scenarios, \%hash);
        $nb_scenarios++;
        $last_fic = $fic;
      }

      # ON RECHERCHE l'ID de L'INCOTERM
      # ===============================
      my $rid_incoterm = $dbh->prepare(" SELECT id FROM ref_incoterms WHERE code_fcs = ? ");
      $rid_incoterm->execute( $incoterm );
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_incoterm->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_incoterm = $rid_incoterm->fetchrow;
      $rid_incoterm->finish;

      # ON RECHERCHE l'ID du LINERTERM
      # ==============================
      my $rid_linerterm = $dbh->prepare(" SELECT id FROM ref_linerterms WHERE designation = ? ");
      $rid_linerterm->execute( $linerterm );
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_linerterm->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_linerterm = $rid_linerterm->fetchrow;
      $rid_linerterm->finish;

      # On recherche l'Id du FND
      # ========================
      my $rid_hub = $dbh->prepare( " SELECT id, is_intermediate, is_final FROM ref_fnds WHERE designation = ? ");
      $rid_hub->execute( $hub);
      if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
        print FICHIER_LOG "\nERROR : $dbh->errstr\n";
        $rid_hub->finish;
        $dbh->rollback;
        $dbh->{AutoCommit} = $old_autocommit;
        $dbh->disconnect;
        exit;
      }
      my $id_hub = 0;
      my $is_intermediate = 0;
      my $is_final = 0;
      while ( my @tab_fnd =  $rid_hub->fetchrow ) {
        if ( $tab_fnd[1] == 1 ) { $is_intermediate = 1; }
        if ( $tab_fnd[2] == 1 ) { $is_final = 1; }
        $id_hub = $tab_fnd[0];
      }
      $rid_hub->finish;

      if ( $is_intermediate == 0 && $is_final == 0 ) { } # HUB/CPC inconnus dans cotation, JE NE FAIS RIEN 
      else {
        # ON CREE LE SHPT
        # ==============
        $rlogistic_routings_shpts->execute( $id_scenario, $esd, $id_transport, $id_pol, $id_pod, $id_hub, $id_incoterm, $id_linerterm, $shpt); 
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rlogistic_routings_shpts->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $rlogistic_routings_shpts->finish;

        # On recherche l'Id du SHPT
        # =========================
        my $id_shpt = '';
        $rid_logistic_routings_shpts->execute( $id_scenario, $esd, $id_transport, $id_pol, $id_pod, $id_hub, $shpt );
        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
          print FICHIER_LOG "\nERROR : $dbh->errstr\n";
          $rid_logistic_routings_shpts->finish;
          $dbh->rollback;
          $dbh->{AutoCommit} = $old_autocommit;
          $dbh->disconnect;
          exit;
        }
        $id_shpt = $rid_logistic_routings_shpts->fetchrow;

        $rid_logistic_routings_shpts->finish;

        my $sku_nodeset = $shpt_node->findnodes('./sku');
        my $id_sku = '';
        foreach my $sku_node ($sku_nodeset->get_nodelist) {
          my $sku = $sku_node->findvalue('@id');
          my $size = $sku_node->findvalue('@size');
          my $color = $sku_node->findvalue('@color');
          my $name = $sku_node->findvalue('@name');
          my $price = eval($sku_node->findvalue('@price'));
          if ( $currency eq 'euro' ) {
            $price = $price * $usd_to_euro_fob_rate;
          }
          if ( $currency eq 'mxn' ) {
            $price = $price * $usd_to_euro_fob_rate;
          }
          my $ctn_long = eval($sku_node->findvalue('@ctn_long'));
          my $ctn_large = eval($sku_node->findvalue('@ctn_large'));
          my $ctn_height = eval($sku_node->findvalue('@ctn_height'));
          my $ctn_weight = eval($sku_node->findvalue('@ctn_weight'));
#          my $pcs = eval($sku_node->findvalue('@pcs'));
          my $ctn = eval($sku_node->findvalue('@ctn'));
          my $pcs_per_ctn = int( eval($sku_node->findvalue('@pcs_per_ctn')) ); 

          # On recherche l'Id du SKU
          # ========================
          $rid_input_model_skus->execute( $id_fic, $sku, $size, $color);
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_input_model_skus->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          my $sku_exist = $rid_input_model_skus->rows;
          $id_sku = $rid_input_model_skus->fetchrow;
          $rid_input_model_skus->finish;

          if ( $sku_exist == 0 ) {
            # ON CREE INPUT_MODEL_SKUS
            # ========================
            $rinput_model_skus->execute( $id_fic, $name, $size, $color, $price, $ctn_long, $ctn_large, $ctn_height, $ctn_weight, $pcs_per_ctn, $sku );
            print( $id_fic, $name, $size, $color, $price, "]", $ctn_long , "[" , $ctn_large , "[" , $ctn_height, "[", $ctn_weight, $pcs_per_ctn, $sku );
			print "\n";
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rinput_model_skus->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            $rinput_model_skus->finish;
          }

          # On recherche l'Id du SKU
          # ========================
          $rid_input_model_skus->execute( $id_fic, $sku, $size, $color);
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_input_model_skus->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          $id_sku = $rid_input_model_skus->fetchrow;
          $rid_input_model_skus->finish;

          if ( $sku_exist == 0 ) {
	    # ON RECHERCHE LA MARGE dans le SCENARIO qui a été importé dans FCS
  	    my $fic_root = substr($fic,0,10);
            $rmarge->execute( $fic_root, $sku, $size, $color );
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rmarge->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
	    my $marge = $rmarge->fetchrow;
            $rmarge->finish;

      	    # ON CREE LOGISTIC_ROUTINGS_PRICE
 	    # ===============================
	    $rlogistic_routings_price->execute( $id_scenario, $id_sku, $price, $marge );
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rlogistic_routings_price->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            $rlogistic_routings_price->finish;
          }

          my $cpc_nodeset = $sku_node->findnodes('./cpc');
          foreach my $cpc_node ($cpc_nodeset->get_nodelist) {
            my $cpc = $cpc_node->findvalue('@cpc');
            my $mad_cpc = $cpc_node->findvalue('@mad_cpc');
            my $qty = $cpc_node->findvalue('@qty');

            # On recherche l'Id du FND
            # ========================
            my $rid_hub = $dbh->prepare( " SELECT id FROM ref_fnds WHERE designation = ? AND is_final = '1' ");
            $rid_hub->execute( $cpc);
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
              print FICHIER_LOG "\nERROR : $dbh->errstr\n";
              $rid_hub->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            my $id_cpc = $rid_hub->fetchrow;
            $rid_hub->finish;
            if ( $id_cpc > 0 ) { # le CPC existe dans COTATION
				print "\n".$input_model_sku_fnds."\n"."( $id_sku, $id_cpc, $mad_cpc, $qty, $mad_hub )" if($dbug);
              $rinput_model_sku_fnds->execute( $id_sku, $id_cpc, $mad_cpc, $qty, $mad_hub );
              if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
				my $sqlr = " INSERT INTO input_model_sku_fnds
				( id_sku, id_fnd, mad, q_pcs, mad_hub, is_enabled )
				VALUES ( '$id_sku', '$id_cpc', '$mad_cpc', '$qty', '$mad_hub', '1' )
				";

				print "\n $dbh->errstr [ fic : $fic ] \n $sqlr \n";
                print FICHIER_LOG "\n $dbh->errstr \n $sqlr \n";
                $rinput_model_sku_fnds->finish;
                $dbh->rollback;
                $dbh->{AutoCommit} = $old_autocommit;
                $dbh->disconnect;
                exit;
              }
              $rinput_model_sku_fnds->finish;

              # ON RECHERCHE L'Id de input_model_sku_fnds
              # =========================================
              $rid_input_model_sku_fnds->execute( $id_sku, $id_cpc, $mad_cpc, $qty );
              if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
                print FICHIER_LOG "\nERROR : $dbh->errstr\n";
                $rid_input_model_sku_fnds->finish;
                $dbh->rollback;
                $dbh->{AutoCommit} = $old_autocommit;
                $dbh->disconnect;
                exit;
              }
              my $id_sku_fnd = $rid_input_model_sku_fnds->fetchrow;
              $rid_input_model_sku_fnds->finish;

              $rlogistic_routings_detail->execute( $id_shpt, $id_sku_fnd, $qty );
              if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
                print FICHIER_LOG "\nERROR : $dbh->errstr\n";
                $rlogistic_routings_detail->finish;
                $dbh->rollback;
                $dbh->{AutoCommit} = $old_autocommit;
                $dbh->disconnect;
                exit;
              }
              $rlogistic_routings_detail->finish;
	    }  
          }
        } # de foreach sku

        my $Au_Moins_Un_CT = 0;
        my $equipment_nodeset = $shpt_node->findnodes('./equipment');
        foreach my $equipment_node ($equipment_nodeset->get_nodelist) {
          my $ct_type = $equipment_node->findvalue('@type');
          my $ct_qty = $equipment_node->findvalue('@qty');
	      my $cbm_charge = $equipment_node->findvalue('@cbm_charge');

          # On recherche l'Id du CT
          # ========================
          my $rid_container = $dbh->prepare( "SELECT id FROM ref_container_kinds WHERE code_fcs = ?" );
          $rid_container->execute( $ct_type);
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_container->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          # Si le container est référencé dasn Quot@Freight
          # ===============================================
          if ( $rid_container->rows > 0 ) {
            my $id_container = $rid_container->fetchrow;
            $rid_container->finish;

            $Au_Moins_Un_CT++;
			print " \n fic $fic " if($dbug);
			print " \n id_shpt $id_shpt " if($dbug);
			print " \n id_container $id_container " if($dbug);
			print " \n ct_qty $ct_qty " if($dbug);
			print " \n cbm_charge $cbm_charge " if($dbug);
            $rlogistic_routings_equipement->execute( $id_shpt, $id_container, $ct_qty, $cbm_charge );
            if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
				print FICHIER_LOG "\nERROR : $dbh->errstr\n";
			  print "\n--$fic \n INSERT INTO logistic_routings_equipement
					( id_shpt, id_container_kinds, nb_container, cbm_charge )
					VALUES ( '$id_shpt', '$id_container', '$ct_qty', '$cbm_charge' );\n";
              $rlogistic_routings_equipement->finish;
              $dbh->rollback;
              $dbh->{AutoCommit} = $old_autocommit;
              $dbh->disconnect;
              exit;
            }
            $rlogistic_routings_equipement->finish;
          }
        }
        # Si AUCUN CONTAINER on REPOSITIONNE le LINERTERM en LCL
        # ======================================================
        if ( $Au_Moins_Un_CT == 0 && $id_shpt != 0 ) {
          # ON RECHERCHE l'ID du LINERTERM
          # ==============================
          my $rid_linerterm = $dbh->prepare(" SELECT id FROM ref_linerterms WHERE designation = 'LCL' ");
          $rid_linerterm->execute();
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rid_linerterm->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          my $id_linerterm = $rid_linerterm->fetchrow;
          $rid_linerterm->finish;
	
          my $sajuste = " UPDATE logistic_routings_shpts SET id_linerterm = '$id_linerterm'
			WHERE id = '$id_shpt'
          ";
          my $rajuste = $dbh->prepare( $sajuste ); 
          $rajuste->execute;
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rajuste->finish;
            $dbh->rollback;
            $dbh->{AutoCommit} = $old_autocommit;
            $dbh->disconnect;
            exit;
          }
          $rajuste->finish;
        }
      } # de création shpt
    } # de foreach shpt
  } # de foreach fic 
  move($xml_file, $done_file);
  $tmp_date = now;
  print FICHIER_LOG "\nFIN TRAITEMENT à $tmp_date\n";
}
else {
  print FICHIER_LOG "\nERROR Pas de fichier ARRIVED_POD à traiter\n";
}

$dbh->commit;
$dbh->{AutoCommit} = $old_autocommit;
$dbh->disconnect;

# ON LANCE le calcul des COTATIONS
for ( my $i=0; $i < $nb_scenarios; $i++ ) {
  my $id_scenario = $loop_scenarios[$i]{'ID'};
  my $fic = $loop_scenarios[$i]{'FIC'};
  my $extract = $loop_scenarios[$i]{'EXTRACT'};
  
	if(index($fic, '~1B') eq -1){
		my $log_redirection = " 1>>".$fichier_log ." 2>>".$fichier_log;
		my $cmd = $cmd_cotation . $id_scenario . " 1"; # with détail fnd-sku, sans Markup
		if ( $extract eq "2" ) { # ARRIVED_POD
			my $cmd .= " 1"; # with détail fnd-sku, with Markup
		}
		system($cmd.$log_redirection) == 0 or print "system $cmd failed: $?" ;
#		if ( $extract eq "1" ) { # CHECKING
#			my $cmd = $cmd_cotation . $id_scenario . " 1"; # with détail fnd-sku, sans Markup
#			system($cmd.$log_redirection) == 0 or die print "system $cmd failed: $?" ;
#		}
#		if ( $extract eq "2" ) { # ARRIVED_POD
#			my $cmd = $cmd_cotation . $id_scenario . " 1" . " 1"; # with détail fnd-sku, with Markup
#			system($cmd.$log_redirection) == 0 or die print "system $cmd failed: $?" ;
#		}
	}
}
close FICHIER_LOG;


# Déplacement des fichiers traités
# ================================

my $src_file_checking = "FCS_EXTRACT_CHECKING.XML";
my $src_file_arrived_pod = "FCS_EXTRACT_ARRIVED_POD.XML";
my $src_file_arrived_pod_dau = "FCS_EXTRACT_ARRIVED_POD_DAU.XML";

my $date_log = now;
my $xml_file = $src_directory . $src_file_checking;
my $done_file = $dau_directory . "done/" . "FCS_EXTRACT_CHECKING." . $date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day );
move($xml_file, $done_file);

$xml_file = $src_directory . $src_file_arrived_pod;
$done_file = $dau_directory . "done/" . "FCS_EXTRACT_ARRIVED_POD." . $date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day );
move($xml_file, $done_file);

$xml_file = $src_directory . $src_file_arrived_pod_dau;
$done_file = $dau_directory . "done/" . "FCS_EXTRACT_ARRIVED_POD_DAU." . $date_log->year . sprintf( "%02d", $date_log->month ) . sprintf( "%02d", $date_log->day );

$date_log = now;
print FICHIER_LOG "\nFIN TRAITEMENT à $date_log\n";
close FICHIER_LOG;


# ON ENVOI LE DIAG PAR MAIL
my $mime_msg = MIME::Lite->new(
  From    => 'YR <no-reply@fcsystem.com>',
  Subject => 'Rapport integration COTYR (tildes) pour DAU',
  To      => 'support@fcsystem.com',
  Type    => 'multipart/mixed'
) or print STDERR ("Error on creating email!\n");
$mime_msg->attach(
  Type => 'TEXT',
  Path => $dau_directory . 'fcs_import_dau.log',
  ReadNow  => 1,
  Filename => $dau_directory . 'fcs_import_dau.log'
) or print STDERR ("Error on attachment email!\n");


$mime_msg->send_by_smtp(esolGetEnv(__FILE__, 'SMTP_SERVER')) or print STDERR ("Error on send email!\n");


sub Efface {
  my ( $dbh, $fic ) = @_;

  my $sqlr = "
          DELETE FROM cotation_detail WHERE id_header IN
                 ( SELECT id FROM cotation_header as ch WHERE ch.id_scenario IN
                          ( SELECT id FROM logistic_routing_scenarios as lrs WHERE lrs.id_input_model IN
		          	( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          	)
                          )
	         );";
	 
	
    my $rscd = $dbh->prepare($sqlr);
    $rscd->execute();
	
	$sqlr = " 
          DELETE FROM cotation_sku_fnd WHERE id_header IN
                 ( SELECT id FROM cotation_header as ch WHERE ch.id_scenario IN
                          ( SELECT id FROM logistic_routing_scenarios as lrs WHERE lrs.id_input_model IN
		          	( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          	)
                          )
                 );
				 ";
	
     my $rscsf = $dbh->prepare($sqlr);
    $rscsf->execute();
	
	$sqlr = "			 
          DELETE FROM cotation_header WHERE id_scenario IN
                 ( SELECT id FROM logistic_routing_scenarios as lrs WHERE lrs.id_input_model IN
		          ( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          )
                 );";
				 
     my $rsch = $dbh->prepare($sqlr);
    $rsch->execute();	
	
	$sqlr = "			 
          DELETE FROM logistic_routings_equipement WHERE id_shpt IN
                 ( SELECT id FROM logistic_routings_shpts WHERE id_scenario IN
                          ( SELECT id FROM logistic_routing_scenarios WHERE id_input_model IN
		          	( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          	)
                          )
                 );";
	
     my $rslre = $dbh->prepare($sqlr);
    $rslre->execute();
	
	$sqlr = "			 
          DELETE FROM logistic_routings_detail WHERE id_shpt IN
	         ( SELECT id FROM logistic_routings_shpts WHERE id_scenario IN
		          ( SELECT id FROM logistic_routing_scenarios WHERE id_input_model IN
		          	( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          	)
		          )
		 );";
	
    my $rslrd = $dbh->prepare($sqlr);
    $rslrd->execute();
	
	$sqlr = " 
          DELETE FROM logistic_routings_shpts WHERE id_scenario IN
	         ( SELECT id FROM logistic_routing_scenarios WHERE id_input_model IN
	          	  ( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          )
		 );";
	
    my $rslrs= $dbh->prepare($sqlr);
    $rslrs->execute();	
	
	$sqlr = "
          DELETE FROM logistic_routings_price WHERE id_scenario IN
	         ( SELECT id FROM logistic_routing_scenarios WHERE id_input_model IN
	          	  ( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          )
		 );";
	
	
	my $rslrp= $dbh->prepare($sqlr);
    $rslrp->execute();
	
	$sqlr = "
          DELETE FROM logistic_routing_scenarios WHERE id_input_model IN
	         ( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		 );";
	
	my $rslrsc= $dbh->prepare($sqlr);
    $rslrsc->execute();
	
	$sqlr = "
	  DELETE FROM input_model_sku_fnds WHERE id_sku IN
	         ( SELECT id FROM input_model_skus as ims WHERE id_header IN
	         	  ( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		          )
		 );";
	my $rsimsf= $dbh->prepare($sqlr);
    $rsimsf->execute();

	
	$sqlr = "	 
	  DELETE FROM input_model_skus WHERE id_header IN
	         ( SELECT id FROM input_model_header as imh
		                	  WHERE imh.fic_num = '$fic'
		 );";
	
    my $rsimsk= $dbh->prepare($sqlr);
    $rsimsk->execute();
	
	$sqlr = "	 
	  DELETE FROM input_model_header WHERE fic_num = '$fic';
  ";
  my $rs = $dbh->prepare($sqlr);
  $rs->execute();
  
  if ( $dbh->errstr ne undef ) {
    print FICHIER_LOG "\nERROR : $dbh->errstr\n$sqlr\n";
    close FICHIER_LOG;
    $rs->finish;
    $dbh->rollback;
    $dbh->disconnect;
    exit;
  }
}


sub get_manufacturer_country_code_from_cotation {
  my ( $dbh, $fic,$dbug ) = @_;
		 #ON EXTRAIT LE PAYS DE FABRICATION DE LA COTATION
		 
		  my $manufacturer_country_code_from_cotation;
		  $fic=substr($fic,0,10);
		  my $sqlr_manufacturer_country_code_from_cotation = 
		  "
		  SELECT
			imh.manufacturer_country_code
			FROM input_model_header as imh
			LEFT JOIN logistic_routing_scenarios as lrs
			ON lrs.id_input_model=imh.id
			WHERE imh.fic_num LIKE '$fic%'
			AND char_length(lrs.user_fcs)>0
			AND char_length(lrs.date_fcs)>0;
		  
		  " ;

		  
		  
		  print $sqlr_manufacturer_country_code_from_cotation."\n" if($dbug);
		  
		  my $rs_manufacturer_country_code_from_cotation = $dbh->prepare($sqlr_manufacturer_country_code_from_cotation);
          $rs_manufacturer_country_code_from_cotation ->execute();
          if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            print FICHIER_LOG "\nERROR : $dbh->errstr\n";
            $rs_manufacturer_country_code_from_cotation ->finish;
            $dbh->disconnect;
            exit;
          }

          if ( $rs_manufacturer_country_code_from_cotation ->rows > 0 ) {
            $manufacturer_country_code_from_cotation = $rs_manufacturer_country_code_from_cotation ->fetchrow;}
            
		$rs_manufacturer_country_code_from_cotation ->finish;
			
		return $manufacturer_country_code_from_cotation ;	
}
