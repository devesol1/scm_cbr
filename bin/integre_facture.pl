#!/usr/bin/perl
use strict;
use Class::Date qw(:errors now);
use File::Copy;
use DBI;

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } );
if ( $dbh eq undef ) {
  print "\n ERREUR Connexion base";
  exit;
}
my $fichier_in = 'factures.csv';
open(FILE_IN, " $fichier_in") or print ("\n Lecture du fichier impossible : $fichier_in !\n");
my $fichier_log = 'result.txt';
open( FICHIER_LOG, "> $fichier_log" ) || die "Impossible ouvrir fichier log !\n";
print FICHIER_LOG "Rapport intégration automatique des factures :\n";

while(<FILE_IN>) {
  my $temp = $_;
  $temp =~ s/\n$//;
  $temp =~ s/\r$//;
	       
  my @split = split ( /;/, $temp );
  my $client = $split[0];
  my $charge = $split[1];
  my $facture = $split[2];
  # my $type = $split[3];
  my $bl = &trimwhitespace($split[4]);
  my $montant = $split[5];
  my $type = 'Facture';
  my $uo = $split[6];
  my $po = &trimwhitespace($split[7]);
  if ( $montant < 0 ) { $type = 'Avoir'; }
  if ( $montant == 0 ) { 
    print FICHIER_LOG "\nUO : $uo >>>>> facture $facture  NON INTEGREE : LE MONTANT est égal à 0 ! ";
    next; 
  }
  $montant =~ s/,/./;
  $montant =~ s/ //g;

  # ON REGARDE SI PO/BL EXISTENT
  my $sq = " SELECT * 
  	     FROM dau_bl db
	     LEFT JOIN dau_po dp
	     ON dp.id = db.id_po
	     WHERE lower(dp.po_num) = '$po'
	     AND dp.id_export_level = '3'
	     AND db.bl_num = '$bl' 
  ";
  my $rq = $dbh->prepare( $sq );
  $rq->execute;
  if ( $dbh->errstr ne undef ) {
    print "\n" . $dbh->errstr . "\n" . $sq;
    $rq->finish;
    exit;
  }
  if ( $rq->rows == 0 ) { 
    print FICHIER_LOG "\nUO: $uo >>>>> facture $facture '$montant' NON INTEGREE : Pas de PO/BL ($po/$bl) !";
    next; 
  }
  $rq->finish;

  my $id_issuer = '';
  if ( $client eq "DHL GLOBAL FORWARDING" ) {
    $id_issuer = '2';
  }
  else { $id_issuer = '1'; }

  my $cost_kind = '';
  if ( $charge eq 'FREIGHT' ) { $cost_kind = 'FREIGHT';} 
  if ( $charge eq 'CUSTOMS' ) { $cost_kind = 'CUSTOMS';} 
  if ( $charge eq 'PORT SERVICE' ) { $cost_kind = 'PORT SERVICES';} 
  if ( $charge eq 'ADMINISTRATIVE LOADS' ) { $cost_kind = 'ADMINISTRATIVE LOADS'; } 

  my $sq = " INSERT INTO dau_bl_invoice
             ( id_bl, 
               cost_kind, 
               invoice_num, 
	       invoice_amount, 
	       id_issuer, 
	       id_currency, 
	       exchange_rate, 
	       comment )
	     VALUES (
	       ( SELECT db.id FROM dau_bl db
	         LEFT JOIN dau_po dp
		 ON dp.id = db.id_po
		 WHERE lower(dp.po_num) = '$po'
	         AND dp.id_export_level = '3'
		 AND db.bl_num = '$bl'
	       ),
	       '$cost_kind',
	       '$facture',
               '$montant',
	       '$id_issuer',
	       '1',
	       '1.25',
	       'Intégration automatique'
	     )
  ";
  my $rq = $dbh->prepare( $sq );
  $rq->execute;
  if ( $dbh->errstr ne undef ) {
    print "\n" . $dbh->errstr . "\n" . $sq;
    $rq->finish;
    exit;
  }
  $rq->finish;
  print FICHIER_LOG "\n UO: $uo, $type $facture : BL $bl, PO $po, Montant : $montant ";
  print FICHIER_LOG "\n                    affectée au fournisseur $client, famille coût $charge ";
}

close FICHIER_LOG;
$dbh->disconnect;

sub trimwhitespace {
  my ($string ) = @_;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}

