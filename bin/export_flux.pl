#!/usr/bin/perl

use strict;
use CGI;
use DBI;
use MIME::Lite;
use Spreadsheet::WriteExcel;
use POSIX qw(strftime);

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use conf_scm_cbr;

my $dbug =0;

my ( $dbh, $acces );

#my $r = shift;
#$r->content_type("text/html; charset=ISO-8859-1");

# Tentative de connexion à la base:

( $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } ) ) or  print "Connexion base impossible.";

#====================
# INITIALISATION DE LA FEUILLE EXCEL =
#====================

my $export_path = esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR')."/bin/stats/";

	my $file_name = "export_flux_";
	$file_name .= (strftime "%Y", localtime);
	$file_name .= (strftime "%m", localtime);
	$file_name .= (strftime "%d", localtime)."-";
	$file_name .= (strftime "%H", localtime);
	$file_name .= (strftime "%M", localtime);
	$file_name .= ".xls";
	
my $file_path = $export_path.$file_name;


my $recipient = 'system@e-solutions.tm.fr';
if ($dbug){
	$recipient = 'system@e-solutions.tm.fr';
}
my $copy = "";
my $forwarder = 'no-reply@fcsystem.com';

my $workbook = Spreadsheet::WriteExcel->new($file_path);

#&add_worksheet_flux($dbh, $workbook, 'Flux', '2009');
&add_worksheet_flux($dbh, $workbook, 'Flux', '2010');
#&add_worksheet_flux_importer($dbh, $workbook, 'Flux importer', '2009');
&add_worksheet_flux_importer($dbh, $workbook, 'Flux importer', '2010');
#&add_worksheet_flux_forwarder($dbh, $workbook, 'Flux forwarder', '2009');
&add_worksheet_flux_forwarder($dbh, $workbook, 'Flux forwarder', '2010');
#&add_worksheet_flux_forwarder_fret($dbh, $workbook, 'Flux forwarder fret', '2009');
&add_worksheet_flux_forwarder_fret($dbh, $workbook, 'Flux forwarder fret', '2010');

$workbook->close();

&send_export($recipient, $forwarder, $copy, $file_path);

sub add_worksheet_flux {
	my ( $dbh, $which_workbook, $worksheet_name, $year ) = @_;

	my $worksheet = $which_workbook->add_worksheet($year."_".$worksheet_name);
	$worksheet->hide_zero();
	
	my $column_G = $which_workbook->add_format();
	$column_G->set_format_properties(right=>2);
	
	$worksheet->freeze_panes(2,3);
	 
	$worksheet->set_column('A:C', 8);
	$worksheet->set_column('D:AZ', 15);
	$worksheet->set_column('C:C', 8, $column_G);
	$worksheet->set_column('G:G', 15, $column_G);
	$worksheet->set_column('E:E', 15, $column_G);
	$worksheet->set_column('I:I', 15, $column_G);
	$worksheet->set_column('K:K', 15, $column_G);
	$worksheet->set_column('M:M', 15, $column_G);
	$worksheet->set_column('O:O', 15, $column_G);
	$worksheet->set_column('Q:Q', 15, $column_G);
	$worksheet->set_column('S:S', 15, $column_G);
	$worksheet->set_column('U:U', 15, $column_G);
	$worksheet->set_column('W:W', 15, $column_G);
	$worksheet->set_column('Y:Y', 15, $column_G);
	$worksheet->set_column('AA:AA', 15, $column_G);
	$worksheet->set_column('AC:AC', 15, $column_G);
	$worksheet->set_column('AE:AE', 15, $column_G);
	$worksheet->set_column('AG:AG', 15, $column_G);
	
	my $format = $which_workbook->add_format();
	$format->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>1,  center_across=>1 );
	my $format_nb_eq = $which_workbook->add_format();
	$format_nb_eq->set_format_properties(bold => 1, top =>1, bottom=>2, left =>2, right=>1,  center_across=>1 );
	
	my $format_cbm = $which_workbook->add_format();
	$format_cbm->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>2,  center_across=>1 );
	
	my $format_total_right = $which_workbook->add_format();
	$format_total_right->set_format_properties(bold => 1, top=>1, bottom=>2, left =>1, right=>2 , center_across=>1 );

	my $format_cells = $which_workbook->add_format();
	$format_cells->set_format_properties(left =>2, bottom =>1, right =>1, top =>1, center_across=>1 );
	
	my $format_nb_lcl = $which_workbook->add_format();
	$format_nb_lcl->set_format_properties(top =>2, bottom=>1, left =>2, right=>1, center_across=>1 );
	
	my $format_tot_left = $which_workbook->add_format();
	$format_tot_left->set_format_properties(bold =>1, top =>1, bottom=>2, left =>2, right=>1, center_across=>1 );
	
	my $format_tot = $which_workbook->add_format();
	$format_tot->set_format_properties(bold =>1, top =>1, bottom=>2, left =>1, right=>2, center_across=>1 );
	
	my $format_title = $which_workbook->add_format();
	$format_title->set_format_properties(top =>1, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_lcl = $which_workbook->add_format();
	$format_lcl->set_format_properties(top =>2, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_right = $which_workbook->add_format();
	$format_right->set_format_properties(top=>1, bottom=>1, left=>1,right=>2, center_across=>1 );
	
	my $format_top = $which_workbook->add_format();
	$format_top->set_format_properties(top=>2, bottom=>1, left=>1,right=>1, center_across=>1 );
	
	my $format_range_left = $which_workbook->add_format();
	$format_range_left->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, right=>2 ) ;
	$format_range_left->set_align('left');

	my $format_range_right = $which_workbook->add_format();
	$format_range_right->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, left=>2);
	$format_range_right->set_align('right');
	
	my $sqlr;

	$sqlr = "
	SELECT
	pol,
	pod,
	SUBSTR(year_month_etd, 1,4) as year,
	SUBSTR(year_month_etd, 5,2) as month,
	year_month_etd,
	MAX(nb_lcl) as nb_lcl,
	MAX(cbm_lcl) as cbm_lcl,
	MAX(weight_lcl) as weight_lcl,
	MAX(nb_20) as nb_20,
	MAX(cbm_20) as cbm_20,
	MAX(weight_20) as weight_20,
	MAX(nb_40) as nb_40,
	MAX(cbm_40) as cbm_40,
	MAX(weight_40) as weight_40,
	MAX(nb_40hc) as nb_40hc,
	MAX(cbm_40hc) as cbm_40hc,
	MAX(weight_40hc) as weight_40hc,

	MAX(nb_lcl)+MAX(nb_20)+MAX(nb_40)+MAX(nb_40hc) as nb_total,
	MAX(cbm_lcl)+MAX(cbm_20)+MAX(cbm_40)+MAX(cbm_40hc) as cbm_total,
	MAX(weight_lcl)+MAX(weight_20)+MAX(weight_40)+MAX(weight_40hc) as weight_total,

	''
	FROM (

		SELECT
		pol,
		pod,
		year_month_etd,
		type_container,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_lcl,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(cbm)
			ELSE 0
		END as cbm_lcl,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(weight)
			ELSE 0
		END as weight_lcl,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '20' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_20,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '20' THEN MAX(cbm)
			ELSE 0
		END as cbm_20,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '20' THEN MAX(weight)
			ELSE 0
		END as weight_20,	
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '40' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_40,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '40' THEN MAX(cbm)
			ELSE 0
		END as cbm_40,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '40' THEN MAX(weight)
			ELSE 0
		END as weight_40,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '40hc' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_40hc,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '40hc' THEN MAX(cbm)
			ELSE 0
		END as cbm_40hc,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '40hc' THEN MAX(weight)
			ELSE 0
		END as weight_40hc,
		''
		FROM (
			SELECT
			MAX(pol) as pol,
			MAX(pod) as pod,
			year_month_etd,
			COUNT(*) as nb_shpt,
			type_container,
			CAST(CAST((SUM(cbm)*1000) AS INTEGER) AS FLOAT)/1000 as cbm,
			SUM(weight) as weight,
			''
			FROM (
				SELECT
				container,
				COUNT(*) as nb_po,
				MAX(pol) as pol,
				MAX(pod) as pod,
				year_month_etd,
				type_container,
				SUM(cbm) as cbm,
				SUM(weight) as weight,
				''
				FROM tdb_lb_container
				GROUP BY
				container,
				pol||pod,
				year_month_etd,
				type_container
				ORDER BY year_month_etd
			) as rs

			GROUP BY
			pol||pod,
			year_month_etd,
			type_container
		) as rs
		GROUP BY
		year_month_etd,
		pol,
		pod,
		type_container
		ORDER BY
		year_month_etd,
		pol,
		pod

	) as rs
	WHERE 1=1
	--		AND year_month_etd >= (TO_CHAR(NOW()- interval '12 month', 'YYYYMM'))
	AND year_month_etd LIKE '$year%'
	AND pol NOT IN ('FRLEH', 'BEANR', 'NLRTM')
	GROUP BY
	year_month_etd,
	pol,
	pod


	";

	my $order_criteria = "
	ORDER BY
	pol,
	pod
	;

	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
		
	my $last_date = '' ;
	my $last_supplier = '' ;
	my $col=3;
	my @array_liaison =() ;
	my $liaison ;
	my $row ;
	my $pos;

	while ( my $data = $rs->fetchrow_hashref ) {
		$liaison = $data->{'supplier'}.$data->{'pol'}.$data->{'pod'} ;
		($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
	}
	
	my $order_criteria = "
	ORDER BY
	year_month_etd,
	pol,
	pod
	;
	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}

	my $last_date = '' ;
	my $col=3;
	my @array_liaison =() ;
	my $liaison ;
	my $row ;
	my $pos;
	
	while ( my $data = $rs->fetchrow_hashref ) {
		
		
		if ( ($last_date == $data->{'year_month_etd'})  || $last_date eq '' ) {
			
		}else{
			$col +=2 ;
		}
		$last_date = $data->{'year_month_etd'} ;
		
		$liaison = $data->{'pol'}.$data->{'pod'} ;
		($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
		$row = ($pos*5+2) ;
		
		$worksheet->write_string(1, 0, 'POL',$format);
		$worksheet->write_string(1, 1, 'POD',$format);
		$worksheet->write_string(1, 2, 'TYPE',$format);
		$worksheet->write_string(1, $col, 'Nb equipments',$format_nb_eq);
		$worksheet->write_string(1, $col+1, 'CBM',$format_cbm);
		
		$worksheet->write_string($row, 0, $data->{'pol'},$format_top);
		$worksheet->write_string($row, 1, $data->{'pod'},$format_top);
		
		$worksheet->write_string(0 ,$col, $data->{'year'},$format_range_right);
		$worksheet->write_string(0 ,$col+1,"- ".$data->{'month'},$format_range_left);
		
		$worksheet->write_string($row, 2, 'LCL',$format_title);
		$worksheet->write_number($row, $col, $data->{'nb_lcl'},$format_nb_lcl);
		$worksheet->write_number($row, $col+1, $data->{'cbm_lcl'},$format_lcl);
		$row++;
		$worksheet->write_string($row, 2, '20',$format_title);
		$worksheet->write_number($row, $col, $data->{'nb_20'},$format_cells);
		$worksheet->write_number($row, $col+1, $data->{'cbm_20'},$format_right);
		$row++;
		$worksheet->write_string($row, 2, '40',$format_title);
		$worksheet->write_number($row, $col, $data->{'nb_40'},$format_cells);
		$worksheet->write_number($row, $col+1, $data->{'cbm_40'},$format_right);
		$row++;
		$worksheet->write_string($row, 2, '40HC',$format_title);
		$worksheet->write_number($row, $col, $data->{'nb_40hc'},$format_cells);
		$worksheet->write_number($row, $col+1, $data->{'cbm_40hc'},$format_right);
		$row++;
		$worksheet->write_string($row, 2, 'Total',$format_tot);
		$worksheet->write_number($row, $col, $data->{'nb_total'},$format_tot_left);
		$worksheet->write_number($row, $col+1, $data->{'cbm_total'},$format_total_right);
				
	}	
	
	$rs->finish;
}

sub add_worksheet_flux_importer {

	my ( $dbh, $which_workbook, $worksheet_name, $year ) = @_;

	# Create a new workbook and add a worksheet
	my $worksheet = $which_workbook->add_worksheet($year."_".$worksheet_name);
	$worksheet->hide_zero();
	
	my $column_G = $which_workbook->add_format();
	$column_G->set_format_properties(right=>2 );
	
	$worksheet->freeze_panes(2,4);
	 
	$worksheet->set_column('A:A', 25); 
	$worksheet->set_column('B:D', 8);
	$worksheet->set_column('E:AZ', 15);
	
	my $format = $which_workbook->add_format();
	$format->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>1,  center_across=>1 );
	
	my $format_sub_total = $which_workbook->add_format();
	$format_sub_total->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>1,  center_across=>1, size=>12 );
	
	my $format_nb_eq = $which_workbook->add_format();
	$format_nb_eq->set_format_properties(bold => 1, top =>1, bottom=>2, left =>2, right=>1,  center_across=>1 );
	
	my $format_cbm = $which_workbook->add_format();
	$format_cbm->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>2,  center_across=>1 );
	
	my $format_tot = $which_workbook->add_format();
	$format_tot->set_format_properties(bold =>1, top =>1, bottom=>2, left =>1, right=>2, center_across=>1 );
		
	my $format_total_right = $which_workbook->add_format();
	$format_total_right->set_format_properties(bold => 1, top=>1, bottom=>2, left =>1, right=>2 , center_across=>1 );

	my $format_cells = $which_workbook->add_format();
	$format_cells->set_format_properties(left =>2, bottom =>1, right =>1, top =>1, center_across=>1 );
	
	my $format_nb_lcl = $which_workbook->add_format();
	$format_nb_lcl->set_format_properties(top =>2, bottom=>1, left =>2, right=>1, center_across=>1 );
	
	my $format_tot_left = $which_workbook->add_format();
	$format_tot_left->set_format_properties(bold =>1, top =>1, bottom=>2, left =>2, right=>1, center_across=>1 );
	
	my $format_title = $which_workbook->add_format();
	$format_title->set_format_properties(top =>1, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_lcl = $which_workbook->add_format();
	$format_lcl->set_format_properties(top =>2, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_right = $which_workbook->add_format();
	$format_right->set_format_properties(top=>1, bottom=>1, left=>1,right=>2, center_across=>1 );
	
	my $format_top = $which_workbook->add_format();
	$format_top->set_format_properties(top=>2, bottom=>1, left=>1,right=>1, center_across=>1 );
	
	my $format_range_left = $which_workbook->add_format();
	$format_range_left->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, right=>2 ) ;
	$format_range_left->set_align('left');

	my $format_range_right = $which_workbook->add_format();
	$format_range_right->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, left=>2);
	$format_range_right->set_align('right');

	
	my $sqlr;

	$sqlr = "
	SELECT
	supplier,
	pol,
	pod,
	year_month_etd,
	SUBSTR(year_month_etd, 1,4) as year,
	SUBSTR(year_month_etd, 5,2) as month,
	MAX(nb_lcl) as nb_lcl,
	MAX(cbm_lcl) as cbm_lcl,
	MAX(weight_lcl) as weight_lcl,
	MAX(nb_20) as nb_20,
	MAX(cbm_20) as cbm_20,
	MAX(weight_20) as weight_20,
	MAX(nb_40) as nb_40,
	MAX(cbm_40) as cbm_40,
	MAX(weight_40) as weight_40,
	MAX(nb_40hc) as nb_40hc,
	MAX(cbm_40hc) as cbm_40hc,
	MAX(weight_40hc) as weight_40hc,

	MAX(nb_lcl)+MAX(nb_20)+MAX(nb_40)+MAX(nb_40hc) as nb_total,
	MAX(cbm_lcl)+MAX(cbm_20)+MAX(cbm_40)+MAX(cbm_40hc) as cbm_total,
	MAX(weight_lcl)+MAX(weight_20)+MAX(weight_40)+MAX(weight_40hc) as weight_total,

	''
	FROM (

		SELECT
		supplier,
		pol,
		pod,
		year_month_etd,
		type_container,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_lcl,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(cbm)
			ELSE 0
		END as cbm_lcl,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(weight)
			ELSE 0
		END as weight_lcl,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '20' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_20,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '20' THEN MAX(cbm)
			ELSE 0
		END as cbm_20,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '20' THEN MAX(weight)
			ELSE 0
		END as weight_20,	
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '40' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_40,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '40' THEN MAX(cbm)
			ELSE 0
		END as cbm_40,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '40' THEN MAX(weight)
			ELSE 0
		END as weight_40,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '40hc' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_40hc,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '40hc' THEN MAX(cbm)
			ELSE 0
		END as cbm_40hc,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '40hc' THEN MAX(weight)
			ELSE 0
		END as weight_40hc,
		''
		FROM (
			SELECT
			supplier,
			MAX(pol) as pol,
			MAX(pod) as pod,
			year_month_etd,
			COUNT(*) as nb_shpt,
			type_container,
			CAST(CAST((SUM(cbm)*1000) AS INTEGER) AS FLOAT)/1000 as cbm,
			SUM(weight) as weight,
			''
			FROM (
				SELECT
				supplier,
				container,
				COUNT(*) as nb_po,
				MAX(pol) as pol,
				MAX(pod) as pod,
				year_month_etd,
				type_container,
				SUM(cbm) as cbm,
				SUM(weight) as weight,
				''
				FROM tdb_lb_container
				GROUP BY
				supplier,
				container,
				pol||pod,
				year_month_etd,
				type_container
				ORDER BY year_month_etd
			) as rs

			GROUP BY
			supplier,
			pol||pod,
			year_month_etd,
			type_container

		) as rs
		GROUP BY
		supplier,
		year_month_etd,
		pol,
		pod,
		type_container
		ORDER BY
		year_month_etd,
		pol,
		pod

	) as rs

	WHERE 1=1
	--		AND year_month_etd >= (TO_CHAR(NOW()- interval '12 month', 'YYYYMM'))
	AND year_month_etd LIKE '$year%'
	AND pol NOT IN ('FRLEH', 'BEANR', 'NLRTM')
	AND LENGTH(supplier)>0
	 
	GROUP BY
	supplier,
	year_month_etd,
	pol,
	pod

	";

	my $order_criteria = "
	ORDER BY
	supplier,
	pol,
	pod
	;
	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
		
	my $last_date = '' ;
	my $last_supplier = '' ;
	my $col=3;
	my @array_liaison =() ;
	my @array_gpe =() ;
	my $liaison ;
	my $row ;
	my $pos;
	my $gpe_pos;
	my $sub_total_nb_total ;
	my $sub_total_cbm_total ;

	while ( my $data = $rs->fetchrow_hashref ) {
		$liaison = $data->{'supplier'}.$data->{'pol'}.$data->{'pod'} ;
		($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
		($gpe_pos, @array_gpe) = &get_gpe_pos($data->{'supplier'}, @array_gpe)  ;		
	}
	
	my $order_criteria = "
	ORDER BY
	year_month_etd,
	supplier,
	pol,
	pod
	;
	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
	
	while ( my $data = $rs->fetchrow_hashref ) {
		
		if ( ($last_date ne $data->{'year_month_etd'}) && $last_date ne '' ) {
			$pos = &get_last_gpe_pos($last_supplier, @array_liaison);
			($gpe_pos, @array_gpe) = &get_gpe_pos($last_supplier, @array_gpe)  ;
			$row = ($pos*5+2+$gpe_pos+5) ;
			$worksheet->write_string($row, 0, 'Total '.$last_supplier ,$format_sub_total);
			$worksheet->write_string($row, 1, '' ,$format_sub_total);
			$worksheet->write_string($row, 2, '' ,$format_sub_total);
			$worksheet->write_string($row, 3, '' ,$format_sub_total);
			$worksheet->write_number($row, $col+1, $sub_total_nb_total ,$format_sub_total);
			$worksheet->write_number($row, $col+2, $sub_total_cbm_total ,$format_sub_total);
			$sub_total_nb_total = '' ;
			$sub_total_cbm_total = '' ;
			$last_supplier ='' ;
			$col +=2 ;
		}
		
		if ($last_supplier ne $data->{'supplier'}  && $last_supplier ne '' ) {
			$pos = &get_last_gpe_pos($last_supplier, @array_liaison);
			($gpe_pos, @array_gpe) = &get_gpe_pos($last_supplier, @array_gpe)  ;
			$row = ($pos*5+2+$gpe_pos+5) ;
			$worksheet->write_string($row, 0, 'Total '.$last_supplier ,$format_sub_total);
			$worksheet->write_string($row, 1, '' ,$format_sub_total);
			$worksheet->write_string($row, 2, '' ,$format_sub_total);
			$worksheet->write_string($row, 3, '' ,$format_sub_total);
			$worksheet->write_number($row, $col+1, $sub_total_nb_total ,$format_sub_total);
			$worksheet->write_number($row, $col+2, $sub_total_cbm_total ,$format_sub_total);
			$sub_total_nb_total = '' ;
			$sub_total_cbm_total = '' ;
		}

		$liaison = $data->{'supplier'}.$data->{'pol'}.$data->{'pod'} ;
		($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
		($gpe_pos, @array_gpe) = &get_gpe_pos($data->{'supplier'}, @array_gpe)  ;
		$row = ($pos*5+2+$gpe_pos) ;
			
		$last_date = $data->{'year_month_etd'} ;
		$last_supplier = $data->{'supplier'} ;
		
		$worksheet->write_string(1, 0, 'Importateur',$format);
		$worksheet->write_string(1, 1, 'POL',$format);
		$worksheet->write_string(1, 2, 'POD',$format);
		$worksheet->write_string(1, 3, 'TYPE',$format);
		$worksheet->write_string(1, $col+1, 'Nb equipments',$format_nb_eq);
		$worksheet->write_string(1, $col+2, 'CBM',$format_cbm);
		
		$worksheet->write_string($row, 0, $data->{'supplier'},$format_top);
		
		$worksheet->write_string($row, 1, $data->{'pol'},$format_top);
		$worksheet->write_string($row, 2, $data->{'pod'},$format_top);
		
		$worksheet->write_string(0 ,$col+1, $data->{'year'},$format_range_right);
		$worksheet->write_string(0 ,$col+2,"- ".$data->{'month'},$format_range_left);
		
		$worksheet->write_string($row, 3, 'LCL',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_lcl'},$format_nb_lcl);
		$worksheet->write_number($row, $col+2, $data->{'cbm_lcl'},$format_lcl);
		$row++;
		$worksheet->write_string($row, 3, '20',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_20'},$format_cells);
		$worksheet->write_number($row, $col+2, $data->{'cbm_20'},$format_right);
		$row++;
		$worksheet->write_string($row, 3, '40',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_40'},$format_cells);
		$worksheet->write_number($row, $col+2, $data->{'cbm_40'},$format_right);
		$row++;
		$worksheet->write_string($row, 3, '40HC',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_40hc'},$format_cells);
		$worksheet->write_number($row, $col+2, $data->{'cbm_40hc'},$format_right);
		$row++;
		$worksheet->write_string($row, 3, 'Total',$format_tot);
		$worksheet->write_number($row, $col+1, $data->{'nb_total'},$format_tot_left);
		$worksheet->write_number($row, $col+2, $data->{'cbm_total'},$format_total_right);
		
		$sub_total_nb_total += $data->{'nb_total'} ;
		$sub_total_cbm_total += $data->{'cbm_total'} ;

	}	
	
	$rs->finish;
}

sub add_worksheet_flux_forwarder {

	my ( $dbh, $which_workbook, $worksheet_name, $year ) = @_;

	# Create a new workbook and add a worksheet
	my $worksheet = $which_workbook->add_worksheet($year."_".$worksheet_name);
	$worksheet->hide_zero();
	
	my $format_sub_total = $which_workbook->add_format();
	$format_sub_total->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>1,  center_across=>1, size=>12 );
	
	my $column_G = $which_workbook->add_format();
	$column_G->set_format_properties(right=>2 );
	
	$worksheet->freeze_panes(2,4);
	 
	$worksheet->set_column('A:A', 22); 
	$worksheet->set_column('B:D', 8);
	$worksheet->set_column('E:AZ', 15);
	
	my $format = $which_workbook->add_format();
	$format->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>1,  center_across=>1 );
	my $format_nb_eq = $which_workbook->add_format();
	$format_nb_eq->set_format_properties(bold => 1, top =>1, bottom=>2, left =>2, right=>1,  center_across=>1 );
	
	my $format_cbm = $which_workbook->add_format();
	$format_cbm->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>2,  center_across=>1 );
	
	my $format_total_right = $which_workbook->add_format();
	$format_total_right->set_format_properties(bold => 1, top=>1, bottom=>2, left =>1, right=>2 , center_across=>1 );
	
	my $format_tot = $which_workbook->add_format();
		$format_tot->set_format_properties(bold =>1, top =>1, bottom=>2, left =>1, right=>2, center_across=>1 );
		
	my $format_cells = $which_workbook->add_format();
	$format_cells->set_format_properties(left =>2, bottom =>1, right =>1, top =>1, center_across=>1 );
	
	my $format_nb_lcl = $which_workbook->add_format();
	$format_nb_lcl->set_format_properties(top =>2, bottom=>1, left =>2, right=>1, center_across=>1 );
	
	my $format_tot_left = $which_workbook->add_format();
	$format_tot_left->set_format_properties(bold =>1, top =>1, bottom=>2, left =>2, right=>1, center_across=>1 );
	
	my $format_title = $which_workbook->add_format();
	$format_title->set_format_properties(top =>1, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_lcl = $which_workbook->add_format();
	$format_lcl->set_format_properties(top =>2, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_right = $which_workbook->add_format();
	$format_right->set_format_properties(top=>1, bottom=>1, left=>1,right=>2, center_across=>1 );
	
	my $format_top = $which_workbook->add_format();
	$format_top->set_format_properties(top=>2, bottom=>1, left=>1,right=>1, center_across=>1 );
	
	my $format_range_left = $which_workbook->add_format();
	$format_range_left->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, right=>2 ) ;
	$format_range_left->set_align('left');

	my $format_range_right = $which_workbook->add_format();
	$format_range_right->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, left=>2);
	$format_range_right->set_align('right');

	
	my $sqlr;

	$sqlr = "
	SELECT
	fwd,
	pol,
	pod,
	year_month_etd,
	SUBSTR(year_month_etd, 1,4) as year,
	SUBSTR(year_month_etd, 5,2) as month,
	MAX(nb_lcl) as nb_lcl,
	MAX(cbm_lcl) as cbm_lcl,
	MAX(weight_lcl) as weight_lcl,
	MAX(nb_20) as nb_20,
	MAX(cbm_20) as cbm_20,
	MAX(weight_20) as weight_20,
	MAX(nb_40) as nb_40,
	MAX(cbm_40) as cbm_40,
	MAX(weight_40) as weight_40,
	MAX(nb_40hc) as nb_40hc,
	MAX(cbm_40hc) as cbm_40hc,
	MAX(weight_40hc) as weight_40hc,

	MAX(nb_lcl)+MAX(nb_20)+MAX(nb_40)+MAX(nb_40hc) as nb_total,
	MAX(cbm_lcl)+MAX(cbm_20)+MAX(cbm_40)+MAX(cbm_40hc) as cbm_total,
	MAX(weight_lcl)+MAX(weight_20)+MAX(weight_40)+MAX(weight_40hc) as weight_total,

	''
	FROM (

		SELECT
		fwd,
		pol,
		pod,
		year_month_etd,
		type_container,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_lcl,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(cbm)
			ELSE 0
		END as cbm_lcl,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = 'lcl' THEN MAX(weight)
			ELSE 0
		END as weight_lcl,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '20' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_20,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '20' THEN MAX(cbm)
			ELSE 0
		END as cbm_20,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '20' THEN MAX(weight)
			ELSE 0
		END as weight_20,	
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '40' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_40,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '40' THEN MAX(cbm)
			ELSE 0
		END as cbm_40,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '40' THEN MAX(weight)
			ELSE 0
		END as weight_40,
		CASE
			WHEN MAX(nb_shpt)> 0 AND LOWER(type_container) = '40hc' THEN MAX(nb_shpt)
			ELSE 0
		END as nb_40hc,
		CASE
			WHEN MAX(cbm)> 0 AND LOWER(type_container) = '40hc' THEN MAX(cbm)
			ELSE 0
		END as cbm_40hc,
		CASE
			WHEN MAX(weight)> 0 AND LOWER(type_container) = '40hc' THEN MAX(weight)
			ELSE 0
		END as weight_40hc,
		''
		FROM (
			SELECT
			fwd,
			MAX(pol) as pol,
			MAX(pod) as pod,
			year_month_etd,
			COUNT(*) as nb_shpt,
			type_container,
			CAST(CAST((SUM(cbm)*1000) AS INTEGER) AS FLOAT)/1000 as cbm,
			SUM(weight) as weight,
			''
			FROM (
				SELECT
				fwd,
				container,
				COUNT(*) as nb_po,
				MAX(pol) as pol,
				MAX(pod) as pod,
				year_month_etd,
				type_container,
				SUM(cbm) as cbm,
				SUM(weight) as weight,
				''
				FROM tdb_lb_container
				GROUP BY
				fwd,
				container,
				pol||pod,
				year_month_etd,
				type_container
				ORDER BY year_month_etd
			) as rs

			GROUP BY
			fwd,
			pol||pod,
			year_month_etd,
			type_container

		) as rs
		GROUP BY
		fwd,
		year_month_etd,
		pol,
		pod,
		type_container
		ORDER BY
		year_month_etd,
		pol,
		pod

	) as rs

	WHERE 1=1
	--		AND year_month_etd >= (TO_CHAR(NOW()- interval '12 month', 'YYYYMM'))
	AND year_month_etd LIKE '$year%'
	AND LENGTH(fwd)>0
	AND pol NOT IN ('FRLEH', 'BEANR', 'NLRTM')
	 
	GROUP BY
	fwd,
	year_month_etd,
	pol,
	pod

	";

	my $order_criteria = "
	ORDER BY
	fwd,
	pol,
	pod
	;
	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
		
	my $last_date = '' ;
	my $last_fwd = '' ;
	my $col=3;
	my @array_liaison =() ;
	my @array_gpe =() ;
	my $liaison ;
	my $row ;
	my $pos;
	my $gpe_pos;
	my $sub_total_nb_total ;
	my $sub_total_cbm_total ;

	while ( my $data = $rs->fetchrow_hashref ) {
		$liaison = $data->{'fwd'}.$data->{'pol'}.$data->{'pod'} ;
		($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
		($gpe_pos, @array_gpe) = &get_gpe_pos($data->{'fwd'}, @array_gpe)  ;
	}
	

	my $order_criteria = "
		ORDER BY
		year_month_etd,
		fwd,
		pol,
		pod
		;
	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
	
	while ( my $data = $rs->fetchrow_hashref ) {
		
		if ( ($last_date ne $data->{'year_month_etd'})  && $last_date ne '' ) {
			$pos = &get_last_gpe_pos($last_fwd, @array_liaison);
			($gpe_pos, @array_gpe) = &get_gpe_pos($last_fwd, @array_gpe)  ;
			$row = ($pos*5+2+$gpe_pos+5) ;
			$worksheet->write_string($row, 0, 'Total fwd '.$last_fwd ,$format_sub_total);
			$worksheet->write_string($row, 1, '' ,$format_sub_total);
			$worksheet->write_string($row, 2, '' ,$format_sub_total);
			$worksheet->write_string($row, 3, '' ,$format_sub_total);
			$worksheet->write_number($row, $col+1, $sub_total_nb_total ,$format_sub_total);
			$worksheet->write_number($row, $col+2, $sub_total_cbm_total ,$format_sub_total);
			$sub_total_nb_total = '' ;
			$sub_total_cbm_total = '' ;
			$last_fwd ="";
			$col +=2;
		}
		
		if ( $last_fwd ne $data->{'fwd'} && $last_fwd ne '') {
			
			$pos = &get_last_gpe_pos($last_fwd, @array_liaison);
			($gpe_pos, @array_gpe) = &get_gpe_pos($last_fwd, @array_gpe)  ;
			$row = ($pos*5+2+$gpe_pos+5) ;
			$worksheet->write_string($row, 0, 'Total fwd '.$last_fwd ,$format_sub_total);
			$worksheet->write_string($row, 1, '' ,$format_sub_total);
			$worksheet->write_string($row, 2, '' ,$format_sub_total);
			$worksheet->write_string($row, 3, '' ,$format_sub_total);
			$worksheet->write_number($row, $col+1, $sub_total_nb_total ,$format_sub_total);
			$worksheet->write_number($row, $col+2, $sub_total_cbm_total ,$format_sub_total);
			$sub_total_nb_total = '' ;
			$sub_total_cbm_total = '' ;
		}
		
		$liaison = $data->{'fwd'}.$data->{'pol'}.$data->{'pod'} ;
		($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
		($gpe_pos, @array_gpe) = &get_gpe_pos($data->{'fwd'}, @array_gpe)  ;
		$row = ($pos*5+2+$gpe_pos) ;
		
		$last_date = $data->{'year_month_etd'} ;
		$last_fwd = $data->{'fwd'} ;
		
		$worksheet->write_string(1, 0, 'Forwarder',$format);
		$worksheet->write_string(1, 1, 'POL',$format);
		$worksheet->write_string(1, 2, 'POD',$format);
		$worksheet->write_string(1, 3, 'TYPE',$format);
		$worksheet->write_string(1, $col+1, 'Nb equipments',$format_nb_eq);
		$worksheet->write_string(1, $col+2, 'CBM',$format_cbm);
		
		$worksheet->write_string($row, 0, $data->{'fwd'},$format_top);
		
		$worksheet->write_string($row, 1, $data->{'pol'},$format_top);
		$worksheet->write_string($row, 2, $data->{'pod'},$format_top);
		
		$worksheet->write_string(0 ,$col+1, $data->{'year'},$format_range_right);
		$worksheet->write_string(0 ,$col+2,"- ".$data->{'month'},$format_range_left);
		
		$worksheet->write_string($row, 3, 'LCL',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_lcl'},$format_nb_lcl);
		$worksheet->write_number($row, $col+2, $data->{'cbm_lcl'},$format_lcl);
		$row++;
		$worksheet->write_string($row, 3, '20',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_20'},$format_cells);
		$worksheet->write_number($row, $col+2, $data->{'cbm_20'},$format_right);
		$row++;
		$worksheet->write_string($row, 3, '40',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_40'},$format_cells);
		$worksheet->write_number($row, $col+2, $data->{'cbm_40'},$format_right);
		$row++;
		$worksheet->write_string($row, 3, '40HC',$format_title);
		$worksheet->write_number($row, $col+1, $data->{'nb_40hc'},$format_cells);
		$worksheet->write_number($row, $col+2, $data->{'cbm_40hc'},$format_right);
		$row++;
		$worksheet->write_string($row, 3, 'Total',$format_tot);
		$worksheet->write_number($row, $col+1, $data->{'nb_total'},$format_tot_left);
		$worksheet->write_number($row, $col+2, $data->{'cbm_total'},$format_total_right);
		
		$sub_total_nb_total += $data->{'nb_total'} ;
		$sub_total_cbm_total += $data->{'cbm_total'} ;
	}	
	
	$rs->finish;
}

sub add_worksheet_flux_forwarder_fret {

	my ( $dbh, $which_workbook, $worksheet_name, $year ) = @_;

	# Create a new workbook and add a worksheet
	my $worksheet = $which_workbook->add_worksheet($year."_".$worksheet_name);
	$worksheet->hide_zero();
	
	my $column_G = $which_workbook->add_format();
	$column_G->set_format_properties(right=>2 );
	
	$worksheet->freeze_panes(2,4);
	
	$worksheet->set_column('A:A', 20); 
	$worksheet->set_column('B:D', 8);
	$worksheet->set_column('E:CA', 15);
	
	my $format = $which_workbook->add_format();
	$format->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>1,  center_across=>1 );
	
	my $format_into = $which_workbook->add_format();
	$format_into->set_format_properties(bold => 1, top =>1, bottom=>1, left =>1, right=>1,  center_across=>1, size =>12 );
	
	my $format_in_cells = $which_workbook->add_format();
	$format_in_cells->set_format_properties(top =>1, bottom=>1, left =>1, right=>1,  center_across=>1 );
	
	my $format_nb_eq = $which_workbook->add_format();
	$format_nb_eq->set_format_properties(bold => 1, top =>1, bottom=>2, left =>2, right=>1,  center_across=>1 );
	
	my $format_cbm = $which_workbook->add_format();
	$format_cbm->set_format_properties(bold => 1, top =>1, bottom=>2, left =>1, right=>2,  center_across=>1 );
	
	my $format_tot = $which_workbook->add_format();
	$format_tot->set_format_properties(bold =>1, top =>1, bottom=>2, left =>1, right=>2, center_across=>1 );
	
	my $format_tot_in = $which_workbook->add_format();
	$format_tot_in->set_format_properties(bold =>1, top =>1, bottom=>2, left =>1, right=>1, center_across=>1 );
		
	my $format_total_right = $which_workbook->add_format();
	$format_total_right->set_format_properties(bold => 1, top=>1, bottom=>2, left =>1, right=>2 , center_across=>1 );

	my $format_cells = $which_workbook->add_format();
	$format_cells->set_format_properties(left =>2, bottom =>1, right =>1, top =>1, center_across=>1 );
	
	my $format_nb_lcl = $which_workbook->add_format();
	$format_nb_lcl->set_format_properties(top =>2, bottom=>1, left =>2, right=>1, center_across=>1 );
	
	my $format_tot_left = $which_workbook->add_format();
	$format_tot_left->set_format_properties(bold =>1, top =>1, bottom=>2, left =>2, right=>1, center_across=>1 );
	
	my $format_title = $which_workbook->add_format();
	$format_title->set_format_properties(top =>1, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_lcl = $which_workbook->add_format();
	$format_lcl->set_format_properties(top =>2, bottom=>1, left =>1, right=>1, center_across=>1 );
	
	my $format_lcl_title = $which_workbook->add_format();
	$format_lcl_title->set_format_properties(top =>2, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_in = $which_workbook->add_format();
	$format_in->set_format_properties(top =>1, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_in_top = $which_workbook->add_format();
	$format_in_top->set_format_properties(top =>2, bottom=>1, left =>1, right=>2, center_across=>1 );
	
	my $format_right = $which_workbook->add_format();
	$format_right->set_format_properties(top=>1, bottom=>1, left=>1,right=>2, center_across=>1 );
	
	my $format_top = $which_workbook->add_format();
	$format_top->set_format_properties(top=>2, bottom=>1, left=>1,right=>1, center_across=>1 );
	
	my $format_range_left = $which_workbook->add_format();
	$format_range_left->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2) ;
	$format_range_left->set_align('left');

	my $format_range_right = $which_workbook->add_format();
	$format_range_right->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2);
	
	my $format_silver = $which_workbook->add_format();
	$format_silver->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, left=>2);
	
	my $format_silver_left = $which_workbook->add_format();
	$format_silver_left->set_format_properties(bold => 1, bg_color=>'silver', size =>16, bottom =>2, right=>2);

	
	my $sqlr;

	$sqlr = "
	SELECT 
	fwd,
	year_month_etd,
	SUBSTR(year_month_etd, 1,4) as year,
	SUBSTR(year_month_etd, 5,2) as month, 
	MAX(pol) as pol,
	MAX(pod) as pod,
	MAX(lcl_quoted_freight) as lcl_quoted_freight, 
	MAX(lcl_engaged_freight) as lcl_engaged_freight, 
	MAX(lcl_shipped_freight) as lcl_shipped_freight, 
	MAX(lcl_invoiced_freight) as lcl_invoiced_freight, 

	MAX(fcl_quoted_freight) as fcl_quoted_freight, 
	MAX(fcl_engaged_freight) as fcl_engaged_freight, 
	MAX(fcl_shipped_freight) as fcl_shipped_freight, 
	MAX(fcl_invoiced_freight) as fcl_invoiced_freight, 

	MAX(lcl_quoted_freight)+MAX(fcl_quoted_freight) as total_quoted_freight, 
	MAX(lcl_engaged_freight)+MAX(fcl_engaged_freight) as total_engaged_freight, 
	MAX(lcl_shipped_freight)+MAX(fcl_shipped_freight) as total_shipped_freight, 
	MAX(lcl_invoiced_freight)+MAX(fcl_invoiced_freight) as total_invoiced_freight, 
	''
	FROM (

		SELECT 
		fwd, 
		MAX(pol) as pol,
		MAX(pod) as pod, 
		year_month_etd, 
		SUM(lcl_quoted_freight) as lcl_quoted_freight, 
		SUM(lcl_engaged_freight) as lcl_engaged_freight, 
		SUM(lcl_shipped_freight) as lcl_shipped_freight, 
		SUM(lcl_invoiced_freight) as lcl_invoiced_freight, 
		SUM(fcl_quoted_freight) as fcl_quoted_freight, 
		SUM(fcl_engaged_freight) as fcl_engaged_freight, 
		SUM(fcl_shipped_freight) as fcl_shipped_freight, 
		SUM(fcl_invoiced_freight) as fcl_invoiced_freight, 
		''
		FROM ( 
			SELECT

			po_shpt,
			linerterm,
			fwd,
			MAX(pol) as pol,
			MAX(pod) as pod,
			SUBSTR(year_month_etd||year_month_esd, 1, 6) as year_month_etd ,
			CASE 
				WHEN LOWER(linerterm)<>'lcl' THEN 0
				ELSE CAST(CAST((SUM(quoted_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as lcl_quoted_freight, 
			CASE 
				WHEN LOWER(linerterm)<>'lcl' THEN 0
				ELSE CAST(CAST((SUM(engaged_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as lcl_engaged_freight, 
			CASE 
				WHEN LOWER(linerterm)<>'lcl' THEN 0
				ELSE CAST(CAST((SUM(shipped_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as lcl_shipped_freight, 
			CASE 
				WHEN LOWER(linerterm)<>'lcl' THEN 0
				ELSE CAST(CAST((SUM(invoiced_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as lcl_invoiced_freight, 

			CASE 
				WHEN LOWER(linerterm)<>'fcl' THEN 0
				ELSE CAST(CAST((MAX(quoted_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as fcl_quoted_freight, 
			CASE 
				WHEN LOWER(linerterm)<>'fcl' THEN 0
				ELSE CAST(CAST((MAX(engaged_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as fcl_engaged_freight, 
			CASE 
				WHEN LOWER(linerterm)<>'fcl' THEN 0
				ELSE CAST(CAST((SUM(shipped_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as fcl_shipped_freight, 
			CASE 
				WHEN LOWER(linerterm)<>'fcl' THEN 0
				ELSE CAST(CAST((SUM(invoiced_freight)*1000) AS INTEGER) AS FLOAT)/1000
			END as fcl_invoiced_freight, 

			''
			FROM tdb_lb_cost
			GROUP BY
			po_shpt,
			fwd,
			pol||pod,
			SUBSTR(year_month_etd||year_month_esd, 1, 6),
			linerterm
		) as rs
		WHERE 1=1
		GROUP BY
		fwd,
		pol||pod,
		year_month_etd,
		linerterm
		ORDER BY year_month_etd

	) as rs
	WHERE 1=1
	--AND year_month_etd >= (TO_CHAR(NOW()- interval '12 month', 'YYYYMM'))
	AND year_month_etd LIKE '$year%'
	AND pol NOT IN ('FRLEH', 'BEANR', 'NLRTM')
	GROUP BY 
	fwd,
	pol||pod,
	year_month_etd

	";



	my $order_criteria = "
	ORDER BY
	fwd,
	pol,
	pod
	;
	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
	
	my $last_date = '' ;
	my $last_fwd = '' ;
	my $col=3;
	my @array_liaison =() ;
	my @array_gpe =() ;
	my $liaison ;
	my $row ;
	my $sub_total_quoted ;
	my $sub_total_engaged ;
	my $sub_total_shipped ;

	my ($pos, $gpe_pos);

	while ( my $data = $rs->fetchrow_hashref ) {
			$liaison = $data->{'fwd'}.$data->{'pol'}.$data->{'pod'} ;
			($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
			($gpe_pos, @array_gpe) = &get_gpe_pos($data->{'fwd'}, @array_gpe)  ;
	}
	

	my $order_criteria = "
	ORDER BY
	year_month_etd,
	fwd,
	pol,
	pod
	;
	";	

	my $rs = $dbh->prepare($sqlr.$order_criteria);
	$rs->execute();

	if ( $dbh->errstr ne undef ) {
		print $dbh->errstr."\n".$sqlr; 
		$rs->finish;
		$dbh->disconnect;
		exit;
	}
	
	while ( my $data = $rs->fetchrow_hashref ) {
		
		
		if ( ($last_date ne $data->{'year_month_etd'})  && $last_date ne '' ) {
			$pos = &get_last_gpe_pos($last_fwd, @array_liaison);
			($gpe_pos, @array_gpe) = &get_gpe_pos($last_fwd, @array_gpe)  ;
			$row = ($pos*3+2+$gpe_pos+3) ;
			$worksheet->write_string($row, 0, 'Total fwd '.$last_fwd ,$format_into);
			$worksheet->write_string($row, 1, '' ,$format_into);
			$worksheet->write_string($row, 2, '' ,$format_into);
			$worksheet->write_string($row, 3, '' ,$format_into);
			$worksheet->write_number($row, $col+1,$sub_total_quoted  ,$format_into);
			$worksheet->write_number($row, $col+2,$sub_total_engaged  ,$format_into);
			$worksheet->write_number($row, $col+3,$sub_total_shipped  ,$format_into);
			$last_fwd ="";
			$sub_total_quoted = '' ;
			$sub_total_engaged = '';
			$sub_total_shipped = '';
			$col +=3;
		}
		
		if ( $last_fwd ne $data->{'fwd'} && $last_fwd ne '') {
			
			$pos = &get_last_gpe_pos($last_fwd, @array_liaison);
			($gpe_pos, @array_gpe) = &get_gpe_pos($last_fwd, @array_gpe)  ;
			$row = ($pos*3+2+$gpe_pos+3) ;
			$worksheet->write_string($row, 0, 'Total fwd '.$last_fwd ,$format_into);
			$worksheet->write_string($row, 1, '' ,$format_into);
			$worksheet->write_string($row, 2, '' ,$format_into);
			$worksheet->write_string($row, 3, '' ,$format_into);
			$worksheet->write_number($row, $col+1,$sub_total_quoted  ,$format_into);
			$worksheet->write_number($row, $col+2,$sub_total_engaged  ,$format_into);
			$worksheet->write_number($row, $col+3,$sub_total_shipped  ,$format_into);
			$sub_total_quoted = '' ;
			$sub_total_engaged = '';
			$sub_total_shipped = '';
		}
		
		$liaison = $data->{'fwd'}.$data->{'pol'}.$data->{'pod'} ;
		($pos, @array_liaison) = &get_pos($liaison, @array_liaison)  ;
		($gpe_pos, @array_gpe) = &get_gpe_pos($data->{'fwd'}, @array_gpe)  ;
		$row = ($pos*3+2+$gpe_pos) ;
		
			$last_date = $data->{'year_month_etd'} ;
			$last_fwd = $data->{'fwd'} ;
			
			$worksheet->write_string(1, 0, 'Forwarder',$format);
			$worksheet->write_string(1, 1, 'POL',$format);
			$worksheet->write_string(1, 2, 'POD',$format);
			$worksheet->write_string(1, 3, 'TYPE',$format);
			$worksheet->write_string(1, $col+1, 'Quoté',$format_nb_eq);
			$worksheet->write_string(1, $col+2, 'Engagé',$format_cbm);
			$worksheet->write_string(1, $col+3, 'Embarqué',$format_cbm);
		
			$worksheet->write_string($row, 0, $data->{'fwd'},$format_top);
			
			$worksheet->write_string($row, 1, $data->{'pol'},$format_top);
			$worksheet->write_string($row, 2, $data->{'pod'},$format_top);
			
			$worksheet->write_string(0 ,$col+1,'',$format_silver);
			$worksheet->write_string(0 ,$col+2, $data->{'year'}." - ".$data->{'month'},$format_range_right);
			$worksheet->write_string(0 ,$col+3,'',$format_silver_left);
			
			$worksheet->write_string($row, 3, 'LCL',$format_lcl_title);
			$worksheet->write_number($row, $col+1, $data->{'lcl_quoted_freight'},$format_nb_lcl);
			$worksheet->write_number($row, $col+2, $data->{'lcl_engaged_freight'},$format_lcl);
			$worksheet->write_number($row, $col+3, $data->{'lcl_shipped_freight'},$format_in_top);
			
			$row++;
			$worksheet->write_string($row, 3, 'FCL',$format_title);
			$worksheet->write_number($row, $col+1, $data->{'fcl_quoted_freight'},$format_cells);
			$worksheet->write_number($row, $col+2, $data->{'fcl_engaged_freight'},$format_in_cells);
			$worksheet->write_number($row, $col+3, $data->{'fcl_shipped_freight'},$format_in);
			
			$row++;
			$worksheet->write_string($row, 3, 'Total',$format_tot);
			$worksheet->write_number($row, $col+1, $data->{'total_quoted_freight'},$format_tot_left);
			$worksheet->write_number($row, $col+2, $data->{'total_engaged_freight'},$format_tot_in);
			$worksheet->write_number($row, $col+3, $data->{'total_shipped_freight'},$format_total_right);
			
			$sub_total_quoted+=$data->{'total_quoted_freight'};
			$sub_total_engaged+=$data->{'total_engaged_freight'} ;
			$sub_total_shipped+=$data->{'total_shipped_freight'} ;
	}	
	
	$rs->finish;
}

sub get_last_gpe_pos{
	my ($gpe_item, @array_liaison) =@_ ;
	
	my $pos = 0 ;
	for ( my $i=0 ; $i < scalar(@array_liaison) ; $i++ ){
		my $liaison = $array_liaison[$i];
		my $cur_gpe = substr($liaison, 0, length($liaison)-10 );
		if($cur_gpe eq $gpe_item ){
			$pos = $i ;
		}
	}
	
	return $pos ;
}

sub get_gpe_pos{
	my ($gpe_item, @array_gpe) =@_ ;
	
	my $pos = 0 ;
	my $is_in_array;
	for ( my $i=0 ; $i < scalar(@array_gpe) ; $i++ ){
		
		if($array_gpe[$i] eq $gpe_item ){
			$pos = $i ;
			$is_in_array = 1;
		}
	}
	if ( $is_in_array ne 1 ){
		push(@array_gpe, $gpe_item ) ;
		$pos = scalar(@array_gpe) -1 ;
	}
	
	return $pos, @array_gpe ;
}

sub get_pos{

	my ($liaison, @array_liaison) =@_ ;
	
	my $pos = 0 ;
	my $is_in_array;
	for ( my $i=0 ; $i < scalar(@array_liaison) ; $i++ ){
		
		if($array_liaison[$i] eq $liaison ){
			$pos = $i ;
			$is_in_array = 1;
		}
	}
	if ( $is_in_array ne 1 ){
		push(@array_liaison, $liaison ) ;
		$pos = scalar(@array_liaison) -1 ;
	}
	
	return $pos, @array_liaison ;
}


#################
#Envoi du fichier
#################

sub send_export {

	my $send_result = "";
	my ($recipient, $forwarder, $copy, $file_path ) = @_;
	
	if ($file_name ne "" && $recipient ne "") {
	
	
		# On expédie le document
		# On prépare le message e-mail
	
	my $message = "
Bonjour, <br />

Vous trouverez ci-joint à ce courrier l'export des flux. <br />
Cordialement. <br /><br />
<B>CLOUD SCM  with  E-SOLUTIONS sas</B><br/>
7 rue Poullain Duparc 35000 Rennes (F)<br/>
Tél :   +33 (0)230 0231 60   or  +33 (0)950 2488 70<br/>
<a href='http://www.fcsystem.com'>http://www.fcsystem.com</a>
	"; 
	
		my $mime_msg = MIME::Lite->new(
			From    => $forwarder,
			To      => $recipient,
			Cc      => $copy,
			Subject => 'Extraction des flux portuaires',
			Type    => 'TEXT/html',
			Data    => $message
		)
		or print STDERR ("Erreur lors de la création de MIME body: $!\n");
	
		# On attache le fichier dispo à l'e-mail
		
		$mime_msg->attach(
			Type => 'BINARY',
			Path => $file_path,
			ReadNow  => 1
		)
		or print STDERR ("Erreur lors de l'attachement du fichier export : $!\n");
		
		my $error;	
		if($mime_msg->send_by_smtp(esolGetEnv(__FILE__, 'SMTP_SERVER'))) {
			$error = 0;
		} else {
			$error = 1;
		}
		
		if ($error) {
			$send_result .= "File export has not been sent to $recipient <br>" ;
			$send_result .= "You can download it to following link : <br>" ;
			$send_result .= "<a href='".$file_path."' target='_blank'>File export</a>" ;
			print  STDERR "[$0] ERREUR envoi export $file_name par e-mail pour $recipient , demandeur : yrgroup\@fcsystem.com \n";
		} else {
			$send_result .= "File export has been sent to $recipient" ;
	#		`rm -rf $file_path`;
		}
#	print $file_path."\n";		
	
	}
	
#	print $send_result."\n";

}