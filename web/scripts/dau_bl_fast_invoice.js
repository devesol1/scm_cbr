remove_fast_invoice_line = function (which_object){
		var which_tr_as_ref = which_object.up('tr');
		var which_bl_num=which_tr_as_ref.select('[name="fast_invoice_bl_num"]').first().value;
		alert(which_bl_num);
		alert($('bl_error_msg').innerHTML);
		if($('bl_error_msg').innerHTML.indexOf(which_bl_num)>-1){
			$('bl_error_msg').innerHTML = "";
		}
		if($('error_msg').innerHTML.indexOf(which_bl_num)>-1){
			$('error_msg').innerHTML = "";
		}
		which_tr_as_ref.remove();
//		which_tr_as_ref.style.display='none';
}

duplicate_fast_invoice_line = function (which_object){
//		alert("duplicate_fast_invoice_line ");
		
//		var which_tr_as_ref = $$('[name="tr_invoice"]').last();
		var which_tr_as_ref = which_object.up('tr');

		var new_tr = new Element( 'tr' );

		new_tr.setAttribute("name", "tr_invoice");
		new_tr.update(which_tr_as_ref.innerHTML);

		
//Les valeurs familles de cout, prestataire et monnaie de la nouvelle ligne doivent etre �gale aux valeurs de la derni�re ligne.
	  	new_tr.select('[name="select_fast_invoice_cost_kind"]').last().value=which_tr_as_ref.select('[name="select_fast_invoice_cost_kind"]').first().value;
	  	new_tr.select('[name="select_fast_invoice_issuer"]').last().value=which_tr_as_ref.select('[name="select_fast_invoice_issuer"]').first().value;
	  	new_tr.select('[name="select_fast_invoice_currency"]').last().value=which_tr_as_ref.select('[name="select_fast_invoice_currency"]').first().value;
		
		change_currency( new_tr.select('[name="select_fast_invoice_currency"]').last() );

//		which_tr_as_ref.select('[title="add_button"]').last().style.display='none';

		which_tr_as_ref.insert ({
			'after'  :  new_tr
		});

}

change_currency = function(which_object){

	var which_tr = which_object.up('tr');
	var which_select = which_tr.select('[name="select_fast_invoice_currency"]').first();
	var which_currency = which_select.options[which_select.selectedIndex].text;
	var which_span_currency = which_tr.select('[name="currency"]').first();

	if(which_currency == 'usd'){
		which_span_currency.style.color="#FF0000";
		which_span_currency.innerHTML="$";
	} else {
		which_span_currency.style.color="#000000";
		which_span_currency.innerHTML="&euro;";
	}

}
save_dau_bl_fast_invoice_form = function(){
	is_error_in_form();
	if($('error_msg').innerHTML == "" && $('bl_error_msg').innerHTML == ""){
		form_bl_invoice_save.submit();
	}
}

is_error_in_form = function(){
	var error=false;
	var msg="";
	if(!error){
	
		var tr_array = $$('[name="tr_invoice"]');
		tr_array.each(
			function(elt, index) {
				var invoice_num = elt.select('[name="fast_invoice_num"]').first().value;		
				if(!error && invoice_num == ''){
					error = true;
					msg = "Il n'y a pas de numero de facture pour la ligne "+(index+1);
	//				alert("Il n'y a pas de numero de facture pour la ligne "+(index+1));
				}
				if(!error && elt.select('[name="select_fast_invoice_cost_kind"]').first().value == '')
					error = true;
				if(!error && elt.select('[name="select_fast_invoice_issuer"]').first().value == '')
					error = true;
				if(!error && elt.select('[name="select_fast_invoice_currency"]').first().value == '')
					error = true;
				if(!error && elt.select('[name="fast_invoice_bl_num"]').first().value == '')
					error = true;
				if(!error && elt.select('[name="fast_invoice_amount"]').first().value == '')
					error = true;
				if(error && invoice_num != "")
					msg = "Des informations sont manquantes pour la facture : "+invoice_num;
			}
		);
	}
	$('error_msg').innerHTML = msg;
	
}

is_bl_contain_several_po = function(){
//	alert("function is_bl_contain_several_po");
	var url = 'ajax/is_bl_contain_several_po.psp';
	var value_to_return = false;
	var list_of_bl=get_list_of_bl();
	var error;
	new Ajax.Request(url, {
		method: 'post',
		parameters:{list_of_bl : list_of_bl},
		onSuccess: function(transport) {
			$('bl_error_msg').innerHTML = transport.responseText;
		}
	});
}

get_list_of_bl = function(){
	var list_of_bl="";
	var tr_array = $$('[name="tr_invoice"]');
	tr_array.each(
		function(elt, index) {
//		alert(elt.select('[name="fast_invoice_bl_num"]').first().value);
			list_of_bl+="'"+elt.select('[name="fast_invoice_bl_num"]').first().value+"', ";
		}
	)
	if(list_of_bl != ""){
		list_of_bl += "''";
	}
	return list_of_bl;
}

check_bl = function(which_object){

	var which_td_as_ref = which_object.up('td');

	var url = 'ajax/is_bl_contain_several_po.psp';
	var value_to_return = false;
	var list_of_bl=which_object.value;
	new Ajax.Request(url, {
		method: 'post',
		parameters:{list_of_bl : list_of_bl},
		onSuccess: function(transport) {
			$('bl_error_msg').innerHTML = transport.responseText;
		},
		onComplete : function(transport){
			alert(transport.responseText);
			if(transport.responseText != ""){
//					this.style.color='#FF0000';
//					alert(which_td_as_ref.innerHTML)
				which_td_as_ref.select('[name="fast_invoice_bl_num_asterisk"]').first().show();
				which_td_as_ref.select('[name="fast_invoice_bl_num_asterisk"]').first().title =transport.responseText;
//				} else {
//					this.style.color='#000000';
//					which_td_as_ref.select('[name="fast_invoice_bl_num_asterisk"]').first().hide();
				which_td_as_ref.select('[name="fast_invoice_bl_num_asterisk"]').first().title="";
			}
		}
	});

}
	
	
/*
new PeriodicalExecuter(
	function(pe) {
		is_bl_contain_several_po()
	}, 
	1
);
*/