//////////////////////////
// Fonctions generiques
//////////////////////////

start_loading = function (which_div){

	$('main_loading_div').innerHTML = "";
	body_node = $('main_loading_div');
	
	new_loading_div=document.createElement("div");

	id_loading_div = "loading_"+which_div;
	new_loading_div.setAttribute("id", id_loading_div);
	body_node.appendChild(new_loading_div);
	$(id_loading_div).innerHTML += "<br><br><img src=ajax/img/ajax-loader.gif><br>LOADING...<br><br>";
	
	$(id_loading_div ).style.width = '400';
	$(id_loading_div ).style.textAlign = 'center';
	$(id_loading_div ).style.fontFamily = 'Arial';
	$(id_loading_div ).style.fontSize = '20px';
	$(id_loading_div ).style.fontWeight = 'bold';
	$(id_loading_div ).style.position = 'absolute';
	$(id_loading_div ).style.backgroundColor = '#FFFFFF';
	$(id_loading_div ).style.top = '100';
	$(id_loading_div ).style.left = getWidthCenterOfPage()-200;
	new Effect.Appear(id_loading_div);
}

go_to_prelevement = function(){
	url ='prelevement_pays.psp?TICKET='+$F('ticket')+'&FIND_UO='+$F('uo_num');
	window.open(url);
}

go_to_invoice = function(){
	url ='dau_bl_invoice.psp?TICKET='+$F('ticket')+'&is_standalone=1&s_uo='+$F('uo_num');
	window.open(url);
}

fill_div = function ( which_div, url, params){

// Cette fonction permet d'afficher le contenu du r�sultat d'une requete Ajax dans le div pass� en param�tre	
//	prompt('',"http://"+document.location.host+"/cbryrocher/"+url+"?"+params);
//	dbug("http://"+document.location.host+"/cbryrocher/"+url+"?"+params);
	//	$(which_div).innerHTML = '';
	var myAjax = new Ajax.Updater( 
	which_div, 
	url, 
	{	
		method: 'get', 
		parameters: params, 
		evalScripts: true, 
		asynchronous:true,
		onCreate: start_loading(which_div),
		onComplete: function(){
						new Effect.Fade("loading_"+which_div);
					}
	}
	); 
}

dbug = function(text){
	$('dbug_text').innerHTML += text+"<br> ";
}

get_tag_value = function(which_tag_name){
// Cette fonction permet de r�cup�rer le param�tre value du tag pass� en param�tre	
	my_node = document.getElementsByTagName(which_tag_name)[0];
	return my_node.innerHTML;
//	return my_node.getAttribute('value');
}

set_tag_value = function(which_tag_name, which_value){
// Cette fonction permet remplir le param�tre value du tag pass� en param�tre	
	my_node = document.getElementsByTagName(which_tag_name)[0];
	my_node.setAttribute('value', which_value);
}

///////////////////////////
// Functions dau_main_grid
///////////////////////////

find_from_criteria = function(which_sens, which_offset){

	if( typeof(which_sens) == 'undefined') which_sens = '';
	if( typeof(which_offset) == 'undefined') which_offset = '';
	if( typeof(which_nomenclature) == 'undefined') which_nomenclature = '';

	var which_ticket = $F('ticket');
	var which_profil = $F('profil');

	var which_uo = $F('s_uo'); 
	var which_po = $F('s_po'); 
//	var which_bl = $F('s_bl'); 
	var which_sku = $F('s_sku'); 
	var which_contact_ada = $('s_contact_ada').options[$('s_contact_ada').selectedIndex].value;
	var which_id_uo_status = $('s_id_uo_status').options[$('s_id_uo_status').selectedIndex].value;
	var which_dpt = $('s_dpt').options[$('s_dpt').selectedIndex].value;
	var which_date_open = $('s_date_open').options[$('s_date_open').selectedIndex].value;
	var which_date_solded = $('s_date_solded').options[$('s_date_solded').selectedIndex].value;
	var which_date_confirmed = $('s_date_confirmed').options[$('s_date_confirmed').selectedIndex].value;
	var which_resultat = $('s_resultat').options[$('s_resultat').selectedIndex].value;
	

	if(which_offset != ''){
		which_offset = parseInt(which_offset);
		
	}
	if (which_sens == "suivant") {
		which_offset += 20;
	}
	if (which_sens == "precedent") {
		which_offset -= 20;
	}
	url="?TICKET="+which_ticket+"&profil="+which_profil+"&OFFSET="+which_offset;

	if(which_uo.length > 0) url += '&s_uo='+which_uo;
	if(which_po.length > 0) url += '&s_po='+which_po;
//	if(which_bl.length > 0) url += '&s_bl='+which_bl;
	if(which_sku.length > 0) url += '&s_sku='+which_sku;
	if(which_contact_ada.length > 0) url += '&s_contact_ada='+which_contact_ada;
	if(which_id_uo_status.length > 0) url += '&s_id_uo_status='+which_id_uo_status;
	if(which_dpt.length > 0) url += '&s_dpt='+which_dpt;
	if(which_date_open.length > 0) url += '&s_date_open='+which_date_open;
	if(which_date_solded.length > 0) url += '&s_date_solded='+which_date_solded;
	if(which_date_confirmed.length > 0) url += '&s_date_confirmed='+which_date_confirmed;
	if(which_resultat.length > 0) url += '&s_resultat='+which_resultat;

  	document.location.href=url;
}


///////////////////////////
// Functions dau_uo_detail
///////////////////////////

show_uo_detail = function( id_uo, is_ada ){
	params = "id_uo="+id_uo;
        params += "&is_ada="+is_ada;
	which_div = "div_dau_uo_detail";
	fill_div(which_div, "./ajax/dau_uo_detail.psp", params);
}

toggle_uo_status = function(){
	if($("td_select_uo_status").style.display == "none"){
		Element.show("td_select_uo_status");
		Element.hide("td_print_uo_status");
	} else {
		Element.show("td_print_uo_status");
		Element.hide("td_select_uo_status");
	}
	
}

save_uo_status = function(){
	var id_status = $('id_dau_status').options[$('id_dau_status').selectedIndex].value;
	var id_uo = $F('id_uo');
	var params = "id_uo="+id_uo;
	params += "&id_status="+id_status;
	
	which_td = "td_select_uo_status";
	if( $('id_dau_status').options[$('id_dau_status').selectedIndex].value == '40' /*Confirm�*/ && $('checkbox_is_not_waiting_invoice0').checked == true ){
		alert("Il est interdit de passer en statut \"Confirm�\" \n cette UO car elle a encore des factures � venir...");
	} else {
		fill_div(which_div, "./ajax/dau_save_uo_status.psp", params);
	}
	
}

///////////////////////////
// Functions dau_pos_skus_list
///////////////////////////

show_pos_skus_list = function( id_uo ){
	params = "id_uo="+id_uo;
	params += "&TICKET="+$F("ticket");
	which_div = "div_dau_po_roots_skus_list";
	fill_div(which_div, "./ajax/dau_pos_skus_list.psp", params);
}

update_list_po_roots = function( which_po_root_num, is_po_root_to_add ){
//	dbug("update_list_po_roots : "+which_po_root_num);
	list_po_roots = new Array();
	if($('list_po_roots_to_explore').innerHTML.length > 0){
		list_po_roots = $('list_po_roots_to_explore').innerHTML.split(',');
	}
	
	list_po_roots.del(which_po_root_num);
	if (is_po_root_to_add){
		list_po_roots.push(which_po_root_num);
	}
	$('list_po_roots_to_explore').innerHTML = list_po_roots;
	
}

update_list_skus = function( which_sku_num, is_sku_to_add ){
//	dbug("update_list_skus : "+which_sku_num);

	list_skus = new Array();
	if($('list_skus_to_explore').innerHTML.length > 0){
		list_skus = $('list_skus_to_explore').innerHTML.split(',');
	}
	
	list_skus.del(which_sku_num);
	if (is_sku_to_add){
		list_skus.push(which_sku_num);
	}
	$('list_skus_to_explore').innerHTML = list_skus;

}


update_po_root_skus = function( which_object ){
	var is_checked = which_object.checked;
	var my_value = which_object.value;
	var my_po_root_num = my_value.substring(("po_root").length, my_value.length);
	var my_sku_num='';
	if(my_po_root_num.indexOf('|') > -1){
		my_sku_num = my_po_root_num.substring(my_po_root_num.indexOf('|')+("sku").length+1, my_po_root_num.length);
		my_po_root_num = my_po_root_num.substring(0, my_po_root_num.indexOf('|'));
	}
	if( my_sku_num == '' ){
		update_every_skus_from_po_root(my_po_root_num, is_checked);
	} else {
		update_po_root_from_sku(my_po_root_num, my_sku_num, is_checked);
	}
	scan_check_box_to_update_list_po_roots_skus();
}

scan_check_box_to_update_list_po_roots_skus = function(){
	var my_input_array = document.getElementsByTagName('input');
	var list_po_roots = new Array();
	var list_skus = new Array();
	for(i=0; i<my_input_array.length; i++){
		if(my_input_array[i].type == "checkbox" ){
			var my_value = my_input_array[i].value;
			var my_po_root_num = my_value.substring(my_value.indexOf("po_root")+("po_root").length, my_value.length);
			var my_sku_num='';
			if(my_po_root_num.indexOf('|') > -1){
				my_sku_num = my_po_root_num.substring(my_po_root_num.indexOf('|')+("sku").length+1, my_po_root_num.length);
				my_po_root_num = my_po_root_num.substring(0, my_po_root_num.indexOf('|'));
			}
			if(my_input_array[i].checked){
				if(my_sku_num == '' ){
					list_po_roots.push(my_po_root_num);
				} else {
					if(list_skus.indexOf(my_sku_num) < 0){
						list_skus.push(my_sku_num);
					}
				}
			}
		}
	}
	$('list_po_roots_to_explore').innerHTML = list_po_roots;
	$('list_skus_to_explore').innerHTML = list_skus;
	refresh_all_po_roots();
}


update_every_skus_from_po_root = function( which_po_root_num, is_to_check ){
	var my_input_array = document.getElementsByTagName('input');
	for(i=0; i<my_input_array.length; i++){
		if(my_input_array[i].type == "checkbox" ){
			var my_value = my_input_array[i].value;
			var my_po_root_num = my_value.substring(my_value.indexOf("po_root")+("po_root").length, my_value.length);
			var my_sku_num='';
			if(my_po_root_num.indexOf('|') > -1){
				my_sku_num = my_po_root_num.substring(my_po_root_num.indexOf('|')+("sku").length+1, my_po_root_num.length);
				my_po_root_num = my_po_root_num.substring(0, my_po_root_num.indexOf('|'));
			}
			if(which_po_root_num == my_po_root_num && my_sku_num != ''){
				my_input_array[i].checked = is_to_check;
			}
		}
	}
}

update_po_root_from_sku = function( which_po_root_num, which_sku_num, is_to_check ){
	var my_input_array = document.getElementsByTagName('input');
	var is_all_sku_unchecked = true;
	var my_po_root_check_box_id;
	for(i=0; i<my_input_array.length; i++){
		if(my_input_array[i].type == "checkbox" ){
			var my_value = my_input_array[i].value;
			var my_po_root_num = my_value.substring(my_value.indexOf("po_root")+("po_root").length, my_value.length);
			var my_sku_num='';
			if(my_po_root_num.indexOf('|') > -1){
				my_sku_num = my_po_root_num.substring(my_po_root_num.indexOf('|')+("sku").length+1, my_po_root_num.length);
				my_po_root_num = my_po_root_num.substring(0, my_po_root_num.indexOf('|'));
			}
			if(which_po_root_num == my_po_root_num && my_sku_num != '' && is_all_sku_unchecked && my_input_array[i].checked){
				is_all_sku_unchecked = false;
			}
			if(which_po_root_num == my_po_root_num && my_sku_num == '' ){
				my_po_root_check_box_id = my_input_array[i].id;
				if ( is_to_check ){
					my_input_array[i].checked = is_to_check;
				}
			}
		}
	}
	if(is_all_sku_unchecked){
		$(my_po_root_check_box_id).checked = false;
	}
}



toggle_sku_list = function(which_id_po_root){

	var reg_exp=new RegExp(".+bullet_toggle_plus.png", "g");
	var img_toggle_src = $("img_toggle_po_root"+which_id_po_root+"_sku_list").src;
	
	if( img_toggle_src.match(reg_exp) ){
		img_toggle_src = img_toggle_src.replace(/plus/, "minus");
		Element.show("div_po_root"+which_id_po_root+"_sku_list");
	}else{
		img_toggle_src = img_toggle_src.replace(/minus/, "plus");
		Element.hide("div_po_root"+which_id_po_root+"_sku_list");
	}
	$("img_toggle_po_root"+which_id_po_root+"_sku_list").src = img_toggle_src;

}


show_extra_cost = function( which_id_po_root ){
	var params = "TICKET="+$F('ticket');
	params += "&id_uo="+$F('id_uo');
	if(is_set(which_id_po_root)){
		params += "&id_po_root="+which_id_po_root;
	}
//	alert($F('is_ada'));
	if($F('is_ada') == 0 ){
		params += "&is_cdg=1";
	}
	var url = "dau_extra_cost.psp?"+params
	var name = "extra_cost";
	var width = 640;
	var height = 600;
	var showChrome = 0;
	var canResize = 1;
	var canScroll = 1;
	var top = 100;
	var left = 1;
	openWindow(url , name , width , height , showChrome , canResize , canScroll , top , left);	
}

show_bl_invoice = function( which_id_po, which_id_bl ){
	var url = "dau_bl_invoice.psp?TICKET="+$F('ticket')+"&id_po="+which_id_po+"&id_bl="+which_id_bl;
	var name = "bl_invoice";
	var width = 640;
	var height = 600;
	var showChrome = 0;
	var canResize = 1;
	var canScroll = 1;
	var top = 100;
	var left = 200;
	openWindow(url , name , width , height , showChrome , canResize , canScroll , top , left);	
}


show_comment = function( which_id_uo, which_id_po_root, which_id_po, which_comment){
	if(!is_set(which_id_uo)){
		which_id_uo = '';
	}
	if(!is_set(which_id_po_root)){
		which_id_po_root = '';
	}
	if(!is_set(which_id_po)){
		which_id_po = '';
	}
	if(!is_set(which_comment)){
		which_comment = '';
	}
	
	which_comment
	var url = "dau_comment.psp?TICKET="+$F('ticket')
	url += "&id_uo="+which_id_uo;
	url += "&id_po_root="+which_id_po_root;
	url += "&id_po="+which_id_po;
	url += "&which_comment="+which_comment;
	var name = "comment";
	var width = 640;
	var height = 180;
	var showChrome = 0;
	var canResize = 1;
	var canScroll = 1;
	var top = 100;
	var left = 200;
	openWindow(url , name , width , height , showChrome , canResize , canScroll , top , left);	
}

show_comments = function( which_id_uo){
	if(!is_set(which_id_uo)){
		which_id_uo = '';
	}
	var url = "dau_view_comments.psp?TICKET="+$F('ticket')
	url += "&id_uo="+which_id_uo;
	var name = "comment";
	var width = 400;
	var height = 400;
	var showChrome = 0;
	var canResize = 1;
	var canScroll = 1;
	var top = 100;
	var left = 200;
	openWindow(url , name , width , height , showChrome , canResize , canScroll , top , left);	
}


Array.prototype.del = function(which_elt_to_remove) {
	var final_array = new Array();
	for(i=0; i<this.length; i++){
		if(this[i] != which_elt_to_remove){
			final_array.push(this[i]);
		}
	}
	this.length=0;
	return this.push.apply(this, final_array);
};




// Functions dau_pos_header

refresh_pos_header = function( which_col, which_export_level){
	var params = "list_po_roots="+$("list_po_roots_to_explore").innerHTML;
	params += "&list_skus="+$("list_skus_to_explore").innerHTML;
	params += "&id_export_level="+which_export_level;
	params += "&which_col="+which_col;

	var which_td = "td_po_header_"+which_col+"_detail";
	
	fill_div(which_td, "./ajax/dau_pos_header.psp", params);
}

refresh_ct_details = function( which_col, which_export_level){
	var params = "list_po_roots="+$("list_po_roots_to_explore").innerHTML;
	params += "&uo_num="+$("uo_num").value;
	params += "&id_export_level="+which_export_level;
	params += "&which_col="+which_col;
	
	var which_td = "td_ct_"+which_col;

	fill_div(which_td, "./ajax/dau_ct_details.psp", params);
}


refresh_costs_grid = function( which_div){
	var params = "list_po_roots="+$("list_po_roots_to_explore").innerHTML;
	params += "&uo_num="+$F("uo_num");
	params += "&profil="+$F("profil");
	params += "&TICKET="+$F("ticket");
	fill_div(which_div, "./ajax/dau_costs_grid.psp", params);

}

var stack_grid_detail = new Array();


stack_refresh_costs_grid_detail = function( which_div, which_po_num, which_bl_num, which_id_export_level, which_nb_bl_per_shpt, which_id_bl){
	var cost_detail = new Object();
	cost_detail["div"] = which_div;
	cost_detail["po_num"] = which_po_num;
	cost_detail["bl_num"] = which_bl_num;
	cost_detail["id_export_level"] = which_id_export_level;
	cost_detail["nb_bl_per_shpt"] = which_nb_bl_per_shpt;
	cost_detail["id_bl"] = which_id_bl;
	stack_grid_detail.push(cost_detail);
	
}

var timeout_cost_detail;

next_if_div_cost_detail_not_empty = function(which_div){
	if($(which_div).innerHTML != "&nbsp;"){
		window.clearTimeout(timeout_cost_detail);
		stack_grid_detail.shift();
		scan_stack_grid_detail();
	}
}

scan_stack_grid_detail = function(){
	if(stack_grid_detail.length>0){
		var cost_detail = new Object();
		cost_detail = stack_grid_detail[0];
		//if( typeof(cost_detail) == 'undefined') which_sens = '';
		refresh_costs_grid_detail( cost_detail["div"], cost_detail["po_num"], cost_detail["bl_num"], cost_detail["id_export_level"], cost_detail["nb_bl_per_shpt"], cost_detail["id_bl"]);
		timeout_cost_detail = setTimeout(function(){ next_if_div_cost_detail_not_empty(cost_detail["div"])},500);
	}
}

refresh_costs_grid_detail = function( which_div, which_po_num, which_bl_num, which_id_export_level, which_nb_bl_per_shpt, which_id_bl){
	var reg_replace_first_letter=new RegExp("^[A-Z]*(.*)", "g");
	var params_po_num = which_po_num;
	if(which_po_num.indexOf('P')== 0 || which_po_num.indexOf('U')== 0){
		params_po_num = params_po_num.substring(1, params_po_num.length);
		if(params_po_num.indexOf('B')== 0 ){
			params_po_num = 'P'+params_po_num;
		}
		
	}

	var is_not_waiting_invoice ;
	if ( $('is_not_waiting_invoice')){
		is_not_waiting_invoice = $F('is_not_waiting_invoice');
	}
	
//	var params = "po_num="+which_po_num.replace(reg_replace_first_letter,"$1");
	var params = "";
	params += "po_num="+params_po_num;
	params += "&bl_num="+which_bl_num;
	params += "&profil="+$F("profil");
	params += "&id_export_level="+which_id_export_level;
	params += "&is_not_waiting_invoice="+$F('is_not_waiting_invoice');
	
	var reg_exp_facture=new RegExp("facture","g");
	var reg_exp_ecart=new RegExp("ecart","g");
	var reg_exp_un_bis=new RegExp("un_bis","g");
	var reg_exp_total_po=new RegExp("^P","g");
	var reg_exp_total_uo=new RegExp("^U","g");

	var which_col = which_div;
	which_col = which_col.substring("td_cost_detail_".length, which_col.indexOf("_po"));
	params += "&which_col="+which_col;
	//alert(params);	
	if(which_div.match(reg_exp_facture)){
		params += "&is_facture=1";
	}

	if(which_div.match(reg_exp_un_bis)){
		params += "&is_un_bis=1";
	}

	if(which_po_num.match(reg_exp_total_po)){
		var is_total_po = "&is_total_po=1";
		if(which_po_num.indexOf("PB")== 0 ){
			is_total_po = "&is_total_po=";
		}
	
		params += is_total_po;

	}
	
	if(which_po_num.match(reg_exp_total_uo)){
		params += "&is_total_uo=1";
	}

	if(which_po_num == which_bl_num){
		params += "&id_bl="+which_id_bl;
	}

	
	if( typeof(which_nb_bl_per_shpt) == 'undefined') which_nb_bl_per_shpt = '';
	params += "&nb_bl_per_shpt="+which_nb_bl_per_shpt;
	
	params += "&TICKET="+$F("ticket");
		
//	dbug("which_div : "+which_div);
//	dbug("http://"+document.location.host+"/cbryrocher/"+"./ajax/dau_costs_details.psp"+"?"+params);
	fill_div(which_div, "./ajax/dau_costs_details.psp", params);
}

refresh_all_po_roots = function(){
	
	
		if (document.getElementById('td_po_header_quotation_figee_header').style.display != 'none'){
			
			refresh_pos_header("quotation_figee", 1);
			
			refresh_ct_details("types", 0);
			refresh_ct_details("quotation_figee", 1);

			refresh_ca_details("title", 0);
			refresh_ca_details("quotation_figee", 1);
			
			refresh_extra_cost_details("title", 0);
	
		}
		
	
		if (document.getElementById('td_po_header_ecart_amende_quotation_header').style.display != 'none'){
			
			refresh_ct_details("ecart_amende_quotation", 1000);

			refresh_ca_details("ecart_amende_quotation", 1000);		
		}
		
		if (document.getElementById('td_po_header_reel_amende_header').style.display != 'none'){
			

			refresh_pos_header("reel_amende", 2);

			refresh_ct_details("reel_amende", 2);
			
			refresh_ca_details("reel_amende", 2);

			refresh_extra_cost_details("reel_amende", 2);
			
		}
		
		if (document.getElementById('td_po_header_ecart_amende_facture_header').style.display != 'none'){
			
			refresh_ct_details("ecart_amende_facture", 1000);

			refresh_ca_details("ecart_amende_facture", 1000);

			refresh_extra_cost_details("ecart_amende_facture", 1000);
			
		}
		
		if (document.getElementById('td_po_header_reel_facture_header').style.display != 'none'){
			
			refresh_pos_header("reel_facture", 3);
			
			refresh_ct_details("reel_facture", 3);

			refresh_ca_details("reel_facture", 3);

			refresh_extra_cost_details("reel_facture", 3);
		
		}
		
		if (document.getElementById('td_po_header_ecart_provision_header').style.display != 'none'){
			
			refresh_ct_details("ecart_provision", 1000);

			refresh_ca_details("ecart_provision", 1000);

			refresh_extra_cost_details("ecart_provision", 1000);
		
		}
		
		if (document.getElementById('td_po_header_reel_engage_header').style.display != 'none'){
			

			refresh_pos_header("reel_engage", 3);

			refresh_ct_details("reel_engage", 3);

			refresh_ca_details("reel_engage", 3);

			refresh_extra_cost_details("reel_engage", 3);
			
		}

	refresh_extra_cost_not_invoiced_details("reel_facture", 3);

	refresh_costs_grid("div_costs_grid");

}

refresh_ca_details = function( which_col, which_export_level){
	var params = "id_uo="+$F("id_uo");
	params += "&id_export_level="+which_export_level;
	params += "&which_col="+which_col;
	params += "&TICKET="+$F("ticket");
	var which_td = "td_ca_"+which_col;
	console.log()
	fill_div(which_td, "./ajax/dau_ca_details.psp", params);
}


refresh_extra_cost_details = function( which_col, which_export_level){
	var params = "id_uo="+$F("id_uo");
	params += "&id_export_level="+which_export_level;
	params += "&which_col="+which_col;
	
	var which_td = "td_extra_cost_"+which_col;

	fill_div(which_td, "./ajax/dau_extra_cost_details.psp", params);
}

refresh_extra_cost_not_invoiced_details = function( which_col, which_export_level){
	var params = "id_uo="+$F("id_uo");
	params += "&id_export_level="+which_export_level;
	params += "&which_col="+which_col;
	
	var which_td = "td_extra_cost_not_invoiced_"+which_col;

	fill_div(which_td, "./ajax/dau_extra_cost_not_invoiced_details.psp", params);
}

show_cols_to_show = function(){

	var my_td_array = document.getElementsByTagName('td');

	var list_cols_to_show;
	if($('list_cols_to_show').innerHTML.length > 0){
		list_cols_to_show = $('list_cols_to_show').innerHTML.split(',');
	}

	for(var i=0; i<my_td_array.length; i++){
		if (is_set(my_td_array[i].id)){
			if(my_td_array[i].id.length>0) {
				var my_id = my_td_array[i].id;
				for( var j=0; j<list_cols_to_show.length; j++){
					var my_col_to_show = list_cols_to_show[j];
//					var my_col_to_show = "reel_amende";
//					dbug("]"+my_id+"[ | ]"+my_col_to_show+"[ : " +my_id.indexOf(my_col_to_show));
					if( my_id.indexOf(my_col_to_show) >=0 ){
//						dbug(my_id);
						Element.show(my_id);
					}
				}
			}
		}
	}

}

calcul_ecart = function(){
	var my_span_array = document.getElementsByTagName('span');
	for(var i=0; i<my_span_array.length; i++){
		my_id = my_span_array[i].id;
		if(my_id.indexOf("field")>-1 && my_id.indexOf("ecart")>-1){
//		dbug(my_id);
			my_id_elts = my_id.split("--");
			var which_field = my_id_elts[0].split("__")[1];
			var which_ecart = my_id_elts[1].split("__")[1];
			which_ecart = which_ecart.substring("ecart_".length, which_ecart.length);
			var which_po_num = my_id_elts[2].split("__")[1];
			var which_bl_num = my_id_elts[3].split("__")[1];
			
			if(which_bl_num == ''){
				var reg_replace_first_letter=new RegExp("^[A-Z]*(.*)", "g");
				which_bl_num = which_po_num.replace(reg_replace_first_letter,"$1");
			}
			
			var pattern = "field__"+which_field+"--col__"+"str_to_replace"+"--po_num__"+which_po_num+"--bl_num__"+"bl_num_to_replace";
			var first_member;
			var second_member;
			
			if(my_id.indexOf("__T")>-1 ){
//				alert(pattern);
//				alert(my_id);
			}
			
			switch(which_ecart){
				case "amende_quotation" :
					first_member = pattern.replace(/str_to_replace/, "reel_amende");
					first_member = first_member.replace(/bl_num_to_replace/, which_po_num);
					second_member = pattern.replace(/str_to_replace/, "quotation_figee");
					second_member = second_member.replace(/bl_num_to_replace/, which_po_num);
					break;
				case "amende_facture" :
					first_member = pattern.replace(/str_to_replace/, "reel_amende");
					first_member = first_member.replace(/bl_num_to_replace/, which_po_num);
					second_member = pattern.replace(/str_to_replace/, "reel_facture");
					second_member = second_member.replace(/bl_num_to_replace/, which_bl_num);
					break;
				case "provision" :
					first_member = pattern.replace(/str_to_replace/, "reel_engage");
					first_member = first_member.replace(/bl_num_to_replace/, which_bl_num);
					second_member = pattern.replace(/str_to_replace/, "reel_facture");
					second_member = second_member.replace(/bl_num_to_replace/, which_bl_num);
					break;
				case "a_date" :
//					first_member = pattern.replace(/str_to_replace/, "un_bis");
					first_member = pattern.replace(/str_to_replace/, "quotation_figee");
					first_member = first_member.replace(/bl_num_to_replace/, which_po_num);
					second_member = pattern.replace(/str_to_replace/, "reel_engage");
					second_member = second_member.replace(/bl_num_to_replace/, which_bl_num);
					break;
				default :
					break;
			} 

			
			if(my_id.indexOf("__T")>-1 ){
//				alert(first_member+" "+second_member);
			}

			var first_member_value = 0;
			var second_member_value = 0;
			
			if(is_set($(first_member))){
				first_member_value = $(first_member).innerHTML;
			}
			if(is_set($(second_member))){
				second_member_value = $(second_member).innerHTML;
			}

			if( first_member_value == '' || first_member_value == '&nbsp;') first_member_value = 0;
			if( second_member_value == '' || second_member_value == '&nbsp;') second_member_value = 0;
			
			
			try {
				first_member_value = first_member_value.replace(/ /g,""); // remove separator for thouhsand
				second_member_value = second_member_value.replace(/ /g,""); // remove separator for thouhsand
			}
			catch(error) {
			  console.error(error);
			  // expected output: SyntaxError: unterminated string literal
			  // Note - error messages will vary depending on browser
			}
			
			
			var ecart = first_member_value-second_member_value;
			console.log(ecart+" = "+first_member_value+" - "+second_member_value);

			
			
			
			ecart = Math.round(ecart*100)/100;
			ecart = new Intl.NumberFormat("fr", { useGrouping: true }).format(ecart); // 100000.10

			if(my_id.indexOf("field__ct")>-1 && ecart == 0){
				ecart = '&nbsp;';
			}
			if(isNaN(parseInt(ecart))){
				ecart = '&nbsp;';
			}

			$(my_id).innerHTML = ecart;
			
			if (1==0){
					$(my_id).innerHTML = '('+pattern+'-'+which_ecart+'-'+which_po_num+'-'+which_bl_num+')';
					$(my_id).innerHTML += '<br> ['+first_member+'-'+second_member+']';
					$(my_id).innerHTML += '<br>]' + first_member_value+'-'+second_member_value+'[';
					$(my_id).innerHTML += '<br> ='+ecart;
				}
			}

	}
	

}

var tm_is_runningID;

calcul_result = function(){
	calcul_result_per_col('quotation_figee');
	//calcul_result_per_col('un_bis');
	calcul_result_per_col('reel_amende');
	calcul_result_per_col('reel_facture');
	calcul_result_per_col('reel_engage');
	calcul_ecart();
	calcul_ecart_result();
/*
	tm_is_runningID = setTimeout("calcul_ecart()",200);
	setTimeout("calcul_ecart_result()",200);
*/	
    hide_row_without_values();

}

calcul_result_per_col = function(which_col){
	var total_ca = 0
	var total_extra_cost = 0; 
	var total_po = 0;
	var result = 0; 
	var result_without_customs = 0;
	var total_fees = 0;
	var total_customs = 0;

	if(is_set($("field__ca--col__"+which_col+"--po_num__T--bl_num__T"))){
		total_ca = $("field__ca--col__"+which_col+"--po_num__T--bl_num__T").innerHTML;
		total_ca = total_ca.replace(/ /g,""); // remove separator for thouhsand
        total_ca = total_ca.replace(/\s/g,"");
	}
	
	if(is_set($("field__extra_cost_not_invoiced--col__"+which_col+"--po_num__T--bl_num__T"))){
		var cost = $("field__extra_cost_not_invoiced--col__"+which_col+"--po_num__T--bl_num__T").innerHTML;
		cost = cost.replace(/ /g,""); // remove separator for thouhsand
        cost = cost.replace(/\s/g,"");
	    total_extra_cost = parseFloat(cost);	
		
	}
	if(is_set($("field__total--col__"+which_col+"--po_num__P--bl_num__P"))){
		
		var cost = $("field__total--col__"+which_col+"--po_num__P--bl_num__P").innerHTML;
		cost = cost.replace(/ /g,""); // remove separator for thouhsand
        cost = cost.replace(/\s/g,"");
	    total_po += parseFloat(cost);	
		
	}
	
	if(is_set($("field__import_dpt_fees--col__"+which_col+"--po_num__P--bl_num__P"))){
		
		var cost = $("field__import_dpt_fees--col__"+which_col+"--po_num__P--bl_num__P").innerHTML;
		cost = cost.replace(/ /g,""); // remove separator for thouhsand
        cost = cost.replace(/\s/g,"");
	    total_fees += parseFloat(cost);	
		
	}
	
	if(is_set($("field__qc_dpt_fees--col__"+which_col+"--po_num__P--bl_num__P"))){
		
		var cost = $("field__qc_dpt_fees--col__"+which_col+"--po_num__P--bl_num__P").innerHTML;
		cost = cost.replace(/ /g,""); // remove separator for thouhsand
        cost = cost.replace(/\s/g,"");
	    total_fees += parseFloat(cost);	
			
	}
	
	if(is_set($("field__import_risk_fees--col__"+which_col+"--po_num__P--bl_num__P"))){
		
		var cost = $("field__import_risk_fees--col__"+which_col+"--po_num__P--bl_num__P").innerHTML;
		cost = cost.replace(/ /g,""); // remove separator for thouhsand
        cost = cost.replace(/\s/g,"");
	    total_fees += parseFloat(cost);	
		
			
	}
	
	if(is_set($("field__purchase_fees--col__"+which_col+"--po_num__P--bl_num__P"))){
		
		var cost = $("field__purchase_fees--col__"+which_col+"--po_num__P--bl_num__P").innerHTML;
		cost = cost.replace(/ /g,""); // remove separator for thouhsand
        cost = cost.replace(/\s/g,"");
	    total_fees += parseFloat(cost);	

	}
	
	if(is_set($("field__other_fees--col__"+which_col+"--po_num__P--bl_num__P"))){
		
	    var cost = $("field__other_fees--col__"+which_col+"--po_num__P--bl_num__P").innerHTML;
		cost = cost.replace(/ /g,""); // remove separator for thouhsand
        cost = cost.replace(/\s/g,"");
	    total_fees += parseFloat(cost);	

	}
	

	if(is_set($("field__customs--col__"+which_col+"--po_num__P--bl_num__P"))){
		
		    var cost = $("field__customs--col__"+which_col+"--po_num__P--bl_num__P").innerHTML;
			cost = cost.replace(/ /g,""); // remove separator for thouhsand
            cost = cost.replace(/\s/g,"");
			total_customs += parseFloat(cost);	
	}
	
	if(total_ca == '' ){
		total_ca = 0;
	}
	if(total_extra_cost == '' ){
		total_extra_cost = 0;
	}
	if(total_po == '' ){
		total_po = 0;
	}

	if(total_customs == '' ){
		total_customs = 0;
	}
	result = parseFloat(total_ca)+parseFloat(total_extra_cost) +parseFloat(total_po) - parseFloat(total_fees);
	//CD20180424 ci-dessous l'ancienne regle
//	result = parseFloat(total_ca) - parseFloat(total_po);
//	result = 1;//parseFloat(total_ca)+parseFloat(total_extra_cost) +parseFloat(total_po) - parseFloat(total_fees);

	
	var tca = parseFloat(total_ca);
	var tex = parseFloat(total_extra_cost) ;
	var tp = parseFloat(total_po) ;
	var tf = parseFloat(total_fees);
	var tc = parseFloat(total_customs);
	//alert(tca+' + '+tex+' + '+tp+' - '+tf);
	result = Math.round(result*100)/100;
	
	
	//result = result/1000; // #Xp 20170905 edit result format with separator of thousands
	result = Math.round(result*100)/100

	var printResult = new Intl.NumberFormat("fr", { useGrouping: true }).format(result); // 100000.10
	$("field__result--col__"+which_col+"--po_num__--bl_num__").innerHTML =  printResult;
	
	result_without_customs  = result - parseFloat(total_customs);
	//CD20180424 ci-dessous l'ancienne regle
//	result_without_customs = parseFloat(total_ca) - parseFloat(total_customs) - parseFloat(total_po) - parseFloat(total_customs);
	
	//alert(tca+' + '+tex+' + '+tp+' - '+tf+' - '+tc);
	result_without_customs = Math.round(result_without_customs *100)/100;

	var printResult = new Intl.NumberFormat("fr", { useGrouping: true }).format(result_without_customs); // 100000.10
	$("field__resultHorsDouane--col__"+which_col+"--po_num__--bl_num__").innerHTML =  printResult ;
		
	if (which_col == "reel_amende" && result == "0")
	{
	 var elementsACacher = document.getElementsByClassName('cacher_'+which_col); //getElementsByClass(which_col);
      for(var i=0;i<elementsACacher.length;i++) {
       elementsACacher[i].style.display = 'none';
      }
	}
}



calcul_ecart_result = function(){
		
//		Sur la derni�re colonne, ligne R�sultat en �, la formule doit �tre : CA import net � engag� = r�sultat
		
	var engageCaValue;
	if ($('field__ca--col__reel_engage--po_num__T--bl_num__T')) {
		engageCaValue= document.getElementById('field__ca--col__reel_engage--po_num__T--bl_num__T').innerHTML;
	}
	var engageTotalPoValue;
	if ($('field__total--col__reel_engage--po_num__P--bl_num__P')) {
		engageTotalPoValue= document.getElementById('field__total--col__reel_engage--po_num__P--bl_num__P').innerHTML;
	}
	
	var engageTotalPoWithoutCustomValue;
	if ($('field__cost_po_without_customs--col__reel_engage--po_num__P--bl_num__P')){
		engageTotalPoWithoutCustomValue = document.getElementById('field__cost_po_without_customs--col__reel_engage--po_num__P--bl_num__P').innerHTML;
	}
	var coteTotalPoValue;
	if ($('field__total--col__quotation_figee--po_num__P--bl_num__P')){
		coteTotalPoValue = document.getElementById('field__total--col__quotation_figee--po_num__P--bl_num__P').innerHTML;
	}
	
	var coteTotalPoWithoutCustomValue;
	if ( $('field__cost_po_without_customs--col__quotation_figee--po_num__P--bl_num__P')) {
		coteTotalPoWithoutCustomValue = document.getElementById('field__cost_po_without_customs--col__quotation_figee--po_num__P--bl_num__P').innerHTML;
	}

	//R�sultat hors douane en � = CA import Net 9041� � cotation douane 5140� - engag� hors douane 3195� = 706�
	
			try {
				engageCaValue = engageCaValue.replace(/ /g,"");
				engageTotalPoValue = engageTotalPoValue.replace(/ /g,"");
				engageTotalPoWithoutCustomValue = engageTotalPoWithoutCustomValue.replace(/ /g,"");
				coteTotalPoValue = coteTotalPoValue.replace(/ /g,""); 
				coteTotalPoWithoutCustomValue = coteTotalPoWithoutCustomValue.replace(/ /g,""); 
				
				engageCaValue = parseInt(engageCaValue);
				engageTotalPoValue = parseInt(engageTotalPoValue);
				engageTotalPoWithoutCustomValue = parseInt(engageTotalPoWithoutCustomValue);
				coteTotalPoValue = parseInt(coteTotalPoValue);
				coteTotalPoWithoutCustomValue = parseInt(coteTotalPoWithoutCustomValue);
			}
			catch(error) {
			  console.error(error);
			  // expected output: SyntaxError: unterminated string literal
			  // Note - error messages will vary depending on browser
			}



			console.log(engageCaValue+' '+engageTotalPoValue);
		
		
		var result = engageCaValue - engageTotalPoValue;
		var printResult = new Intl.NumberFormat("fr", { useGrouping: true }).format(result);
		
		var oResult = document.getElementById('field__result--col__ecart_a_date--po_num__--bl_num__');
		oResult.innerHTML  = printResult;

		var result = engageCaValue - (coteTotalPoValue - coteTotalPoWithoutCustomValue) - engageTotalPoWithoutCustomValue;
		var printResult = new Intl.NumberFormat("fr", { useGrouping: true }).format(result);
		console.log(printResult );
		
		var oResultWithoutCustom = document.getElementById('field__resultHorsDouane--col__ecart_a_date--po_num__--bl_num__');
		oResultWithoutCustom.innerHTML  = printResult;
		console.log(oResultWithoutCustom.innerHTML);
		
}


calcul_ecart_resultOld = function(){
		
//		Sur la derni�re colonne, ligne R�sultat en �, la formule doit �tre : CA import net � engag� = r�sultat
		
		var engageCaValue = document.getElementById('field__ca--col__reel_engage--po_num__T--bl_num__T').innerHTML;
		var engageTotalPoValue = document.getElementById('field__total--col__reel_engage--po_num__P--bl_num__P').innerHTML;
		var engageTotalPoWithoutCustomValue = document.getElementById('field__cost_po_without_customs--col__reel_engage--po_num__P--bl_num__P').innerHTML;
		var coteTotalPoValue = document.getElementById('field__total--col__quotation_figee--po_num__P--bl_num__P').innerHTML;
		var coteTotalPoWithoutCustomValue = document.getElementById('field__cost_po_without_customs--col__quotation_figee--po_num__P--bl_num__P').innerHTML;

		try {
			engageCaValue = engageCaValue.replace(/ /g,""); // remove separator for thouhsand
			engageTotalPoValue = engageTotalPoValue.replace(/ /g,""); // remove separator for thouhsand
			engageCaValue = parseInt(engageCaValue);
			engageTotalPoValue = engageTotalPoValue.replace(/ /g,""); // remove separator for thouhsand
			engageTotalPoValue = parseInt(engageTotalPoValue);
			engageTotalPoValue = parseInt(engageTotalPoValue);
		}
		catch(error) {
		  console.error(error);
		  // expected output: SyntaxError: unterminated string literal
		  // Note - error messages will vary depending on browser
		}		
		
		var result = engageCaValue - engageTotalPoValue;
		var printResult = new Intl.NumberFormat("fr", { useGrouping: true }).format(result);
		
		var oResultEcart = document.getElementById('field__result--col__ecart_a_date--po_num__--bl_num__');
		oResultEcart.innerHTML  = printResult;

		var oResultEcartWithoutCustom = document.getElementById('field__resultHorsDouane--col__ecart_a_date--po_num__--bl_num__');
		
}



getElementsByClass =function (searchClass,node,tag) {
	
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i ].className) ) {
			classElements[j] = els[i ];
			j++;
		}
	}
	return classElements;
}

launch_calcul_ecart = function(){

	if($F("is_cost_detail_running") == 0){

		calcul_result();
//		calcul_ecart();
//		dbug("launch_calcul_ecart");
	} else {
		tm_is_runningID = setTimeout("launch_calcul_ecart()",500);
//		dbug("setTimeout  launch_calcul_ecart");
	}
}


set_is_cost_detail_running = function(is_running){
	if(!is_set(is_running)){
		is_running = false
	}
	if(is_running){
		clearTimeout(tm_is_runningID);
		$("is_cost_detail_running").value= 1;
	} else {
		$("is_cost_detail_running").value= 0;
	}
//	dbug($("is_cost_detail_running").value);
}

pos_header_calcul_ecart_a_date = function(){
	set_difference("total_sku_pcs", "reel_engage", "quotation_figee", "ecart_a_date", "pcs");
	set_difference("total_sku_ctns", "reel_engage", "quotation_figee", "ecart_a_date", "ctns");
	set_difference("total_sku_cbm", "reel_engage", "quotation_figee", "ecart_a_date", "m<sup>3</sup>");
	set_difference("total_sku_kgs", "reel_engage", "quotation_figee", "ecart_a_date", "kgs");
	set_difference("total_lcl_cbm", "reel_engage", "quotation_figee", "ecart_a_date", "m<sup>3</sup> ( LCL )");
	set_difference("total_air_cbm", "reel_engage", "quotation_figee", "ecart_a_date", "m<sup>3</sup> ( Air )");
}
			
set_difference = function(which_id, first_col, second_col, which_dest_col, which_unit ){
	var first_member = 	$F("hidden_"+which_id+"_col_"+first_col);
	var second_member = 	$F("hidden_"+which_id+"_col_"+second_col);
	var result = parseInt((first_member-second_member)*1000)/1000;
	result+=" "+which_unit;
	
	$("span_"+which_id+"_col_"+which_dest_col).innerHTML = result;
}

set_is_not_waiting_invoice = function(elt){
	
	var is_not_waiting_invoice = elt.value;
	
	var params = "id_uo="+$F('id_uo');
	params += "&is_not_waiting_invoice="+elt.value;
	var which_span = "span_is_not_waiting_invoice";
	fill_div(which_span, "./ajax/dau_main_save_is_not_waiting_invoice.psp", params);

	
}

mini_find_from_criteria = function (){

  var which_ticket = $F('ticket');
  var which_profil = $F('profil');
  var which_uo = $F('s_uo');
  var which_po = $F('s_po');
  var which_sku = $F('s_sku');
  
  var url = document.location.href.substring( 0, document.location.href.lastIndexOf('/')+1);
  url+="dau_main_grid.psp?TICKET="+which_ticket+"&profil="+which_profil;
  if(which_uo.length > 0) url += '&s_uo='+which_uo;
  if(which_po.length > 0) url += '&s_po='+which_po;
  if(which_sku.length > 0) url += '&s_sku='+which_sku;

  document.location = url;
  
 }

launch_again_alive_po = function(which_po){
	var which_po = which_po+"~1B";
	var url = "cotyr.fcsystem.com/logistic_routings_detail.psp";
	if(document.location.host.indexOf('vpn') > -1 ){
		url = "vpn-"+url;
	}
	url = "http://"+url+"?fic_num="+which_po;
	window.open(url);
}
 
 
 formatThousand =  function(id, value_to_convert){
	 	  
  if( value_to_convert.length > 0)
	{
		  value_to_convert = Math.round(value_to_convert);
  
		  value_to_convert += '';
		  var sep = ' ';
		  var reg = /(\d+)(\d{3})/;
		  while( reg.test(value_to_convert)) {
			value_to_convert = value_to_convert.replace( reg, '$1' +sep +'$2');
		  }
		  document.getElementById(id).innerHTML = value_to_convert;
		  
    }
	
		};
 /*
#CD20170811 Nouvelles fonctions hide_ct_if_ � la demande de Rozenn Brainstorming du 19/05/2017 pour masquer les equipements vides
*/
 

hide_ct_if_empty = function(){
	
	hide_ct_type_if_empty('ctAIR');	 
	hide_ct_type_if_empty('ctLCL');	 
	hide_ct_type_if_empty('ctCFS/CY');	 
	hide_ct_type_if_empty('ct20');	 
	hide_ct_type_if_empty('ct40');	 
	hide_ct_type_if_empty('ct40HC');	 
 }
 
 launch_hide_ct_if_empty = function(){

	if($("is_cost_detail_running")){
		if($F("is_cost_detail_running") == 0){
		hide_ct_if_empty();
		}
	} else {
		tm_is_runningID = setTimeout("launch_hide_ct_if_empty()",500);
	}
	
}

 

hide_ct_type_if_empty = function(ct_type){

//alert(ct_type);
	var quotation_figee = document.getElementById('field__'+ct_type+'--col__quotation_figee--po_num__--bl_num__').innerHTML;
	var ecart_amende_quotation = document.getElementById('field__'+ct_type+'--col__ecart_amende_quotation--po_num__--bl_num__').innerHTML;
	var reel_amende = document.getElementById('field__'+ct_type+'--col__reel_amende--po_num__--bl_num__').innerHTML;
	var ecart_amende_facture = document.getElementById('field__'+ct_type+'--col__ecart_amende_facture--po_num__--bl_num__').innerHTML;
	var reel_facture = document.getElementById('field__'+ct_type+'--col__reel_facture--po_num__--bl_num__').innerHTML;
	var ecart_provision = document.getElementById('field__'+ct_type+'--col__ecart_provision--po_num__--bl_num__').innerHTML;
	var reel_engage = document.getElementById('field__'+ct_type+'--col__reel_engage--po_num__--bl_num__').innerHTML;

	var str_to_test = (quotation_figee+ecart_amende_quotation+reel_amende+ecart_amende_facture+reel_facture+ecart_provision+reel_engage);
	var str_to_test = str_to_test.replace(/&nbsp;/g, '');

/*
	alert(']'+str_to_test+'[');
	alert(']'+str_to_test.length+'[');
*/	
	if(str_to_test.length == 0){
		remove('tr__'+ct_type+'--col__title--po_num__--bl_num__');
		remove('tr__'+ct_type+'--col__quotation_figee--po_num__--bl_num__');
		remove('tr__'+ct_type+'--col__ecart_amende_quotation--po_num__--bl_num__');
		remove('tr__'+ct_type+'--col__reel_amende--po_num__--bl_num__');
		remove('tr__'+ct_type+'--col__ecart_amende_facture--po_num__--bl_num__');
		remove('tr__'+ct_type+'--col__reel_facture--po_num__--bl_num__');
		remove('tr__'+ct_type+'--col__ecart_provision--po_num__--bl_num__');
		remove('tr__'+ct_type+'--col__reel_engage--po_num__--bl_num__');
	}

}

function remove(id) {
    var elem = document.getElementById(id);
    return elem.parentNode.removeChild(elem);
}

calcul_result_per_row = function(which_row,which_po_num, which_bl_num){
	
	var quotation_figee = 0;
	var un_bis = 0;
	var ecart_amende_quotation = 0;
	var reel_amende = 0;
	var ecart_amende_facture = 0;
	var reel_facture = 0;
	var ecart_provision = 0;
	var reel_engage = 0;
	var ecart_a_date = 0;


	if(is_set($("field__"+which_row+"--col__quotation_figee--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		quotation_figee = $("field__"+which_row+"--col__quotation_figee--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		quotation_figee = quotation_figee.replace(/ /g,""); // remove separator for thouhsand
        quotation_figee = quotation_figee.replace(/\s/g,"");
	}
	
	if(is_set($("field__"+which_row+"--col__un_bis--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		un_bis = $("field__"+which_row+"--col__un_bis--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		un_bis = un_bis.replace(/ /g,""); // remove separator for thouhsand
        un_bis = un_bis.replace(/\s/g,"");
	}
	
	if(is_set($("field__"+which_row+"--col__ecart_amende_quotation--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		ecart_amende_quotation = $("field__"+which_row+"--col__ecart_amende_quotation--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		ecart_amende_quotation = ecart_amende_quotation.replace(/ /g,""); // remove separator for thouhsand
        ecart_amende_quotation = ecart_amende_quotation.replace(/\s/g,"");
	}
	
	if(is_set($("field__"+which_row+"--col__reel_amende--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		reel_amende = $("field__"+which_row+"--col__reel_amende--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		reel_amende = reel_amende(/ /g,""); // remove separator for thouhsand
        reel_amende= reel_amende.replace(/\s/g,"");
	}
	
	if(is_set($("field__"+which_row+"--col__ecart_amende_facture--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		ecart_amende_facture = $("field__"+which_row+"--col__ecart_amende_facture--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		ecart_amende_facture = ecart_amende_facture(/ /g,""); // remove separator for thouhsand
        ecart_amende_facture= ecart_amende_facture.replace(/\s/g,"");
	}
	
	
	if(is_set($("field__"+which_row+"--col__reel_facture--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		reel_facture = $("field__"+which_row+"--col__reel_facture--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		reel_facture = reel_facture(/ /g,""); // remove separator for thouhsand
        reel_facture= reel_facture.replace(/\s/g,"");
	}
	
		
	if(is_set($("field__"+which_row+"--col__ecart_provision--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		ecart_provision = $("field__"+which_row+"--col__ecart_provision--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		ecart_provision = ecart_provision(/ /g,""); // remove separator for thouhsand
        ecart_provision= ecart_provision.replace(/\s/g,"");
	}
	
	if(is_set($("field__"+which_row+"--col__reel_engage--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		reel_engage = $("field__"+which_row+"--col__reel_engage--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		reel_engage = reel_engage(/ /g,""); // remove separator for thouhsand
        reel_engage= reel_engage.replace(/\s/g,"");
	}
	
		
	if(is_set($("field__"+which_row+"--col__ecart_a_date--po_num__"+which_po_num+"--bl_num__"+which_bl_num))){
		ecart_a_date = $("field__"+which_row+"--col__ecart_a_date--po_num__"+which_po_num+"--bl_num__"+which_bl_num).innerHTML;
		ecart_a_date = ecart_a_date(/ /g,""); // remove separator for thouhsand
        ecart_a_date= ecart_a_date.replace(/\s/g,"");
	}
	
	
	result = parseFloat(quotation_figee)+parseFloat(un_bis)+parseFloat(ecart_amende_quotation)+parseFloat(reel_amende) + parseFloat(ecart_amende_facture) + parseFloat(reel_facture) + parseFloat(ecart_provision) + parseFloat(reel_engage) + parseFloat(ecart_a_date) ;

   return result;
}

calcul_result_per_field = function(which_row){
	
	var cost = 0.0;
	var arrayRow = document.getElementsByClassName("cost_value_"+which_row);

	for (var i = 0; i < arrayRow.length; i ++) {
		cost = cost + parseFloat(arrayRow[i].innerHTML);
	}

   return cost;
}


hide_row = function (which_row){


var arrayRow = document.getElementsByClassName("cost_"+which_row);

for (var i = 0; i < arrayRow.length; i ++) {
    arrayRow[i].style.display = 'none';
}

}

hide_row_without_values = function (){

    if( calcul_result_per_field('total_customs') ==0) {
	hide_row('total_customs');
	//hide_row('customs');
	}	
	
	if( calcul_result_per_field('total_port_services') ==0) {
	hide_row('total_port_services');
	hide_row('port_services');
	}	
	
	if( calcul_result_per_field('total_other_cost') ==0) {
	hide_row('total_other_cost');
	hide_row('other_cost');
	}	
	
	if( calcul_result_per_field('total_other_fees') ==0) {
	hide_row('total_other_fees');
	hide_row('other_fees');
	}	
	
	if( calcul_result_per_field('total_exchange_rate_fees') ==0) {
	hide_row('total_exchange_rate_fees');
	hide_row('exchange_rate_fees');
	}	
	
   if( calcul_result_per_field('total_purchase_dpt_fees') ==0) {
	hide_row('total_purchase_dpt_fees');
	hide_row('purchase_dpt_fees');
	}	
	
	if( calcul_result_per_field('total_qc_dpt_fees') ==0) {
	hide_row('total_qc_dpt_fees');
	hide_row('qc_dpt_fees');
	}	
	
	if( calcul_result_per_field('total_import_dpt_fees') ==0) {
	hide_row('total_import_dpt_fees');
	hide_row('import_dpt_fees');
	}	
	
	if( calcul_result_per_field('total_import_risk_fees') ==0) {
	hide_row('total_import_risk_fees');
	hide_row('import_risk_fees');
	}	
	
	if( calcul_result_per_field('total_warehousing') ==0) {
	hide_row('total_warehousing');
	hide_row('warehousing');
	}	
	
	
	if( calcul_result_per_field('total_final_delivery') ==0) {
	hide_row('total_final_delivery');
	hide_row('final_delivery');
	}	
	
	if( calcul_result_per_field('total_coverage_spread') ==0) {
	hide_row('total_coverage_spread');
	hide_row('coverage_spread');
	}	
		
}	


getUnitCostPrice = function(my_fic_num, my_name_div) 
{
  
	if ( my_name_div == 'div_quotation_figee' || my_name_div == 'div_reel_engage' ){
		
		my_id_export_level =  '1';
		 
		if ( my_name_div == 'div_reel_engage' ) {
		my_id_export_level =  '3';
		}
		var url = 'ajax/dau_quotation_cost_price.psp';
		new Ajax.Request
		(url, {
			method: 'post',
			parameters:{fic_num : my_fic_num, id_export_level: my_id_export_level},
			onSuccess: function(transport) 
			{
			$(my_name_div).innerHTML = transport.responseText;
		 
			},
			onFailure: function() {}
			}
			
		);

	}
}



////////////////////////////
// OLD
////////////////////////////
