
function onclick_del(id_row) {
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  if (confirm("Voulez-vous vraiment supprimer cette ligne ?")) {
    params = "TICKET="+ticket;
    params += "&OFFSET="+offset;
    params += "&ACTION=DEL";
    params += "&ID_ROW="+id_row;
    params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
    params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
    params += "&FIND_PRELEVE="+find_preleve;
    params += "&FIND_MOISPRELEVE="+find_moispreleve;
    params += "&FIND_SKU="+find_sku;
	params += "&FIND_BL="+find_bl;
    params += "&FIND_UO="+find_uo;
    params += "&FIND_PO="+find_po;
    params += "&FIND_CONTACT="+find_contact;
    params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
    params += "&FIND_FACTURE="+find_facture;
    params += "&FIND_IMPORTER="+find_importer;
    params += "&FIND_DPT="+find_dpt;
    params += "&FIND_POD="+find_pod;
    document.location.href='prelevement_pays.psp?'+params;
  }
}

function onclick_add(event, id_row) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 280;
  var height = 250;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=ADD";
  params += "&ID_ROW="+id_row;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var PreleveWindow=window.open(url, 'WindowPreleve', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

auto_preleve = function( id_row, is_preleve ){
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var offset = offset;
  var is_preleve = is_preleve;
  
  params = "TICKET="+ticket;
  params += "&ACTION=IS_PRELEVE";
  params += "&ID_ROW="+id_row;
  params += "&OFFSET="+offset;
  params += "&IS_PRELEVE="+is_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;
  document.location.href='prelevement_pays.psp?'+params;
}

auto_facture = function( id_row, is_facture ){
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var offset = offset;
  var is_facture = is_facture;
  
  params = "TICKET="+ticket;
  params += "&ACTION=IS_FACTURE";
  params += "&ID_ROW="+id_row;
  params += "&OFFSET="+offset;
  params += "&IS_FACTURE="+is_facture;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;
  document.location.href='prelevement_pays.psp?'+params;
}

function onclick_preleve(event, id_row, is_preleve, date_preleve) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 280;
  var height = 250;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var is_preleve = is_preleve;
  var date_preleve = date_preleve;
  var url="ajax/saisie_prelevement_pays.psp?";

  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=PRELEVE";
  params += "&ID_ROW="+id_row;
  params += "&IS_PRELEVE="+is_preleve;
  params += "&FIND_MOIPRELEVE="+find_moispreleve;
  params += "&DATE_PRELEVE="+date_preleve;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var PreleveWindow=window.open(url, 'WindowPreleve', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=0, directories=no, resizable=no, scrollbars=no, location=no");
}

function onclick_contact(event, id_row, contact) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 250;
  var height = 100;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var contact = contact;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=CONTACT";
  params += "&ID_ROW="+id_row;
  params += "&CONTACT="+contact;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var ContactWindow=window.open(url, 'WindowContact', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

function onclick_sku_type(event, id_row, sku_type) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 250;
  var height = 50;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var sku_type = sku_type;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=SKU_TYPE";
  params += "&ID_ROW="+id_row;
  params += "&SKU_TYPE="+sku_type;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var TypeWindow=window.open(url, 'WindowType', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

function onclick_pcs_preleve(event, id_row, pcs_preleve) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 250;
  var height = 50;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var pcs_preleve = pcs_preleve;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=PCS_PRELEVE";
  params += "&ID_ROW="+id_row;
  params += "&PCS_PRELEVE="+pcs_preleve;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var PreleveWindow=window.open(url, 'WindowPreleve', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

function onclick_fob1(event, id_row, prix_fob1, currency_fob1, txd_fob1) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 300;
  var height = 50;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var prix_fob1 = prix_fob1;
  var currency_fob1 = currency_fob1;
  var txd_fob1 = txd_fob1;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=FOB1";
  params += "&ID_ROW="+id_row;
  params += "&PRIX_FOB1="+prix_fob1;
  params += "&CURRENCY_FOB1="+currency_fob1;
  params += "&TXD_FOB1="+txd_fob1;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var Fob1Window=window.open(url, 'WindowFob1', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

function onclick_fob2(event, id_row, prix_fob2, currency_fob2, txd_fob2) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 300;
  var height = 50;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var prix_fob2 = prix_fob2;
  var currency_fob2 = currency_fob2;
  var txd_fob2 = txd_fob2;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=FOB2";
  params += "&ID_ROW="+id_row;
  params += "&PRIX_FOB2="+prix_fob2;
  params += "&CURRENCY_FOB2="+currency_fob2;
  params += "&TXD_FOB2="+txd_fob2;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var Fob2Window=window.open(url, 'WindowFob2', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

function onclick_vente(event, id_row, prix_facture, currency_facture, txd_facture) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 250;
  var height = 50;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var prix_facature = prix_facture;
  var currency_facture = currency_facture;
  var txd_facture = txd_facture;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=VENTE";
  params += "&ID_ROW="+id_row;
  params += "&PRIX_FACTURE="+prix_facture;
  params += "&CURRENCY_FACTURE="+currency_facture;
  params += "&TXD_FACTURE="+txd_facture;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var VenteWindow=window.open(url, 'WindowVente', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

function onclick_facture(event, id_row, is_facture, date_facture) {
  var x = event.screenX;
  var y = event.screenY;
  var width = 280;
  var height = 250;
  y = y + 10;
  var ticket = $F('TICKET');
  var offset = $F('OFFSET');
  var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
  var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
  var find_preleve = $F('FIND_PRELEVE');
  var find_moispreleve = $F('FIND_MOISPRELEVE');
  var find_sku = $F('FIND_SKU');
  var find_bl = $F('FIND_BL');
  var find_uo = $F('FIND_UO');
  var find_po = $F('FIND_PO');
  var find_contact = $F('FIND_CONTACT');
  var find_pays_reseau = $F('FIND_PAYS_RESEAU');
  var find_facture = $F('FIND_FACTURE');
  var find_importer = $F('FIND_IMPORTER');
  var find_dpt = $F('FIND_DPT');
  var find_pod = $F('FIND_POD');
  var id_row = id_row;
  var is_facture = is_facture;
  var date_facture = date_facture;

  var url="ajax/saisie_prelevement_pays.psp?";
  params = "TICKET="+ticket;
  params += "&OFFSET="+offset;
  params += "&ACTION=FACTURE";
  params += "&ID_ROW="+id_row;
  params += "&IS_FACTURE="+is_facture;
  params += "&DATE_FACTURE="+date_facture;
  params += "&FIND_DATE_POD_APRES="+find_date_pod_apres;
  params += "&FIND_DATE_POD_AVANT="+find_date_pod_avant;
  params += "&FIND_PRELEVE="+find_preleve;
  params += "&FIND_MOISPRELEVE="+find_moispreleve;
  params += "&FIND_SKU="+find_sku;
  params += "&FIND_BL="+find_bl;
  params += "&FIND_UO="+find_uo;
  params += "&FIND_PO="+find_po;
  params += "&FIND_CONTACT="+find_contact;
  params += "&FIND_PAYS_RESEAU="+find_pays_reseau;
  params += "&FIND_FACTURE="+find_facture;
  params += "&FIND_IMPORTER="+find_importer;
  params += "&FIND_DPT="+find_dpt;
  params += "&FIND_POD="+find_pod;

  url = url+params;
  var FactureWindow=window.open(url, 'WindowFacture', "top=" + y + ", left=" + x + ", width=" + width + ", height=" + height + ", toolbar=no, menubar=no, status=no, directories=no, resizable=no, scrollbars=no, location=no");
}

function find_from_criteria(which_sens, which_offset){
        if( typeof(which_sens) == 'undefined') which_sens = '';
        if( typeof(which_offset) == 'undefined') which_offset = '';
        if( typeof(which_nomenclature) == 'undefined') which_nomenclature = '';

        var which_ticket = $F('TICKET');
        var find_date_pod_apres = $F('FIND_DATE_POD_APRES');
        var find_date_pod_avant = $F('FIND_DATE_POD_AVANT');
        var find_preleve = $F('FIND_PRELEVE');
        var find_moispreleve = $F('FIND_MOISPRELEVE');
        var find_sku = $F('FIND_SKU');
		var find_bl = $F('FIND_BL');
        var find_uo = $F('FIND_UO');
  	    var find_po = $F('FIND_PO');
     	var find_contact = $F('FIND_CONTACT');
        var find_pays_reseau = $F('FIND_PAYS_RESEAU');
        var find_facture = $F('FIND_FACTURE');
        var find_importer = $F('FIND_IMPORTER');
        var find_dpt = $F('FIND_DPT');
        var find_pod = $F('FIND_POD');
        if(which_offset != ''){
                which_offset = parseInt(which_offset);
        }
	if (which_sens == "suivant") {
	        which_offset += 20;
	}
	if (which_sens == "precedent") {
	        which_offset -= 20;
        }
        url="?TICKET="+which_ticket+"&OFFSET="+which_offset+"&FIND_DATE_POD_AVANT="+find_date_pod_avant+"&FIND_DATE_POD_APRES="+find_date_pod_apres+"&FIND_PRELEVE="+find_preleve+"&FIND_MOISPRELEVE="+find_moispreleve+"&FIND_SKU="+find_sku+"&FIND_BL="+find_bl+"&FIND_UO="+find_uo+"&FIND_PO="+find_po+"&FIND_CONTACT="+find_contact+"&FIND_IMPORTER="+find_importer+"&FIND_DPT="+find_dpt+"&FIND_POD="+find_pod+"&FIND_PAYS_RESEAU="+find_pays_reseau+"&FIND_FACTURE="+find_facture;
		alert(url);
        document.location.href=url;
}

function find_from_filtre( find_date_pod_apres, find_date_pod_avant ){
        var which_ticket = $F('TICKET');
        var which_offset = $F('OFFSET');
	var which_sku =	$F('findsku');
	var which_bl =	$F('findbl');
	var which_uo =	$F('finduo');
	var which_po =	$F('findpo');
        var which_date_pod_apres = find_date_pod_apres.value;
        var which_date_pod_avant = find_date_pod_avant.value;
	var preleve = document.getElementById("selectpreleve");
	for(var i=0,l=preleve.length;i<l;i++) {
          if (preleve[i].selected ) {
	    which_preleve = preleve[i].value;
          }
	}
	var moispreleve = document.getElementById("selectmoispreleve");
	for(var i=0,l=moispreleve.length;i<l;i++) {
          if (moispreleve[i].selected ) {
	    which_moispreleve = moispreleve[i].value;
          }
	}
	var pays = document.getElementById("selectpays");
	for(var i=0,l=pays.length;i<l;i++) {
          if (pays[i].selected ) {
	    which_pays = pays[i].value;
          }
	}
	var facture = document.getElementById("selectfacture");
	for(var i=0,l=facture.length;i<l;i++) {
          if (facture[i].selected ) {
	    which_facture = facture[i].value;
          }
	}
	var importer = document.getElementById("selectimporter");
	for(var i=0,l=importer.length;i<l;i++) {
          if (importer[i].selected ) {
	    which_importer = importer[i].value;
          }
	}
	var contact = document.getElementById("selectcontact");
	for(var i=0,l=contact.length;i<l;i++) {
          if (contact[i].selected ) {
	    which_contact = contact[i].value;
          }
	}
	var dpt = document.getElementById("selectdpt");
	for(var i=0,l=dpt.length;i<l;i++) {
          if (dpt[i].selected ) {
	    which_dpt = dpt[i].value;
          }
	}
	var pod = document.getElementById("selectpod");
	for(var i=0,l=pod.length;i<l;i++) {
          if (pod[i].selected ) {
	    which_pod = pod[i].value;
          }
	}

        url="?TICKET="+which_ticket+"&OFFSET="+which_offset+"&FIND_DATE_POD_APRES="+which_date_pod_apres+"&FIND_DATE_POD_AVANT="+which_date_pod_avant+"&FIND_PRELEVE="+which_preleve+"&FIND_MOISPRELEVE="+which_moispreleve+"&FIND_SKU="+which_sku+"&FIND_BL="+which_bl+"&FIND_UO="+which_uo+"&FIND_PO="+which_po+"&FIND_CONTACT="+which_contact+"&FIND_IMPORTER="+which_importer+"&FIND_DPT="+which_dpt+"&FIND_POD="+which_pod+"&FIND_PAYS_RESEAU="+which_pays+"&FIND_FACTURE="+which_facture;
        document.location.href=url;
}

function To_Excell( find_date_pod_apres, find_date_pod_avant ){
        var which_ticket = $F('TICKET');
	var which_sku =	document.getElementById('findsku').value;
	var which_bl =  document.getElementById('findbl').value;
	var which_uo =	document.getElementById('finduo').value;
	var which_po =	document.getElementById('findpo').value;
	console.log(which_po);
        var find_date_pod_apres = find_date_pod_apres.value; 
        var find_date_pod_avant = find_date_pod_avant.value; 
	var preleve = document.getElementById("selectpreleve");
	for(var i=0,l=preleve.length;i<l;i++) {
          if (preleve[i].selected ) {
	    which_preleve = preleve[i].value;
          }
	}
	var moispreleve = document.getElementById("selectmoispreleve");
	for(var i=0,l=moispreleve.length;i<l;i++) {
          if (moispreleve[i].selected ) {
	    which_moispreleve = moispreleve[i].value;
          }
	}
	var pays = document.getElementById("selectpays");
	for(var i=0,l=pays.length;i<l;i++) {
          if (pays[i].selected ) {
	    which_pays = pays[i].value;
          }
	}
	var facture = document.getElementById("selectfacture");
	for(var i=0,l=facture.length;i<l;i++) {
          if (facture[i].selected ) {
	    which_facture = facture[i].value;
          }
	}
	var importer = document.getElementById("selectimporter");
	for(var i=0,l=importer.length;i<l;i++) {
          if (importer[i].selected ) {
	    which_importer = importer[i].value;
          }
	}
	var contact = document.getElementById("selectcontact");
	for(var i=0,l=contact.length;i<l;i++) {
          if (contact[i].selected ) {
	    which_contact = contact[i].value;
          }
	}
	var dpt = document.getElementById("selectdpt");
	for(var i=0,l=dpt.length;i<l;i++) {
          if (dpt[i].selected ) {
	    which_dpt = dpt[i].value;
          }
	}
	var pod = document.getElementById("selectpod");
	for(var i=0,l=pod.length;i<l;i++) {
          if (pod[i].selected ) {
	    which_pod = pod[i].value;
          }
	}

        url="?";
		url+="TICKET="+which_ticket;
		url+="&TO_EXCELL=1";
		url+="&FIND_DATE_POD_APRES="+find_date_pod_apres;
		url+="&FIND_DATE_POD_AVANT="+find_date_pod_avant;
		url+="&FIND_PRELEVE="+which_preleve;
		url+="&FIND_MOISPRELEVE="+which_moispreleve;
		url+="&FIND_SKU="+which_sku;
		url+="&FIND_BL="+which_bl;
		url+="&FIND_UO="+which_uo;
		url+="&FIND_PO="+which_po;
		url+="&FIND_IMPORTER="+which_importer;
		url+="&FIND_DPT="+which_dpt;
		url+="&FIND_POD="+which_pod;
		url+="&FIND_PAYS_RESEAU="+which_pays;
		url+="&FIND_FACTURE="+which_facture;
		document.location.href=url;
	
}
