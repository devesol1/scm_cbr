/*======================================================================
# Nom : veriform.js                                             
# Objet : v�rification en javascript du remplissage des formulaires 
# Auteur : Cedric Delamarre E-SOLUTIONS                                    
# Date cr�ation : 30/08/2004                                       
#
# Historique :
# 
#======================================================================
*/

forbidden = "&~#{[<>|`]}'()=+�?;/:�!%*�,$��@0123456789";

//***********************************************************
//** Michel Huet le 21 Octobre 2004                        **
//** Teste si la date d'arriv�e en douane doit �tre saisie **
//***********************************************************
function isNotEmptyCustomDate( elementcustom , elementdate, elementhr, elementmn, elementLabel ){
	
	tmpCustom = elementcustom.value;
	tmpCustom = tmpCustom.replace(/^\s+/, "");
	tmpCustom = tmpCustom.replace(/\s+$/, "");

	if ( tmpCustom == '' ) { return true; }

	if ( elementdate.value == '' ) { 
		alert("Field '" + elementLabel + "' need to be filled to continue !");
		elementdate.focus();
		return false; 
	}
	if ( elementhr.value == '' ) { 
		alert("Field '" + elementLabel + "' need to be filled to continue !");
		elementhr.focus();
		return false; 
	}
	if ( elementmn.value == '' ) { 
		alert("Field '" + elementLabel + "' need to be filled to continue !");
		elementmn.focus();
		return false; 
	}

	return true;

}//fin de la function

//*******************************************
//** test if field contain characters **
//*******************************************
function isNotEmptyField( elementToVerify , elementLabel ){
	
	tmpString = elementToVerify.value;
	tmpString = tmpString.replace(/^\s+/, "");
	tmpString = tmpString.replace(/\s+$/, "");
	if (tmpString == '') {
		if (elementLabel != null) {
			alert("Field '" + elementLabel + "' need to be filled to continue !");
		}
		elementToVerify.focus();
		return false;
	}//fin du if

	return true;

}//fin de la function


//*****************************************************************
//** test if field contains only numbers **
//*****************************************************************

function isValidNumberField( elementToVerify , elementLabel){
	
	tmpString = elementToVerify.value;

	if (!isNotEmptyField( elementToVerify , elementLabel )){
		return false
	}
	else if (!isValidNumber( tmpString )){
		alert ("Field '" + elementLabel + "' need to contain only number to continue !");
		elementToVerify.focus();
		return false;
	}
	
	return true;

}

function isValidNumber( tmpString ){
	
	for (i = 0 ; i <tmpString.length ; i++) {
		if ( (tmpString.charAt(i) != "0" ) && (tmpString.charAt(i) != "1" ) && (tmpString.charAt(i) != "2" ) && (tmpString.charAt(i) != "3" ) && (tmpString.charAt(i) != "4" ) && (tmpString.charAt(i) != "5" ) && (tmpString.charAt(i) != "6" ) && (tmpString.charAt(i) != "7" ) && (tmpString.charAt(i) != "8" ) && (tmpString.charAt(i) != "9" ) ) {
			return false;
		}// fin du if
	}//fin du for i
	return true;
}

//******************************
//** test if combo has been changed **
//******************************

function isChangeCombo( elementToVerify , elementLabel ){
	
	currentValue = getSelectValue( elementToVerify );
	if ( currentValue == "none" || currentValue == "default" || currentValue == "0" || currentValue == "" ) {
		alert ("You need to select something in list '" + elementLabel + "' to continue");
		return false;
	}

	return true;

}// fin de la function test_sex

function getSelectValue( elementToVerify ){
	ValueToReturn = elementToVerify.options[elementToVerify.selectedIndex].value;

return ValueToReturn;
}

//*******************************************
//** Auteur : Michel Huet E-SOLUTIONS                                    
//** test si date 1 >= date 2
//** 7 arguments : date1, hh1, mm1, date2, hh2, mm2, commentaire
//*******************************************
function isDat1SupEgalDat2( date1 , hh1, mm1, date2, hh2, mm2, message ) {
	
	// Si seconde date non renseignee, OK
	if ( date2 == '' ) { return true; }

	// si les 2 dates non renseign�es, OK
	if ( date1 == '' && date2 == '' ) { return true; }

	// si premi�re date non renseign�e et seconde date renseignee, NOK 
	if ( date1 == '' && date2 != '' ) { 
		alert( message );
		return false;
	}

	var tabdate1 = date1.split("/");
	var jour_date1 = tabdate1[0];
	var mois_date1 = tabdate1[1];
	var annee_date1 = tabdate1[2];
	var tabdate2 = date2.split("/");
	var jour_date2 = tabdate2[0];
	var mois_date2 = tabdate2[1];
	var annee_date2 = tabdate2[2];
	var dt1 = annee_date1 + mois_date1 + jour_date1;
	var dt2 = annee_date2 + mois_date2 + jour_date2;

		// les 2 dates sont renseign�es, si date1 < date2, NOK
	if ( dt1 < dt2 ) {
		alert( message  );
		return false;
	}
	// les 2 dates sont renseign�es, si date1 = date2
	if ( dt1 == dt2 ) {
		// les 2 dates sont �gales, si heure1 < heure2
		if ( hh1 < hh2 ) { 
			alert( message  );
			return false; 
		}
		// les 2 dates sont �gales, si heure1 = heure2
		if ( hh1 == hh2 ) {
			// dates et heures sont �gales, si mm1 >= mm2
			if ( mm1 > mm2 ) { return true; }
			// dates et heures sont �gales, si mm1 < mm2
			else {
				alert( message  );
				return false; 
			}
		}
		// les 2 dates sont �gales, si heure1 > heure2
		else {
		  return true;
		}
	}
	// les 2 dates sont renseign�es, si date1 > date2, OK
	else {
		return true;
	}

}


//***************************************
//** test if field contains valid mail **
//***************************************

function isValidMail( elementToVerify , elementLabel ){

	tmpString = elementToVerify.value;
	
	cptpoint = 0;
	cptat = 0;
	
	for (i=0; i<tmpString.length; i++) {

		for (j=0; j<(forbidden.length-11); j++) {

			if (tmpString.charAt(i) == forbidden.charAt(j)) {

				alert ("email in '" + elementLabel + "' is incorrect !");
				elementToVerify.focus();
				return false;
				
			}//fin du if

		}// findu for j

	}// fin du for i

	for (i=0; i<(tmpString.length+1); i++) {

		if (tmpString.charAt(i) == ".") {

			cptpoint++;

			p=i;
			
		}// fin du if

		if (tmpString.charAt(i) == "@") {

			cptat++;

			a=i;
			
		}// fin du if

	}// fin du for

	if ((cptpoint == 0)||(cptat != 1)) {
		
		alert ("email in '" + elementLabel + "' hasn't all specials chars as @ or . !");
		elementToVerify.focus();
		return false;
		
	}// fin du if

	if (p<(a+3)) {

		alert ("name server of email in '" + elementLabel + "' is incorrect !");
		elementToVerify.focus();
		return false;
		
	}// fin du if

	if (p>=(tmpString.length-2)) {

		alert ("domain name of email in '" + elementLabel + "' is incorrect !");
		elementToVerify.focus();
		return false;
		
	}// fin du if

	return true;

}// fin de la function test_mail



