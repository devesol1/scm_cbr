//////////////////////////
// Fonctions generiques
//////////////////////////

start_loading = function (which_div){

	$('main_loading_div').innerHTML = "";
	body_node = $('main_loading_div');
	
	new_loading_div=document.createElement("div");

	id_loading_div = "loading_"+which_div;
	new_loading_div.setAttribute("id", id_loading_div);
	body_node.appendChild(new_loading_div);
	$(id_loading_div).innerHTML += "<br><br><img src=ajax/img/ajax-loader.gif><br>LOADING...<br><br>";
	
	$(id_loading_div ).style.width = '400';
	$(id_loading_div ).style.textAlign = 'center';
	$(id_loading_div ).style.fontFamily = 'Arial';
	$(id_loading_div ).style.fontSize = '20px';
	$(id_loading_div ).style.fontWeight = 'bold';
	$(id_loading_div ).style.position = 'absolute';
	$(id_loading_div ).style.backgroundColor = '#FFFFFF';
	$(id_loading_div ).style.top = '100';
	$(id_loading_div ).style.left = getWidthCenterOfPage()-200;
	new Effect.Appear(id_loading_div);
}

fill_div = function ( which_div, url, params){

// Cette fonction permet d'afficher le contenu du r�sultat d'une requete Ajax dans le div pass� en param�tre	
//	prompt('',"http://"+document.location.host+"/cbryrocher/"+url+"?"+params);
	//	$(which_div).innerHTML = '';
	var myAjax = new Ajax.Updater( 
	which_div, 
	url, 
	{	
		method: 'get', 
		parameters: params, 
		evalScripts: true, 
		asynchronous:true,
		onCreate: start_loading(which_div),
		onComplete: function(){
						new Effect.Fade("loading_"+which_div);
					}
	}
	); 
}

add_invoice = function(which_ticket, which_invoice_id){

	which_div= "new_invoice";
	params = "TICKET="+which_ticket;
	if(is_set(which_invoice_id)){
		params += "&invoice_id="+which_invoice_id;
	}

	fill_div(which_div, "./ajax/dau_invoice.psp", params);
}

add_cost_section = function( which_shpt_pos, which_cost_section){

	div_list_cost_section_pos = "div_shpt"+which_shpt_pos+"_list_cost_section" ; 
	list_cost_section=$(div_list_cost_section_pos);
	cost_section_pos = get_pos(div_list_cost_section_pos);
	new_cost_section=document.createElement("div");
	node_cost_section_pos = "div_shpt"+which_shpt_pos+"_cost_section"+cost_section_pos;
	new_cost_section.setAttribute("id", node_cost_section_pos);
	new_cost_section.setAttribute("pos", cost_section_pos);
	list_cost_section.appendChild(new_cost_section);

	var params="shpt_pos="+which_shpt_pos;
	params+="&cost_section_pos="+cost_section_pos;
	
	div_shpt_node = $("div_shpt"+which_shpt_pos);
	id_shpt = get_id_shpt(div_shpt_node);
	params += "&id_shpt="+id_shpt;
	params += "&currency="+get_currency();

	params += "&forbidden="+get_already_used_cost_section(div_shpt_node);
	if(is_set(which_cost_section)){
		params += "&cost_section="+which_cost_section;
	}

	fill_div(node_cost_section_pos, "./ajax/dau_invoice_shpt_cost_section.psp", params);

}

get_id_shpt = function( which_node ){
	var id_shpt = "";
	select_table = which_node.getElementsByTagName('select');
	
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_bl") {
			if(select_table[i].value.length > 0){
				id_shpt = select_table[i].value;
			}
		}
	}
	// on enl�ve la derni�re virgule
	return id_shpt;

}

add_shpt = function(which_id_shpt){

	list_shpts = $("div_list_shpts") ; 
	shpt_pos = get_pos('div_list_shpts');
	new_shpt=document.createElement("div");
	node_shpt_pos = "div_shpt"+shpt_pos;
	new_shpt.setAttribute("id", node_shpt_pos);
	new_shpt.setAttribute("pos", shpt_pos);
	list_shpts.appendChild(new_shpt);

	var params = "shpt_pos="+shpt_pos;
	
	forbidden = get_already_used_bl();
	params += "&forbidden="+forbidden;
	params += "&currency="+get_currency();
	if(is_set(which_id_shpt)){
		params += "&id_shpt="+which_id_shpt;
	}
	
	fill_div(node_shpt_pos, "./ajax/dau_invoice_shpt.psp", params);

}

get_already_used_cost_section = function(which_node){
	var already_used = "";
	select_table = which_node.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_cost_sections") {
			if(select_table[i].value.length > 0){
				already_used += "'"+select_table[i].value+"',";
			}
		}
	}
	// on enl�ve la derni�re virgule
	already_used = already_used.substring(0,already_used.length-1);
	return already_used;

}

get_already_used_bl = function(){
	var already_used = "";
	select_table = document.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_bl") {
			if(select_table[i].value.length > 0){
				already_used += select_table[i].value+",";
			}
		}
	}
	// on enl�ve la derni�re virgule
	already_used = already_used.substring(0,already_used.length-1);
	return already_used;

}

get_pos = function( which_parent_node ) {
	my_pos = 0;
	if ($(which_parent_node).childNodes.length>0) {
		for (i=0; i<$(which_parent_node).childNodes.length;i++) {
			if ($(which_parent_node).childNodes[i].nodeName == "DIV") {
				cur_pos = parseInt($(which_parent_node).childNodes[i].getAttribute("pos"));
				if(cur_pos>=my_pos){
					my_pos = cur_pos+1
				}
			}
		}
	}
	return my_pos;
}

save_invoice_form = function(){
	$('form_invoice').submit();
}


update_bl_amount = function(which_bl_select){

	var which_shpt_pos = "";
	rest_of_total_amount = get_rest_of_total_amount();

	tr_node = get_parent("tr", which_bl_select); 
	
	input_table = tr_node.getElementsByTagName('input');

	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "bl_amount") {
			if(input_table[i].value.length > 0){
				rest_of_total_amount += parseFloat(input_table[i].value);
			}
			if(rest_of_total_amount<0 || which_bl_select.value.length <= 0){
				rest_of_total_amount = 0;
			}
			input_table[i].value=rest_of_total_amount;
		}
		if (input_table[i].name == "shpt_pos") {
			which_shpt_pos = input_table[i].value;
		}
	}
	
	if( which_bl_select.value.length > 0 ){
		add_cost_section(which_shpt_pos);	
	} else {
		$("div_shpt"+which_shpt_pos+"_list_cost_section").innerHTML = "";
	}
	
}


get_rest_of_total_amount = function(){
	input_table = document.getElementsByTagName('input');
	total_amount = 0;
	total_bl_amount = 0;
	
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "total_amount") {
			if(input_table[i].value.length == 0){
				input_table[i].value = 0;
			} else {
				total_amount += parseFloat(input_table[i].value);
			}
//			alert("total_amount:"+total_amount);
		}
		if (input_table[i].name == "bl_amount") {
			if(input_table[i].value.length == 0){
				input_table[i].value = 0;
			} else {
				total_bl_amount += parseFloat(input_table[i].value);
			}
//			alert("total_bl_amount:"+total_bl_amount);
		}
	}

	return (total_amount-total_bl_amount);

}


update_cost_section_amount = function(which_cost_section_select){

	rest_of_bl_amount = get_rest_of_bl_amount(which_cost_section_select);

	tr_node = get_parent("tr", which_cost_section_select); 
	
	input_table = tr_node.getElementsByTagName('input');

	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "cost_section_amount") {
			if(input_table[i].value.length > 0){
				rest_of_bl_amount += parseFloat(input_table[i].value);
			}
			if(rest_of_bl_amount<0){
				rest_of_bl_amount = 0;
			}
			input_table[i].value=rest_of_bl_amount;
			return false;
		}
	}

}


get_rest_of_bl_amount = function(which_node){

	which_node = get_parent("div", which_node); 
	my_shpt_node = $(which_node.id.replace("_list_cost_section", ""));
	
	input_table = my_shpt_node.getElementsByTagName('input');
	total_bl_amount = 0;
	total_cost_section_amount = 0;
	
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "bl_amount") {
			if(input_table[i].value.length == 0){
				input_table[i].value = 0;
			} else {
				total_bl_amount += parseFloat(input_table[i].value);
			}
//			alert("total_bl_amount:"+total_bl_amount);
		}
		if (input_table[i].name == "cost_section_amount") {
			if(input_table[i].value.length == 0){
				input_table[i].value = 0;
			} else {
				total_cost_section_amount += parseFloat(input_table[i].value);
			}
//			alert("total_cost_section_amount:"+total_cost_section_amount);
		}
	}

	return (total_bl_amount-total_cost_section_amount);

}


get_parent = function(which_parent_node_name, which_node){
//	alert( which_node.getAttribute("id") );
	if(which_node.nodeName.toLowerCase() == which_parent_node_name){
		return which_node.parentNode;
	} else {
		return get_parent(which_parent_node_name, which_node.parentNode);	
	}
	
}


get_cost_section_waited = function( which_shpt_pos, which_cost_section_pos ){

	var id_shpt = "";
	var cost_section = "";
	
	which_node = $("div_shpt"+which_shpt_pos);

	select_table = which_node.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_bl") {
			if(select_table[i].value.length > 0){
				id_shpt = select_table[i].value;
			}
		}
	}

	which_node = $("div_shpt"+which_shpt_pos+"_cost_section"+which_cost_section_pos);

//	alert(which_node.id);
	
	select_table = which_node.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_cost_sections") {
			if(select_table[i].value.length > 0){
				cost_section = select_table[i].value;
			}
		}
	}

	var cost_section_area = "shpt"+which_shpt_pos+"_cost_section"+which_cost_section_pos
	var span_cost_section_waited = cost_section_area+"_waited";
	var input_cost_section_amount = "input_"+cost_section_area+"_amount";

	var cost_section_amount = $F(input_cost_section_amount);
	
	var params = "id_shpt="+id_shpt;
	params += "&cost_section_amount="+cost_section_amount;
	params += "&cost_section="+cost_section;
	
	$(span_cost_section_waited).innerHTML = "";
	dbug("./ajax/dau_invoice_get_cost_section_waited_and_settles.psp?"+params);
	fill_div(span_cost_section_waited, "./ajax/dau_invoice_get_cost_section_waited_and_settles.psp", params);
	work_cost_section_fields( which_shpt_pos, which_cost_section_pos );
}

	

work_cost_section_fields = function( which_shpt_pos, which_cost_section_pos ){

	var cost_section_area = "shpt"+which_shpt_pos+"_cost_section"+which_cost_section_pos

	span_cost_section_waited_and_settles = cost_section_area+"_waited";
	span_settle_amount = cost_section_area+"_settle"
	lnk_span_settle_amount = "lnk_"+span_settle_amount;
	
	input_cost_section_amount = "input_"+cost_section_area+"_amount";
	input_cost_section_waited = "input_"+cost_section_area+"_waited";
	input_cost_section_settles = "input_"+cost_section_area+"_settle";
	input_cost_section_is_ok_to_save = "input_"+cost_section_area+"_is_ok_to_save";
	div_cost_section_id = "div_"+cost_section_area
	
	while( $(span_cost_section_waited_and_settles).innerHTML.length <= 0 ){
		function_to_set_timeout = "work_cost_section_fields('"+which_shpt_pos+"','"+which_cost_section_pos+"')";
		setTimeout(function_to_set_timeout,1000);
		return false;
	}

	tab_cost_section_waited_and_settles = $(span_cost_section_waited_and_settles).innerHTML.split('|');
	$(input_cost_section_waited).value=tab_cost_section_waited_and_settles[0];
	$(input_cost_section_settles).value=tab_cost_section_waited_and_settles[1];

	var cost_section_amount = parseFloat($F(input_cost_section_amount));
	var cost_section_waited = parseFloat($F(input_cost_section_waited));

	var settle_amount = cost_section_waited-cost_section_amount;
	
	$(span_settle_amount).innerHTML = settle_amount+" "+get_currency();
	
	
	$(span_settle_amount).style.fontWeight="bold";
	
	var settle_amount_color = "#AA0000";
	var is_ok_to_save = 0;
	if(settle_amount>0 || $(input_cost_section_settles).value> 0){
		settle_amount_color="#00AA00";
		is_ok_to_save="1";
	}
	
	$(input_cost_section_is_ok_to_save).value=is_ok_to_save;
	$(span_settle_amount).style.color=settle_amount_color;
	$(span_settle_amount).style.fontWeight="bold";
	
}


open_settles = function( which_shpt_pos, which_cost_section_pos ){


	var id_shpt = "";
	var cost_section = "";
	var cost_section_amount=0;
	var waited_cost_section_amount=0;
	var settle_amount=0;

	which_node = $("div_shpt"+which_shpt_pos);

	select_table = which_node.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_bl") {
			if(select_table[i].value.length > 0){
				id_shpt = select_table[i].value;
			}
		}
	}

	which_node = $("div_shpt"+which_shpt_pos+"_cost_section"+which_cost_section_pos);

	select_table = which_node.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_cost_sections") {
			if(select_table[i].value.length > 0){
				cost_section = select_table[i].value;
			}
		}
	}

	input_table = which_node.getElementsByTagName('input');
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "cost_section_amount") {
			if(input_table[i].value.length > 0){
				cost_section_amount = input_table[i].value;
			}
		}
		if (input_table[i].name == "waited") {
			if(input_table[i].value.length > 0){
				waited_cost_section_amount = input_table[i].value;
			}
		}
	}

	input_waited_cost_section = "input_shpt"+which_shpt_pos+"_cost_section"+which_cost_section_pos+"_waited";
	
	settle_amount = -1*(parseInt(waited_cost_section_amount )-Math.round( parseInt(cost_section_amount)));
	
	var url = "dau_invoices_settles.psp";
	var params = "id_shpt="+id_shpt;
	params += "&cost_section="+cost_section;
	params += "&settle_amount="+settle_amount;
	params += "&currency="+get_currency();
	params += "&exchange_rate="+$F('exchange_rate');
	
	url += "?"+params;

	dbug(settle_amount);
	if(settle_amount<0){
		return false;
	}
	openWindow(url , "invoice_settles" , 400 , 400 , 0 , 0 , 1 , 100 , getWidthCenterOfPage()-200);
}

change_currency = function(which_select){
	which_currency = which_select.options[which_select.selectedIndex].text;
	if(which_currency.toLowerCase() != 'euro'){
		Element.show("td_exchange_rate");
	}else{
		Element.hide("td_exchange_rate");
	}
	tab_currency = document.getElementsByTagName("currency");
	for(i=0; i<tab_currency.length; i++){	
		tab_currency[i].innerHTML = which_currency;
	}
	
	
}

get_currency = function(){
	return $('select_currency').options[$('select_currency').selectedIndex].text;
}

remove_shpt_node = function (which_shpt_pos){
	if( confirm( 'any remove is definitive!' )){
		var parent_node = $("div_list_shpts");
		var child_node = $("div_shpt"+which_shpt_pos);
		parent_node.removeChild(child_node);
	}
}

remove_cost_section_node = function (which_shpt_pos, which_cost_section_pos){
	if( confirm( 'any remove is definitive!' )){
		var parent_node = $("div_shpt"+which_shpt_pos+"_list_cost_section");
		var child_node = $("div_shpt"+which_shpt_pos+"_cost_section"+which_cost_section_pos);
		parent_node.removeChild(child_node);
	}
}

is_waiting_invoice_checked = function(which_shpt_pos){

	var input_table = document.getElementsByTagName('input');
	var reg_exp=new RegExp("shpt_"+which_shpt_pos+"_is_waiting_invoice", "g");
	is_checked= false;
	for(i=0; i<input_table.length; i++){
		if ( input_table[i].id.match(reg_exp) && input_table[i].checked) {
			is_checked = (input_table[i].value=="1");
		}
	}
	return is_checked;

}

is_waiting_more_invoice = function(which_shpt_pos){
	
	is_checked = is_waiting_invoice_checked(which_shpt_pos);
	which_node = $("div_shpt"+which_shpt_pos);

	td_table = which_node.getElementsByTagName('td');
	for(i=0; i<td_table.length; i++){
//		if (td_table[i].id.match(/td_settle_amount/g) || td_table[i].id.match(/td_get_settle_amount/g) ) {
		if (td_table[i].id.match(/.*settle_amount/g) ) {
			if(is_checked){
				Element.hide(td_table[i].id);
			} else {
				Element.show(td_table[i].id);
			}
		}
	}
	span_table = which_node.getElementsByTagName('span');
	for(i=0; i<span_table.length; i++){
		if (span_table[i].id.match(/.+settle/g) ) {
			if(is_checked){
				span_table[i].innerHTML = "";
			}
		}
	}

	input_table = which_node.getElementsByTagName('input');
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "is_ok_to_save" ) {
			if(is_checked){
				input_table[i].value = "0";
			}
		}
	}
	
}


dbug = function(text){
	$('dbug_textarea').innerHTML += text+"\n";
}

is_loading = function(){
	value_to_return = true;
	which_node = $('main_loading_div');
	div_table = which_node.getElementsByTagName('div');
	for(i=0; i<div_table.length; i++){
		if(value_to_return == true && div_table[i].style.display == 'none'){
			value_to_return = false;
		}
	}
	return value_to_return;
}



get_total_shpt_amount = function(){
	var shpt_amount = 0;
	var input_table = document.getElementsByTagName('input');
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "bl_amount" ) {
			shpt_amount+= parseFloat(input_table[i].value);
		}
	}
	return shpt_amount;
}

get_total_cost_section_amount = function(){
	var cost_section_amount = 0;
	var input_table = document.getElementsByTagName('input');
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "cost_section_amount" ) {
			cost_section_amount+= parseFloat(input_table[i].value);
		}
	}
	return cost_section_amount;
}

get_is_bl_ok_to_save = function(){
	var is_bl_ok_to_save = true;
	var input_table = document.getElementsByTagName('input');
	var reg_exp=new RegExp("shpt_([0-9]+)_is_waiting_invoice", "g");
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "is_waiting_invoice" && !input_table[i].checked && is_bl_ok_to_save) {
			var shpt_pos = input_table[i].id.replace(reg_exp, "$1");
			is_bl_ok_to_save = get_is_cost_section_ok_to_save(shpt_pos);
		}
	}
	return is_bl_ok_to_save;

}

get_is_cost_section_ok_to_save = function( which_shpt_pos ){
	var is_cost_section_ok_to_save = true;
	var which_node = $("div_shpt"+which_shpt_pos);

	var input_table = which_node.getElementsByTagName('input');
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "is_ok_to_save" ){
			if ( input_table[i].value != "1" && is_cost_section_ok_to_save ) {
				is_cost_section_ok_to_save = false;
			}
		}
	}
	
	return is_cost_section_ok_to_save;
}

//.name == "cost_section_amount"
save_invoice_form = function(){
	var total_amount = $F('total_amount');
	var total_shpt_amount = get_total_shpt_amount();
	var total_cost_section_amount = get_total_cost_section_amount();
	var is_continue=true;

	if(total_cost_section_amount!=total_amount && is_continue ){
		alert("La somme des couts par famille ne correspond pas au montant total !")
		is_continue = false;
	}
	if(total_cost_section_amount!=total_shpt_amount && is_continue ){
		alert("La somme des couts par shipment ne correspond pas au montant total !")
		is_continue = false;
	}
	if(!get_is_bl_ok_to_save() && is_continue ){
		alert("Des surcouts restent � justifier !")
		is_continue = false;
	}
	if(is_continue){
		document.form_invoice.submit();
	}
}
