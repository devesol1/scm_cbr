
getLogin = function(){
	var whichLogin=$('utilisateur').value;
	return whichLogin;
}
handleLogin = function(){

	var whichLogin=getLogin();	
	var url = 'reauthentification.psp';
	new Ajax.Request(url, {
		method: 'post',
		parameters:{username : whichLogin},
		onSuccess: function() {},
		onFailure: function() {}
	});
}

checkPswd = function(password)
{
    var minLength = 8;
	var password = $('verfiPswd');
	var validation = $('confirmer');
	var myRegex = new RegExp("^(?=.*[a-z]+)(?=.*[A-Z]+)(?=.*[0-9]+)([^\\s\'\"\<\>]{8,})$", "g");
	
	if(password.value.match(myRegex)){
		
		password.style.backgroundColor = "white";		
		validation.style.opacity=1;
		return true;
	} else {
		password.style.backgroundColor = "grey";
		var msg='Votre nouveau mot de passe doit comporter au moins 8 caract&egrave;res, un chiffre, une minuscule et une majuscule<br><br>La fen&ecirc;tre de saisie reste gris&eacute;e tant que votre nouveau mot de passe ne satisfait ces conditions <br><br>';
		var divMsg = document.getElementById('msgpswd');
		divMsg .innerHTML = msg;		
		validation.style.opacity=0.2;
		return false;
	}
}


 resetForm = function() {
	document.forms[0].reset();
	document.forms[0].elements[0].focus();
}

 checkForm = function() {
	var f = document.forms[0];
    var minLength = 8; // Taille minimale du mot de passe
	var maxLength = 21; // Taille maximale du mot de passe 
	var valueToReturn =true;
	
	f.username.value == getLogin();
	
	if (f.username.value == '') {
		alert('Vous devez taper votre nom utilisateur.');
		return false;
	}
	if (f.password.value == '') {
			  alert('Vous devez taper votre mot de passe.');
		return false;
	}
	
	if (f.password.value.length < minLength) {
			  alert('Votre mot de passe doit comporter au moins 8 caracteres.');
		return false;
	}
	
	if (f.password.value.length > maxLength) {
			  alert('Votre mot de passe ne peut comporter plus de 20 caracteres.');
		return false;
	}

		if (!checkPswd(f.password.value)){
			  alert('Votre mot de passe doit comporter au moins une lettre majuscule, une lettre minuscule et un chiffre et doit contenir au moins 8 caracteres');
			  resetForm();
		return false;
	}

	if (f.cf_password.value == '') {
			  alert('Vous devez confirmer votre mot de passe.');
		return false;
	}

	/*if (f.cf_password.value != f.password.value) {
			  alert('Les mots de passe saisis ne sont pas identiques');
			  resetForm();
		return false;
	}*/
	
	if (!checkFormOldPaswd()) {  
		return false;
	}
	
	//handleLogin();
	return valueToReturn;
}

 setFirstElementActive = function() 
 {
	 if (document.forms[0] != null) { if (document.forms[0].elements[0] != null) document.forms[0].elements[0].focus(); }
 }

checkEmail = function (email)
{ 
var valueToReturn =false;
//var reg = new RegExp ( "^\\w[\\w+\.\-]*@[\\w\-]+\.\\w[\\w+\.\-]*\\w$", "gi" ) ;
var reg = new RegExp("^[^@]+@[^@]+\.[^@]+$$", "gi");
var validation = document.getElementById('valid_mail');
validation.disabled = false;

if ( email.search( reg ) == -1 )
   { 
   validation.style.opacity=1;
   valueToReturn= false;
   //validation.disabled = true;
  }
else
  {
   validation.style.opacity=1;
   validation.disabled = false;
   valueToReturn= true;
   }
}

checkFormMail = function() {

	var valueToReturn =false;
    var f_email = $('verifemail').value;
	if (f_email == '') {
			  alert('Vous devez saisir votre courriel.');
		return false;
	}		
	else
    {	
    var url = 'ajax/check_email.psp';
	new Ajax.Request
	(url, {
		method: 'post',
		parameters:{email : f_email},
		onSuccess: function(transport) 
		{
		$('msg').innerHTML = transport.responseText;
		if(transport.responseText == '')
		  {
           document.forms["form_authen"].submit();
		  } 
		},
		onFailure: function() {}
	    }
	);
	}	
	return valueToReturn;
}
checkFormOldPaswd = function() 
{

    var password = $('verfiPswd').value;

	var conf_password= $('verfiCFPswd').value;

	var login     = $('utilisateur').value;
	var valueToReturn=false;
    var url = 'ajax/check_old_passwd.psp';
	new Ajax.Request
	(url, {
		method: 'post',
		parameters:{new_pswd : password, cf_password: conf_password, user: login},
		onSuccess: function(transport) 
		{
		$('msgpswd').innerHTML = transport.responseText;
		if(transport.responseText == '')
		  {
           document.forms["form_authen"].submit();
		  } 	 
		},
		onFailure: function() {}
	    });
	//alert(valueToReturn);	
	return valueToReturn;
}
