save_fast_invoice = function(){

	var my_input_array = document.getElementsByTagName('input');
	var my_select_array = document.getElementsByTagName('select');
	var invoice_array_elt = my_input_array+my_select_array;
	
	var ticket = $('ticket');
	var invoice_num;
	var bl_num;
	var invoice_amount;
	var cost_kind;
	var id_issuer;
	
	for(i=0; i<my_input_array.length; i++){
		if(my_input_array[i].name.indexOf("fast_invoice")>-1 ){
			if(my_input_array[i].id.indexOf("invoice_num")>-1){
				invoice_num = my_input_array[i].value;
			}
			if(my_input_array[i].name.indexOf("bl_num")>-1){
				bl_num = my_input_array[i].value;
			}
			if(my_input_array[i].name.indexOf("invoice_amount")>-1){
				invoice_amount = my_input_array[i].value;
			}
		}
	}
	for(i=0; i<my_select_array.length; i++){
		if(my_select_array[i].name.indexOf("fast_invoice")>-1 ){
			if(my_select_array[i].name.indexOf("cost_kind")>-1){
				cost_kind = my_select_array[i].value;
			}
			if(my_select_array[i].name.indexOf("issuer")>-1){
				id_issuer = my_select_array[i].value;
			}
		}
	}
	
	var params;
	params += "ticket="+ticket;
	params += "&invoice_num="+invoice_num;
	params += "&bl_num="+bl_num;
	params += "&invoice_amount="+invoice_amount;
	params += "&cost_kind="+cost_kind;
	params += "&id_issuer="+id_issuer;
	
	fill_div("hidden_treatment_div", "./ajax/dau_bl_fast_invoice_save.psp", params);

//	alert(invoice_num+ bl_num+ invoice_amount+ cost_kind);
}


show_cfs_details_from_fcs = function( which_id_po, which_id_bl ){
	var url = "dau_cfs_invoice_detail.psp?";
	url += "TICKET="+$F('ticket');
	url += "&invoice_num="+$F('invoice_num');
	url += "&ct_num="+$F('ct_num');
	url += "&invoice_amount="+$F('invoice_amount');
	url += "&cost_kind="+$F('select_cost_kind');
	url += "&currency="+$F('select_fast_invoice_currency');
	url += "&tx_EURUSD="+$F('tx_EURUSD');
	var name = "cfs_invoice";
	var width = 640;
	var height = 350;
	var showChrome = 0;
	var canResize = 1;
	var canScroll = 1;
	var top = 100;
	var left = 200;
	openWindow(url , name , width , height , showChrome , canResize , canScroll , top , left);	
}

var charArray = new Array();
charArray[32] = ' ';
charArray.push('!', '"', '#', '$', '%', '&', "'", '(', ')',
'*', '+', ',', '-', '.', '/', '0', '1', '2', '3', 
'4', '5', '6', '7', '8', '9', ':', ';', '<', '=', 
'>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 
'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 
'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', 
'\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e',
'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', '', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'�', '�', '�', '�', '_', '_', '_', '�', '�', '�', '�', '�', '�', '�',
'�', '+', '+', '�', '�', '+', '+', '-', '-', '+', '-', '+', '�', '�',
'+', '+', '-', '-', '�', '-', '+', '�', '�', '�', '�', '�', '�', 'i',
'�', '�', '�', '+', '+', '_', '_', '�', '�', '_', '�', '�', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'_', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '_', ' ');



is_only_numbers = function (e, t, nb_digit){
	//if nb_digit is undefined we accept real
   // code 8 : backspace
   // code 44 : virgule
   // code 45 : tiret "-"
   // code 46 : point
	// code 48-57 : chiffre
	
	is_continue = false;
	my_event_code = e.which; 

	if(document.all){
		my_event_code = e.keyCode; 
	}
	my_char = charArray[my_event_code];

	if (t.value.indexOf('.')>0 && (t.value.length-t.value.indexOf('.')) >nb_digit && my_event_code != 0 && my_event_code != 8) {
		return is_continue;
	}

	if (my_char == ',' && t.value.indexOf('.')<=0 && t.value.length > 0 && nb_digit!=0) {
	 	t.value= t.value+".";
        }
	if(	my_event_code == 0 
		|| my_event_code == 8 
		|| (my_char == '.' && t.value.indexOf('.')<=0 && t.value.length > 0 && nb_digit!=0) 
		|| ( my_char >= '0' && my_char <= '9' )
		|| ( my_char == '-' )  
	){
      is_continue =  true; 
	}
   return is_continue;
}


resetInvoice = function (){

if($('total_amount'))
{$('total_amount').clear();}

if($('comment'))
{$('comment').clear();}

if($('old_invoice_num'))
{$('old_invoice_num').clear();}

if($('invoice_num'))
{$('invoice_num').clear();}

if($('id_issuer'))
{$('id_issuer').value="0";}

if($('bl_amount'))
{$('bl_amount').clear();}

if($('select_bl'))
{$('select_bl').value="0";}

var node = $('div_list_shpts');
//node.parentNode.removeChild(node);
if (node.hasChildNodes())
{
var shptChild=node.childNodes;

 for (var i = 1; i < shptChild.length; i++) 
  {
  shptChild[i].remove();
  }
}
var nodeCost=$('div_shpt0_list_cost_kind');
if($('div_shpt0_list_cost_kind'))
{
 var costChild=nodeCost.childNodes;

   for (var i = 0; i < costChild.length; i++) 
   {
   costChild[i].remove();
   }
   if($('select_cost_kind'))
   {
   $('select_cost_kind').value="0";
   }
   if($('cost_kind_amount'))
   {
   $('cost_kind_amount').clear();
   }
 }  
}

deleteInvoice = function(id_bl_invoice) {
if (confirm("Souhaitez vous supprimer cette facture? Le confirmez vous?"))
 {
    var url = 'ajax/dau_bl_invoice_remove.psp';
//	alert(url+"?id_bl_invoice="+id_bl_invoice);
	new Ajax.Request
	(url, {
		method: 'post',
		parameters:{id_bl_invoice : id_bl_invoice},
		onSuccess: function(transport) 
		{
		},
		onFailure: function() {}
	    }
	);
}
$(id_bl_invoice).hide();
$('Total').hide();
}





