//////////////////////////
// Fonctions generiques
//////////////////////////

start_loading = function (which_div){

	$('main_loading_div').innerHTML = "";
	body_node = $('main_loading_div');
	
	new_loading_div=document.createElement("div");

	id_loading_div = "loading_"+which_div;
	new_loading_div.setAttribute("id", id_loading_div);
	body_node.appendChild(new_loading_div);
	$(id_loading_div).innerHTML += "<br><br><img src=ajax/img/ajax-loader.gif><br>LOADING...<br><br>";
	
	$(id_loading_div ).style.width = '400';
	$(id_loading_div ).style.textAlign = 'center';
	$(id_loading_div ).style.fontFamily = 'Arial';
	$(id_loading_div ).style.fontSize = '20px';
	$(id_loading_div ).style.fontWeight = 'bold';
	$(id_loading_div ).style.position = 'absolute';
	$(id_loading_div ).style.backgroundColor = '#FFFFFF';
	$(id_loading_div ).style.top = '100';
	$(id_loading_div ).style.left = getWidthCenterOfPage()-200;
	new Effect.Appear(id_loading_div);
}

fill_div = function ( which_div, url, params){

// Cette fonction permet d'afficher le contenu du r�sultat d'une requete Ajax dans le div pass� en param�tre	
//	prompt('',"http://"+document.location.host+"/cbryrocher/"+url+"?"+params);
	//	$(which_div).innerHTML = '';
	var myAjax = new Ajax.Updater( 
	which_div, 
	url, 
	{	
		method: 'get', 
		parameters: params, 
		evalScripts: true, 
		asynchronous:true,
		onCreate: start_loading(which_div),
		onComplete: function(){
						new Effect.Fade("loading_"+which_div);
					}
	}
	); 
}

get_pos = function( which_parent_node ) {
	my_pos = 0;
	if ($(which_parent_node).childNodes.length>0) {
		for (i=0; i<$(which_parent_node).childNodes.length;i++) {
			if ($(which_parent_node).childNodes[i].nodeName == "DIV") {
				cur_pos = parseInt($(which_parent_node).childNodes[i].getAttribute("pos"));
				if(cur_pos>=my_pos){
					my_pos = cur_pos+1
				}
			}
		}
	}
	return my_pos;
}



add_settle_detail = function(which_dlis_id){

	list_settles = $("div_settles_detail") ; 
	settle_pos = get_pos('div_settles_detail');
	new_settle=document.createElement("div");
	node_settle_pos = "div_settle"+settle_pos;
	new_settle.setAttribute("id", node_settle_pos);
	new_settle.setAttribute("pos", settle_pos);
	list_settles.appendChild(new_settle);

	var params = "settle_pos="+settle_pos;
	
	settle_amount = parseInt($('global_settle_amount').innerHTML);
	params += "&cost_section="+$F('cost_section');
	params += "&settle_pos="+settle_pos;
	params += "&currency="+$F('currency');
	params += "&exchange_rate="+$F('exchange_rate');
	
	if(is_set(which_dlis_id)){
		params += "&dlis_id="+which_dlis_id;
	}

	fill_div(node_settle_pos, "./ajax/dau_invoice_settle_detail.psp", params);

}


update_settle_amount = function(which_select){


	var global_settle_amount = $F("balance_settle_amount");
	
	div_node = get_parent("div", which_select); 

/*
	if(which_select.selectIndex == 0){
		div_node.getElementById("tr_amount").style.display="none";
	} else {
		div_node.getElementById("tr_amount").style.display="block";
	}
*/	
	input_table = document.getElementsByTagName('input');

	var settle_amount_to_update;
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "amount") {
			if(input_table[i].value.length <= 0){
				input_table[i].value = get_rest_of_balance_amount();
			}
		}
	}
		
}

get_rest_of_balance_amount = function(){
	input_table = document.getElementsByTagName('input');
	settle_amount = $F("balance_settle_amount");
	
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "amount") {
			if(input_table[i].value.length == 0){
				input_table[i].value = 0;
			} else {
				settle_amount -= parseFloat(input_table[i].value);
			}
		}
	}

	return settle_amount;

}

get_parent = function(which_parent_node_name, which_node){

	if(which_node.nodeName.toLowerCase() == which_parent_node_name){
		return which_node.parentNode;
	} else {
		return get_parent(which_parent_node_name, which_node.parentNode);	
	}
	
}

remove_invoice_settle = function (which_pos){
	if( confirm( 'any remove is definitive!' )){
		var parent_node = $("div_settles_detail");
		var child_node = $("div_settle"+which_pos);
		parent_node.removeChild(child_node);
	}
}


update_opener_settles = function(which_id_shpt, which_cost_section){
	var which_bl_node;
	var which_cost_section_node;
	var which_shpt_pos;
	var which_cost_section_pos;

	which_node = window.opener.document.getElementById('div_list_shpts');

	select_table = which_node.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_bl") {
			if(select_table[i].value == which_id_shpt){
				which_bl_node = get_parent("table", select_table[i]);
			}
		}
	}

	select_table = which_bl_node.getElementsByTagName('select');
	for(i=0; i<select_table.length; i++){
		if (select_table[i].name == "select_cost_sections") {
			if(select_table[i].value == which_cost_section){
				which_cost_section_node = get_parent("table", select_table[i]);
			}
		}
	}

	span_table = which_cost_section_node.getElementsByTagName('span');
	for(i=0; i<span_table.length; i++){
		if (span_table[i].id.match(/.+settle/g) ) {
			which_shpt_pos=span_table[i].id;
			which_cost_section_pos=span_table[i].id;
			which_shpt_pos = which_shpt_pos.replace(/.*([0-9]+).+_settle/, "$1");
			which_cost_section_pos = which_cost_section_pos.replace(/.*([0-9]+)_settle/, "$1");
		}
	}
	window.opener.get_cost_section_waited( which_shpt_pos, which_cost_section_pos );	
	window.close();
		
}

get_total_settle_amount = function(){
	var settle_amount = 0;
	var input_table = document.getElementsByTagName('input');
	for(i=0; i<input_table.length; i++){
		if (input_table[i].name == "amount" ) {
			settle_amount+= parseFloat(input_table[i].value);
		}
	}
	return settle_amount;
}


save_invoice_settles = function(){
	var is_continue=true;
	var balance_settle_amount = $F('balance_settle_amount');
	var total_settle_amount = get_total_settle_amount();
	if(balance_settle_amount!=total_settle_amount && is_continue ){
		alert("La somme des surcouts saisis ne correspond pas au montant total des surcouts � justifier!")
		is_continue = false;
	}
	if(is_continue){
		$('main_form').submit();
	}
	
}
