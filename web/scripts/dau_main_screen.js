//////////////////////////
// Fonctions generiques
//////////////////////////

start_loading = function (which_div){

	$('main_loading_div').innerHTML = "";
	body_node = $('main_loading_div');
	
	new_loading_div=document.createElement("div");

	id_loading_div = "loading_"+which_div;
	new_loading_div.setAttribute("id", id_loading_div);
	body_node.appendChild(new_loading_div);
	$(id_loading_div).innerHTML += "<br><br><img src=ajax/img/ajax-loader.gif><br>LOADING...<br><br>";
	
	$(id_loading_div ).style.width = '400';
	$(id_loading_div ).style.textAlign = 'center';
	$(id_loading_div ).style.fontFamily = 'Arial';
	$(id_loading_div ).style.fontSize = '20px';
	$(id_loading_div ).style.fontWeight = 'bold';
	$(id_loading_div ).style.position = 'absolute';
	$(id_loading_div ).style.backgroundColor = '#FFFFFF';
	$(id_loading_div ).style.top = '100';
	$(id_loading_div ).style.left = getWidthCenterOfPage()-200;
	new Effect.Appear(id_loading_div);
}

fill_div = function ( which_div, url, params){

// Cette fonction permet d'afficher le contenu du r�sultat d'une requete Ajax dans le div pass� en param�tre	
//	prompt('',"http://"+document.location.host+"/cbryrocher/"+url+"?"+params);
//	dbug("http://"+document.location.host+"/cbryrocher/"+url+"?"+params);
	//	$(which_div).innerHTML = '';
	var myAjax = new Ajax.Updater( 
	which_div, 
	url, 
	{	
		method: 'get', 
		parameters: params, 
		evalScripts: true, 
		asynchronous:true,
		onCreate: start_loading(which_div),
		onComplete: function(){
						new Effect.Fade("loading_"+which_div);
					}
	}
	); 
}

show_charges_designation = function(which_uo, which_fic_num, which_shpt_num_involvement, which_shpt_num_involved){
	var which_shpt_num;
	$('main_loading_div').innerHTML = "";
	which_div= "charges_designation_div";
	params = "uo_num="+which_uo;
	if (is_set(which_fic_num)){
		params += "&fic_num="+which_fic_num;
	}

	if (is_set(which_shpt_num_involvement)){
		params += "&shpt_num_involvement="+which_shpt_num_involvement;
		which_shpt_num = which_shpt_num_involvement;
	}
	if (is_set(which_shpt_num_involved)){
		params += "&shpt_num_involved="+which_shpt_num_involved;
		which_shpt_num = which_shpt_num_involved;
	}


	fill_div(which_div, "./ajax/dau_main_screen_show_charges_designation.psp", params);

	
}

update_total_cost = function(which_cost_kind, which_quot_total, which_real_total, which_invoice_amount_total, which_real_amount_total){

		if( which_quot_total != "" ){which_quot_total += " �"};
		if( which_real_total != "" ){which_real_total += " �"};
		if( which_invoice_amount_total != "" ){which_invoice_amount_total += " �"};
		if( which_real_amount_total != "" ){which_real_amount_total += " �"};
		$('total_quot_'+which_cost_kind).innerHTML = which_quot_total;
		$('total_real_'+which_cost_kind).innerHTML = which_real_total;
		$('total_invoice_amount_'+which_cost_kind).innerHTML = which_invoice_amount_total;
		$('total_real_amount_'+which_cost_kind).innerHTML = which_real_amount_total;
}

show_headers = function(which_uo_num, which_fic_num, which_shpt_num_involvement, which_shpt_num_involved ){
	root_params = "uo_num="+which_uo_num;
	if (is_set(which_fic_num)){
		root_params += "&fic_num="+which_fic_num;
	}

	which_div= "header_quot";
	params = root_params+"&is_fcs_tracking=0";
	if (is_set(which_shpt_num_involvement)){
		params += "&shpt_num="+which_shpt_num_involvement;
	}
	fill_div(which_div, "./ajax/dau_main_screen_show_header.psp", params);

	which_div= "header_real";
	params = root_params+"&is_fcs_tracking=1";
	if (is_set(which_shpt_num_involved)){
		params += "&shpt_num="+which_shpt_num_involved;
	}
	fill_div(which_div, "./ajax/dau_main_screen_show_header.psp", params);
	
}

show_tree_view = function(which_uo_num, which_fic_num, which_shpt_num_involvement, which_shpt_num_involved ){
	params = "uo_num="+which_uo_num;
	if (is_set(which_fic_num)){
		params += "&fic_num="+which_fic_num;
	}
	if (is_set(which_shpt_num_involvement) ){
		if(which_shpt_num_involvement.length>0){
			params += "&shpt_num="+which_shpt_num_involvement;
		} else if(is_set(which_shpt_num_involved) ){
			params += "&shpt_num="+which_shpt_num_involved;
		}
	}
	which_div= "tree_hidden";
	$('charges_designation_div').innerHTML = "";

	fill_div(which_div, "./ajax/dau_main_screen_tree_view.psp", params);
	if(which_uo_num.length == 6){
		show_charges_designation(which_uo_num, which_fic_num, which_shpt_num_involvement, which_shpt_num_involved );
	}
}




toggle_cost_kind = function( which_id_cost_kind ){
	is_minus_inside = new RegExp("minus","g");
	img_toggle_src = $('img_toggle_cost_kind_'+which_id_cost_kind).src;
	is_to_show = is_minus_inside.test(img_toggle_src); 
	
	td_list = document.getElementsByTagName('td');
	for(i=0; i<td_list.length; i++){
		if(td_list[i].getAttribute('id_cost_kind') == which_id_cost_kind){
			which_id_to_toggle = td_list[i].id;
			if( is_to_show ){
				Element.hide(which_id_to_toggle);
			} else {
				Element.show(which_id_to_toggle);
			}
		}
	}

	if( is_to_show ){
		$('img_toggle_cost_kind_'+which_id_cost_kind).src = img_toggle_src.replace('minus', 'plus') ;
	} else {
		$('img_toggle_cost_kind_'+which_id_cost_kind).src = img_toggle_src.replace('plus', 'minus') ;
	}
	
};

show_invoice_detail = function(which_uo_num, which_fic_num, which_id_charge, which_designation_charge){
	url = "./dau_invoices_settles_details.psp";
	params = "?uo_num="+which_uo_num
	params += "&fic_num="+which_fic_num
	params += "&id_charge="+which_id_charge
	params += "&designation_charge="+which_designation_charge
	openWindow(url+params , "invoice_detail" , 400 , 400 , 0 , 1 , 1 , 100 , 100);
}

move_treeview_content = function(){
	while( !is_main_screen_hide() ){
		function_to_set_timeout = "move_treeview_content()";
		setTimeout(function_to_set_timeout,200);
		return false;
	}
	while( $('tree_show').innerHTML.length == 0){
		$('tree_show').innerHTML = $('tree_hidden').innerHTML;
		function_to_set_timeout = "move_treeview_content()";
		setTimeout(function_to_set_timeout,200);
		return false;
	}
	$('tree_hidden').innerHTML = '';
//	setTimeout(function_to_set_timeout,1000);
}

toggle_fic = function( which_fic_id ){
	var reg_exp=new RegExp(".+bullet_toggle_plus.png", "g");
	img_toggle_src = $("img_toggle"+which_fic_id).src;
	
	if( img_toggle_src.match(reg_exp) ){
		img_toggle_src = img_toggle_src.replace(/plus/, "minus");
		Element.show("div_fic"+which_fic_id);
	}else{
		img_toggle_src = img_toggle_src.replace(/minus/, "plus");
		Element.hide("div_fic"+which_fic_id);
	}
	$("img_toggle"+which_fic_id).src = img_toggle_src;
}

is_main_screen_hide = function(){
	var value_to_return = true;
	var div_table = document.getElementsByTagName('div');
	for(i=0; i<div_table.length; i++){
		if ( div_table[i].id.match(/^loading_.*/g) ){
			if( div_table[i].style.display != 'none' && value_to_return ){
				value_to_return = false;
			}
		}
	}
	return value_to_return;
}

dbug = function(text){
	$('dbug_textarea').innerHTML += text+"\n";
}
