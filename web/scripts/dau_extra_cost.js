//////////////////////////
// Fonctions generiques
//////////////////////////

start_loading = function (which_div) {

	$('main_loading_div').innerHTML = "";
	body_node = $('main_loading_div');

	new_loading_div = document.createElement("div");

	id_loading_div = "loading_" + which_div;
	new_loading_div.setAttribute("id", id_loading_div);
	body_node.appendChild(new_loading_div);
	$(id_loading_div).innerHTML += "<br><br><img src=ajax/img/ajax-loader.gif><br>LOADING...<br><br>";

	$(id_loading_div).style.width = '400';
	$(id_loading_div).style.textAlign = 'center';
	$(id_loading_div).style.fontFamily = 'Arial';
	$(id_loading_div).style.fontSize = '20px';
	$(id_loading_div).style.fontWeight = 'bold';
	$(id_loading_div).style.position = 'absolute';
	$(id_loading_div).style.backgroundColor = '#FFFFFF';
	$(id_loading_div).style.top = '100';
	$(id_loading_div).style.left = getWidthCenterOfPage() - 200;
	Element.show(id_loading_div);
}

fill_div = function (which_div, url, params) {

	// Cette fonction permet d'afficher le contenu du résultat d'une requete Ajax dans le div passé en paramétre	
	//	prompt('',"http://"+document.location.host+"/cbryrocher/"+url+"?"+params);
	//	$(which_div).innerHTML = '';
	var myAjax = new Ajax.Updater(
		which_div,
		url,
		{
			method: 'get',
			parameters: params,
			evalScripts: true,
			asynchronous: true,
			onCreate: start_loading(which_div),
			onComplete: function () {
				Element.hide("loading_" + which_div);
			}
		}
	);
}

is_exchange_rate_visible = function (elt) {
	if (elt.options[elt.selectedIndex].text.toLowerCase() == 'usd') {
		Element.show("input_exchange_rate");
	} else {
		Element.hide("input_exchange_rate");
	}
}

change_type_piece = function (elt) {
	var fcs_racine_web = document.location.href;
	fcs_racine_web = fcs_racine_web.substring(0, fcs_racine_web.lastIndexOf('/') + 1);
	var url_to_redirect = fcs_racine_web + elt.options[elt.selectedIndex].value;
	var text_type_piece = elt.options[elt.selectedIndex].text;
	var coeff_type_piece = '1';
	if (text_type_piece.toLowerCase() == 'avoir') {
		coeff_type_piece = '-1';
	}
	url_to_redirect += "?TICKET=" + $F('ticket') + "&id_po=" + $F('id_po') + "&coeff_type_piece=" + coeff_type_piece;
	document.location.href = url_to_redirect;
}


/******************************************

Functions invoices

*******************************************/

find_from_criteria = function (which_sens, which_offset) {

	if (is_set(which_sens)) which_sens = '';
	if (is_set(which_offset)) which_offset = '';
	//if( is_set(which_nomenclature) ) which_nomenclature = '';


	var which_ticket = $F('ticket');
	var is_standalone = $F('is_standalone');
	//var is_cfs = $F('is_cfs');
	var is_cfs = '';
	if (is_set($('is_cfs'))) { is_cfs = $F('is_cfs'); }

	var which_uo = $F('s_uo');

	//var which_currency = $F('s_currency');
	var which_currency = '';
	if (is_set($('s_currency'))) { which_currency = $F('s_currency'); }

	var which_po = $F('s_po');
	var which_bl = '';
	var which_invoice_num = '';
	if (is_set($('s_bl'))) which_bl = $F('s_bl');
	if (is_set($('s_invoice_num'))) which_invoice_num = $F('s_invoice_num');

	var which_contact_ada = $('s_contact_ada').options[$('s_contact_ada').selectedIndex].value;
	var which_extra_cost_status = '';
	if (is_set($("s_extra_cost_status"))) {
		which_extra_cost_status = $('s_extra_cost_status').options[$('s_extra_cost_status').selectedIndex].value;
	}

	if (which_offset != '') {
		which_offset = parseInt(which_offset);
	}
	if (which_sens == "suivant") {
		which_offset += 20;
	}
	if (which_sens == "precedent") {
		which_offset -= 20;
	}
	url = "?TICKET=" + which_ticket + "&is_standalone=" + is_standalone + "&OFFSET=" + which_offset;


	if (is_cfs > 0) url += '&is_cfs=' + is_cfs;
	if (which_uo.length > 0) url += '&s_uo=' + which_uo;
	if (which_po.length > 0) url += '&s_po=' + which_po;
	if (which_bl.length > 0) url += '&s_bl=' + which_bl;
	//	if(which_sku.length > 0) url += '&s_sku='+which_sku;
	if (which_invoice_num.length > 0) url += '&s_invoice_num=' + which_invoice_num;
	if (which_contact_ada.length > 0) url += '&s_contact_ada=' + which_contact_ada;
	if (which_currency.length > 0) url += '&s_currency=' + which_currency;
	if (which_extra_cost_status.length > 0) url += '&s_extra_cost_status=' + which_extra_cost_status;


	//	prompt('', url);

	document.location.href = url;

}

excel_from_criteria = function () {

	try {

		var is_cfs = document.getElementById('is_cfs').value;
		var which_uo = document.getElementById('s_uo').value;
		if (which_uo.length == 0) {
			if (is_set(document.getElementById('hidden_uo_num'))) {
				which_uo = document.getElementById('hidden_uo_num').value;
			}
		}
		var which_po = document.getElementById('s_po').value;
		var which_currency = document.getElementById('s_currency').value;
		var which_bl = '';
		var which_invoice_num = '';
		if (is_set(document.getElementById('s_bl'))) which_bl = document.getElementById('s_bl').value;
		if (is_set(document.getElementById('s_invoice_num'))) which_invoice_num = document.getElementById('s_invoice_num').value;

		var which_contact_ada = document.getElementById('s_contact_ada').value;
		var which_extra_cost_status = '';

		var url = "dau_bl_invoice_to_excel.psp";
		var params = 's_uo=' + which_uo;

		if (which_po.length > 0) params += '&s_po=' + which_po;
		if (which_bl.length > 0) params += '&s_bl=' + which_bl;
		//	if(which_sku.length > 0) params += '&s_sku='+which_sku;
		if (which_currency.length > 0) params += '&s_currency=' + which_currency;
		if (which_invoice_num.length > 0) params += '&s_invoice_num=' + which_invoice_num;
		if (which_contact_ada.length > 0) params += '&s_contact_ada=' + which_contact_ada;
		if (is_cfs.length > 0) params += '&is_cfs=' + is_cfs;

		//	prompt('', url+"?"+params);
		var name = "cfs_invoice";
		var width = 640;
		var height = 350;
		var showChrome = 0;
		var canResize = 1;
		var canScroll = 1;
		var top = 100;
		var left = 200;

		//	alert(url);

		openWindow(url + "?" + params, name, width, height, showChrome, canResize, canScroll, top, left);
	} catch (err) {
		console.log(err);
	}

}


get_pos = function (which_parent_node) {
	my_pos = 0;
	if ($(which_parent_node).childNodes.length > 0) {
		for (i = 0; i < $(which_parent_node).childNodes.length; i++) {
			if ($(which_parent_node).childNodes[i].nodeName == "DIV") {
				cur_pos = parseInt($(which_parent_node).childNodes[i].getAttribute("pos"));
				if (cur_pos >= my_pos) {
					my_pos = cur_pos + 1
				}
			}
		}
	}
	return my_pos;
}

get_parent = function (which_parent_node_name, which_node) {
	//	alert( which_node.getAttribute("id") );
	if (which_node.nodeName.toLowerCase() == which_parent_node_name) {
		return which_node.parentNode;
	} else {
		return get_parent(which_parent_node_name, which_node.parentNode);
	}

}

get_already_used_bl = function () {
	var already_used = "";
	select_table = document.getElementsByTagName('select');
	for (i = 0; i < select_table.length; i++) {
		if (select_table[i].name == "select_bl") {
			if (select_table[i].value.length > 0) {
				already_used += select_table[i].value + ",";
			}
		}
	}
	// on enléve la derniére virgule
	already_used = already_used.substring(0, already_used.length - 1);
	return already_used;

}

change_currency_every_where = function (elt) {
	var currency_table = document.getElementsByTagName('currency');
	var txt_currency = elt.options[elt.selectedIndex].text;
	var print_html_currency = '&#128;';
	if (txt_currency == 'usd') {
		print_html_currency = '&#036;';
	}

	for (i = 0; i < currency_table.length; i++) {
		currency_table[i].innerHTML = print_html_currency;
	}
}

get_currency = function () {
	return $('id_currency').options[$('id_currency').selectedIndex].text;
}

get_rest_of_total_amount = function () {
	input_table = document.getElementsByTagName('input');
	total_amount = 0;
	total_bl_amount = 0;

	for (i = 0; i < input_table.length; i++) {
		if (input_table[i].name == "total_amount") {
			if (input_table[i].value.length == 0) {
				input_table[i].value = 0;
			} else {
				total_amount += parseFloat(input_table[i].value);
			}
			//			alert("total_amount:"+total_amount);
		}
		if (input_table[i].name == "bl_amount") {
			if (input_table[i].value.length == 0) {
				input_table[i].value = 0;
			} else {
				total_bl_amount += parseFloat(input_table[i].value);
			}
			//			alert("total_bl_amount:"+total_bl_amount);
		}
	}

	return (total_amount - total_bl_amount);

}

add_shpt = function (which_id_bl) {

	list_shpts = $("div_list_shpts");
	shpt_pos = get_pos('div_list_shpts');
	new_shpt = document.createElement("div");
	node_shpt_pos = "div_shpt" + shpt_pos;
	new_shpt.setAttribute("id", node_shpt_pos);
	new_shpt.setAttribute("pos", shpt_pos);
	list_shpts.appendChild(new_shpt);

	var params = "shpt_pos=" + shpt_pos;

	forbidden = get_already_used_bl();
	params += "&forbidden=" + forbidden;
	params += "&currency=" + get_currency();
	params += "&id_po=" + $F('id_po');
	params += "&invoice_num=" + $F('invoice_num');
	if (is_set(which_id_bl)) {
		params += "&id_bl=" + which_id_bl;
	}

	fill_div(node_shpt_pos, "./ajax/dau_bl_invoice_shpt.psp", params);

}



remove_shpt_node = function (which_shpt_pos) {
	if (confirm('any remove is definitive!')) {
		var parent_node = $("div_list_shpts");
		var child_node = $("div_shpt" + which_shpt_pos);
		parent_node.removeChild(child_node);
	}
}

update_bl_amount = function (which_bl_select) {


	var which_shpt_pos = "";
	rest_of_total_amount = get_rest_of_total_amount();

	tr_node = get_parent("tr", which_bl_select);

	input_table = tr_node.getElementsByTagName('input');

	for (i = 0; i < input_table.length; i++) {
		if (input_table[i].name == "bl_amount") {
			if (input_table[i].value.length > 0) {
				rest_of_total_amount += parseFloat(input_table[i].value);
			}
			if (rest_of_total_amount < 0 || which_bl_select.value.length <= 0) {
				rest_of_total_amount = 0;
			}
			input_table[i].value = rest_of_total_amount;
		}
		if (input_table[i].name == "shpt_pos") {
			which_shpt_pos = input_table[i].value;
		}
	}
	if (which_bl_select.value.length > 0) {
		add_cost_kind(which_shpt_pos);
	} else {
		$("div_shpt" + which_shpt_pos + "_list_cost_kind").innerHTML = "";
	}
}


get_rest_of_bl_amount = function (which_node) {

	which_node = get_parent("div", which_node);
	my_shpt_node = $(which_node.id.replace("_list_cost_kind", ""));

	input_table = my_shpt_node.getElementsByTagName('input');
	total_bl_amount = 0;
	total_cost_kind_amount = 0;

	for (i = 0; i < input_table.length; i++) {
		if (input_table[i].name == "bl_amount") {
			if (input_table[i].value.length == 0) {
				input_table[i].value = 0;
			} else {
				total_bl_amount += parseFloat(input_table[i].value);
			}
			//			alert("total_bl_amount:"+total_bl_amount);
		}
		if (input_table[i].name == "cost_kind_amount") {
			if (input_table[i].value.length == 0) {
				input_table[i].value = 0;
			} else {
				total_cost_kind_amount += parseFloat(input_table[i].value);
			}
			//			alert("total_cost_kind_amount:"+total_cost_kind_amount);
		}
	}

	return (total_bl_amount - total_cost_kind_amount);

}

add_cost_kind = function (which_shpt_pos, which_cost_kind, which_cost_kind_amount) {


	div_list_cost_kind_pos = "div_shpt" + which_shpt_pos + "_list_cost_kind";
	list_cost_kind = $(div_list_cost_kind_pos);
	cost_kind_pos = get_pos(div_list_cost_kind_pos);
	new_cost_kind = document.createElement("div");

	node_cost_kind_pos = "div_shpt" + which_shpt_pos + "_cost_kind" + cost_kind_pos;
	new_cost_kind.setAttribute("id", node_cost_kind_pos);
	new_cost_kind.setAttribute("pos", cost_kind_pos);
	list_cost_kind.appendChild(new_cost_kind);

	var params = "shpt_pos=" + which_shpt_pos;
	params += "&cost_kind_pos=" + cost_kind_pos;

	div_shpt_node = $("div_shpt" + which_shpt_pos);
	id_shpt = get_id_shpt(div_shpt_node);
	params += "&id_shpt=" + id_shpt;
	params += "&currency=" + get_currency();

	params += "&forbidden=" + get_already_used_cost_kind(div_shpt_node);
	if (is_set(which_cost_kind)) {
		params += "&cost_kind=" + which_cost_kind;
	}
	if (is_set(which_cost_kind_amount)) {
		params += "&cost_kind_amount=" + which_cost_kind_amount;
	}

	fill_div(node_cost_kind_pos, "./ajax/dau_bl_invoice_shpt_cost_kind.psp", params);

}

get_id_shpt = function (which_node) {
	var id_shpt = "";
	select_table = which_node.getElementsByTagName('select');

	for (i = 0; i < select_table.length; i++) {
		if (select_table[i].name == "select_bl") {
			if (select_table[i].value.length > 0) {
				id_shpt = select_table[i].value;
			}
		}
	}
	// on enléve la derniére virgule
	return id_shpt;

}

get_already_used_cost_kind = function (which_node) {
	var already_used = "";
	select_table = which_node.getElementsByTagName('select');
	for (i = 0; i < select_table.length; i++) {
		if (select_table[i].name == "select_cost_kind") {
			if (select_table[i].value.length > 0) {
				already_used += "'" + select_table[i].value + "',";
			}
		}
	}
	// on enléve la derniére virgule
	already_used = already_used.substring(0, already_used.length - 1);
	return already_used;

}

remove_cost_kind_node = function (which_shpt_pos, which_cost_kind_pos) {
	if (confirm('any remove is definitive!')) {
		var parent_node = $("div_shpt" + which_shpt_pos + "_list_cost_kind");
		var child_node = $("div_shpt" + which_shpt_pos + "_cost_kind" + which_cost_kind_pos);
		parent_node.removeChild(child_node);
	}
}

update_cost_kind_amount = function (which_cost_kind_select) {

	rest_of_bl_amount = get_rest_of_bl_amount(which_cost_kind_select);
	tr_node = get_parent("tr", which_cost_kind_select);
	tr_node = get_parent("tr", tr_node);

	input_table = tr_node.getElementsByTagName('input');

	for (i = 0; i < input_table.length; i++) {
		if (input_table[i].name == "cost_kind_amount") {
			if (input_table[i].value.length > 0) {
				rest_of_bl_amount += parseFloat(input_table[i].value);
			}
			if (rest_of_bl_amount < 0) {
				rest_of_bl_amount = 0;
			}
			input_table[i].value = rest_of_bl_amount;
			return false;
		}
	}

}

get_extra_cost_invoice = function (which_id_extra_cost, which_coeff_type_facture) {
	var params = "id_extra_cost=" + which_id_extra_cost;
	params += "&coeff_type_facture=" + which_coeff_type_facture;

	var which_span = "span_extra_cost_list" + which_id_extra_cost + "_coeff" + which_coeff_type_facture;
	fill_div(which_span, "./ajax/dau_extra_cost_invoice.psp", params);

}


attach_extra_cost_invoice = function (which_id_extra_cost, which_coeff_type_facture) {
	var params = "id_extra_cost=" + which_id_extra_cost;
	params += "&coeff_type_facture=" + which_coeff_type_facture;

	var which_span = "span_extra_cost_add" + which_id_extra_cost + "_coeff" + which_coeff_type_facture;
	fill_div(which_span, "./ajax/dau_extra_cost_attach.psp", params);

}

save_attach_extra_cost_invoice = function (which_id_extra_cost, which_id_bl_invoice, which_amount, which_id_currency, which_coeff_type_facture) {
	var params = "id_extra_cost=" + which_id_extra_cost;
	params += "&id_bl_invoice=" + which_id_bl_invoice;
	params += "&amount=" + which_amount;
	params += "&id_currency=" + which_id_currency;
	var which_span = "span_extra_cost_add" + which_id_extra_cost + "_coeff" + which_coeff_type_facture;
	fill_div(which_span, "./ajax/dau_extra_cost_attach_save.psp", params);
}

change_extra_cost_status = function (which_status, which_id_extra_cost, is_checked) {

	if (is_checked) {
		is_checked = 1;
	} else {
		is_checked = 0;
	}

	var params = "id_extra_cost=" + which_id_extra_cost;
	params += "&status=" + which_status;
	params += "&is_checked=" + is_checked;
	var which_span = "span_extra_cost" + which_id_extra_cost + "_status_" + which_status;
	fill_div(which_span, "./ajax/dau_extra_cost_change_status.psp", params);
}

del_extra_cost_invoice = function (which_id_extra_cost, which_coeff_type_facture, which_id_extra_cost_invoice) {
	var params = "id_extra_cost_invoice=" + which_id_extra_cost_invoice;
	params += "&id_extra_cost=" + which_id_extra_cost;
	var which_span = "span_extra_cost_list" + which_id_extra_cost + "_coeff" + which_coeff_type_facture;
	//	var which_div = 'dbug_text';
	fill_div(which_span, "./ajax/dau_extra_cost_attach_delete.psp", params);
}

dbug = function (text) {
	$('dbug_text').innerHTML += text + "<br> ";
}


