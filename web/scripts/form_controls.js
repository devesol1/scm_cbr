var charArray = new Array();
charArray[32] = ' ';
charArray.push('!', '"', '#', '$', '%', '&', "'", '(', ')',
'*', '+', ',', '-', '.', '/', '0', '1', '2', '3', 
'4', '5', '6', '7', '8', '9', ':', ';', '<', '=', 
'>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 
'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 
'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', 
'\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e',
'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', '', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'�', '�', '�', '�', '_', '_', '_', '�', '�', '�', '�', '�', '�', '�',
'�', '+', '+', '�', '�', '+', '+', '-', '-', '+', '-', '+', '�', '�',
'+', '+', '-', '-', '�', '-', '+', '�', '�', '�', '�', '�', '�', 'i',
'�', '�', '�', '+', '+', '_', '_', '�', '�', '_', '�', '�', '�', '�',
'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
'_', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '_', ' ');


is_only_numbers = function (e, t, nb_digit){
	//if nb_digit is undefined we accept real
   // code 8 : backspace
   // code 44 : virgule
   // code 45 : tiret
   // code 46 : point
	// code 48-57 : chiffre
	
	
	is_continue = false;
	my_event_code = e.which; 

	if(document.all){
		my_event_code = e.keyCode; 
	}
	my_char = charArray[my_event_code];

	if (t.value.indexOf('.')>0 && (t.value.length-t.value.indexOf('.')) >nb_digit && my_event_code != 0 && my_event_code != 8 ) {
		return is_continue;
	}

	if (my_char == ',' && t.value.indexOf('.')<=0 && t.value.length > 0 && nb_digit!=0) {
	 	t.value= t.value+".";
        }
	if(	my_event_code == 0 
		|| my_event_code == 8 
		|| my_event_code == 45 
		|| (my_char == '.' && t.value.indexOf('.')<=0 && t.value.length > 0 && nb_digit!=0) 
		|| ( my_char >= '0' && my_char <= '9' )  
	){
      is_continue =  true; 
	}
   return is_continue;
}

is_only_regular_char_or_numbers = function (e, t, list_special_forbidden_chars, isLowCase){
	//regular_chars are numbers, chars, score, underscore

	// list_special_forbidden_chars doit contenir les caract�res que l'on souhaite interdir s�par�s par une virgule
	// ceci permet d'interdir des caract�res faisant partie des regular_chars ( espace, tiret, lettre en particulier )

   // code 8 : backspace
   // code 32 : espace
   // code 44 : virgule
	// code 45 : tiret
   // code 46 : point
	// code 48-57 : chiffre
	// code 95 : tiret bas
	
	is_continue = false;
	my_event_code = e.which; 
	if(document.all){
		my_event_code = e.keyCode; 
	}
	my_char = charArray[my_event_code];
	if( 	my_event_code == 0 
			||	my_event_code == 8 
			|| my_char == ' ' 
			|| my_char == '-' 
			|| my_char == '_' 
			|| ( my_char >= '0' && my_char <= '9' ) 
			|| ( my_char >= 'a' && my_char <= 'z' ) 
			|| ( my_char >= 'A' && my_char <= 'Z' ) 
			){
      is_continue =  true; 
	}
   
	if (list_special_forbidden_chars != null) {
		var reg=new RegExp(",", "g");
		var array_special_forbidden_chars=list_special_forbidden_chars.split(reg);
		for (var i=0; i<array_special_forbidden_chars.length; i++) {
			if ( charArray[my_event_code] == array_special_forbidden_chars[i] ){
				is_continue =  false; 
			}
		}	
	}
	if (is_continue && is_set(isLowCase) &&  ( ( my_char >= 'a' && my_char <= 'z' ) || ( my_char >= 'A' && my_char <= 'Z' ) ) ){
		if ( !isLowCase && ( my_char >= 'a' && my_char <= 'z' )  ){
			my_event_code-=32;
		} else if( isLowCase && ( my_char >= 'A' && my_char <= 'Z' )  ){
			my_event_code+=32;
		}
		t.value = t.value+String.fromCharCode(my_event_code);
		is_continue=false;
	}

	return is_continue;
}

is_set = function( which_object ){
	value_to_return = true;
	if (typeof(which_object) == 'undefined' || which_object == null) {
		value_to_return = false;
	}
	return value_to_return;
}

 select_value = function(which_select){
	return document.getElementById(which_select).options[document.getElementById(which_select).selectedIndex].value ;
 }

