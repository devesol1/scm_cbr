showInvoiceDetail = function(whichObject){
	removeTdInvoiceDetails();
	var whichInvoiceNum=getInvoiceNumFromTrInvoiceSummary(whichObject);	
	var whichIdCurrency=getIdCurrencyFromTrInvoiceSummary(whichObject);	
	var url = 'ajax/dau_bl_e_invoice_detail.psp';
	var which_tr_as_ref = whichObject.up('tr[name="trInvoiceSummary"]');
	which_tr_as_ref.insert({
	  after: "<tr><td colspan='9' id='invoiceDetail'></td></tr>"
	});
	var params = "?";
	params += "&TICKET="+$F('ticket');
	params += "&invoice_num="+whichInvoiceNum
	params += "&id_currency="+whichIdCurrency
	new Ajax.Updater("invoiceDetail", url,{method: 'get', parameters: params, evalScripts:true});
}

goToMainDetail = function(idUo, uoNum){
	var url = "dau_main_detail.psp";
	var params = "?TICKET="+$F('ticket');
	params += "&id_uo="+idUo;
	params += "&uo_num="+uoNum;
	params += "&profil=ada";
	window.open(url+params, '_blank');
}

saveInvoice = function(whichObject){
	
	//var url = 'ajax/dau_bl_e_invoice_save.psp';
	//processOnInvoice(url, whichObject);
	alert('save');
}


checkInvoice = function(whichObject){

var ticket = $F('ticket');
var invoice_num = $F('invoice_num');
var invoice_amount= $F('invoice_amount');
var id_uo = $F('id_uo');

var url = 'ajax/dau_bl_invoice_check.psp';
new Ajax.Request
(url, {
	method: 'post',
	parameters:{ticket : ticket, invoice_num: invoice_num, invoice_amount: invoice_amount, id_uo: id_uo},
	onSuccess: function(transport) 
	{
	$('msgcheck').innerHTML = transport.responseText;
	console.log('msgcheck');
	console.log(transport.responseText);
	if(transport.responseText == 'not saved')
	  {
		saveInvoice(whichObject);
	  } 
	else {
		if( confirm( "Un montant de" + invoice_amount + 'a d�j� �t� enregistr� pour la facture ' + invoice_num +". Souhaitez vous continuer") )
		 	{
			saveInvoice(whichObject);
			} 
		else {
			alert("Enregistrement abondonn�");
			}
		}	 
	},
	onFailure: function() {}
	});



}


removeInvoice = function(whichObject){
	var url = 'ajax/dau_bl_e_invoice_remove.psp';
	if(confirm('Toute suppression est définitive!')){
		processOnInvoice(url, whichObject);
	}
}

removeTdInvoiceDetails = function(){
	if( $('invoiceDetail') != null){
		var which_tr_as_ref = $('invoiceDetail').up('tr');
		which_tr_as_ref.remove();
	}
}

getInvoiceNumFromTrInvoiceSummary = function(whichObject){
	var which_tr_as_ref = whichObject.up('tr[name="trInvoiceSummary"]');
	var whichInvoiceNum=which_tr_as_ref.select('span[name="invoiceNum"]').first().innerHTML;
	return whichInvoiceNum;
}

getIdCurrencyFromTrInvoiceSummary = function(whichObject){
	var which_tr_as_ref = whichObject.up('tr[name="trInvoiceSummary"]');
	var whichIdCurrency=which_tr_as_ref.select('span[name="idCurrency"]').first().innerHTML;
	return whichIdCurrency;
}

processOnInvoice = function(url, whichObject){
	var which_tr_as_ref = whichObject.up('tr[name="trInvoiceSummary"]');
	var whichInvoiceNum = getInvoiceNumFromTrInvoiceSummary(whichObject);
	var whichTicket = $F('ticket');

	if(url.indexOf('save')>-1){
		which_td_to_fill = which_tr_as_ref.down('td[name="tdAction"]');
		which_td_to_fill.innerHTML="Saved";
	} else {
		which_tr_as_ref.style.display = "none";
		removeTdInvoiceDetails();
	}
    console.log(' whichInvoiceNum '+ whichInvoiceNum);

//	$('hidden_treatment_div').innerHTML = url+"?invoice_num="+whichInvoiceNum+"&TICKET="+whichTicket;

	new Ajax.Request(url, {
		method: 'post',
		parameters:{invoice_num : whichInvoiceNum,TICKET : whichTicket},
		onSuccess: function(transport) {
//			$('hidden_treatment_div').innerHTML = transport.responseText;
			$('hidden_treatment_div').innerHTML = '';
		}
	});

}


var loaded = false;

startLoading = function() {
	loaded = false;
	window.setTimeout('showLoadingImage()', 1000);
}

showLoadingImage = function() {
	var el = $("loading_box").show();
	if (el && !loaded) {
		el.innerHTML = '<img src="/ajax/img/ajax-loader.gif">';
		new $('loading_box').show();
	}
}

stopLoading = function() {
	Element.hide('loading_box');
	loaded = true;
}
/*
Ajax.Responders.register({
    onCreate : startLoading,
    onComplete : stopLoading
  });
*/
///////////////////////////
// Functions 
///////////////////////////

find_from_criteria = function(){

	
	var which_ticket = $F('ticket');
	var which_uo = $F('s_uo'); 
	var which_currency = $F('s_currency');
	var which_po = $F('s_po');
	
	var which_date_apres=$F('s_date_apres');
	var which_date_avant=$F('s_date_avant');
	
	var which_bl = ''; 
	var which_invoice_num = ''; 
	if( is_set($('s_bl') ) ) which_bl = $F('s_bl');
	if( is_set($('s_invoice_num') ) ) which_invoice_num = $F('s_invoice_num');

	
	var which_fournisseur = $('s_fournisseur').options[$('s_fournisseur').selectedIndex].value;
	var which_countersigning = $('s_countersigning').options[$('s_countersigning').selectedIndex].value;
	var which_contact_ada = $('s_contact_ada').options[$('s_contact_ada').selectedIndex].value;
		
	
	
	  var url="ajax/dau_bl_e_invoice_grid.psp"
	  var params ="?TICKET="+which_ticket;
	
	if(which_uo.length > 0) params += '&s_uo='+which_uo;
	if(which_po.length > 0) params += '&s_po='+which_po;
	if(which_bl.length > 0) params += '&s_bl='+which_bl;

	if(which_invoice_num.length > 0) params += '&s_invoice_num='+which_invoice_num;
	
	//if(which_date_apres.length > 0) params += '&s_date_apres='+which_date_apres;
	if(which_date_apres.length > 0) params += '&s_date_apres='+which_date_apres;
	if(which_date_avant.length > 0) params += '&s_date_avant='+which_date_avant;

	
	if(which_fournisseur.length > 0) params += '&s_fournisseur='+which_fournisseur;
	if(which_countersigning.length > 0) params += '&s_countersigning='+which_countersigning;
	if(which_contact_ada.length > 0) params += '&s_contact_ada='+which_contact_ada;
	if(which_currency.length > 0) params += '&s_currency='+which_currency;
	
	show_grid(url, params);
    
}

show_search = function(){
	var url="ajax/dau_bl_e_invoice_search.psp";
	new Ajax.Updater('div_search', url, {evalScripts:true});
}
show_grid = function(url, params){
	new Ajax.Updater('div_grid', url,{method: 'get', parameters: params, evalScripts:true});
}

fill_div = function ( which_div, url, params){

// Cette fonction permet d'afficher le contenu du résultat d'une requete Ajax dans le div passé en paramétre	
	var myAjax = new Ajax.Updater( 
	which_div, 
	url, 
	{	
		method: 'get', 
		parameters: params, 
		evalScripts: true, 
		asynchronous:true
	}
	); 
}


is_set = function( which_object ){
	value_to_return = true;
	if (typeof(which_object) == 'undefined' || which_object == null) {
		value_to_return = false;
	}
	return value_to_return;
}


excel_from_criteria = function(){

	//hide_criteria();
	var which_div = "div_background_action";
	var url = "dau_bl_e_to_excell.psp";
	var params = "";
	params += get_criteria_params();
	new Ajax.Updater('div_background_action', url,{method: 'get', parameters: params, evalScripts:true});
}

get_criteria_params = function(){

   
	var params = "?";
	params += "&s_uo="+$F('s_uo');
	params += "&s_po="+$F('s_po');
	params += "&s_bl="+$F('s_bl');
	params += "&s_fournisseur="+$F('s_fournisseur');
	params += "&s_invoice_num="+$F('s_invoice_num');
	params += "&s_contact_ada="+$F('s_contact_ada');
	params += "&s_currency="+$F('s_currency');
	params += "&s_date_avant="+$F('s_date_avant');
	params += "&s_date_apres="+$F('s_date_apres');
	params += "&s_countersigning="+$F('s_countersigning');
	
	return params;
	
}

