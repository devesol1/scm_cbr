//syntaxe pour l'inclusion d'un fichier js : <script language="javascript" src="cheminDuFichier.js"></script> 

function openWindow(url , name , width , height , showChrome , canResize , canScroll , top , left) {
	
addScrolls = "no";
if (canScroll == true ) addScrolls = "auto";

	var options = 'status='+showChrome+', resizable='+canResize+', menubar='+showChrome+', toolbar='+showChrome+',location='+showChrome+',width='+width+',height='+height+',scrollbars='+canScroll;
	if ( top != "" ) {
		options+= ',top='+top;
	}
	if ( left != "" ){
		options+= ',left='+left;
	}
	newWindow = window.open( url, name, options);
    newWindow.focus()
}


function getMouseX(e) {
  var tmp;
  if (document.all) {
    tmp = event.clientX;
  } else {  
    tmp = e.pageX;
  }
  return tmp;
}

function getMouseY(e) {
  var tmp;
  if (document.all) {
    tmp = event.clientY;
  } else {  
    tmp = e.pageY;
  }  
  return tmp
}



function getWidthCenterOfPage(){
	return (getWindowWidth()/2);
}

function getWindowWidth() {
	width=0;	
	if ( document.all ) width = document.body.clientWidth;
	//ie needs 16 px than ns
	else width = window.innerWidth-16;	
	
	return width;
}

	
function getWindowHeight() {
	if ( document.all ) height = document.body.clientHeight;
	if ( document.layers ) height=window.innerHeight; 
	
	return height;
}

	
   function setSize(width,height) {
		if (window.outerWidth) {
			window.outerWidth = width;
			window.outerHeight = height;
		} else if (window.resizeTo) {
			window.resizeTo(width,height);
		} else {
			alert("Not supported.");
		}
	}


