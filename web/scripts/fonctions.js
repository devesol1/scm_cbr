
function empty_input(unChamp){
   document.getElementById(unChamp).value = ""; 
}

is_set = function( which_object ){
	value_to_return = true;
	if (typeof(which_object) == 'undefined' || which_object == null) {
		value_to_return = false;
	}
	return value_to_return;
}

function search_verif_date (num_po, date_inf, date_sup, date_jour) {
    
	num_po = num_po.replace(/^\s+/, "");
	num_po = num_po.replace(/\s+$/, "");
		
	//~ //on v�rifie que les 3 champs ne sont pas vides
	if (num_po == '' && date_inf == '' && date_sup == '')	{
		// on laisse courir si les 3 champs du filtre sont vides: �a permet de revenir � la liste enti�re...
	
	
		//~ alert ("The 3 fields are empty!");
		//~ document.form_search_for_pos_list.NUM_PO_ORIGINAL.focus();
		document.form_search_for_pos_list.submit();
		return false;
	}
	else	{
		
		if (date_inf != '')	{
			var dateInf = new Date();
			var tabInf = date_inf.split("/");
			var jour_inf = tabInf[0];
			var mois_inf = tabInf[1];
			var an_inf = tabInf[2];
			
			mois_inf--;
			
			dateInf.setDate(jour_inf);
			dateInf.setMonth(mois_inf);
			dateInf.setFullYear(an_inf);
			
			var dateDuJour = new Date();
			var tabJour = date_jour.split("/");
			var jour_jour = tabJour[0];
			var mois_jour = tabJour[1];
			var an_jour = tabJour[2];
			
			mois_jour--;
			
			dateDuJour.setDate(jour_jour);
			dateDuJour.setMonth(mois_jour);
			dateDuJour.setFullYear(an_jour);
			
			//~ // on v�rifie que la date_inf de cr�ation de PO n'est pas sup�rieure � la date du jour:
			if (dateInf > dateDuJour)	{
				alert ("PO creation date should not be higher than the current date"); 
				return false;
			}
			
			if (date_sup != '')	{
				var dateSup = new Date();
				var tabSup = date_sup.split("/");
				var jour_sup = tabSup[0];
				var mois_sup = tabSup[1];
				var an_sup = tabSup[2];
				
				mois_sup--;
					
				dateSup.setDate(jour_sup);
				dateSup.setMonth(mois_sup);
				dateSup.setFullYear(an_sup);
	
				if (dateSup < dateInf)	{
					alert ("PO creation date max < PO creation date min");
					return false;
				}
			}
		}

		document.form_search_for_pos_list.submit();		
	}
		
	return false;
}

function amend_verif_date (num_po, date_inf, date_sup, date_jour) {
    
	num_po = num_po.replace(/^\s+/, "");
	num_po = num_po.replace(/\s+$/, "");
		
	//~ //on v�rifie que les 3 champs ne sont pas vides
	if (num_po == '' && date_inf == '' && date_sup == '')	{
		// on laisse courir si les 3 champs du filtre sont vides: �a permet de revenir � la liste enti�re...
	
	
		//~ alert ("The 3 fields are empty!");
		//~ document.form_search_for_pos_list.NUM_PO_ORIGINAL.focus();
		document.grille_po_amend.submit();
		return false;
	}
	else	{
		
		if (date_inf != '')	{
			var dateInf = new Date();
			var tabInf = date_inf.split("/");
			var jour_inf = tabInf[0];
			var mois_inf = tabInf[1];
			var an_inf = tabInf[2];
			
			mois_inf--;
			
			dateInf.setDate(jour_inf);
			dateInf.setMonth(mois_inf);
			dateInf.setFullYear(an_inf);
			
			var dateDuJour = new Date();
			var tabJour = date_jour.split("/");
			var jour_jour = tabJour[0];
			var mois_jour = tabJour[1];
			var an_jour = tabJour[2];
			
			mois_jour--;
			
			dateDuJour.setDate(jour_jour);
			dateDuJour.setMonth(mois_jour);
			dateDuJour.setFullYear(an_jour);
			
			//~ // on v�rifie que la date_inf de cr�ation de PO n'est pas sup�rieure � la date du jour:
			if (dateInf > dateDuJour)	{
				alert ("PO creation date should not be higher than the current date"); 
				return false;
			}
			
			if (date_sup != '')	{
				var dateSup = new Date();
				var tabSup = date_sup.split("/");
				var jour_sup = tabSup[0];
				var mois_sup = tabSup[1];
				var an_sup = tabSup[2];
				
				mois_sup--;
					
				dateSup.setDate(jour_sup);
				dateSup.setMonth(mois_sup);
				dateSup.setFullYear(an_sup);
	
				if (dateSup < dateInf)	{
					alert ("PO creation date max < PO creation date min");
					return false;
				}
			}
		}

		document.grille_po_amend.submit();		
	}
		
	return false;
}


//~ -------------------------------------------------------------------------------------------------------------------------------
//~ -------------------------------------------------------------------------------------------------------------------------------
function NelleFenetre(mapage,nom,largeur,hauteur){

	var win=null;

	var settings= 'width=' +largeur + ',height=' + hauteur + ',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
	
	win=window.open(mapage,nom,settings);
}

//~ -------------------------------------------------------------------------------------------------------------------------------
//~ -------------------------------------------------------------------------------------------------------------------------------
function changeValeurSKU_edition(valeur)	{
	// on change la valeur de la varialbe SKU_edition de la frame
	parent.transport.document.SAISIE_TRANSPORT.SKU_EDITION.value = valeur;
	parent.transport.document.CANCEL_TRANSPORT.SKU_EDITION.value = valeur;
	parent.transport.document.DELETE_TRANSPORT.SKU_EDITION.value = valeur;

	return false;
}

//~ -------------------------------------------------------------------------------------------------------------------------------
//~ -------------------------------------------------------------------------------------------------------------------------------
function afficheListePorts(ticket, mode, champ_saisie)	{
	
	var win=null;
	
	//~ RightPosition=0;
	//~ TopPosition=20;
	
	//~ var settings= 'width=670, height=300, top='+TopPosition+',right='+RightPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
	
	var settings= 'width=670, height=310, scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
	
	var monUrl = 'list_ref_ports.cgi?TICKET=' + ticket + '&MODE=' + mode + '&CHAMP_SAISIE=' + champ_saisie;
	
	win=window.open(monUrl, "list_ports", settings);
}

//~ -------------------------------------------------------------------------------------------------------------------------------
//~ -------------------------------------------------------------------------------------------------------------------------------
function saisiePort(champ_saisie, port, pays)	{
	var monport = port.value;
	var monpays = pays.value;
	
	monport = monport.replace(/-(.*)$/, "");
	monpays = monpays.replace(/\s+$/, "");
	
	if (monpays == '')	{
		alert('Choose a pays, please');
	}
	else	{
	
		if (monport == '')	{
			alert('Choose a port, please');
		}
		else	{
			var monChampSaisie = eval('window.opener.' + champ_saisie);
			monChampSaisie.value = monpays + monport;
			window.close();
		}
	}
}

//~ -------------------------------------------------------------------------------------------------------------------------------
//~ -------------------------------------------------------------------------------------------------------------------------------
function afficheSaisieContainer(ticket, equipement)	{
	
	var win=null;
	var monequipement = eval(equipement).value;
	monequipement = monequipement.replace(/;/g, "|");
	
	//~ RightPosition=0;
	//~ TopPosition=20;
	
	//~ var settings= 'width=270, height=350, top='+TopPosition+',right='+RightPosition+',scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
	
	var settings= 'width=270, height=350, scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes';
	
	
	var monUrl = 'saisie_type_container.cgi?TICKET=' + ticket + '&EQUIPEMENT=' + monequipement + '&CHAMP_SAISIE=' + equipement;
	
	win=window.open(monUrl, "saisie_container", settings);
}

//~ -------------------------------------------------------------------------------------------------------------------------------
//~ -------------------------------------------------------------------------------------------------------------------------------
function saisieContainer(champ_saisie)	{

	//on va r�cup�rer les types et quantit�s de container:
	var equipement = '';
	var code_erreur = 0;
	
	for (i = 0; i < document.form_container.tab_ct_type.length; i++)	{
		
		var montype = document.form_container.tab_ct_type[i].value; 
		var maquantite = document.form_container.tab_ct_qt[i].value; 
		
		montype = montype.replace(/\s+$/, "");
		maquantite = maquantite.replace(/\s+/g, "");
		
		if (maquantite != '')	{
			//on v�rifie qu'il s'agit d'un chiffre
			var modele = /^\d+$/;
			
			if (maquantite.search(modele) == -1)	{
				code_erreur = 1;
			}
			else	{
				equipement += montype + 'x' + maquantite + ';'
			}
		}
	}
	
	if (code_erreur != 0)	{
		alert('At least one quantity is not valid ');
	}
	else	{
		var monChampSaisie = eval('window.opener.' + champ_saisie);
		monChampSaisie.value = equipement;
		window.close();
	}
}
function confirmDelete(urlToRedirect){
// cette fonction permet d'afficher une boite de dialogue avant de supprimer un element	
	if(confirm('Warning ! Any delete is definitive.')) {
		document.location.href=urlToRedirect;
	}

}




	function fillFormAndSubmit( whichTS ){
		document.calendarHeader.TS.value=whichTS;
		document.calendarHeader.submit();
	}


	function getMyDate( now ){
		myDay = now.getDate()+'';
		if (myDay.length == 1) {
			myDay='0'+myDay;	
		}
		myMonth = (now.getMonth()+1)+'';
		if (myMonth.length == 1) {
			myMonth='0'+myMonth;	
		}
		myYear = now.getYear()+1900;
		return myDay+'/'+myMonth+'/'+myYear;
	}

	function getMyHours( now ){
		myHours = now.getHours()+'';
		if (myHours.length == 1) {
			myHours='0'+myHours;	
		}
		return myHours;
	}
	function getMyMinutes( now ){
		myMinutes = now.getMinutes()+'';
		if (myMinutes.length == 1) {
			myMinutes='0'+myMinutes;	
		}
		return myMinutes;
	}



