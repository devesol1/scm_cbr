

function N(){}
N.raise=function(a){
	if(typeof Error!="undefined"){
		throw new Error(a||"Assertion Failed");
	}else{
		throw a;
	}
};
N.fail=function(a){
	if(a===undefined)
		a="Assertion failed";
	if(!(sa===undefined))
		sa(a+"\n");
	N.raise(a)
};
N.isTrue=function(a,b){
	if(!a){
		if(b===undefined)
			b="Assertion failed";
		N.fail(b)
		}
	};
N.equals=function(a,b,c){
	if(a!=b){
		if(c===undefined){
			c="AS_Assert.equals failed: <"+a+"> != <"+b+">"
				}
		N.fail(c)
		}
	};
N.typeOf=function(a,b,c){
	if(typeof a==b)
		return;
	if(a||a==""){
		try{
			if(b==N.TYPE_MAP[typeof a]||a instanceof b){
				return;
			}
		} catch(d){
		}
	}
	if(c===undefined){
		if(typeof b=="function"){
			var e=b.toString().match(/^\s*function\s+([^\s\{]+)/);
			if(e)
				b=e[1]
		}
		c="AS_Assert.typeOf failed: <"+a+"> not typeof "+b
	}
	N.fail(c)
};
N.TYPE_MAP={
	string:String,number:Number,"boolean":Boolean
};
N.numArgs=function(a,b){
	var c=N.numArgs.caller;
	if(c&&c.arguments.length!=a){
		if(b===undefined){
			b=c.name+" expected "+a+" arguments  but received "+c.arguments.length
		}
		N.fail(b)
	}
};
var sb=false;
function sa(a){
	try{
		throw a;
	}catch(b){
		wb(b)
	}
}
function wb(a,b){
	var c="Javascript exception: "+(b?b:"")+" "+a;
	if(Z()){
		c+=" "+a.name+": "+a.message+" ("+a.number+")"
	}
	var d="";
	if(typeof a=="string"){
		d=a+"\n"
	}else{
		for(var e in a){
			try{
				d+=e+": "+a[e]+"\n"
			}catch(f){
			}
		}
	}
	d+=qb(wb.caller);
	hc(c+"\n"+d,1)
}
var Rc=/function (\w+)/;
function fc(a){
	var b=Rc.exec(String(a));
	if(b){
		return b[1]
	}
	return""
}
function qb(a){
	try{
		if(Ua()){
			return Error().stack
		}
		if(!a)return"";
		var b="- "+fc(a)+"(";
		for(var c=0;c<a.arguments.length;c++){
			if(c>0)
				b+=", ";
			var d=String(a.arguments[c]);
			if(d.length>40){
				d=d.substr(0,40)+"..."
			}
			b+=d
		}
		b+=")\n";
		b+=qb(a.caller);
		return b
	}catch(e){
		return"[Cannot get stack trace]: "+e+"\n"
	}
}
var ic;
var ga=null,Va=false;
function gc(){
	if((ga==null||ga.closed)&&!Va){
		try{
			Va=true;
			ga=window.open("","debug","width=700,height=500,toolbar=no,resizable=yes,scrollbars=yes,left=16,top=16,screenx=16,screeny=16");
			ga.blur();
			ga.document.open();
			Va=false;
			var a="<font color=#ff0000><b>To turn off this debugging window,hit 'D' inside the main caribou window, then close this window.</b></font><br>";
			rb(a)
		}catch(b){
		}
	}
}
function hc(a,b){
	if(!sb){
		if(typeof Wb!="undefined"){
			Wb(ab(a))
		}
		return
	}
	try{
		var c=(new Date).getTime()-ic,d="["+c+"] "+ab(a).replace(/\n/g,"<br>")+"<br>";
		if(b==1){
			d="<font color=#ff0000><b>Error: "+d+"</b></font>";
			ga.focus()
		}
		rb(d)
	}catch(e){
	}
}
function rb(a){
	if(!sb){
		return
	}
	try{
		gc();
		ga.document.write(a);
		ga.scrollTo(0,1000000)
	} catch(b){
	}
};
function Ga(a){
	if(a in Ta){
		return Ta[a]
	}
	return Ta[a]=navigator.userAgent.toLowerCase().indexOf(a)!=-1
}
var Ta={};
function Z(){
	return Ga("msie")&&!window.opera
}
function Ua(){
	return!Z()&&!(Ga("safari")||Ga("konqueror"))&&Ga("mozilla")
}
function lb(a){
	if(typeof Error!="undefined"){
		throw new Error(a||"Assertion Failed");
	}else{
		throw a;
	}
}
function Ca(a){
	if(a===undefined)a="Assertion failed";
	if(Ib(sa))sa(a+"\n");
	lb(a)
}
function E(a,b){
	if(!a){
		if(b===undefined)b="Assertion failed";
		Ca(b)
	}
}
function Sa(a,b,c){
	if(a!=b){
		if(c===undefined){
			c="AssertEquals failed: <"+a+"> != <"+b+">"
		}
		Ca(c)
	}
}
function G(a,b,c){
	if(typeof a==b)return;
	if(a||a==""){
		try{
			if(b==Zb[typeof a]||a instanceof b)
				return
		}catch(d){
		}
	}
	if(c===undefined){
		if(typeof b=="function"){
			var e=b.toString().match(/^\s*function\s+([^\s\{]+)/);
			if(e)
				b=e[1]
		}
		c="AssertType failed: <"+a+"> not typeof "+b
	}
	Ca(c)
}
var Zb={
	string:String,number:Number,"boolean":Boolean
};
function Ob(a,b){
	return a.document.getElementById(b)
}
function wc(a,b){
	var c=a.document.getElementById(b);
	if(!c){
		sa("Element "+b+" not found.")
	}
	return c
}
function xc(a){
	try{
		return a.parentNode
	}catch(b){
		return a
	}
}
function Jb(a,b){
	do{
		if(a===b)return true;
		b=xc(b)
	} while(b&&b!==document.body);
	return false
}
function mb(a,b,c){
	var d=a.document.createElement(c);b.appendChild(d);return d
}
function ec(a,b){
	var c=Ob(a,b);if(!c){c=mb(a,a.document.body,"div");c.id=b}return c
}
function pb(a,b,c){
	var d=Ob(a,b);
	if(!d){
		var e=mb(a,a.document.body,"div");
		e.innerHTML="<iframe id="+b+" name="+b+" src="+c+"></iframe>";d=wc(a,b)
	}
	return d
}
function yc(a,b){
	if(a==null||a.className==null)
		return false;
	if(a.className==b){
		return true
	}
	var c=a.className.split(" ");
	for(var d=0;d<c.length;d++){
		if(c[d]==b){
			return true
		}
	}
	return false
}
function Yb(a,b){if(yc(a,b))return;a.className+=" "+b}
function Hc(a,b){if(a.className==null)return;if(a.className==b){a.className="";return}var c=a.className.split(" "),d=[],e=false;for(var f=0;f<c.length;f++){if(c[f]!=b){if(c[f]){d.push(c[f])}}else{e=true}}if(e){a.className=d.join(" ")}}
function yb(a){var b=
a.offsetTop;if(a.offsetParent!=null)b+=yb(a.offsetParent);return b}
function Za(a){return $a(a,Tc)}var Tc={xa:function(a){return a.document.body.scrollTop},ya:function(a){return a.document.documentElement.scrollTop},ta:function(a){return a.pageYOffset}};
function zb(a){return $a(a,Sc)}var Sc={xa:function(a){return a.document.body.scrollLeft},ya:function(a){return a.document.documentElement.scrollLeft},ta:function(a){return a.pageXOffset}};
function Ic(a,b,c){var d=yb(b),e=d+b.offsetHeight,f=Za(a),g=
$a(a,Uc),h=f+g;if(d<f||e>h){var k;if(c=="b"){k=e-g+5}else if(c=="m"){k=(d+e)/2-g/2}else{k=d-5}a.scrollTo(0,k)}}var Uc={xa:function(a){return a.document.body.clientHeight},ya:function(a){return a.document.documentElement.clientHeight},ta:function(a){return a.innerHeight}};
function $a(a,b){try{if(!window.opera&&"compatMode"in a.document&&a.document.compatMode=="CSS1Compat"){return b.ya(a)}else if(Z()){return b.xa(a)}}catch(c){}return b.ta(a)}var Nc=/&/g,Zc=/</g,Vc=/>/g;
function ab(a){if(!a)return"";
return a.replace(Nc,"&amp;").replace(Zc,"&lt;").replace(Vc,"&gt;").replace($c,"&quot;")}
function zc(a){if(!a)return"";return a.replace(/&#(\d+);/g,function(b,c){return String.fromCharCode(parseInt(c,10))}).replace(/&#x([a-f0-9]+);/gi,function(b,c){return String.fromCharCode(parseInt(c,16))}).replace(/&(\w+);/g,function(b,c){c=c.toLowerCase();return c in Ab?Ab[c]:"?"})}var Ab={lt:"<",gt:">",quot:'"',nbsp:" ",amp:"&",apos:"'"};var $c=/\"/g;function Ma(a){return Ma.Za[a]}function Rb(a){if(!Ma.Za){var b={};b["\\"]="\\\\";b["'"]="\\047";b["\u0008"]="\\b";b['"']="\\042";b["<"]="\\074";b[">"]="\\076";b["&"]="\\046";b["\n"]="\\n";b["\r"]="\\r";b["\u0085"]="\\205";b["\u2028"]="\\u2028";b["\u2029"]="\\u2029";Ma.Za=b}return"'"+a.toString().replace(/[\'\\\r\n\b\"<>&\u0085\u2028\u2029]/g,Ma)+"'"}function ob(a){var b={};b.clientX=a.clientX;b.clientY=a.clientY;b.pageX=a.pageX;b.pageY=a.pageY;b.type=a.type;b.srcElement=a.srcElement;b.target=a.target;b.cancelBubble=a.cancelBubble;b.explicitOriginalTarget=a.explicitOriginalTarget;
return b}function Ib(a){return typeof a!="undefined"}function xb(a){var b;if(a.keyCode){b=a.keyCode}else if(a.which){b=a.which}return b}
function Oc(a){
	return document.getElementById(a)
}
function Pc(a){return document.all[a]}var t=document.getElementById?Oc:Pc;function Ya(a){E(a,"func passed to GetFnName() is undefined");var b;if(!("name"in a)){var c=/\W*function\s+([\w\$]+)\(/.exec(a);if(!c){throw new Error("Cannot extract name from function: "+a);}b=c[1];a.name=b}else{b=a.name}if(!b||b=="anonymous"){throw new Error("Anonymous function has no name: "+
a);}return a.name}function Wb(a){try{if(window.parent!=window&&window.parent.log){window.parent.log(window.name+"::"+a);return}}catch(b){}var c=t("log");if(c){var d="<p class=logentry><span class=logdate>"+new Date+"</span><span class=logmsg>"+a+"</span></p>";c.innerHTML=d+c.innerHTML}else{window.status=a}};function ua(a){return a<0?-1:1}
function La(a){
	return a|0
}
function Fb(a){
	sa(a);
	throw a;
}
function x(a,b){var c=a.toString();while(c.length<b){c="0"+c}return c}var db=[undefined,31,undefined,31,30,31,30,31,31,30,31,30,31];function aa(a,b){if(2!==b){return db[b]}var c=a<<4,d=db[c];if(!d){d=Math.round((Date.UTC(a,2,1)-Date.UTC(a,1,1))/86400000);db[c]=d}return d}var Gb={};function Ac(a,b){var c=a<<4|b,d=Gb[c];if(!d){d=(new Date(a,b-1,1,0,0,0,0)).getDay();Gb[c]=d}return d}function eb(a){return(a.date-1+
Ac(a.year,a.month))%7}
function Db(a,b,c,d,e,f){var g;if(a===d){if((g=b-e)===0){return c-f}else if(g<0){g=c-f;do{g-=aa(a,b++)}while(b<e);return g}else{g=c-f;do{g+=aa(d,e++)}while(e<b);return g}}else{return Math.round((Date.UTC(a,b-1,c)-Date.UTC(d,e-1,f))/86400000)}}
function Eb(a,b){return Db(a.year,a.month,a.date,b.year,b.month,b.date)}
function H(a,b,c,d,e,f){if(!isNaN(a)){this.year=a}if(!isNaN(b)){this.month=b}if(!isNaN(c)){this.date=c}if(!isNaN(d)){this.hour=d}if(!isNaN(e)){this.minute=e}if(!isNaN(f)){this.second=
f}}H.prototype.year=NaN;H.prototype.month=NaN;H.prototype.date=NaN;H.prototype.hour=NaN;H.prototype.minute=NaN;H.prototype.second=NaN;H.prototype.Bb=function(){return eb(this)};H.prototype.toString=function(){if(this.o!==undefined)return this.o;this.o=this.J();return this.o};
function ma(){}ma.prototype=new H;ma.prototype.constructor=ma;

function o(a,b,c){
	E(b&&c,"invalid date params: "+b+" "+c);
	H.call(this,a,b,c,NaN,NaN,NaN)
}
o.prototype=new ma;
o.prototype.constructor=o;
o.now=function(){
	var a=new Date;
	return o.create(a.getFullYear(),a.getMonth()+1,a.getDate())
};
o.prototype.type="Date";
o.prototype.j=function(){
	return this
};
o.prototype.aa=function(){
	return new A(this.year,this.month,this.date,0,0,0)
};
o.prototype.a=function(){
	if(undefined===this.f){
		this.f=bb(this.year,this.month,this.date)
	}
	return this.f
};

function bb(a,b,c){
	return(((a-1970)*12+b<<5)+c)*86400
}

o.prototype.Ba=function(){
	return true
};
o.prototype.J=function(){
	return x(this.date,2)+x(this.month,2)+x(this.year,4);
};
o.prototype.equals=function(a){
	return this.constructor===a.constructor&&this.date===a.date&&this.month===a.month&&this.year===a.year
};
o.oa={};
o.sb=0;
o.ob=200;
o.create=function(a,b,c){
	var d=bb(a,b,c);
	if(d in o.oa){
		return o.oa[d]
	} else {
		var e=new o(a,b,c);
		e.f=d;
		if(o.sb<o.ob){
			o.oa[d]=e
		}
		return e
	}
};

function A(a,b,c,d,e,f){
	H.call(this,a,b,c,d,e,f)
}
A.prototype=new ma;
A.prototype.constructor=A;
A.now=function(){
	var a=new Date;
	return new A(a.getFullYear(),a.getMonth()+1,a.getDate(),a.getHours(),a.getMinutes(),a.getSeconds())
};
A.prototype.type="DateTime";
A.prototype.j=function(){
	return o.create(this.year,this.month,this.date)
};
A.prototype.aa=function(){
	return this
};
A.prototype.lb=function(){
	return new $(this.hour,this.minute,this.second)
};
A.prototype.a=function(){
	if(undefined===this.f){
		this.f=(((((this.year-1970)*12+this.month<<5)+this.date)*24+this.hour)*60+this.minute)*60+this.second
	}
	return this.f
};
A.prototype.Ba=function(){return true};
A.prototype.J=function(){
	return x(this.year,4)+x(this.month,2)+x(this.date,2)+"T"+x(this.hour,2)+x(this.minute,2)+x(this.second,2)
};
A.prototype.equals=function(a){
	return this.constructor===a.constructor&&this.date===a.date&&this.month===a.month&&this.year===a.year&&this.hour===a.hour&&this.minute===a.minute&&this.second===a.second
};
A.prototype.clone=function(){
	var a=new A(this.year,this.month,this.date,this.hour,this.minute,this.second);
	if(this.o!==undefined)a.o=this.o;return a
};
function $(a,b,c){
	H.call(this,NaN,NaN,NaN,a,b,c)
}
$.prototype=new H;
$.prototype.constructor=$;
$.prototype.type="Time";
$.prototype.lb=function(){
	return this
};
$.prototype.J=function(){
	return"T"+x(this.hour,2)+x(this.minute,2)+x(this.second,2)
};
$.prototype.equals=function(a){
	return this.constructor===a.constructor&&this.hour===a.hour&&this.minute===a.minute&&this.second===a.second
};
$.prototype.a=function(){
	return(this.hour*60+this.minute)*60+this.second
};
function R(a,b,c,d){
	var e=d+60*(c+60*(b+24*a)),f=La(e/86400);
	e-=f*86400;
	var g=La(e/3600);
	e-=g*3600;
	var h=La(e/60);e-=h*60;
	var k=La(e);
	H.call(this,NaN,NaN,f,g,h,k)
}
R.prototype=new H;
R.prototype.constructor=R;
R.prototype.type="Duration";
R.prototype.Ec=function(){
	return this.date
};
R.prototype.Fc=function(){
	return this.date*24+this.hour
};
R.prototype.Gc=function(){
	return 1440*this.date+this.hour*60+this.minute
};
R.prototype.Hc=function(){
	return this.second+this.minute*60+this.hour*3600+86400*this.date
};
R.prototype.a=function(){
	if(undefined===this.f){
		this.f=((this.date*24+this.hour)*60+this.minute)*60+this.second
	}return this.f
};
R.prototype.J=function(){
	var a=this.year?ua(this.year):(this.month?ua(this.month):(this.date?ua(this.date):(this.hour?ua(this.hour):(this.minute?ua(this.minute):(this.second?ua(this.second):0))))),b=a<0?"-P":"P";
	if(this.year){
		b+=a*this.year+"Y"
	}
	if(this.month){
		b+=a*this.month+"N"
	}
	if(this.date){
		b+=this.date%7?a*this.date+"D":a*this.date/7+"W"
	}
	if(this.hour||this.minute||this.second){
		b+="T"
	}
	if(this.hour){
		b+=a*this.hour+"H"
	}
	if(this.minute){
		b+=a*this.minute+"M"
	}
	if(this.second){
		b+=a*this.second+"S"
	}
	if(!a){
		b+="0D"
	}
	return b
};
R.prototype.equals=function(a){
	return this.constructor===a.constructor&&this.date===a.date&&this.hour===a.hour&&this.minute===a.minute&&this.second===a.second
};
function za(a){G(a,H);var b=new v;b.year=a.year||0;b.month=a.month||0;b.date=a.date||0;b.hour=a.hour||0;b.minute=a.minute||0;b.second=a.second||0;return b}function Qa(a,b,c){E(!(isNaN(a)|isNaN(b)|isNaN(c)));var d=new v;d.year=a||0;d.month=b||0;d.date=c||0;return d}function v(){}v.prototype=new H;v.prototype.constructor=v;v.prototype.type="DTBuilder";v.prototype.year=(v.prototype.month=(v.prototype.date=(v.prototype.hour=
(v.prototype.minute=(v.prototype.second=0)))));v.prototype.a=function(){this.normalize();var a;if(isNaN(this.hour)){a=bb(this.year,this.month,this.date)}else{a=(((((this.year-1970)*12+this.month<<5)+this.date)*24+this.hour)*60+this.minute)*60+this.second}return a};v.prototype.advance=function(a){if(a.date){this.date+=a.date}if(a.hour){this.hour+=a.hour}if(a.minute){this.minute+=a.minute}if(a.second){this.second+=a.second}};v.prototype.normalize=function(){this.Ob();this.Ga();var a=aa(this.year,this.month);
while(this.date<1){this.month-=1;this.Ga();a=aa(this.year,this.month);this.date+=a}while(this.date>a){this.date-=a;this.month+=1;this.Ga();a=aa(this.year,this.month)}};v.prototype.Ob=function(){var a;if(this.second<0){a=Math.ceil(this.second/-60);this.second+=60*a;this.minute-=a}else if(this.second>=60){a=Math.floor(this.second/60);this.second-=60*a;this.minute+=a}if(this.minute<0){a=Math.ceil(this.minute/-60);this.minute+=60*a;this.hour-=a}else if(this.minute>=60){a=Math.floor(this.minute/60);this.minute-=
60*a;this.hour+=a}if(this.hour<0){a=Math.ceil(this.hour/-24);this.hour+=24*a;this.date-=a}else if(this.hour>=24){a=Math.floor(this.hour/24);this.hour-=24*a;this.date+=a}};v.prototype.Ga=function(){var a;if(this.month<1){a=Math.ceil((this.month-1)/-12);this.month+=12*a;this.year-=a}else if(this.month>12){a=Math.floor((this.month-1)/12);this.month-=12*a;this.year+=a}};v.prototype.j=function(){this.normalize();return o.create(this.year,this.month,this.date)};v.prototype.aa=function(){this.normalize();
return new A(this.year,this.month,this.date,this.hour,this.minute,this.second)};v.prototype.jb=function(){this.normalize();return new L(isFinite(this.year)?this.year:undefined,isFinite(this.month)?this.month:undefined,isFinite(this.date)?this.date:undefined)};v.prototype.kb=function(){this.normalize();return new M(isFinite(this.year)?this.year:undefined,isFinite(this.month)?this.month:undefined,isFinite(this.date)?this.date:undefined,isFinite(this.hour)?this.hour:undefined,isFinite(this.minute)?this.minute:
undefined,isFinite(this.second)?this.second:undefined)};v.prototype.lb=function(){this.normalize();return new $(this.hour,this.minute,this.second)};v.prototype.ac=function(){if(this.year||this.month){Fb("Can't convert months or years to ICAL_Duration");return undefined}else{return new R(this.date,this.hour,this.minute,this.second)}};v.prototype.bc=function(){return"number"==typeof this.year&&1+this.year%1===1&&"number"==typeof this.month&&1+this.month%1===1&&"number"==typeof this.date&&1+this.date%
1===1};v.prototype.Ic=function(){return this.bc()&&this.cc()};v.prototype.cc=function(){return"number"==typeof this.hour&&1+this.hour%1===1&&"number"==typeof this.minute&&1+this.minute%1===1&&"number"==typeof this.second&&1+this.second%1===1};v.prototype.toString=function(){return"["+(undefined!==this.year?x(this.year,4):"????")+"/"+(undefined!==this.month?x(this.month,2):"??")+"/"+(undefined!==this.date?x(this.date,2):"??")+" "+(undefined!==this.hour?x(this.hour,2):"??")+" "+(undefined!==this.minute?
x(this.minute,2):"??")+" "+(undefined!==this.second?x(this.second,2):"??")+"]"};v.prototype.equals=function(a){return this.constructor===a.constructor&&this.date===a.date&&this.month===a.month&&this.year===a.year&&this.hour===a.hour&&this.minute===a.minute&&this.second===a.second};function ta(a,b){E(a instanceof A||a instanceof o);this.start=a;if(b.constructor==R){var c=za(a);c.advance(b);this.end=this.start instanceof A?c.aa():c.j()}else{Sa(b.constructor,a.constructor);this.end=b}this.duration=Bb(this.end,
this.start)}ta.prototype.type="PeriodOfTime";ta.prototype.toString=function(){if(this.o!==undefined)return this.o;this.o=this.start+"/"+this.end;return this.o};ta.prototype.equals=function(a){return this.constructor===a.constructor&&this.start.equals(a.start)&&this.end.equals(a.end)};ta.prototype.overlaps=function(a){return a.end.a()>this.start.a()&&a.start.a()<this.end.a()};
ta.prototype.sc=function(a,b){
	return b.a()>this.start.a()&&a.a()<this.end.a()
};
ta.prototype.contains=function(a){return this.start.a()<=
a.start.a()&&this.end.a()>=a.end.a()};function cb(a,b){E(a instanceof M||a instanceof L);this.start=a;Sa(b.constructor,a.constructor);this.end=b;try{this.duration=Bb(this.end,this.start)}catch(c){this.duration=null}}cb.prototype.type="PartialPeriodOfTime";cb.prototype.J=function(){return this.start+"/"+this.end};cb.prototype.equals=function(a){return this.constructor===a.constructor&&this.start.equals(a.start)&&this.end.equals(a.end)};function Bb(a,b){if(isNaN(a.year)!=isNaN(b.year)||isNaN(a.hour)!=
isNaN(b.hour)){Fb("diff("+a+", "+b+")");return undefined}var c=za(a);if(isNaN(a.year)){c.hour-=b.hour;c.minute-=b.minute;c.second-=b.second}else{c.year=NaN;c.month=NaN;c.date=Db(a.year,a.month,a.date,b.year,b.month,b.date);if(!isNaN(a.hour)){c.hour-=b.hour;c.minute-=b.minute;c.second-=b.second}}return c.ac()}function L(a,b,c){this.year=a;this.month=b;this.date=c}L.prototype=new ma;L.prototype.constructor=L;L.prototype.type="PartialDate";L.prototype.j=function(){return o.create(this.year||0,this.month||
1,this.date||1)};L.prototype.aa=function(){return new A(this.year||0,this.month||1,this.date||1,0,0,0)};L.prototype.jb=function(){return this};L.prototype.kb=function(){return new M(this.year,this.month,this.date,0,0,0)};L.prototype.Ba=function(){return!isNaN(this.a())};L.prototype.a=function(){if(undefined===this.f){this.f=(((this.year-1970)*12+this.month<<5)+this.date)*86400}return this.f};L.prototype.equals=function(a){return this.constructor===a.constructor&&(this.date===a.date||isNaN(this.date)&&
isNaN(a.date))&&(this.month===a.month||isNaN(this.month)&&isNaN(a.month))&&(this.year===a.year||isNaN(this.year)&&isNaN(a.year))};L.prototype.J=function(){return(undefined!==this.year?x(this.year,4):"????")+(undefined!==this.month?x(this.month,2):"??")+(undefined!==this.date?x(this.date,2):"??")};
function M(a,b,c,d,e,f){
	this.year=a;
	this.month=b;
	this.date=c;
	this.hour=d;
	this.minute=e;
	this.second=f
}
M.prototype=new ma;
M.prototype.constructor=M;
M.prototype.type="PartialDateTime";
M.prototype.j=function(){
	return o.create(this.year||0,this.month||1,this.date||1)
};
M.prototype.aa=function(){return new A(this.year||0,this.month||1,this.date||1,this.hour||0,this.minute||0,this.second||0)};M.prototype.jb=function(){return new L(this.year,this.month,this.date)};M.prototype.kb=function(){return this};M.prototype.Ba=function(){return!isNaN(this.a())};M.prototype.a=function(){if(undefined===this.f){this.f=(((((this.year-1970)*12+this.month<<5)+this.date)*24+this.hour)*60+this.minute)*60+this.second}return this.f};M.prototype.equals=function(a){return this.constructor===
a.constructor&&(this.date===a.date||isNaN(this.date)&&isNaN(a.date))&&(this.month===a.month||isNaN(this.month)&&isNaN(a.month))&&(this.year===a.year||isNaN(this.year)&&isNaN(a.year))&&(this.hour===a.hour||isNaN(this.hour)&&isNaN(a.hour))&&(this.minute===a.minute||isNaN(this.minute)&&isNaN(a.minute))&&(this.second===a.second||isNaN(this.second)&&isNaN(a.second))};M.prototype.J=function(){return(undefined!==this.year?x(this.year,4):"????")+(undefined!==this.month?x(this.month,2):"??")+(undefined!==
this.date?x(this.date,2):"??")+"T"+(undefined!==this.hour?x(this.hour,2):"??")+(undefined!==this.minute?x(this.minute,2):"??")+(undefined!==this.second?x(this.second,2):"??")};var va=undefined,Hb=[];function Cb(a,b,c){var d=b>2&&29===aa(a,2);return Cb.pb[b]+d+c-1}Cb.pb=[undefined,0,31,59,90,120,151,181,212,243,273,304,334];function Xb(){var a=new Date,b=va;va=o.create(a.getFullYear(),a.getMonth()+1,a.getDate());if(b&&!b.equals(va)){for(var c=0;c<Hb.length;++c){var d=Hb[c];try{d(va)}catch(e){}}}var f=
new Date(a.getFullYear(),a.getMonth(),a.getDate(),0,0,0,0);
f.setDate(f.getDate()+1);
var g=f.getTime()-a.getTime();
if(g<0||g>=1800000){
	g=1800000
}window.setTimeout(Xb,g)}Xb();;function Dc(a){a=a.replace(/^\s+/,"").replace(/\s+$/,"").replace(/([0-9]+)([a-zA-Z]+)/g,"$1 $2").replace(/([a-zA-Z])([0-9])/g,"$1 $2");var b=a.split(/\b|_/),c=[],d=[];for(var e=0;e<b.length;++e){c[e]=b[e].length;d[e]=0;if(b[e].match(/^[0-9]+/)){var f=parseInt(b[e],10);b[e]=f;if(0===f){if(c[e]==2){d[e]|=1}}else if(f>12&&f<=31){d[e]|=4}else if(f<=12){d[e]|=6}else if(f<100||f>=1900){d[e]|=1}}else{var f=Ec(b[e]);if(f){b[e]=f[0];d[e]|=10}}}var g=0,h=0,k=0,l=0,m=0;for(var e=0;e<d.length;++e){if(!d[e]&&
/\w/.test(b[e])){++m}if(6===(d[e]&6)){++k}else{if(d[e]&4){++g}if(d[e]&2){++h}}if(d[e]&1){++l}}if(k){var B=false,q=false;if(!g||!h){if(g){B=true}else if(h){q=true}else if(1===k){B=true}else{for(var e=0;e<d.length;++e){var u=false;if(d[0]===1&&b[1]&&/^\s*-\s*$/.test(b[1])&&d[2]===6&&b[3]&&/^\s*-\s*$/.test(b[3])){u=true}if(6==(d[e]&6)){if(u||!Nb||Nb()){d[e]&=~4;++h;--k;q=true}else{d[e]&=~2;++g;--k;B=true}break}}}}if(B){for(var e=0;e<d.length;++e){if(6==(d[e]&6)){d[e]&=~4;++h;--k}}}else if(q){for(var e=
0;e<d.length;++e){if(6==(d[e]&6)){d[e]&=~2;++g;--k}}}}var n,r,s,p=0,W=7;if(g){for(var e=0;e<d.length;++e){if(d[e]&4){n=b[e];d[e]=0;--g;break}}E(n)}else{n=null;W&=~4}if(h){var ca=false,e;for(e=0;e<d.length;++e){if(d[e]&2){r=b[e];d[e]&=~2;--h;ca=0!==(d[e]&8);break}}if(ca){var C=c[e];for(var T=e+1;T<d.length;++T){if(d[T]&8){if(c[T]>C){d[e]|=2;d[T]&=~2;r=b[T];C=c[T];e=T}}}}}else{r=null;W&=~2}var O=!(!l);if(O){for(var e=0;e<d.length;++e){if(d[e]&1){s=b[e];d[e]=0;--l;break}}}else{s=null;if(g|h|k){var da=
-1,X=0;for(var e=0;e<d.length;++e){if(d[e]&&!(d[e]&8)){if(b[e]>X){da=e;X=b[e]}}}if(da>=0){s=X;switch(d[da]){case 4:--g;break;case 2:--h;break;case 6:--k;break}d[da]=0}}if(null==s){p-=1;s=va.year+(r&&r<va.month?1:0);W&=~1}}if(null==n){n=1;p-=0.5}if(null==r){if(O){r=1}else{return null}}if(s<100){s+=s<50?2000:1900}p-=g+h+l+k+(m>>2);return new Mb(o.create(s,r,n),p,W)}function Mb(a,b,c){this.date=a;this.confidence=b;this.specified=c}Mb.prototype.toString=function(){return this.date.toString()};function Ec(a){var b=
-1,c=-1;for(var d=0;d<Ba.length;++d){var e=Ba[d];if(!e){continue}var f=wa[d],g=Math.max(Lb(e,a),Lb(f,a));if(g&&g>c){b=d;c=g}}return c>=a.length+1>>1?[b,c]:null}function Lb(a,b){a=a.toLowerCase();b=b.toLowerCase();var c=Math.min(a.length,b.length),d;for(d=0;d<c;++d){if(a.charAt(d)!=b.charAt(d)){break}}return d};var Ha=["Su","M","Tu","W","Th","F","Sa"],wa=[,"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],Ba=[,"January","February","March","April","May","June","July","August","September","October","November","December"];function oa(a,b,c){this.x=a;this.y=b;this.coordinateFrame=c||null}oa.prototype.toString=function(){return"[P "+this.x+","+this.y+"]"};oa.prototype.clone=function(){return new oa(this.x,this.y,this.coordinateFrame)};function ra(a,b){this.dx=a;this.dy=b}ra.prototype.toString=function(){return"[D "+this.dx+","+this.dy+"]"};function fa(a,b,c,d,e){this.x=a;this.y=b;this.w=c;this.h=d;this.coordinateFrame=e||null}fa.prototype.contains=function(a){return this.x<=a.x&&a.x<this.x+this.w&&this.y<=a.y&&a.y<this.y+
this.h};
fa.prototype.intersects=function(a){
	var b=function(c,d){
		return new oa(c,d,null)
	};
	return this.contains(b(a.x,a.y))||this.contains(b(a.x+a.w,a.y))||this.contains(b(a.x+a.w,a.y+a.h))||this.contains(b(a.x,a.y+a.h))||a.contains(b(this.x,this.y))||a.contains(b(this.x+this.w,this.y))||a.contains(b(this.x+this.w,this.y+this.h))||a.contains(b(this.x,this.y+this.h))
};
fa.prototype.toString=function(){return"[R "+this.w+"x"+this.h+"+"+this.x+"+"+this.y+"]"};fa.prototype.clone=function(){return new fa(this.x,
this.y,this.w,this.h,this.coordinateFrame)};function z(a){function b(l){for(var m=a.offsetParent;m&&m.offsetParent;m=m.offsetParent){if(m.scrollLeft){l.x-=m.scrollLeft}if(m.scrollTop){l.y-=m.scrollTop}}}var c;if(a.ownerDocument&&a.ownerDocument.parentWindow){c=a.ownerDocument.parentWindow}else{c=window}if(a.ownerDocument&&a.ownerDocument.getBoxObjectFor){var d=a.ownerDocument.getBoxObjectFor(a),e=new fa(d.x,d.y,d.width,d.height,c);b(e);return e}if(a.getBoundingClientRect){var f=a.getBoundingClientRect();return new fa(f.left+zb(c),f.top+Za(c),
f.right-f.left,f.bottom-f.top,c)}var g=0,h=0;for(var k=a;k.offsetParent;k=k.offsetParent){g+=k.offsetLeft;h+=k.offsetTop}var e=new fa(g,h,a.offsetWidth,a.offsetHeight,c);b(e);return e}function Ra(a){var b=z(a);return new oa(b.x,b.y,b.coordinateFrame)}function vc(a,b){E(a,"p1 passed to Distance is undefined");E(b,"p2 passed to Distance is undefined");E(a.coordinateFrame==b.coordinateFrame);var c=a.x-b.x,d=a.y-b.y;return Math.sqrt(c*c+d*d)}
function S(a){
	var b=0,c=0;
	if(a.pageX||a.pageY){
		b=a.pageX;c=a.pageY
	}else if(a.clientX||a.clientY){
		var d=a.target?a.target:a.srcElement,e;
		if(d.ownerDocument&&d.ownerDocument.parentWindow){
			e=d.ownerDocument.parentWindow
		}else{
			e=window}b=a.clientX+zb(e);
			c=a.clientY+Za(e)
		}
	return new oa(b,c,window)
	};
	Function.prototype.bind=function(a){
		if(typeof this!="function"){
			throw new Error("Bind must be called as a method of a function object.");
		}
		var b=this,c=Array.prototype.splice.call(arguments,1,arguments.length);
		return function(){
			var d=c.concat();
			for(var e=0;e<arguments.length;e++){
				d.push(arguments[e])
			}
			return b.apply(a,d)
		}
	};
	var K,qa;
	(function(){
		var a={},b=0;
		function c(f){
			if(f.ab==null){
				f.ab=++b
			}
			return f.ab
		}
		function d(f,g,h,k){
			var l=c(f),m=c(h);
			k=!(!k);
			var B=l+"_"+g+"_"+m+"_"+k;
			return B
					}
			K=function(f,g,h,k){
				var l=d(f,g,h,k);
				if(l in a){
					return l
				}
				var m=e.bind(null,l);
				a[l]={
					listener:h,proxy:m
				};
				if(f.addEventListener){
					f.addEventListener(g,m,k)
				}else if(f.attachEvent){
					f.attachEvent("on"+g,m)
				}else{
					throw new Error("Node {"+f+"} does not support event listeners.");
				}
				return l
			};
			qa=function(f,g,h,k){
			var l=d(f,g,h,k);
			if(!(l in a)){
				return false
			}
var m=a[l].proxy;
if(f.removeEventListener){f.removeEventListener(g,m,k)}else if(f.detachEvent){f.detachEvent("on"+g,m)}delete a[l];return true};function e(f){var g=Array.prototype.splice.call(arguments,1,arguments.length);return a[f].listener.apply(null,g)}})();function jc(a){return function(b,c,d,e){var f=z(c),g=Ra(c.offsetParent),h;switch(d){case 1:if(undefined!==e){if(w&1){f.x+=e.dx}if(w&2){f.y+=e.dy}}h=a(f);if(!h){return false}if(w&1){if(undefined!==e){e.dx+=h.x-f.x+g.x}else{c.style.left=h.x+"px"}}if(w&2){if(undefined!==e){e.dy+=h.y-f.y+g.y}else{c.style.top=h.y+"px"}}break;case 2:if(undefined!==e){if(w&1){f.w+=e.dx}if(w&2){f.h+=e.dy}}if(undefined===e){var k=new oa(f.x+f.w,f.y+f.h,window);h=a(k);if(!h){return false}if(w&1){var l=h.x-c.offsetLeft;if(l<=
0){return false}c.style.width=l+"px"}if(w&2){var m=h.y-c.offsetTop;if(m<=0){return false}c.style.height=m+"px"}}break;case 5:if(e){var h=a(D);if(!h){return false}if(w&1){if(D.x-h.x<h.w/2){e.dx-=h.x-D.x}else{e.dx-=h.x+h.w-D.x}}if(w&2){if(D.y-h.y<h.h/2){e.dy-=h.y-D.y}else{e.dy-=h.y+h.h-D.y}}}break}return true}}
var lc=2*Math.PI/16,i=undefined,y=0,Y=undefined,D=undefined,Fa=undefined,ya=undefined,la=undefined,w=undefined,V=undefined,ka=undefined,Tb=false,Pa=undefined,Q=undefined;
function _DD_Install(a){
	if(!a){a=document
}
Q=a;K(Q.body,"mousedown",mc)}var hb=[];
function oc(a){
	hb.push(a)
}
function ub(a,b){
	for(var c=hb.length-1;c>=0;--c){
		var d=hb[c](a,b);
		if(d){
			return d
		}
	}
	return undefined
}
var xa;
function mc(a){xa=ob(a||window.event);K(Q.body,"mousemove",Wa);K(Q.body,"mouseup",Xa);return false}function nc(a){var b=undefined;for(var c=a.srcElement||a.target;c;c=c.parentNode){b=ub(c,a);if(!b)continue;D=S(a);if(b.T(a,c)){if(!(i&&y>=0&&y<6&&1===1+y%1&&w&&0===(w&~3))){var d="Bogus drag: el="+i+", type="+y+", axis mask="+
w;Ja();lb(d)}a.cancelBubble=true;Y=b;Fa=D;ya={x:i.offsetLeft,y:i.offsetTop,w:i.offsetWidth,h:i.offsetHeight};if(y===4){la=[D]}V=new ra(0,0);ka=new ra(0,0);if(y===1||y===2||y===5){if(y!==2&&Y.useUserDefinedAlpha&&!Y.useUserDefinedAlpha()){tb(i,true)}Pa=i.style.zIndex;i.style.zIndex=2000}if(Z()){K(Q.body,"mouseleave",Ka)}else{K(Q.body,"mouseout",Ka)}}else{Ja()}return false}return true}function Xa(a){if(xa){xa=null;qa(Q.body,"mousemove",Wa);qa(Q.body,"mouseup",Xa);var b=undefined;for(var c=a.srcElement||
a.target;c;c=c.parentNode){b=ub(c,a);if(b&&b.ga){b.ga(c)}}return}var d=a||Q.parentWindow.event;if(!Y.da(d,i,y)){Ia()}else{Ja()}}function Wa(a){if(xa){var b=nc(xa);xa=null;if(b){Ia();return true}}if(!i){return true}if(!Y){return true}var c=a||Q.parentWindow.event;c.cancelBubble=true;var d=S(c),e=new ra(d.x-D.x+V.dx,d.y-D.y+V.dy);if(!(e.dx|e.dy)){return false}var f=new ra(e.dx,e.dy);D=d;Tb=true;var g=Ra(i);if(w&1){ka.dx+=f.dx}if(w&2){ka.dy+=f.dy}if(Y.G(c,i,y,f,ka)){var h=Ra(i);ka.dx-=h.x-g.x;ka.dy-=
h.y-g.y;switch(y){case 1:case 2:case 5:V.dx=e.dx-f.dx;V.dy=e.dy-f.dy;break}switch(y){case 1:if(!(w&1)){f.dx=0}if(!(w&2)){f.dy=0}var k=i.currentStyle?i.currentStyle:i.ownerDocument.defaultView.getComputedStyle(i,"");if(k&&k.position=="relative"){i.style.left=ka.dx+"px";i.style.top=ka.dy+"px"}else{var l=i.offsetLeft+f.dx,m=i.offsetTop+f.dy;if(f.dx){i.style.left=l+"px"}if(f.dy){i.style.top=m+"px"}if(f.dx&&l!==i.offsetLeft){i.style.left=l+l-i.offsetLeft+"px"}if(f.dy&&m!==i.offsetTop){i.style.top=m+m-
i.offsetTop+"px"}}break;case 5:var B=z(i);if(w&1){var q=D.x+V.dx,u=Math.min(q,Fa.x),n=Math.abs(q-Fa.x);if(u!==B.x){var l=u;i.style.left=l+"px";if(l!==i.offsetLeft){i.style.left=l+l-i.offsetLeft+"px"}}if(n!==B.w){i.style.width=n+"px";if(n!=i.offsetWidth){n=Math.max(0,n+n-i.offsetWidth);i.style.width=n+"px"}}}if(w&2){var r=D.y+V.dy,s=Math.min(r,Fa.y),p=Math.abs(r-Fa.y);if(s!==B.y){var m=s;i.style.top=m+"px";if(m!==i.offsetTop){i.style.top=m+m-i.offsetTop+"px"}}if(p!==B.h){i.style.height=p+"px";if(p!=
i.offsetHeight){p=Math.max(0,p+p-i.offsetHeight);i.style.height=p+"px"}}}break;case 2:if(f.dx&&w&1){var W=i.offsetWidth,n=W+f.dx;if(n<0){V.dx+=n;n=0}if(n<1){V.dx+=n-1;n=1}i.style.width=n+"px";if(n!=i.offsetWidth){n+=n-i.offsetWidth;if(n<1){n=1}i.style.width=n+"px"}}if(f.dy&&w&2){var ca=i.offsetHeight,p=ca+f.dy;if(p<0){V.dy+=p;p=0}if(p<1){V.dy+=p-1;p=1}i.style.height=p+"px";if(p!=i.offsetHeight){p+=p-i.offsetHeight;if(p<1){p=1}i.style.height=p+"px"}}break;case 3:if(!(w&1)){f.dx=0}if(!(w&2)){f.dy=0}if(i.scrollBy){i.scrollBy(f.dx,
f.dy)}else{f.dx*=-1;f.dy*=-1;var C=i.scrollLeft,T=i.scrollLeft+i.scrollWidth-i.offsetWidth,O=i.scrollTop,da=i.scrollTop+i.scrollHeight-i.offsetHeight;f.dx=Math.max(Math.min(f.dx,T),-C);f.dy=Math.max(Math.min(f.dy,da),-O);if(f.dx){i.scrollLeft=i.scrollLeft+f.dx}if(f.dy){i.scrollTop=i.scrollTop+f.dy}}break;case 4:if(f.dx|f.dy){if(la.length>=1){var X=f,ea=la[la.length-1],U=new ra(X.dx+ea.dx,X.dy+ea.dy),na=false;if(U.dx*U.dx+U.dy*U.dy<=25){na=true}else{var Na=Math.atan2(X.dx,X.dy),Da=Math.atan2(ea.dx,
ea.dy),Oa=Math.abs((Na-Da+2*Math.PI)%(2*Math.PI));na=Oa<lc}if(na){ea.dx=U.dx;ea.dy=U.dy}else{la.push(f)}}else{la.push(f)}vb(la)}break}}else{Ia()}return false}function Ka(a){a=a||window.event;var b=a.relatedTarget||a.toElement;if(!b){Ia()}}function Ja(){if(y!==0){if(i){if(y===4){vb([])}Y.ub(i,y)}if(y===1||y===2||y===5){if(y!==2&&Y.useUserDefinedAlpha&&!Y.useUserDefinedAlpha()){tb(i,false)}if(undefined!==Pa){i.style.zIndex=Pa;Pa=undefined}else{delete i.style.zIndex}}}i=undefined;y=0;D=undefined;ya=
undefined;w=0;V=undefined;Tb=false;if(Y){if(Z()){qa(Q.body,"mouseleave",Ka)}else{qa(Q.body,"mouseout",Ka)}}Y=undefined;qa(Q.body,"mousemove",Wa);qa(Q.body,"mouseup",Xa)}function Ia(){switch(y){case 1:var a=i.currentStyle?i.currentStyle:window.getComputedStyle(i,"");if(a&&a.position=="relative"){i.style.left="0px";i.style.top="0px"}else{i.style.left=ya.x+"px";i.style.top=ya.y+"px"}break;case 2:i.style.width=ya.w+"px";i.style.height=ya.h+"px";break;case 3:break;case 4:la=[];break;case 0:break;case 5:break;
default:lb("failed to cancel drag with dd_dragType="+y)}Ja()}function tb(a,b){if(Z()){a.style.filter=b?"alpha(opacity=50)":"alpha(opacity=100)"}else{a.style.MozOpacity=b?0.5:1}}var ib=false,jb=undefined;function vb(a){jb=a;if(!ib){ib=true;window.setTimeout(kc,100)}}
function kc(){
	var a=jb;
	jb=undefined;
	ib=false;
	if(!a){
		return
	}
	var b="";
	if(a.length){
		var c=a[0].x,d=a[0].y,e=0,f=0;
		for(var g=1;g<a.length;g++){
			var h=a[g];
			if(!(h.dx|h.dy)){
				continue
			}
			if(g!=0){
				b+="<img class=gestimg src=images/joiner.png width=5 height=5 style=left:"+(c-2)+"px;top:"+(d-2)+"px>"
			}
			var k=Math.atan2(h.dx,h.dy),l,m=Math.floor(k/(Math.PI/6));
			switch(m){
				case 0:case 6:case -6:l="vert.png";
				break;
				case 1:case -4:l="negslope.png";
				break;
				case 2:case -2:case 3:case -3:l="horz.png";
				break;
				case 4:case -1:l="posslope.png";
				break;
				case 5:case -5:l="vert.png";
				break
			}
			var B=Math.max(1,Math.abs(h.dx)),q=Math.max(1,Math.abs(h.dy));
			b+="<img class=gestimg src=images/"+l+" width="+B+" height="+q+" style=left:"+(c+Math.min(h.dx,0))+"px;top:"+(d+Math.min(h.dy,0))+"px>";
			c+=h.dx;
			d+=h.dy;
			e=Math.max(c,e);
			f=Math.max(d,f)
		}
	}
	var u=t("gesture");
	u.style.display="none";
	u.innerHTML=b;
	u.style.display="inline"
}
function Jc(){}Jc.prototype.G=function(a,b,c,d,e){throw new Error("Unimplemented");};function F(a){this.eb=a;this.fb=a?F.M(a):undefined;this.Xa=false;this.Lb=null;this.na=3}F.M=function(a,b){b=b||[];if(a.className&&a.className.match(/\bddSelected\b/)){b.push(a)}else{for(var c=a.firstChild;c;c=c.nextSibling){F.M(c,b)}}return b};F.clearSelection=function(a){var b=F.M(a);for(var c=b.length;--c>=0;){var d=b[c];d.className=d.className.replace(/\s*\bddSelected\b/g,"")}return b};F.prototype.Ib=function(a,b){a.style.left=b.x+"px";a.style.top=b.y+"px";
if(this.na&1){
	a.style.width="0px"
		}
if(this.na&2){a.style.height="0px"}};F.prototype.Jb=function(a){a.style.display="block"};F.prototype.T=function(a,b){var c=t("ddLasso");if(!c){c=document.createElement("div");c.id="ddLasso";c.style.position="absolute";c.style.display="none";document.body.appendChild(c)}var d=S(a);this.Ib(c,d);this.Jb(c);y=5;w=this.na;i=c;this.Lb=c;var e=this;window.setTimeout(function(){if(!e.finished){e.pa(c)}},200);return true};F.prototype.da=function(a,b,c){return true};F.prototype.Kb=function(a){return a.className&&a.className.match(/\bddSelectable\b/)};
F.prototype.Ua=function(a,b,c){if(this.Kb(a)){var d=z(a);if(!(b.x+b.w<d.x||d.x+d.w<b.x||b.y+b.h<d.y||d.y+d.h<b.y)){c.push(a)}return}else{for(var e=a.firstChild;e;e=e.nextSibling){this.Ua(e,b,c)}}};F.prototype.pa=function(a){if(!this.eb)return;var b=[];this.Ua(this.eb,z(a),b);this.S(b);if(!this.Xa){var c=this;window.setTimeout(function(){if(!c.finished){c.pa(a)}},200)}};F.prototype.S=function(a){var b=this.fb;for(var c=a.length;--c>=0;){a[c].dd_newSelection=true}for(var c=b.length;--c>=0;){var d=b[c];
d.dd_oldSelection=true;if(!d.dd_newSelection){d.className=d.className.replace(/\s*\bddSelected\b/g,"")}}for(var c=a.length;--c>=0;){var d=a[c];if(!d.dd_oldSelection){d.className=(d.className||" ")+" ddSelected"}d["dd_newSelection"]=false}for(var c=b.length;--c>=0;){b[c].dd_oldSelection=false}this.fb=a};F.prototype.G=function(a,b,c,d){return true};F.prototype.ub=function(a,b){this.Xa=true;this.pa(a);a.style.display="none"};function Aa(a,b){F.call(this,a);this.$b=jc(b);this.gridFn=b}Aa.prototype=new F(undefined);
Aa.prototype.constructor=Aa;Aa.prototype.G=function(a,b,c,d){return F.prototype.G(a,b,c,d)&&this.$b(a,b,c,d)};Aa.prototype.T=function(a,b){if(F.prototype.T.call(this,a,b)){var c=this.gridFn(D,true);if(w&1){D.x=D.x-c.x<c.w>>1?c.x:c.x+c.w}if(w&2){D.y=D.y-c.y<c.h>>1?c.y:c.y+c.h}return true}else{return false}};var pa=new Gc,Vb=false;function Gc(){this.A=[]}function _PC_Install(a){if(Vb)return false;Vb=true;var b=a?a:document;K(b.body,"mousedown",Fc);return true}function Fc(a){if(pa.A.length==0)return false;var a=a||window.event;for(var b=pa.A.length-1;b>=0;--b){var c=pa.A[b];Qb(c);if(c.deactivate(a)){pa.A.splice(b,1)}}return true}function Pb(a){Qb(a);for(var b=0;b<pa.A.length;++b){if(a===pa.A[b])return false}pa.A.push(a);return true}function Qb(a){G(a.deactivate,Function,"popup missing deactivate function")}
;
function j(a,b,c,d,e){
	E(a,"element passed to DP_DatePicker constructor is null");
	this.V=a;
	this.g=c?c:this.V.id+"_";this.c=d?d:"DP_";
	j.W[this.g]=this;
	if(e){
		G(e,o,"opt_today is not an ICAL_Date");
		this.l=e
	}else{
		this.l=o.now()
	}
	this.D=o.create(this.l.year,this.l.month,1);
	this.X=0;
	this.vb=!(!b);
	this.la=false;
	this.db=null;
	this.bb=null;
	this.U={};
	this.u={};
	this.N={};
	this.b={};
	this.q=null;
	this.$=null;
	this.Ia=new ja(this);
	this.Ea=new ja(this);
	this.Ma=false;
	this.i=false;
	this.e=new ha;
	this.v=0;this.L=null;
	this.ra=null;
	this.Ka=true;
	this.Ha=null;
	this.qa=null;
	this.Fa=null;
	this.H();
	this.Na=false;
	this.hb(0);
	this.Sb(0);
	this.za=false;
	this.r=null;
	this.d=null;
	this.p=null;
	this.Q=null;
	this.P=null;
	this.$a=null;
	this.Y=false;
	this.nb=null;
	this.mb=null;
	var f=this,g=function(h){
		var k=h.startDate,l=h.endDate,m;
		if(!k){
			m=j.MSG_DATE_SELECTION[this.R]
		}else if(!l||k.equals(l)){
			m="Selected: "+f.va(k,true)
		}else{
			m="Selected: "+f.va(k)+" - "+f.va(l)
		}
		f.Pb(m)};
		if(this.Ma)
			this.Qa(g);
		this.Da=new ja(this)
		};
		j.MSG_DATE_SELECTION={};
		j.MSG_DATE_SELECTION[0]="Select a date";
		j.MSG_DATE_SELECTION[1]="Select a range of dates";
		j.MSG_DATE_SELECTION[2]="Select dates";
		j.MSG_DATE_SELECTION[3]="&nbsp;";
		j.prototype.Sb=function(a,b){
			if(a!=0&&a!=1&&a!=7&&a!=30&&a!=-1&&!(b instanceof Function)){
				throw new Error("Invalid click mode: "+a);
			}
			this.tb=a;
			this.Ta=b
		};
		j.prototype.qc=function(){
			return this.la
		};
		j.prototype.Yb=function(a){
			if(a!=this.la){
				this.la=a;this.H()
			}
		};
		j.prototype.Ab=function(){
			return this.tb
		};
		j.prototype.hb=function(a){if(!(0<=a&&a<=3)){Ca("Invalid selection mode: "+a)
	}
	if(this.R==a){
		return
	}
	this.R=a;
	this.B()
};
j.prototype.wa=function(){
	return this.R
};
j.prototype.show=function(){
	this.i=true;this.H()
};
j.prototype.hide=function(){
	this.V.innerHTML="";
	this.i=false;
    
	if (document.all) {
		document.body.removeChild(g_PopupIFrame);
		g_PopupIFrame=null;		
	}
};
j.prototype.Z=function(){
	return this.i
};
j.prototype.mc=function(a){
	return this.u[a.id]
};
j.prototype.oc=function(a){
	return this.N[a.id]
};
j.prototype.gc=function(a){
	return this.b[a.id]
};
j.prototype.fa=function(){
	return t(this.g+"tbl")
};
j.prototype.Ub=function(a){
	E(0<=a&&a<=6,a+" not a valid first day of week");
	this.X=a;this.H()
};
j.prototype.Ya=function(){
	return this.X
};
j.prototype.wc=function(a){
	if(a)G(a,Function);
	this.L=a;
	this.H();
	return true
};
j.prototype.xc=function(a){
	if(a)G(a,Function);
	this.ra=a
};
j.prototype.hc=function(){
	return this.L
};
j.prototype.ic=function(){
	return this.q
};
j.prototype.jc=function(){
	if(!this.i)
		return null;
	return this.b[this.q.id]
};
j.prototype.lc=function(){
	if(!this.i)return null;
	var a=t(this.g+"day_"+(this.v-1)+"_6");
	return this.b[a.id]
};
j.prototype.Dc=function(a){
	if(a!=this.Ka){
		this.Ka=a;
		this.H()
	}
};
j.prototype.Xb=function(a){
	G(a,Function);
	this.Ha=a
};
j.prototype.Tb=function(a){
	G(a,Function);
	this.qa=a
};
j.prototype.Wb=function(a){
	G(a,Function);
	this.Fa=a
};
j.prototype.Db=function(){
	return wa
};
j.prototype.kc=function(){
	return Ba
};
j.prototype.H=function(){
	if(!this.i){
		return
	}
	var a=this.g,b,c=this.D.month,d=this.D.year,e=[c==1?12:c-1,c,c==12?1:c+1],f=o.create(this.l.year,this.l.month,1),g=Qa(d,c-1,1).j(),h=Qa(d,c+1,1).j();
	if(this.Ha){
		e[0]=this.Ha(g)
	}else{
		var k=g.a()>=f.a()?"&laquo;":"&lsaquo;&nbsp;";
		e[0]=k+wa[e[0]]
	}
	if(this.qa){
		e[1]=this.qa(this.D)
	}else{
		e[1]=Ba[e[1]]+" "+d
	}
	if(this.Fa){
		e[2]=this.Fa(h)
	}else{
		var l=h.a()-f.a()<=0?"&raquo;":"&nbsp;&rsaquo;";
		e[2]=wa[e[2]]+l
	}
	var m=aa(d,c),B=aa(g.year,g.month),q=new Array(49),u=this.D.Bb()-this.X;
	if(u<0)
		u+=7;
	if(m<30||u<5)
		u+=7;
	for(var n=0;n<u;++n){
		q[n]=o.create(g.year,g.month,B-u+n+1)
	}
	for(var n=u,r=0;r<m;++n){
		q[n]=o.create(d,c,++r)
	}
	for(var n=u+m,r=0;n<q.length;++n){
		q[n]=o.create(h.year,h.month,++r)
	}
	this.nb=q[0];
	this.mb=q[q.length-1];
	var s=[],p=this.vb?[2,3,2]:[1,5,1];
	s.push('<table cols=7 cellspacing="0" cellpadding="0" border="0" id="',a,'tbl"',' class="',this.c,'monthtable" ',' style="-moz-user-select:none; cursor:pointer;border: 1px #A2BBDD solid">','<tr class="',this.c,'heading" id="',a,'header">',"<td colspan=",p[0]," unselectable=on",' onmousedown="'+Ya(uc)+"(",Rb(this.g),')"',' id="',a,'mhl" class="',this.c,'prev">',e[0],"</td>","<td colspan=",p[1],' unselectable="on"',' id="',a,'mhc" class="',this.c,'cur">',e[1],"</td>","<td colspan=",p[2],' unselectable="on"',' onmousedown="'+
Ya(tc)+"(",Rb(this.g),')"',' id="',a,'mhr" class="',this.c,'next">',e[2],"</td>","</tr>");
	if(this.la){
		s.push('<tr class="',this.c,'days" id="',a,'dow">');
		for(var n=0;n<Ha.length;++n){
			if((n+this.X)%7 == 1){
//				s.push('<td></td>')
				s.push('<td unselectable="on"',' class="',this.c,'dayh" style="border-bottom: 1px #A2BBDD solid;border-top: 1px #A2BBDD solid;" id="',a,"day_",n,'" align="right"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><b>week.</b><br><img src="images/clear.gif" width="40" height="1"></td><td width="100%" align="center">',Ha[(n+this.X)%7],"</td></tr></table></td>")
			} else {
				s.push('<td unselectable="on"',' class="',this.c,'dayh" style="border-bottom: 1px #A2BBDD solid;border-top: 1px #A2BBDD solid;" id="',a,"day_",n,'">',Ha[(n+this.X)%7],"</td>")
			}
		}
		s.push("</tr>")
	}
	var W=(7-this.X)%7,ca=(W+6)%7;
	this.U={};
	var b=null,C=null,T=Ya(sc);

	for(var n=0,r=-1;n<7;++n){
		s.push('<tr id="',a,"week_",n,'">');

		for(var O=0;O<Ha.length;++O){
			++r;
			var da=this.e.contains(q[r]);
			C=[];
			if(n==0)
				C.push(this.c+"day_top");
			if(O==0)
				C.push(this.c+"day_left");
			else if
				(O==6)C.push(this.c+"day_right");
			C.push(this.c+"week"+(O==W||O==ca?"end":"day")+(da||(this.e.ka==0 && this.l==q[r])?"_selected":""));
			if(r<u||r>=u+m){
				C.push(this.c+"offmonth");
				if(q[r].date<=7){
					C.push(this.c+"month_top");
					if(q[r].date==1&&O!=0){
						C.push(this.c+"month_left")
					}
				}
			}else{
				C.push(this.c+"onmonth");
				if(q[r].date<=7){
					C.push(this.c+"month_top");
					if(q[r].date==1&&O!=0){
						C.push(this.c+"month_left")
					}
				}
				if(q[r].date==this.l.date&&c==this.l.month&&d==this.l.year){
               C.push(this.c+"today"+(da?"_selected":""))
				}
			}
			var X="";
			if(this.ra){
				var ea=this.ra(q[r]);
				if(ea){
					X=' style="'+ea+'"'
				}
			}
			if (r%7 == 0) {
				weeknumber = getWeekNr(q[r].year,q[r].month,q[r].date); 
				s.push('<td id="',a,"day_",n,"_",O,'"','  onclick="',T,'(this)"',' unselectable="on" ><table width="100%" cellpadding="0" cellspacing="0" border="0" ><tr><td align="center" width="15px" bgcolor="#ffffff"><b>',weeknumber,'</b><br><img src="images/clear.gif" width="30" height="1"></td><td width="100%" align="center" class="',C.join(" "),'"',X,'>',q[r].date,"</td></tr></table></td>");
//				s.push('<td id="',a,"day_",n,"_",O,'"',' class="',C.join(" "),'"',X,' onclick="',T,'(this)"',' unselectable="on">',weeknumber,' ',q[r].date,"</td>");
			} else {
//				s.push('<td id="',a,"day_",n,"_",O,'"',' class="',C.join(" "),'"',X,' onclick="',T,'(this)"',' unselectable="on">',r,' ',q[r].date,"</td>");
				s.push('<td id="',a,"day_",n,"_",O,'"',' class="',C.join(" "),'"',X,' onclick="',T,'(this)"',' unselectable="on">',q[r].date,"</td>");
			}

		}
		s.push("</tr>")
	}
	if(this.Ma){
		s.push('<tr class="',this.c,'months">','<td colspan="7" id="',a,'sel"></td></tr>')
	}
	s.push("</table>");
//prompt("",s.join("")); 
	this.V.innerHTML=s.join("");
	this.q=t(a+"day_0_0");
	this.$=t(a+"day_6_6");
	var b=this.q,U=b.parentNode,na=null,Na=null,r=-1,Da=-1;
	while(U!=null){
		++Da;
		if(Da==7)break;
		var Oa=-1;
		while(b!=null){
			++r;
			++Oa;
			var fb=a+"day_"+Da+"_"+Oa;
			this.b[fb]=q[r];
			this.U[q[r].toString()]=b;
			this.N[fb]=na;
			if(na)
				this.u[Na]=b;
			na=b;
			Na=fb;
			b=b.nextSibling
		}
		U=U.nextSibling;
		if(U!=null){
			b=U.firstChild
		}
	}
	this.v=7;
	if(!this.Ka){
		var Cc=t(a+"week_4"),Kb=t(a+"week_5"),gb=t(a+"week_6");
		if(this.b[a+"day_4_0"].month!=c){
			Cc.style.display="none";
			Kb.style.display="none";
			gb.style.display="none";
			this.v=4
		}else if(this.b[a+"day_5_0"].month!=c){
			Kb.style.display="none";
			gb.style.display="none";
			this.v=5
		}else if(this.b[a+"day_6_0"].month!=c){
			gb.style.display="none";
			this.v=6
		}
	}
	this.db=g;
	this.bb=h;
	if(this.L){
		this.L.call(null,this)
	}
	this.qb()
};
j.prototype.refresh=function(){if(this.L){this.L.call(null,this)}};j.prototype.Qa=function(a){return this.Ia.add(a)};j.prototype.vc=function(a){return this.Ia.remove(a)};j.prototype.B=function(a){a=arguments.length===0||a;var b=this.e.ma();for(var c=0;c<b.length;++c){var d=this.U[b[c].toString()];this.z(d,false)}this.e.clear();if(!this.Y){this.gb(null);this.Ja(null)}if(a)this.E()};
j.prototype.rc=function(a){G(a,H);return this.e.contains(a)};j.prototype.Sa=function(a){if(this.Ta){this.Ta.call(null,a);return}var b=t(a),c=this.e;switch(this.R){case 1:var d=this.Ab();if(d==0)break;if(d!=1&&(d!=-1||!c.contains(this.b[a]))){var e=this.b[b.id],f;switch(d){case -1:if(c.t()>7&&this.ha()){var g=b.id.substr(b.id.length-3,1);e=this.b[this.g+"day_"+g+"_0"]}f=c.t()-1;break;case 7:var g=b.id.substr(b.id.length-3,1);e=this.b[this.g+"day_"+g+"_0"];f=6;break;case 30:e=this.b[b.id];e=o.create(e.year,
e.month,1);var h=za(e);f=aa(e.year,e.month)-1;break;default:Ca("Invalid click mode: "+d)}E(f!==undefined,"duration should have been defined");var h=za(e);h.date+=f;var k=h.j();this.S(e,k);return}E(d==1||d==-1&&c.contains(this.b[a]),"not a case for single date selection");this.B(false);case 0:if(c.t()>0){var l=c.ma()[0];c.remove(l);var m=this.U[l.toString()];if(m)this.z(m,false)}c.add(this.b[b.id]);this.z(b);this.E(this.b[b.id]);break;case 2:break;case 3:default:break}};j.prototype.gb=function(a){this.d=
a;this.Q=a?this.b[a.id]:null};j.prototype.Ja=function(a){this.p=a;this.P=a?this.b[a.id]:null};j.prototype.Hb=function(){return this.Y};j.prototype.T=function(a,b){this.B(false);this.Y=true;this.gb(this.ca(a));var c=this.b[this.d.id];this.e.add(c);this.z(this.d);this.S(c);this.Ja(this.d)};j.prototype.da=function(a,b,c){this.Y=false;this.p=this.ca(a);if(this.ha()){this.E(this.Q,this.P,false);return}var d,e;if(this.b[this.p.id].a()<this.b[this.d.id].a()){d=this.b[this.p.id];e=this.b[this.d.id]}else{d=
this.b[this.d.id];e=this.b[this.p.id]}this.E(d,e,false)};j.prototype.G=function(a,b,c,d){this.$a=ob(a);if(this.R!=1||this.za)return;this.za=true;var e=this;window.setTimeout(function(){try{if(e.Y){e.xb.call(e,b,c,d)}}finally{e.za=false}},50)};j.prototype.Ac=function(a){if(this.Na==a)return;this.Na=!(!a);this.B()};j.prototype.ha=function(){return this.Na};j.prototype.xb=function(a,b,c){var d=this.$a,e=this.ca(d);if(e===this.p)return;var f=this.p;this.Ja(e);E(f!=e);var g=this.b,h=g[f.id].a()<g[e.id].a(),
k=g[f.id].a()<g[this.d.id].a(),l=g[e.id].a()<g[this.d.id].a(),m=g[this.d.id].a()<g[e.id].a(),B=g[this.d.id].a()<g[f.id].a(),q,u,n,r,s=l?this.p:this.d,p=l?this.d:this.p;if(this.ha()){var W=Eb(g[p.id],g[s.id]);if(W>=7){var ca,C;ca=parseInt(s.id.charAt(s.id.length-3),10);C=parseInt(p.id.charAt(p.id.length-3),10);s=t(this.g+"day_"+ca+"_0");p=t(this.g+"day_"+C+"_6")}this.I(this.q,s,false);this.I(p,this.$,false);this.I(s,p,true);this.Q=g[s.id];this.P=g[p.id]}else{if(h){if(k){u=l?this.N[e.id]:this.N[this.d.id];
this.I(f,u,false)}if(m){q=B?this.u[f.id]:this.u[this.d.id];this.I(q,e,true)}}else{if(B){q=m?this.u[e.id]:this.u[this.d.id];this.I(q,f,false)}if(l){u=l?this.N[this.d.id]:this.N[f.id];this.I(e,u,true)}}}n=g[s.id];r=g[p.id];this.E(n,r,true)};j.prototype.I=function(a,b,c){E(a);E(b);G(c,Boolean);var d=false;while(a){if(c){d=this.e.add(this.b[a.id])}else{d=this.e.remove(this.b[a.id])}if(d){this.z(a,c)}if(a.id===b.id)break;a=this.u[a.id];E(a,"did not find endCell: "+b.id)}};j.LAST_DAY_OF_WEEK={4:"day_3_6",
5:"day_4_6",6:"day_5_6",7:"day_6_6"};j.prototype.yc=function(a){if(a){this.r={};this.r.x=a.x;this.r.y=a.y}else{this.r=null}};j.prototype.rb=function(a,b){if(!this.r)return;if(b){a.x-=this.r.x;a.y-=this.r.y}else{a.x+=this.r.x;a.y+=this.r.y}};j.prototype.ca=function(a){var b=z(this.q),c=this.Fb(),d=S(a);this.rb(d);var e=7,f=this.Wa(c.x,b.w,e,d.x),g=this.Wa(c.y,b.h,this.v,d.y);return t(this.g+"day_"+g+"_"+f)};
j.prototype.Wa=function(a,b,c,d){
	if(d<a)
		return 0;
	var e=Math.floor((d-a)/b);
	return e>=c?c-1:e
};
j.prototype.Fb=function(){
	var a=this.g,b=this.v,c=z(this.q),d=z(t(a+j.LAST_DAY_OF_WEEK[b]));
	return new fa(c.x,c.y,d.x+d.w-c.x,d.y+d.h-c.y,c.coordinateFrame)
};
j.prototype.va=function(a,b){
	var c=b?Ba:wa;return c[a.month]+" "+a.date
};
j.prototype.E=function(a,b,c){var d={};d.startDate=a;d.endDate=b||a;d.Hb=!(!c);d.mode=this.wa();this.Ia.ua(d)};j.prototype.pc=function(){return this.l};j.prototype.Bc=function(a){G(a,o,"today is not an ICAL_Date");if(a.equals(this.l))return;return this.l=a};
j.prototype.Ra=function(a){
	if(a instanceof o)return a;
	if(a instanceof A){
		return o.create(a.year,a.month,a.date)
	}else{
		E(false,"Invalid arg: "+a)
	}
};
j.prototype.S=function(a,b,c){
	var d=this.wa();
	c=c!==false;
	if(a)
		a=this.Ra(a);
	if(b)
		b=this.Ra(b);
	if(a)
		this.ib(a);
	if(!a||d==3){
		this.B(c);
		return
	}
	if(d==0){
		this.B(false);
		var e=this.U[a.toString()];
		this.e.add(a);
		this.z(e);
		if(c)
			this.E(a)
	}else if(d==1){
		if(!b)b=a;
		var f=Eb(b,a),g=false;
		if(this.ha()&&f>=7){
			var h=eb(a)+7,k=eb(b)+7;
			h=(h-this.Ya())%7;
			k=(k-this.Ya())%7;
			var l;l=Qa(a.year,a.month,a.date-h);
			a=l.j();
			l=Qa(b.year,b.month,b.date+(6-k));
			b=l.j();
			g=this.ib(a)
		}
		if(g){
			this.B(false)
		}
		var e=this.q;
		this.Q=a;
		this.P=b;
		var m=this.$,B=a.a(),q=b.a();
		for(;e;e=this.u[e.id]){
			var u=this.b[e.id],n=this.e.contains(u),r=u.a()>=B&&u.a()<=q;
			if(n!=r){
				if(r){
					this.e.add(u);
					this.z(e)
				} else {
					this.e.remove(u);
					this.z(e,false)
				}
			}
			if(e===m)
				break
		}
		if(!e){
			m=this.$;
			var l=za(this.b[this.$.id]),s=null;
			do{
				l.date+=1;s=l.j();this.e.add(s)
			} while(!s.equals(b))
		}
		if(c)
			this.E(a,b)
	}
};
j.prototype.La=function(a,b){
	if(this.D.month==a.month && this.D.year==a.year)
		return false;
	b=arguments.length==1||b;

	this.D=o.create(a.year,a.month,1);

	this.H();
	if(b)
		this.Ea.ua();
	return true
};
j.prototype.ib=function(a,b){
/*	if(a.a()>=this.nb.a()&&a.a()<=this.mb.a()){
		return false
	}
*/
   return this.La(a,b)
};
j.prototype.M=function(){
	switch(this.wa()){case 0:if(this.e.t()){return this.e.ma()[0]}else{return null}case 1:var a=this.Q?this.Q:null,b=this.P?this.P:null;if(!a||!b)return null;return[a,b];case 2:return null;case 3:default:return null}};
j.prototype.nc=function(){
	return this.e.t()
};
j.prototype.Pb=function(a){
	if(this.Ma){
		t(this.g+"sel").innerHTML=a
	}
};
j.prototype.z=function(a,b){
	if(!a)
		return;
	if(!Ib(b))
		b=true;
	var c=[],d=[],e=" "+a.className+" ",f=" "+this.c;
	if(b){
		if(-1!=e.indexOf(f+"today ")){
			c.push(f+"today ");
			d.push(f+"today_selected ")
		}
		if(-1!=e.indexOf(f+"weekday ")){
			c.push(f+"weekday ");
			d.push(f+"weekday_selected ")
		} else if(-1!=e.indexOf(f+"weekend ")){
			c.push(f+"weekend ");
			d.push(f+"weekend_selected ")
		}
	} else {
		if(-1!=e.indexOf(f+"today_selected ")){
			d.push(f+"today ");
			c.push(f+"today_selected ")
		}if(-1!=e.indexOf(f+"weekday_selected ")){
			d.push(f+"weekday ");
			c.push(f+"weekday_selected ")
		} else if(-1!=e.indexOf(f+"weekend_selected ")){
			d.push(f+"weekend ");
			c.push(f+"weekend_selected ")
		}
	}
	for(var g=0;g<c.length;++g){
		e=e.replace(c[g],d[g])
	}
	if(c.length!=0){
		a.className=e
	}
};
j.prototype.fc=function(a){
	this.Ea.add(a)
	};
j.prototype.uc=function(a){
	this.Ea.remove(a)
};
j.W={};
j.prototype.dc=function(){
	return this.g
};
j.staticGetPickerById=function(a){
	return j.W[a]
};
function uc(a){
	var b= j.W[a];return b.La(b.db)
}
function tc(a){
	var b=j.W[a];
//	alert(b.La(b.bb));
	return b.La(b.bb)
}
function sc(a){
	var b=a.id;
	var c=b.match(/(.*)day_\d+_\d+/);
	var d=j.W[c[1]];

	return d.Sa(b)
};
j.prototype.qb=function(){if(this.Mb===true)return;this.Mb=true;var a=this.g,b=this,c=this.V;oc(function(d){if(b.R==1&&d===c){var e=new F(d);e.escapedStartPoint=false;e.T=function(f,g){var h=S(f),k=z(b.q),l=z(t(a+j.LAST_DAY_OF_WEEK[b.v]));if(h.x<k.x||h.x>=l.x+l.w||h.y<k.y||h.y>=l.y+l.h)return false;this.startPoint=h.clone();this.startEvent=f;var m=
t(a+"lasso");if(!m){m=document.createElement("div");m.id=a+"lasso";m.style.position="absolute";m.style.display="none";document.body.appendChild(m)}y=5;w=3;i=m;return true};e.G=function(f,g,h,k){if(!this.escapedStartPoint){var l=vc(this.startPoint,S(f));if(l>5){this.escapedStartPoint=true;b.T.call(b,this.startEvent,g)}else{return true}}b.G.apply(b,arguments);return true};e.da=function(f,g,h){if(this.escapedStartPoint){b.da.apply(b,arguments)}else{if(this.startPoint){var k=b.ca(this.startEvent).id;
b.Sa.call(b,k)}}return true};return e}else{return undefined}})};
j.prototype.ec=function(a){return this.Da.add(a)};
j.prototype.tc=function(a){return this.Da.remove(a)};
j.prototype.log=function(){this.Da.ua(arguments)};
j.prototype.Cb=function(){return this.V};function ha(){this.s={};this.ka=0}ha.prototype.t=function(){return this.ka};ha.prototype.add=function(a){var b=this.Pa(a);if(b in this.s)return false;this.s[b]=a.j();++this.ka;return true};ha.prototype.remove=function(a){var b=this.Pa(a);if(!(b in
this.s))return false;delete this.s[b];--this.ka;return true};ha.prototype.clear=function(a){this.s={};this.ka=0};ha.prototype.contains=function(a){var b=this.Pa(a);return b in this.s};ha.prototype.ma=function(){var a=new Array(this.t()),b=-1;for(var c in this.s)a[++b]=this.s[c];return a};ha.prototype.Pa=function(a){E(a instanceof o||a instanceof A,"expected a date or datetime: "+a);return a.toString().substr(0,9)};function ja(a){this.Qb=a;this.n=[]}ja.prototype.add=function(a){G(a,Function);if(!a)return false;
for(var b=0;b<this.n.length;++b){if(a===this.n[b])return false}this.n.push(a);return true};ja.prototype.remove=function(a){if(!a)return false;for(var b=0;b<this.n.length;++b){if(a===this.n[b]){this.n.splice(b,1);return true}}return false};ja.prototype.ua=function(){for(var a=0;a<this.n.length;++a){this.n[a].apply(this.Qb,arguments)}};ja.prototype.t=function(){return this.n.length};ja.prototype.iterator=function(){return new Ea(this)};function Ea(a){this.Ca=a;this.Aa=0;this.C=null}Ea.prototype.Gb=
function(){return this.Aa<this.Ca.t()};Ea.prototype.next=function(){if(this.Gb()){this.C=this.Ca.n[this.Aa++]}else{this.C=null}return this.C};Ea.prototype.current=function(){return this.C};Ea.prototype.remove=function(){if(!this.C)throw new Error("no current element!");this.Ca.remove(this.C);this.C=null;--this.Aa};var P=null;

function J(a,b){
	G(a,String);
	G(b,Function);
	if(Z()&&P==null){
		P=pb(window,"CB_Iframe","javascript:false");
		P.style.position="absolute";
		P.parentNode.style.display="none"
	}
	this.O=a;
	var c=this.ea();
	K(c,"keydown",dc(this));
	this.Vb(b);
	this.cb=null;
	this.Va=this.O+"_combobox";
	ec(window,this.O+"_combobox");
	var d=this.m();
	d.className="CB_menu";
	this.Nb=6;
	this.ba=null;
	this.K=null;
	this.ja=null;
	this.k=-1;
	this.i=false;
	K(d,"mousemove",ac(this));
	K(d,"click",$b(this));
	K(c,"focus",cc(this));
	K(c,"blur",bc(this));
d.style.display="none";
d.style.position="absolute";d.style.overflow="auto"}J.prototype.Z=function(){return this.i};function dc(a){return function(b){if(!a.Z())return;b=b||window.event;var c=xb(b);if(c==38||c==40){var d=a.k;d+=c==38?-1:1;if(d<0||d>=a.K.length)return;a.select(d,true)}else if(c==13){nb(a);var e=a.ea();e.onchange(e);a.hide()}}}J.prototype.ea=function(){return t(this.O)};J.prototype.m=function(){return t(this.Va)};J.prototype.Vb=function(a){this.yb=a};function cc(a){return function(){a.show()}}
function bc(a){
	return function(b){
		b=b||window.event;
		var c=false;
		if(Z()){
			var d=z(a.m());
			c=d.contains(S(b))
		}else{
			var e=b.explicitOriginalTarget;
			c=Jb(a.m(),e)
		}
		if(!c)a.hide()
	}
}
function ac(a){
	return function(b){
		var c=a.zb(b);
		if(c){
			a.select(a.wb(c))
		}else{
			a.sa(a.k)
		}
	}
}
function Bc(a,b){
	E(a);
	G(b,String);
	var c=a[b];
	if(c){
		if(typeof c=="string"){
			eval(c)
		}else if(typeof c=="function"){
			c()
		}
	}
}
function nb(a){
	if(a.k<0)
		return;
	var b=a.F(a.k),c=t(b).innerHTML;
	c=zc(c);
	var d=a.Eb();
	if(d){
		c=d.call(null,c,a.k)
	}
	a.ea().value=c
}
function $b(a){return function(b){nb(a);Bc(a.ea(),"onchange");a.hide()}}J.prototype.zc=function(a){if(a)G(a,Function);this.cb=a};J.prototype.Eb=function(){return this.cb};J.prototype.F=function(a){return this.Va+"_"+a};J.prototype.wb=function(a){return parseInt(a.id.match(/_(\d+)$/)[1],10)};J.prototype.zb=function(a){a=a||window.event;var b=S(a),c=this.m();if(!z(c).contains(b))return null;b.y+=c.scrollTop;for(var d=0;d<this.K.length;++d){var e=t(this.F(d));if(z(e).contains(b)){return e}}return null};
J.prototype.Zb=function(a){this.sa(this.k);this.K=new Array(a.length);var b="<div>";for(var c=0;c<this.K.length;++c){b+='<div id="'+this.F(c)+'" class="CB_option">'+ab(a[c])+"</div>";this.K[c]=a[c].toString()}b+="</div>";this.m().innerHTML=b};J.prototype.Cc=function(a){G(a,Number);if(this.ba===a){return}if(a){a|=0}this.ba=a;if(this.i){var b=t(this.O),c=z(b),d=this.m();d.style.left=c.x+"px";d.style.width=this.ba+"px"}};J.prototype.show=function(){if(this.i)return;var a=this.yb.call(null,this);G(a,
Array);Sa(2,a.length);var b=a[0],c=a[1];this.Zb(b);var d=t(this.O),e=z(d),f=this.m(),g=e.y+e.h;f.style.top=g+"px";if(this.ba){f.style.left=e.x+"px";f.style.width=this.ba+"px"}else{f.style.left=e.x+"px";f.style.width=e.w+"px"}f.style.display="block";var h=this.F(0);if(h){this.ja=z(t(h)).h}else{this.ja=0}var k=Math.min(this.K.length,this.Nb);f.style.height=k*this.ja+"px";f.style.zIndex=d.style.zIndex+2;this.i=true;if(Ua()){this.ia=new Array(this.K.length);this.ia[0]=0;var l=t(this.F(0));for(var m=1;m<
this.ia.length;++m){l=l.nextSibling;var B=Ra(l);this.ia[m]=B.y-g}}if(P){var q=z(f);P.parentNode.style.display="";P.style.left=q.x+"px";P.style.top=+"px";P.style.width=q.w+"px";P.style.height=q.h+"px";P.style.zIndex=d.style.zIndex+1;P.style.display="block"}this.select(c,true);Pb(this)};J.prototype.hide=function(){this.m().style.display="none";this.i=false;if(P){P.parentNode.style.display="none"}};J.prototype.select=function(a,b){if(a==this.k)return;if(b){var c;if(Ua()){c=this.ia[a]}else{c=this.ja*
a}var d=this.m();if(d.scrollTop!=c)d.scrollTop=c}if(this.k>=0)this.sa(this.k);this.k=a;var e=t(this.F(a));Yb(e,"CB_selected")};J.prototype.sa=function(a){if(a<0)return;this.k=-1;var b=t(this.F(a));Hc(b,"CB_selected")};J.prototype.deactivate=function(a){if(this.i){var b=S(a);if(z(this.m()).contains(b)||z(this.O).contains(b)){return false}else{this.hide();return true}}else{return true}};function Sb(a,b,c,d,e){var f=document.createElement("input");f.name=a;f.type=b;f.value=c;
if(d){f.size=d}if(e){f.maxLength=e}return f}function ia(a,b,c){this.tBody=document.getElementById(a);this.cellId=b;this.cols=[];this._counter=1;this.lastRow=document.getElementById(c);this.numberRows=false}ia.prototype._setRowNumbering=function(a){this.numberRows=a};ia.prototype._setCounter=function(a){if(a==0){a=1}this._counter=a};ia.prototype._setSizeField=function(a){this.sizeField=a};
ia.prototype._setSizeFieldValue=function(a){
	document.forms[0].elements[this.sizeField].value=a
};
ia.prototype._getSizeFieldValue=function(){return document.forms[0].elements[this.sizeField].value};ia.prototype._addCol=function(a){this.cols[this.cols.length]=a};ia.prototype._addRow=function(){var a=document.createElement("tr");a.setAttribute("id",this.cellId+this._counter);var b,c;if(this.numberRows){b=document.createElement("td");var d=this._counter+1;b.appendChild(document.createTextNode(d+"."));a.appendChild(b)}for(var e=0;e<this.cols.length;e++){b=
document.createElement("td");var f=this.cols[e];c=Sb(f.name+"["+this._counter+"]",f.type,f.value,f.size,f.maxLength);b.appendChild(c);a.appendChild(b)}this.tBody.insertBefore(a,this.lastRow);this._counter++;if(this.sizeField){this._setSizeFieldValue(this._counter)}};var _ET=ia,_createInput=Sb;function _uploadPreview(a,b){var c=document.getElementById("preview-div");while(c.childNodes.length>0){c.removeChild(c.childNodes[0])}var d="<iframe id='previewpane' name='previewpane' ";d+="width='160' height='120' frameborder='0' marginwidth='0'";d+="marginheight='0' scrolling='no'>";d+="</iframe>";document.getElementById("preview-div").innerHTML=d;Mc(document.getElementById("previewpane"),"load",Qc,true);if(document.getElementById("previewpane").contentDocument||document.getElementById("previewpane").contentWindow){document.getElementById("upload-message").style.display=
"inline"}var e=document.forms["thumbnailForm"];e.action=a;e.elements["token"].value=b;e.target="previewpane";e.submit()}function Mc(a,b,c,d){if(document.addEventListener){a.addEventListener(b,c,d)}else if(document.attachEvent){a.attachEvent("on"+b,c)}}function Qc(){document.getElementById("upload-message").style.display="none";if(document.forms["thumbnailForm"].elements["offset"]){document.forms["thumbnailForm"].elements["offset"].value=-1}if(document.imageGrid){document.imageGrid._unselectAll()}}
function _reset(a,b,c){document.getElementById("previewpane").src=a.image;if(document.forms["thumbnailForm"].elements["offset"]){document.forms["thumbnailForm"].elements["offset"].value=a.offset}document.forms["thumbnailForm"].elements["thumbnailFile"].value="";document.forms["thumbnailForm"].elements["token"].value=c;if(b){b._unselectAll()}}function _onSubmitThumbnail(a){var b=document.forms["thumbnailForm"];b.target="";b.action="SetThumbnail";b.elements["token"].value=a}function Kc(a,b){this.offset=
a;this.image=b}function ba(a,b){this.grid=document.getElementById(a);this.thumbs=b}ba.prototype.Rb="#cc0000";ba.prototype.Oa="#cccccc";ba.prototype._setThumbDimensions=function(a,b){this.thumbWidth=a;this.thumbHeight=b};ba.prototype._setDefaults=function(a,b){this.currentOffset=a;this.currentImage=b};ba.prototype._init=function(){if(this.thumbs.length==0){var a=document.createTextNode("There are no generated thumbnails for your video");this.grid.appendChild(a)}for(var b=0;b<this.thumbs.length;b++){var c=
this.thumbs[b],d=c.image;d.style.border="2px solid "+this.Oa;d.style.width=this.thumbWidth;d.style.height=this.thumbHeight;d.hspace=3;d.vspace=3;this.grid.appendChild(d)}var e=this;if(document.body.addEventListener){this.grid.addEventListener("click",function(f){e.ga(f)},true)}else{this.grid.onclick=function(f){e.ga(f)}}};ba.prototype._unselectAll=function(){for(var a in this.thumbs){var b=this.thumbs[a].image;b.style.borderColor=this.Oa}};ba.prototype.ga=function(a){var b=a?a:window.event,c=S(b);
for(var d in this.thumbs){var e=this.thumbs[d],f=e.image,g=z(f);if(g.contains(c)){f.style.borderColor=this.Rb;this.select(e)}else{f.style.borderColor=this.Oa}}};ba.prototype.select=function(a){var b=document.getElementById("previewpane");b.src=a.image.src;document.forms[0].elements["offset"].value=a.offset};var _ImageGrid=ba,_Thumb=Kc;function Nb(){return true};var I=null;function rc(a){return function(b){return"&laquo;"}}function pc(a){return function(b){return a.Db()[b.month]+" "+b.year}}function qc(a){return function(b){return"&raquo;"}}var kb={};

function Lc(a,b){
//les lignes en commentaire dans cette fonction cr��s des  probl�mes dans IE
//	if(Z()&&I==null){
		
//		I=pb(window,"DP_Iframe","javascript:false");
		
//		I.parentNode.style.display="none";
//		I.style.position="absolute"
//	}
	
	var c,d;
//	if(kb[a]){
//		c=kb[a];
//		d=c.Cb()
//	}else{
	var nb_div=0;
	var array_div = document.getElementsByTagName('div');
	for(i=0; i<array_div.length; i++){
		if(array_div[i].id == a+"dp_div"){
			nb_div ++
		}
	}
	//alert(document.getElementById(a+"dp_div").has

		d=document.createElement("div");
		d.id=a+"dp_div";
		d.style.position="absolute";
		d.style.display="none";
		document.getElementById(a).parentNode.appendChild(d)
/*
CD20100330
Ce qui suit a �t� ajout� pour la nouvelle fonction route , ada track ou pre order
Je le d�sactive car beaucoup d'interf�rence dans les programmes d�j� existant...
Dans la version modifi�e c'est la ligne document.getElementById(a)... qui �tait en commentaire

		if(nb_div>0){
			d=document.getElementById(a+"dp_div");
		} else {
			document.getElementsByTagName('body')[0].appendChild(d);
		}
*/
		c=new j(d,false,undefined,"DP_popup_");
		c.hb(0);
		c.Xb(rc(c));
		c.Tb(pc(c));
		c.Wb(qc(c));
		c.Yb(true);
		c.Ub(b);
		kb[a]=c;
		c.Qa(Ub(c,a));
		c.deactivate=function(g){
			if(!c.Z())
				return true;
			var h=z(c.fa()),k=S(g);
			if(!h.contains(k)){
				c.hide();
//				if(I){
//					I.parentNode.style.display="none"
//				}
				return true
			}
			return false
		}
//	}
	var e=Xc(d.id,a,c),f=t(a);
	K(f,"focus",e,false);
	K(f,"click",e,false);
	K(f,"blur",Wc(c),false);
	K(f,"keydown",Yc(c,a),false);
	
	return c
}
function Xc(a,b,c){
	return function(){
		var d=t(b),e=z(d),f=t(a);
		
		div_width = 280;
		div_height = 226
		input_date_width = 70;
		window_width =document.body.clientWidth 
		window_height =document.body.clientHeight 
		posX=(e.x-div_width+input_date_width);
		
		posY=(e.y-(div_height/2));
		
		if (posX< 0) {
			posX= 0;
		}
		if (posY< 0 || posY> window_height) {
			posY= 0;
		}

		//window.status=posY;
		f.style.left=(posX)+"px";
		f.style.top = (posY)+"px";
		//document.title = f.offsetTop+"]["+posY;

		f.className="DP_popup_div";
		f.style.display="";
		if (
		  (navigator.appVersion.indexOf('MSIE')>0) &&
		  (navigator.userAgent.indexOf('Opera')<0) 
		) {
			if(g_PopupIFrame){
				document.body.removeChild(g_PopupIFrame);
				g_PopupIFrame=null;
			}
			var iFrame = document.createElement("IFRAME");
			iFrame.setAttribute("src", "");
			
			//Match IFrame position with divPopup
			iFrame.style.position="absolute";
			iFrame.style.left =f.offsetLeft + 'px';

			iFrame.style.top =f.offsetTop + 'px';
//			alert(f.offsetWidth+" "+f.offsetHeight);
			iFrame.style.width =(div_width-28)+'px';
			iFrame.style.height =(div_height+20)+ 'px';
			
			document.body.appendChild(iFrame);
			g_PopupIFrame=iFrame;

		}

		var g=d.currentStyle?d.currentStyle:d.ownerDocument.defaultView.getComputedStyle(d,"");
		f.style.zIndex=(g.zIndex||0)+1;
		f.style.zIndex=99;
		c.show();
		if(I){
			var h=z(c.fa());
			I.parentNode.style.display="";
			I.style.left=h.x+"px";
			I.style.top=h.y+"px";
			I.style.width=h.w+"px";
			I.style.height=h.h+"px";
//			I.style.zIndex=f.style.zIndex-1;
//			I.style.zIndex=f.style.zIndex-1;
			I.style.display=""
		}
		english_date="";
		if ( d.value.length>0) {

			day_pos = date_format.indexOf("DD");
			month_pos = date_format.indexOf("MM");
			year_pos = date_format.indexOf("YYYY");

			english_date = d.value.substr(year_pos,4)+"/"+d.value.substr(month_pos,2)+"/"+d.value.substr(day_pos,2);
		}

		var reg=new RegExp("^[0-9]{4}[/]{1}[0-9]{2}[/]{1}[0-9]{2}$","g");
		if (!reg.test(english_date)) {
			english_date = "";
		}
		
		var k=Dc(english_date);
		if(k){
			k=k.confidence>-2?k.date:null
		}
		if(k){
			c.S(k,k,false)
		}Pb(c);
		return true
	}
}
function Wc(a){
	return function(b){
		if(!a.Z()){
			return true
		}
		var b=b||window.event,c;
		if(Z()){
			var d=z(a.fa());
			c=d.contains(S(b))
		}else{
			var e=b.explicitOriginalTarget;
			c=Jb(a.fa(),e)
		}
/*
		if(!c){
			a.hide();
			if(I){
				I.parentNode.style.display="none"
			}
		}
*/
		return true
	}
}
function Ub(a,b){
	return function(){
		var c=a.M();
		if(!c)return;
		var d=t(b);
		//ce qui suit pour afficher des dates et mois � 2 chiffres

		my_month = get_too_digit_format(c.month);
		my_date = get_too_digit_format(c.date);
		my_year = c.year
		//ce qui suit pour appliquer un format d�finit dans la page principale
		d.value=date_format;
		d.value=d.value.replace(/MM/g,my_month)
		d.value=d.value.replace(/DD/g,my_date)
		d.value=d.value.replace(/YYYY/g,my_year)

//		d.value=c.date+"/"+c.month+"/"+c.year;
		
		a.hide();
		if(I){
			I.parentNode.style.display="none"
		}
		if("onchange" in d&&d.onchange){
			d.onchange()
		}
	}
}

function get_too_digit_format(tmp_str){
	if (parseInt(tmp_str) <10) {
		tmp_str = "0"+tmp_str;
	}
	return tmp_str;

}

function Yc(a,b){
	return function(c){
		if(!a.Z())
			return;
		c=c||window.event;
		var d=xb(c);
		if(d==38||d==40){
			var e=a.M();
			if(!e)return;
			var f=d==38?-1:1,g=za(e);
			g.date+=f;
			a.S(g.j(),undefined,false)
		}else if(d==13){
				window.setTimeout(Ub(a,b),0)
		}
	}
}
week = function(d) {
   w = d.getDay()
   return Math.floor((yearday(d)-1-w)/7)+2
}
yearday = function (d) {
   var d1 = new Date(d);
   d1.setMonth(0); d1.setDate(1)
   return Math.round((d.getTime()-d1.getTime())/(24*3600000))+1
}


function getWeekNr(aaaa, mm, dd)
{
	var today = new Date(aaaa,mm-1,dd,0,0,0);
	epoch_today = today.getTime();
	epoch_today += (86400000*3);
	today.setTime(epoch_today);

	return today.getWeekNr();

}


Date.prototype.getWeekNr = function()
{
    var firstYearDay = new Date(this.getFullYear(), 0,1,0,0,0);
    var day = Math.ceil((this - firstYearDay) / 86400000) + 1
    var firstDay = firstYearDay.getDay();
	 
	 var days = firstDay + day - 2;
    var week = (days/7)+1;
	week = Math.floor(week);

	if(	this.getFullYear() == '2010' || 
		this.getFullYear() == '2011' || 
		this.getFullYear() == '2016' || 
		this.getFullYear() == '2021' || 
		this.getFullYear() == '2022' || 
		this.getFullYear() == '2027' || 
		this.getFullYear() == '2028' || 
		this.getFullYear() == '2033' || 
		this.getFullYear() == '2038' || 
		this.getFullYear() == '2039'){
		week --;
	}

    return week;
}



var _DP_addToInput=Lc;
var _DP_DatePicker,_ScrollIntoView,_CG_DAYS_OF_THE_WEEK,_MONTHS;
_DP_DatePicker=j;
_ScrollIntoView=Ic;
_CG_DAYS_OF_THE_WEEK=Ha;
_MONTHS=wa;

	  var date_format = "DD/MM/YYYY";
      _MONTHS[1] = "jan.";
      _MONTHS[2] = "feb.";
      _MONTHS[3] = "mar";
      _MONTHS[4] = "apr.";
      _MONTHS[5] = "may";
      _MONTHS[6] = "june";
      _MONTHS[7] = "july";
      _MONTHS[8] = "aug.";
      _MONTHS[9] = "sep.";
      _MONTHS[10] = "oct.";
      _MONTHS[11] = "nov.";
      _MONTHS[12] = "dec.";
    

    
      _CG_DAYS_OF_THE_WEEK[0] = "sun.";
      _CG_DAYS_OF_THE_WEEK[1] = "mon.";
      _CG_DAYS_OF_THE_WEEK[2] = "tue.";
      _CG_DAYS_OF_THE_WEEK[3] = "wed.";
      _CG_DAYS_OF_THE_WEEK[4] = "thu.";
      _CG_DAYS_OF_THE_WEEK[5] = "fri.";
      _CG_DAYS_OF_THE_WEEK[6] = "sat.";


      var g_PopupIFrame;
