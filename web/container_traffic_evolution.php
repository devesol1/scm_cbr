<?php

include "libchart/classes/libchart.php";

$Graph = $_GET['Graph']; 
$AnDebut = $_GET['AnDebut']; 
$AnFin = $_GET['AnFin']; 

$connection_string = "host=localhost port=5432 dbname=cbryrocher user=postgres password=postgres";
$dbconn = pg_connect($connection_string) or die("Connexion impossible");

# ON RECHERCHE TOUS LES VOYAGES
$query = "
	select distinct on ( pol.country, pod.country)
	pol.country as polcountry, pod.country as podcountry
	from containers_analysies
	left join ports as pol on pol.port_id = containers_analysies.pol_id
	left join ports as pod on pod.port_id = containers_analysies.pod_id
	left join periods on periods.jour = containers_analysies.departure_id
	WHERE periods.annee >= '$AnDebut' AND periods.annee <= '$AnFin'
	ORDER by pol.country, pod.country
";
$result = pg_query($query) or die('échec requéte : ' . pg_last_error());
$nbreg = 0;
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
  $tabtmp[$nbreg] = $line[polcountry].";".$line[podcountry];
  $nbreg++;
}
pg_free_result($result);

# ON RECHERCHE TOUTES LES PERIODES 
$query = "
	select distinct on ( periods.annee )
	periods.annee as year
	from containers_analysies
	left join periods on periods.jour = containers_analysies.departure_id
	WHERE periods.annee >= '$AnDebut' AND periods.annee <= '$AnFin'
	order by periods.annee
";
$result = pg_query($query) or die('échec requéte : ' . pg_last_error());
$nbkey = 0;
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
  for ( $i = 0; $i < $nbreg; $i++ ) { 
    $tableau[$nbkey] = $line[year].";".$tabtmp[$i];
    $nbkey++;
  }
}
pg_free_result($result);

# ON RECHERCHE LES DONNEES
$query = "select periods.annee as year, 
	pol.country as polcountry, 
	pod.country as podcountry,
	count(*) as container_nombre 
	from containers_analysies 
	left join periods on periods.jour = containers_analysies.departure_id
	left join ports as pol on pol.port_id = containers_analysies.pol_id
	left join ports as pod on pod.port_id = containers_analysies.pod_id
	WHERE periods.annee >= '$AnDebut' AND periods.annee <= '$AnFin'
	GROUP by periods.annee, pol.country, pod.country
	ORDER by periods.annee, pol.country, pod.country
";
$result = pg_query($query) or die('échec requéte : ' . pg_last_error());
$keytableau = 0;
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    for ( $i = $keytableau; $i < $nbkey; $i++ ) { 
      if ( $line[year].";".$line[polcountry].";".$line[podcountry] == $tableau[$i] ) { 
	$tabres[$keytableau] = $tableau[$i].";".$line[container_nombre];
        $keytableau++;
        break;
      }
      else {
        $tabres[$keytableau] = $tableau[$i].";0";
        $keytableau++;
      }
    }    
}
pg_free_result($result);
pg_close($dbconn);

while ( $keytableau < $nbkey ) {
        $tabres[$keytableau] = $tableau[$keytableau].";0";
        $keytableau++;
}   

if ( $Graph == 'V' ) { $chart = new VerticalBarChart(1000, 500); }
if ( $Graph == 'L' ) { $chart = new LineChart(); }
$dataSet = new XYSeriesDataSet();

$annee = '';
$serie = '';
for ( $i = 0; $i < $nbkey; $i++ ) {
  list ($year, $polcountry, $podcountry, $nombre) = split ( ";", $tabres[$i] );  
#  echo "<BR>$year, $polcountry, $podcountry, $nombre";
  if ( $annee != $year ) { 
    if ( $annee != '' ) { $dataSet->addSerie("$annee", $serie); }
    $annee = $year; 
  //  $serie = new XYDataSet(); 
    $serie = new XYDataSet(); 
  }
  $serie->addPoint(new Point(substr($polcountry,0,5).'-'.substr($podcountry,0,5), $nombre));
}
$dataSet->addSerie("$annee", $serie);

header ("Content-type: image/png");
$chart->setDataSet($dataSet);
$chart->getPlot()->setGraphCaptionRatio(0.75);
$chart->setTitle("Containers by countries");
$chart->render();
?>
