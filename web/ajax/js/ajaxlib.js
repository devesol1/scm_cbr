function getXMLHttpRequest() {
	var xhr = null;
	if(window.XMLHttpRequest) xhr = new XMLHttpRequest();
	else if(window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	else xhr = false;
	return xhr;
}

function nodeCleaner(n) {
	if(!n.data.replace(/\s/g,'')) n.parentNode.removeChild(n);
}
			
function cleanXML(docElement) {
	
	var node = docElement.getElementsByTagName('*');
	
	for(i = 0; i < node.length; i++) {
		a = node[i].previousSibling;
		if(a && a.nodeType == 3) nodeCleaner(a);
		b = node[i].nextSibling;
		if(b && b.nodeType == 3) nodeCleaner(b);
	}
	return docElement;
}
