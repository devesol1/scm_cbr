#!/usr/bin/perl
#===========================================================================
#           Nom : cree_action.psp
#         Objet : Ce programme permet de creer une action FCS
#        Auteur : e-SOLUTIONS A.F.
# Date creation : 03/06/2002 A.F.
# Date modific. : 16/06/2003 A.F.
#
# Historique :
#
# 23/01/2003 A.F. : Modification modules, utilisation fcs_conf
#
# 16/06/2003 A.F. : Correction methode finish avant test si erreur
#
#===========================================================================
use strict;

use CGI;
use DBI;
use HTML::Template;

# ENVIRONNEMENT SPECIFIQUE
use esol::env;
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'params';
use File::FindLib esolGetEnv(__FILE__, 'PATH_CBR_ROOT_DIR').'lib';
use conf_scm_cbr;
use lib_scm_cbr;

my $r = shift;
$r->content_type("text/html; charset=ISO-8859-15");

my $hdl_cgi    = new CGI;
my $ticket     = $hdl_cgi->param('TICKET');
my $methode    = $ENV{REQUEST_METHOD};
my $adresse_ip = $ENV{REMOTE_ADDR};
my $navigateur = $ENV{HTTP_USER_AGENT};
my $etape      = $hdl_cgi->param('ETAPE');

my ( $dbh, $acces );

( $dbh = DBI->connect( esolGetEnv(__FILE__, 'BDD_CBR_DSN'), esolGetEnv(__FILE__, 'BDD_USER'), esolGetEnv(__FILE__, 'BDD_PASSWORD'), { AutoCommit => 1 } ) ) or &FCS_AfficheErreur( "authentification.psp", "POST", $ticket, "Connexion base impossible." );

( $acces, $ticket ) = &FCS_GereConnexion( $dbh, $ticket, "", "", $adresse_ip, $navigateur );

&FCS_Entete( $ticket, "Add action" );

# On ajoute l'action :
if ( $etape == 1 ) {
    my $indic_erreur = 0;
    my @tab_erreur   = ();

    my $action_id          = $hdl_cgi->param('action_id');
    my $action_description = $hdl_cgi->param('action_description');
    my $action_etat        = $hdl_cgi->param('action_etat');

    $action_id          =~ s/^\s+//;
    $action_id          =~ s/\s+$//;
    $action_description =~ s/^\s+//;
    $action_description =~ s/\s+$//;
    $action_etat        =~ s/^\s+//;
    $action_etat        =~ s/\s+$//;

    if ( $action_etat eq '' ) {
        $action_etat = 0;
    }

    if ( $action_id eq '' ) {
        $indic_erreur = 1;
        push @tab_erreur, "Action field can't be blank.<br>";
    }

    if ( $indic_erreur == 0 ) {

        # On regarde si l'action existe deje dans la base :
        my $cur_action = $dbh->prepare("select id_action from actions where id_action = ?");
        $cur_action->execute($action_id);

        if ( $cur_action->rows != 0 ) {
            $indic_erreur = 1;
            $action_id    = '';
            push @tab_erreur, "action $action_id exists already.<br>";
        }
        $cur_action->finish;
    }

    if ( $indic_erreur == 1 ) {
        my $template = HTML::Template->new( filename => esolGetEnv(__FILE__, 'PATH_CBR_WEB_DIR').'/templates/cree_action.tmpl', global_vars => 1, die_on_bad_params => 0 );
        $template->param( TICKET             => $ticket );
        $template->param( ETAPE              => $etape );
        $template->param( form_action        => 'cree_action.psp' );
        $template->param( form_method        => 'POST' );
        $template->param( form_target        => '' );
        $template->param( form_submit        => 'Add' );
        $template->param( action_id          => $action_id );
        $template->param( action_description => $action_description );
        $template->param( action_etat        => $action_etat );
        $template->param( ERREUR             => $indic_erreur );
        $template->param( MESSAGE_ERREUR     => join ( "", @tab_erreur ) );
        print $template->output;
    }
    else {
        my $cur_insert = $dbh->prepare("insert into actions (id_action, description, etat) values (?, ?, ?)");
        $cur_insert->execute( $action_id, $action_description, $action_etat );

        if ( $dbh->errstr ne undef ) {    # ERREUR EXECUTION SQL
            $cur_insert->finish;
            $dbh->disconnect;
            &FCS_AfficheErreur( "liste_action.psp", "POST", $ticket, "Unable to create action : $action_id." );
            exit;
        }
        $cur_insert->finish;
        &FCS_AfficheMessage( "liste_action.psp", "POST", $ticket, "action : $action_id has been created." );
    }
}

# On demande les infos de l'action e ajouter
else {
    $etape = 1;
    my $template = HTML::Template->new( filename => esolGetEnv(__FILE__, 'PATH_CBR_WEB_DIR').'/templates/cree_action.tmpl', global_vars => 1, die_on_bad_params => 0 );
    $template->param( TICKET      => $ticket );
    $template->param( ETAPE       => $etape );
    $template->param( form_action => 'cree_action.psp' );
    $template->param( form_method => 'POST' );
    $template->param( form_target => '' );
    $template->param( form_submit => 'Add' );

    print $template->output;
}
$dbh->disconnect;
