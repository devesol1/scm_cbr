<?php
	/* Libchart - PHP chart library
	 * Copyright (C) 2005-2007 Jean-Marc Tr�meaux (jm.tremeaux at gmail.com)
	 * 
	 * This program is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
	 * 
	 */
	
	/**
	 * Direct PNG output demonstration (image not saved to disk)
	 *
	 */
/*print_r(get_loaded_extensions());
print_r(var_dump(gd_info()));*/


#header ("Content-type: image/png");
#$im = ImageCreate (200, 100) or die ("Erreur lors de la cr�ation de l'image");        
#$couleur_fond = ImageColorAllocate ($im, 255, 0, 0);
#ImagePng ($im); 

/*phpinfo();
gd_info();
*/

include "libchart/classes/libchart.php";

header ("Content-type: image/png");
 
/*$chart = new VerticalBarChart(500, 250);

$dataSet = new XYDataSet();
$dataSet->addPoint(new Point("Jan 2005", 273));
$dataSet->addPoint(new Point("Feb 2005", 321));
$dataSet->addPoint(new Point("March 2005", 442));
$dataSet->addPoint(new Point("April 2005", 711));

$chart->setDataSet($dataSet);

$chart->setTitle("Monthly usage for www.example.com");
$chart->render();
*/
$chart = new PieChart(500, 300);

$dataSet = new XYDataSet();
$dataSet->addPoint(new Point("Bleu d'Auvergne", 50));
$dataSet->addPoint(new Point("Tomme de Savoie", 75));
$dataSet->addPoint(new Point("Crottin de Chavignol", 30));
$chart->setDataSet($dataSet);

$chart->setTitle("Preferred Cheese");
$chart->render();
?>
